<?php
include('admin/General/DBConnection.php');


$HomeGeneral=mysqli_fetch_array(mysqli_query($con,"select * from tbl_home_general where id=1"));
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="DUIAC" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="KMECC MARAYAMANGALAM ">
<link href="favicon.png" rel="icon">

<!-- Fonts
    ============================================= -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100italic,100,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700italic,700,800,800italic,900,900italic%7COpen+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic%7CUbuntu:400,300,300italic,400italic,500,700,500italic,700italic%7CRoboto+Slab:400,100,300,700%7CLora:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">


<!-- Document Title
    ============================================= -->
<title> KMECC MARAYAMANGALAM  |  TEAM</title>
</head>
<body>

<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">

<?php include('header.php'); ?>

		



<!-- Service Block #1
============================================= -->






			

           	
 


		
		
		

<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="assets/images/page-title/title-18.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12  col-sm-12  col-md-12">
				<div class="title-1 text-center">
					<div class="title-heading">
						<h2>kmecc team</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->

<!-- Team Section
============================================= -->
<section id="team" class="pb-0 team team-2">
	<div class="container">
		<div class="row">
			
			
			<?php $staffQuery= mysqli_query($con,"select * from tbl_staff  order by DisplayOrder asc");
			while($staffArray=mysqli_fetch_array($staffQuery)) 
			{
			
			?>
			
			<!-- Member #1 -->
			
			
			<div class="col-xs-12 col-sm-6 col-md-3 member">
				<div class="member-img">
					<img src="<?php echo "Resource/staff/".$staffArray['Image']; ?>" alt="member"/>
					<?php if(!empty($staffArray['FbLink'])) {?>
					<div class="member-hover">
						<div class="member-social">
							<a href="<?php echo $staffArray['FbLink']; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
							
						</div>
					</div><!-- .member-hover end -->
					<?php } ?>
				</div><!-- .member-img end -->
				<div class="member-overlay">
					<h6><?php echo $staffArray['Designation']; ?></h6>
					<h5><?php echo $staffArray['Name']; ?></h5>
				</div><!-- .memebr-ovelay end -->
			</div><!-- .member end -->
			<?php } ?>
			<!-- Member #2 -->
		

			
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #team end -->




<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/functions.js"></script>
</body>

<!-- Mirrored from 7oroof.com/tfdemos/mount/contact-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Nov 2016 05:26:56 GMT -->
</html>