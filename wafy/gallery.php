<?php
include('admin/General/DBConnection.php');
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="DUIAC" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="KMECC MARAYAMANGALAM ">
<link href="favicon.png" rel="icon">


<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">


<!-- Document Title
    ============================================= -->
<title>KMECC MARAYAMANGALAM  | Gallery</title>
</head>
<body>

<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">

<?php include('header.php'); ?>

		

<!-- Page Title #1
============================================= -->
<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="Resource/gallerey.jpg" alt="Gallary"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="title-1 text-center">
					
					<div class="title-heading">
						<h2>Gallery</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->





<!-- Portfolio Gallery 2 Column Without Space
============================================= -->
<section id="portfolio-fullwidth" class="portfolio portfolio-gallery-space portfolio-3col">
	<div class="container">

        
		<div class="row">
			<!-- Portfolio Filter
			============================================= -->
			<div class="col-xs-12  col-sm-12  col-md-12 portfolio-filter">
				<ul class="list-inline">
					<li><a class="active-filter" href="#" data-filter="*">All</a></li>
					<?php $galleryCatquery=mysqli_query($con,"select * from tbl_gallery_category");
					while($galleryCatResult=mysqli_fetch_array($galleryCatquery))
					{
					?>
					<li><a href="#" data-filter=".<?php echo $galleryCatResult['CategoryId'];?>"><?php echo $galleryCatResult['Category'];?></a></li>
					
					<?php } ?>
				</ul>
			</div>
			<!-- .Portfolio-filter end --> 
		</div><div id="portfolio-all" class="row">
			<!-- Portfolio #1 -->
			
		<?php $galleryquery=mysqli_query($con,"select * from tbl_gallery where Approve=1 order by GalleryId desc ");
					while($galleryResult=mysqli_fetch_array($galleryquery))
					{
					?>
			
			<div class="col-xs-12 col-sm-6 col-md-4  portfolio-item  <?php echo $galleryResult['CategoryId'];?>">
				<div class="portfolio-img">
					<img src="<?php echo "Resource/Gallery/thump_".$galleryResult['Image'];?>" alt="Wafy Gallery Image">
					<div class="portfolio-hover">
						<div class="portfolio-meta">
							<div class="portfolio-cat">
								<span>In </span><span><?php  
		$Category=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_gallery_category where CategoryId ='".$galleryResult['CategoryId']."'"));
		echo $Category[0]; ?>
						</span>
							</div>
							<div class="portfolio-title">
								<h4><?php echo $galleryResult['Heading'];?></h4>
							</div>
						</div><!-- .Portfolio-meta end -->
						<div class="portfolio-action">
						<div class="portfolio-zoom-like">
							<a class="img-popup zoom" href="<?php echo "Resource/Gallery/".$galleryResult['Image'];?>" title="<?php echo $galleryResult['Content'];?>" style="float: left">
								<i class="fa fa-search"></i>
							</a>
							 <div class="portfolio-link" style="float: left">
								<a href="gallery_single.php?GalleryId=<?php echo  $galleryResult['GalleryId'];?>&<?php echo strtolower(str_replace(' ', '_',$galleryResult['Heading'])); ?>"><i class="fa fa-link"></i></a>
							</div>
							
						</div>
						  
					</div><!-- .Portfolio-action end -->
					</div><!-- .Portfolio-hover end --> 
				</div><!-- .Portfolio-img end -->
			</div><!-- . portfolio-item  end -->
			
			
			<?php } ?>
		</div>
	
	</div>
		</section>
			
			


<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/functions.js"></script>
</body>

<!-- Mirrored from 7oroof.com/tfdemos/mount/contact-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Nov 2016 05:26:56 GMT -->
</html>