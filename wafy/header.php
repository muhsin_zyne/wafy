<?php
include('admin/General/DBConnection.php');


$HomeGeneral=mysqli_fetch_array(mysqli_query($con,"select * from tbl_home_general where id=1"));


?>

<header id="navbar-spy" class="header header-4 transparent-header ">
	<nav id="primary-menu" class="navbar navbar-fixed-top affix-top">
	    <div id="top-bar" class="top-bar">
			<div class="container">
			<div class="top-bar-border">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 hidden-xs">
						<ul class="list-inline top-contact">
							<li>
								<p>Phone: <span><a href="tel:04933206266">04933 206 266</a>, <a href="tel:9446240340">9446 240 340</a> </span></p>
							</li>
							<li>
								<p>Email: <span><a href="mailto:info@kmeccmarayamangalam.org">info@kmeccmarayamangalam.org</a></span></p>
							</li>
						</ul>
					</div><!-- .col-md-6 end -->

				</div>
			</div>
			</div>
		</div>
		<div class="nav-logo-container">
			<div class="container">
				<a class="logo" href="index.php">
					<img src="<?php echo "Resource/AboutUs/".$HomeGeneral['logo'];?>" alt="kmeccmarayamangalam">
				</a>
			</div>
		</div>
		<div class="nav-menu">
			<div class="container" style="">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					
				</div>
				<div class="collapse navbar-collapse" id="navbar-collapse-1">
					<ul class="nav navbar-nav navbar-left">
						<li> <a href="index.php"  class="link-hover" >Home</a> </li>
						<li> <a href="about.php"  class="link-hover" >About Us</a> </li>
						<li> <a href="staff_team.php"  class="link-hover" >The Team</a> </li>
						<li class="has-dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle link-hover"> Facilities </a>
							<ul class="dropdown-menu">
								<li> <a href="facilities.php"> Library </a> </li>
								<li> <a href="facilities.php"> Computer Lab </a> </li>
								<li> <a href="facilities.php"> Campus Masjid </a> </li>
								<li> <a href="facilities.php"> Canteen </a> </li>
							</ul>
						</li>
						<li class="has-dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle link-hover"> Institutions </a>
							<ul class="dropdown-menu">
								<li> <a href="home-agency-1.html"> Wafy College </a> </li>
								<li> <a href="home-agency-1.html"> Ansar English School </a> </li>
								<li> <a href="home-agency-1.html"> Hifzul Quran College </a> </li>
							</ul>
						</li>
						<li> <a href="gallery.php"  class="link-hover" >Gallery</a> </li>
						<li class="has-dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle link-hover"> Management </a>
							<ul class="dropdown-menu">
								<li> <a href="india_committee.php"> Director Board </a> </li>
								<li> <a href="qatar_committee.php"> Qatar Committee </a> </li>
								<li> <a href="uae_committee.php "> UAE Committee </a> </li>
								<li> <a href="ksa_committee.php"> KSA Committee </a> </li>
							</ul>
						</li>
						<li> <a href="contact.php"  class="link-hover" >Contact Us</a> </li>
						<li> <a href="news.php"  class="link-hover" >News</a> </li>
						<li> <a href="index.php"  class="link-hover" >Downloads</a> </li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
</header>

<style type="text/css">
	div.nav-logo-container {
		background: #fff !important;
	}
</style>