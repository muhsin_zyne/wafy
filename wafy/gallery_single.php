<?php
include('admin/General/DBConnection.php');
if(isset($_GET['GalleryId']))
{
	$ArticleId=$_GET['GalleryId'];
	$ArtResult=mysqli_fetch_array(mysqli_query($con,"select * from tbl_gallery where Approve=1 and GalleryId = '".$ArticleId."'"));
	if(empty($ArtResult[0]))
	{
		echo "<script>window.location='gallery.php'</script>";
	}
}
else
{
	
	echo "<script>window.location='gallery.php'</script>";
}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="DUIAC" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="<?php echo substr($ArtResult['Content'],0,200); ?>">
<link href="favicon.png" rel="icon">
<meta property="og:image" content="http://www.wafycampus.com/Resource/Gallery/<?php echo $ArtResult['Image'];?> "/>


<!-- Fonts
    ============================================= -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100italic,100,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700italic,700,800,800italic,900,900italic%7COpen+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic%7CUbuntu:400,300,300italic,400italic,500,700,500italic,700italic%7CRoboto+Slab:400,100,300,700%7CLora:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">


<!-- Document Title
    ============================================= -->
<title>KMECC MARAYAMANGALAM  | <?php echo $ArtResult['Heading']; ?></title>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" id="st_insights_js" src="http://w.sharethis.com/button/buttons.js?publisher=410547db-9da8-4fad-aa95-c3739d198b33"></script>
<script type="text/javascript">stLight.options({publisher: "410547db-9da8-4fad-aa95-c3739d198b33", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>


</head>
<body>

<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">

<?php include('header.php'); ?>

		

<!-- Page Title #1
============================================= -->
<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="Resource/gallerey.jpg" alt="gallery"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="title-1 text-center">
					
					<div class="title-heading">
						<h2>Gallery</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->




<!-- Portfolio Single #6
============================================= -->
<section id="portfolio-single" class="portfolio portfolio-single portfolio-single-6 pb-0">
	<div class="container">
		<div class="row">
			<div class="col-xs-12  col-sm-12  col-md-7">
			<?php $cnt= mysqli_fetch_array(mysqli_query($con,"select count(Image) from tbl_gallary_images where GalleryId= '".$ArtResult['GalleryId']."'"));
				if($cnt[0]>0) {?>
			
			<div class="portofolio-gallery">
				<img src="<?php echo "Resource/Gallery/".$ArtResult['Image'];?>" alt="Gallery image" width="100%">
					<?php $imgquery=mysqli_query($con,"select Image from tbl_gallary_images where GalleryId= '".$ArtResult['GalleryId']."'");
						 while($imgArray=mysqli_fetch_array($imgquery))
							{
							?>
						<img src="<?php echo "Resource/Gallery/".$imgArray['Image'];?>" alt="Gallery image" width="100%">
							<?php } ?>
				</div>
				<?php } else { ?>
				
				<img src="<?php echo "Resource/Gallery/".$ArtResult['Image'];?>" alt="Gallery image" width="100%">
				<?php } ?>
				
				
			
			</div><!-- .col-md-6 end -->
			<div class="col-xs-12  col-sm-12  col-md-3">
				<div class="portoflio-single-heading">
				<h3><?php echo  $ArtResult['Heading'];?></h3>
				</div>
				<div class="portoflio-single-body">
					<p><?php echo  $ArtResult['Content'];?></p>
				</div>
			</div><!-- .col-md-3 end -->
			<div class="col-xs-12  col-sm-12  col-md-2">
				<div class="portfolio-single-meta">
					<div class="portfolio-date">
						<h4>Date</h4>
						<p><?php echo date('M d Y',strtotime($ArtResult['Date']));?></p>
					</div>
					<div class="portfolio-client">
						<h4>Category</h4>
						<p><?php $cat=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_gallery_category where CategoryId='".$ArtResult['CategoryId']."'")); echo $cat[0];?>  </p>
					</div>
				
					
					<div class="portfolio-share">
						<h4>Share </h4>
					<span class='st_whatsapp_large' displayText='WhatsApp'></span>
<span class='st_facebook_large' displayText='Facebook'></span>
<span class='st_twitter_large' displayText='Tweet'></span>
<span class='st_googleplus_large' displayText='Google +'></span>
<span class='st_linkedin_large' displayText='LinkedIn'></span>
<span class='st_pinterest_large' displayText='Pinterest'></span>

<span class='st_email_large' displayText='Email'></span>
				
					</div>
				</div>
			</div><!-- .col-md-3 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
	<div class="portfolio-related mt-60">
		<div class="container">
			<div class="row">
				<div class="col-xs-12  col-sm-12  col-md-12">
					<div class="portfolio-related-title">
						<h3>Rlated  Gallery:</h3>
					</div>
				</div><!-- .col-md-12 end -->
			</div><!-- .row end -->
		</div><!-- .container end -->
		<div class="container-fluid portfolio-gallery portfolio-fullwidth mt-md">
			<div class="row">
				<!-- Portfolio #1 -->
				 <?php $galleryquery=mysqli_query($con,"select * from tbl_gallery where Approve=1 and CategoryId='".$ArtResult['CategoryId']."' order by GalleryId desc limit 5");
					while($galleryResult=mysqli_fetch_array($galleryquery))
					{
					?>
				
				
				<div class="col-xs-12 col-sm-6 col-md-15  portfolio-item ">
					<div class="portfolio-img">
						<img src="<?php echo  "Resource/Gallery/thump_".$galleryResult['Image'];?>" alt="wafy image gallery">
						<div class="portfolio-hover">
							<div class="portfolio-meta">
								<div class="portfolio-cat">
									<span>In </span><span><?php $cat=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_gallery_category where CategoryId='".$galleryResult['CategoryId']."'")); echo $cat[0];?> </span>
								</div>
								<div class="portfolio-title">
									<h4><?php echo  $galleryResult['Heading'];?></h4>
								</div>
							</div><!-- .Portfolio-meta end -->
							<div class="portfolio-action">
							<div class="portfolio-zoom-like">
								<a class="img-popup zoom" href="<?php echo  "Resource/Gallery/".$galleryResult['Image'];?>" title="<?php echo  $galleryResult['Content'];?>" style="float: left">
									<i class="fa fa-search"></i>
								</a>
								<div class="portfolio-link" style="float: left">
								<a href="gallery_single.php?GalleryId=<?php echo  $galleryResult['GalleryId'];?>&<?php echo strtolower(str_replace(' ', '_',$galleryResult['Heading'])); ?>"><i class="fa fa-link"></i></a>
							</div>
							</div>
							
						</div><!-- .Portfolio-action end -->
						</div><!-- .Portfolio-hover end --> 
					</div><!-- .Portfolio-img end -->
				</div><!-- . portfolio-item  end -->
				<?php } ?>
	
			</div><!-- .row end -->
		</div><!-- .container end -->
	</div><!-- .portfolio-related end -->
	
</section><!-- #page-title end -->


<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/functions.js"></script>
</body>

<!-- Mirrored from 7oroof.com/tfdemos/mount/contact-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Nov 2016 05:26:56 GMT -->
</html>