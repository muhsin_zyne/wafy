<?php include('General/Header.php'); 




if(isset($_POST['StudentsSubmit']))
{
	$Name=trim(mysqli_real_escape_string($con,$_POST['Name']));
	$BatchId=trim(mysqli_real_escape_string($con,$_POST['BatchId']));
	$Occupation=trim(mysqli_real_escape_string($con,$_POST['Occupation']));
	$DisplayOrder=trim(mysqli_real_escape_string($con,$_POST['DisplayOrder']));
	$Address=trim(mysqli_real_escape_string($con,$_POST['Address']));
	$Email=trim(mysqli_real_escape_string($con,$_POST['Email']));
	$Phone=trim(mysqli_real_escape_string($con,$_POST['Phone']));
	$Mobile=trim(mysqli_real_escape_string($con,$_POST['Mobile']));
	$RegCIC=trim(mysqli_real_escape_string($con,$_POST['RegCIC']));
	$RegCLG=trim(mysqli_real_escape_string($con,$_POST['RegCLG']));
	$FbLink=trim(mysqli_real_escape_string($con,$_POST['FbLink']));
	$TwitterLink=trim(mysqli_real_escape_string($con,$_POST['TwitterLink']));
	$GoogleLink=trim(mysqli_real_escape_string($con,$_POST['GoogleLink']));
	$LinkedInLink=trim(mysqli_real_escape_string($con,$_POST['LinkedInLink']));
	
		
	
	if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
	{
			$filename=$_FILES['mainImage']['name'];
			$temp=$_FILES['mainImage']['tmp_name'];
			$ra=rand(10,10000000000);
			$rb=rand(10,10000000000);
			$r="Students_".$ra.$rb."_";
			$image_name1=$r.$filename;
			move_uploaded_file($temp,"../Resource/Students/".$image_name1);
		
	$result=mysqli_query($con,"INSERT INTO tbl_students (StudentsId,Image,Name,BatchId,Occupation,DisplayOrder,Address,Email,Phone,Mobile,RegCIC,RegCLG,FbLink,TwitterLink,GoogleLink,LinkedInLink)
										VALUES((SELECT IFNULL((SELECT MAX(StudentsId)+1 FROM  tbl_students temp),1)),'$image_name1','$Name','$BatchId','$Occupation','$DisplayOrder','$Address','$Email','$Phone','$Mobile','$RegCIC','$RegCLG','$FbLink','$TwitterLink','$GoogleLink','$LinkedInLink')");

		
			if($result)
			{
				echo '<script>alert(" Successfully Inserted");</script>';		
			}
			else	
			{			
				echo '<script>alert("Data Not Inserted");</script>';			
			}
			echo "<script>window.location='Students.php'</script>";
		}
	else
	{
		echo '<script>alert(" Please Select Image");</script>';	
	}
}
?>
	<script>
        function validateForm() {
            var name = document.forms["FormStudents"]["Name"].value;
			var designation = document.forms["FormStudents"]["Address"].value;
			
			
			var Email = document.forms["FormStudents"]["Email"].value;
			
			
			var Phone = document.forms["FormStudents"]["Phone"].value;
			var Mobile = document.forms["FormStudents"]["Mobile"].value;
			var RegCIC = document.forms["FormStudents"]["RegCIC"].value;
			var RegCLG = document.forms["FormStudents"]["RegCLG"].value;
			

		   var Category = document.forms["FormStudents"]["BatchId"].value;
			
			var imgpath=document.getElementById('mainImage');
			
			  if (Category==0) 
			{
				alert("Please Select Batch.");     
				return false;       
			}
			else if (!name||!designation||!Email||!Phone||!Mobile||!RegCIC||!RegCLG) 
			{
				alert("Please Fill All Fields.");     
				return false;       
			}
			else if (imgpath.value=="")
			{
				alert("Please Select Image.");     
				return false;    
			}
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Students Details</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Add New</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormStudents">

				

			

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Name" required class="form-control col-md-7 col-xs-12">
				</div></div>
				
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">CIC Reg. No. <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="number" name="RegCIC" required class="form-control col-md-7 col-xs-12" min="1">
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">College Reg. No.<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="number" name="RegCLG" required class="form-control col-md-7 col-xs-12" min="1">
				</div></div>
				
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Batch <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="BatchId" id="BatchId" >
                            <option value="0" selected="selected" >- Select Batch -</option>
                                <?php 
                                $NewsCategoryRow=mysqli_query($con,"SELECT * FROM tbl_students_category order by CategoryId desc");
                                while($NewsCategoryResult=mysqli_fetch_array($NewsCategoryRow)){?>
                                    <option value="<?php echo $NewsCategoryResult['CategoryId'];?>" ><?php echo $NewsCategoryResult['Category']; ?></option>
                                <?php } ?>
                        </select>
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Addres<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Address" required class="form-control col-md-7 col-xs-12">
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="email" name="Email" class="form-control col-md-7 col-xs-12">
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Phone<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Phone" required class="form-control col-md-7 col-xs-12">
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mobile<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Mobile" required class="form-control col-md-7 col-xs-12">
				</div></div>
				
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Occupation<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Occupation" class="form-control col-md-7 col-xs-12">
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Facebook Link<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text"  name="FbLink" class="form-control col-md-7 col-xs-12"  >
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Twitter Link<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text"  name="TwitterLink" class="form-control col-md-7 col-xs-12"  >
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Google Plus Link<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text"  name="GoogleLink" class="form-control col-md-7 col-xs-12"  >
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Linked In Link<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text"  name="LinkedInLink" class="form-control col-md-7 col-xs-12"  >
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Display Order<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number"  name="DisplayOrder" required class="form-control col-md-7 col-xs-12" min="1" value="<?php $or= mysqli_fetch_array(mysqli_query($con,"(SELECT IFNULL((SELECT MAX(StudentsId)+1 FROM  tbl_students temp),1))"));echo $or[0];  ?>"  >
				</div></div>
				
            
				
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<font color="#FF0004">Max-Image Size: <b>500KB</b> &nbsp; (Image Dimension --- width: <b>270px</b> , Height: <b>400px</b> )</font>
						<input type="file" name="mainImage" id="mainImage"  class="form-control col-md-7 col-xs-12" accept="image/*" required >
				</div></div>
               

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" name="StudentsSubmit" class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>

 		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Students List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                         <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>
                             <th>Name</th>
                              <th>Batch</th>
                              <th>Reg# CIC</th>
                               <th>Reg# CLG</th>
                               
                               <th>Address</th>
                                <th>Phone</th>
                                 <th>Mobile</th>
                                 
                              <th>Image</th>
                             
                             
                             <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
				

			$StudentsRow=mysqli_query($con,"select * from tbl_students order by StudentsId desc");

  
	while($StudentsResult=mysqli_fetch_array($StudentsRow))
	{
		$i=$i+1;
	 ?>
                            <tr>
                              <td><?php echo $i; ?></td>
                              
                              
                              <td><?php echo $StudentsResult['Name']; ?></td>
								<td><?php 	$Category=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_students_category where CategoryId ='".$StudentsResult['BatchId']."'"));
		echo $Category[0];?></td>
                            
                             <td><?php echo $StudentsResult['RegCIC']; ?></td>
                              <td><?php echo $StudentsResult['RegCLG']; ?></td>
                               <td><?php echo $StudentsResult['Address']; ?></td>
                                <td><?php echo $StudentsResult['Phone']; ?></td>
                                 <td><?php echo $StudentsResult['Mobile']; ?></td>
                   
                             
                             
                                <td> <img src="<?php echo "../Resource/Students/".$StudentsResult['Image'];?>" width="100" ></td>
                           	
                             
                             <td> 
                             <form method="post" enctype="multipart/form-data" action="Edit_Students.php"  style="float: left"> 
                             <input type="hidden" name="StudentsIdUpdate" value="<?php echo $StudentsResult['StudentsId']; ?>">
                             	<input type="submit" class="btn btn-info" value="Edit" >
							</form>
        					
         					
          					<form method="post" enctype="multipart/form-data" action="action.php" onSubmit="return confirm('Are you sure want to delete this record ?');"  style="float: left">
                            	<input type="hidden" name="StudentsIdDelete" value="<?php echo $StudentsResult['StudentsId']; ?>"> 
                             	<input type="submit" class="btn btn-danger" value="Delete">
							</form>
            </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>

          </div></div></div></div>
	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
