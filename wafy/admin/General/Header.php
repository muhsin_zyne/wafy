
<?php
@session_start();

date_default_timezone_set('Asia/Kolkata');

 //echo $lan;exit;
include ('General/CheckLog.php');
check_loig();

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>KMECC MARAYAMANGALAM </title>

<!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

<!-- Custom styling plus plugins -->
  <link href="css/custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.3.css" />
  <link href="css/icheck/flat/green.css" rel="stylesheet" />
  <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

<link href="js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

  <script src="js/jquery.min.js"></script>
  <script src="js/nprogress.js"></script>

</head>

<body class="nav-md">

	<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col">
		<div class="left_col scroll-view">

			<div class="navbar nav_title" style="border: 0;">
				<a href="Home.php" class="site_title"> <span style="color:#007fec"> <b>DUIAC</b></span></a>
			</div>
			<div class="clearfix"></div>

          <!-- menu prile quick info -->
			<div class="profile">
				<div class="profile_pic">
					<img src="<?php if(empty($_SESSION['DUIImage'])) echo "../Resource/User/userimg.png"; else echo "../Resource/User/".$_SESSION['DUIImage'];?>" alt="User Image" class="img-circle profile_img">
				</div>
				<div class="profile_info">
					<span>Welcome,</span>
					<h2><?php echo $_SESSION['DUIName']; ?></h2>
				</div>
			</div>
<!-- /menu prile quick info -->
			<br />
<!-- sidebar menu -->
			<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

				<div class="menu_section">
					<h3>&nbsp;</h3>
					<ul class="nav side-menu">
					<?php if($_SESSION['DUIRoleId']==1) { ?>
						<li><a><i class="fa fa-user"></i> User <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu" style="display: none">
								<li>		<a href="User.php">User Register</a>		</li>
							</ul>
						</li>
						
						<li><a><i class="fa fa-edit"></i> Home Page <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu" style="display: none">
								
									<li>		<a href="Home_General.php">General</a>		</li>
								<li>		<a href="Slider.php">Slider</a>		</li>
								<li>		<a href="AboutUs_Eng.php">About Us - English</a>		</li>
								<li>		<a href="AboutUs_Arb.php">About Us - Arabic</a>		</li>
								<li>		<a href="AboutUs_Mal.php">About Us - Malayalam</a>		</li>
							
							</ul>
						</li>
						
						
						
						<?php } ?>
						<li><a><i class="fa fa-edit"></i> News <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu" style="display: none">
								
								
								<li>		<a href="News.php">News Content</a>		</li>
							<?php if($_SESSION['DUIRoleId']==1) { ?>	
							<li>		<a href="NewsCategory.php">News Category</a>		</li>
							<?php } ?>
							</ul>
						</li>
						
						
						<li><a><i class="fa fa-edit"></i> Gallery <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu" style="display: none">
								
								
								<li>		<a href="Gallery.php">Gallery Content</a>		</li>
							<?php if($_SESSION['DUIRoleId']==1) { ?>	
							<li>		<a href="GalleryCategory.php">Gallery Category</a>		</li>
							<?php } ?>
							</ul>
						</li>
						
						
						<li><a><i class="fa fa-edit"></i> Article <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu" style="display: none">
								
								
								<li>		<a href="Article.php">Article Content</a>		</li>
							<?php if($_SESSION['DUIRoleId']==1) { ?>	
							<li>		<a href="ArticleCategory.php">Article Category</a>		</li>
							<?php } ?>
							</ul>
						</li>
						
						<?php if($_SESSION['DUIRoleId']==1) { ?>	
						<li><a><i class="fa fa-edit"></i> Testimonial <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu" style="display: none">
								
								
								<li>		<a href="Testimonial.php">Testimonial</a>		</li>
							
							
							</ul>
						</li>
						<?php } ?>
						<?php if($_SESSION['DUIRoleId']==1) { ?>	
						<li><a><i class="fa fa-edit"></i> Staff / Committee <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu" style="display: none">
								
								
								<li>		<a href="Staff.php">Staff Details</a>		</li>
								<li>		<a href="committee.php">Committee Details</a>		</li>
							
							
							</ul>
						</li>
						<?php } ?>
						
						<?php if($_SESSION['DUIRoleId']==1) { ?>	
						<li><a><i class="fa fa-edit"></i> Student Details <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu" style="display: none">
								
								
								<li>		<a href="Students.php">Student Details</a>		</li>
							<li>		<a href="StudentsCategory.php">Student Batch</a>		</li>
							
							</ul>
						</li>
						<?php } ?>
						
						
						<li><a><i class="fa fa-edit"></i> File Upload <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu" style="display: none">
								
								
								<li>		<a href="Upload.php">File Upload</a>		</li>
							
							</ul>
						</li>
						

						<li><a><i class="fa fa-edit"></i> Contacts <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu" style="display: none">
								
								
								<li>		<a href="Contacts.php">Add Contacts</a>		</li>
							<?php if($_SESSION['DUIRoleId']==1) { ?>	
							<li>		<a href="ContactsCategory.php">Contacts Category</a>		</li>
							<?php } ?>
							</ul>
						</li>
						
						
						

              </ul>
            </div>
            

          </div>

        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="<?php if(empty($_SESSION['DUIImage'])) echo "../Resource/User/userimg.png"; else echo "../Resource/User/".$_SESSION['DUIImage'];?>" alt=""><?php echo $_SESSION['DUIName']; ?>
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                  <li><a href="UserSettings.php?UserId=<?php echo  $_SESSION['DUIUserId'];?>">  Settings</a>
                  </li>

                  <li><a href="Logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                  </li>
                </ul>
				</li>
            </ul>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->