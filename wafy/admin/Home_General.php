<?php include('General/Header.php'); 
if($_SESSION['DUIRoleId']!=1)
{ 
	echo "<script>window.location='Home.php'</script>";
}
$result=mysqli_fetch_array(mysqli_query($con,"select * from tbl_home_general where id=1"));
function ImageUpload($image)
{
	$filename=$_FILES[$image]['name'];
	$temp=$_FILES[$image]['tmp_name'];
	$ra=rand(10,10000000000);
	$rb=rand(10,10000000000);
	$r="AboutUs_Logo_".$ra.$rb."_";
	$image_name=$r.$filename;
	move_uploaded_file($temp,"../Resource/AboutUs/".$image_name);
	return $image_name;
}

if(isset($_POST['UserSubmit']))
{
	$favourites=trim(mysqli_real_escape_string($con,$_POST['favourites']));
	$gallery=trim(mysqli_real_escape_string($con,$_POST['gallery']));
	$articles=trim(mysqli_real_escape_string($con,$_POST['articles']));
	
	$braucher=trim(mysqli_real_escape_string($con,$_POST['braucher']));
	$fecilities=trim(mysqli_real_escape_string($con,$_POST['fecilities']));
	
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select logo,braucher_file from tbl_home_general WHERE id=1"));
	
	$Mainimage=$ImageQuery['logo'];
	$braucherfile=$ImageQuery['braucher_file'];
	

	

		
	
	if(isset($_FILES['logo']['name']) && !empty($_FILES['logo']['name']))
	{
		if(!empty($Mainimage))
		{
			unlink("../Resource/AboutUs/".$Mainimage);
		}
		
		$image_name1= ImageUpload('logo');
		
		if(!empty($image_name1))
			$Mainimage=$image_name1;
	}
	
	
	if(isset($_FILES['braucherfile']['name']) && !empty($_FILES['braucherfile']['name']))
	{
		if(!empty($braucherfile))
		{
			unlink("../Resource/AboutUs/".$braucherfile);
		}
		
		$filename=$_FILES['braucherfile']['name'];
		$temp=$_FILES['braucherfile']['tmp_name'];
		$ra=rand(10,10000000000);
		$rb=rand(10,10000000000);
		$r="AboutUs_Brch_".$ra.$rb."_";
		$image_name=$r.$filename;
		move_uploaded_file($temp,"../Resource/AboutUs/".$image_name);
		$braucherfile= $image_name;
	}
	
	

		$result=mysqli_query($con,"update tbl_home_general set logo='$Mainimage',favourites='$favourites',gallery='$gallery',articles='$articles',braucher='$braucher',fecilities='$fecilities',braucher_file='$braucherfile' where id=1");
		
		if($result)
		{		
			echo '<script>alert(" Successfully Updated");</script>';
		}
		else
		{		
			echo '<script>alert("Data Not Updated");</script>';		
		}
		echo "<script>window.location='Home_General.php'</script>";
}
?>
 

<script>
	

	
/*	var MobilePhoto=document.getElementById('MobilePhoto');
	var flag = true;
	var Photo;
	var ImageName;
	for (var i=0; i<MobilePhoto.files.length; i++) 
	{
		Photo=MobilePhoto.files[i].name;
		var blnValidImage = false;
		var ImagevalidFileExtensions = [".jpg", ".jpeg", ".gif", ".png"];  
		for (var j = 0; j < ImagevalidFileExtensions.length; j++)
		{
			var sCurExtension = ImagevalidFileExtensions[j];
			if (Photo.substr(Photo.length - sCurExtension.length, sCurExtension.length).toLowerCase()== sCurExtension.toLowerCase()) 
			{
				blnValidImage = true;
				break;
			}
		}
		if (!blnValidImage) 
		{
			flag=false;
			ImageName=Photo;
			break;
		}
	} 
	if (!flag) 
	{
		swal("", ImageName+" file extension not valid.");
		return '0';
	}
	else 
	{
		return '1';
	}
	
	*/
	function validateForm() 
	{
		var favourites = document.forms["FormUser"]["favourites"].value;
		var gallery = document.forms["FormUser"]["gallery"].value;
		var articles = document.forms["FormUser"]["articles"].value;
		var braucher = document.forms["FormUser"]["braucher"].value;
		var fecilities = document.forms["FormUser"]["fecilities"].value;
	


		if (!favourites || !gallery ||!articles ||!braucher ||!fecilities  ) 
		{
			alert("Plese Fill All Fields");
			return false;
		}

	}
</script>
       
<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>General </h3>
            </div>
          </div>
          <div class="clearfix"></div>
          
           
           
           <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edit </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                 
                  <br />
                  <form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data"
                   onsubmit="return validateForm()" name="FormUser">
                   
                   
                       <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Logo <span class="required">*</span>			</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                    
                          <input type="file" id="logo" name="logo"  class="form-control col-md-7 col-xs-12" accept="image/*">
                          <br />
                          <a href="<?php echo "../Resource/AboutUs/".$result['logo'];?>" download>  <img src="<?php echo "../Resource/AboutUs/".$result['logo'];?>" width="200">
						  </a>
                    </div>
                    </div>
                    
                    
                   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Favourites Description <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <input type="text" name="favourites" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['favourites']; ?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Braucher Description <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <input type="text" name="braucher" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['braucher']; ?>">
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Braucher File(PDF) <span class="required">*</span>			</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                    
                          <input type="file" id="braucherfile" name="braucherfile"  class="form-control col-md-7 col-xs-12" accept="application/pdf">
                          <br />
                          <a href="<?php echo "../Resource/AboutUs/".$result['braucher_file'];?>" download>  Braucher
						  </a>
                    </div>
                    </div>
                    
                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gallery Description <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <input type="text" name="gallery" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['gallery']; ?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Articles Description <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <input type="text" name="articles" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['articles']; ?>">
                      </div>
                    </div>
                    
                    
                  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fecilities Description <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <input type="text" name="fecilities" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['fecilities']; ?>">
                      </div>
                    </div>
                    
                    
                    
                    
                    
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                     
                        <button type="submit" name="UserSubmit" class="btn btn-success">Update</button>
                    </div></div>
                  </form>
          </div></div></div></div>
          


</div>
</div>
            <script src="ckeditor/ckeditor.js" type="text/javascript"></script>
                    
            <script>
                CKEDITOR.replace( 'Summary' );
                CKEDITOR.replace( 'Description' );
            </script>
            
          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>

<?php include('General/Footer.php'); ?>
