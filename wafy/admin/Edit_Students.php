<?php include('General/Header.php'); 


if(isset($_REQUEST['StudentsIdUpdate']))
{
   $id=$_REQUEST['StudentsIdUpdate'];
   //echo $id;exit;
   $result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_students WHERE StudentsId='$id'"));
}
else
{
	echo "<script>window.location='Students.php'</script>";
}
	

?>
	<script>
        function validateForm() {
            var name = document.forms["FormStudents"]["Name"].value;
			var designation = document.forms["FormStudents"]["Address"].value;
			
			
			var Email = document.forms["FormStudents"]["Email"].value;
			
			
			var Phone = document.forms["FormStudents"]["Phone"].value;
			var Mobile = document.forms["FormStudents"]["Mobile"].value;
			var RegCIC = document.forms["FormStudents"]["RegCIC"].value;
			var RegCLG = document.forms["FormStudents"]["RegCLG"].value;
			

		   var Category = document.forms["FormStudents"]["BatchId"].value;
			
		
			  if (Category==0) 
			{
				alert("Please Select Batch.");     
				return false;       
			}
			else if (!name||!designation||!Email||!Phone||!Mobile||!RegCIC||!RegCLG) 
			{
				alert("Please Fill All Fields.");     
				return false;       
			}
	
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Students</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Update</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="action.php" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormStudents">

				

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Name" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Name'];?>">
				</div></div>
				
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">CIC Reg. No. <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="number" name="RegCIC" required class="form-control col-md-7 col-xs-12" min="1" value="<?php echo $result['RegCIC'];?>">
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">College Reg. No.<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="number" name="RegCLG" required class="form-control col-md-7 col-xs-12" min="1" value="<?php echo $result['RegCLG'];?>">
				</div></div>
				
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Batch <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="BatchId" id="BatchId" >
                            <option value="0" selected="selected" >- Select Batch -</option>
                                <?php 
                                $NewsCategoryRow=mysqli_query($con,"SELECT * FROM tbl_students_category order by CategoryId desc");
                                while($NewsCategoryResult=mysqli_fetch_array($NewsCategoryRow)){?>
                                    <option value="<?php echo $NewsCategoryResult['CategoryId'];?>" ><?php echo $NewsCategoryResult['Category']; ?></option>
                                <?php } ?>
                        </select>
                           <script>
							var Category= document.getElementById('BatchId');
							Category.value=<?php echo $result['BatchId'];?>
							</script>	
							
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Addres<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Address" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Address'];?>">
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="email" name="Email" class="form-control col-md-7 col-xs-12" value="<?php echo $result['Email'];?>" >
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Phone<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Phone" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Phone'];?>">
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mobile<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Mobile" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Mobile'];?>">
				</div></div>
				
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Occupation<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Occupation" class="form-control col-md-7 col-xs-12" value="<?php echo $result['Occupation'];?>">
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Facebook Link<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text"  name="FbLink" class="form-control col-md-7 col-xs-12"  value="<?php echo $result['FbLink'];?>">
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Twitter Link<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text"  name="TwitterLink" class="form-control col-md-7 col-xs-12"  value="<?php echo $result['TwitterLink'];?>">
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Google Plus Link<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text"  name="GoogleLink" class="form-control col-md-7 col-xs-12"  value="<?php echo $result['GoogleLink'];?>">
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Linked In Link<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text"  name="LinkedInLink" class="form-control col-md-7 col-xs-12"  value="<?php echo $result['LinkedInLink'];?>">
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Display Order<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number"  name="DisplayOrder" required class="form-control col-md-7 col-xs-12" min="1" value="<?php echo $result['DisplayOrder'];?>" >
				</div></div>
				
            
				
			

				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							
						<input type="file" name="mainImage" id="mainImage"  class="form-control col-md-7 col-xs-12" accept="image/*"  >
						<br />
						<a href="<?php echo "../Resource/Students/".$result['Image'];?>" download> <img src="<?php echo "../Resource/Students/".$result['Image'];?>" width="100" > </a>
				
				</div></div>
               <input type="hidden" name="StudentsUpdate" value="<?php echo $result['StudentsId'];?>">

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" name="StudentsSubmit" class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>

	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
