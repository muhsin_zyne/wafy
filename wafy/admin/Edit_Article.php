<?php include('General/Header.php'); 

if(isset($_REQUEST['ArticleIdUpdate']))
{
   $id=$_REQUEST['ArticleIdUpdate'];
   //echo $id;exit;
   $result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_article WHERE ArticleId='$id'"));
}
else
{
	echo "<script>window.location='Article.php'</script>";
}

?>
	<script>
        function validateForm() {
            
            var Category = document.forms["FormArticle"]["Category"].value;
			var imgpath=document.getElementById('mainImage');
			 var imageOrVideo = document.forms["FormArticle"]["imageOrVideo"].value;
			var videoLink = document.forms["FormArticle"]["videoLink"].value;
            if (Category==0) 
			{
				alert("Please Select Category.");     
				return false;       
			}
			else if (!imgpath.value=="")
			{
				var imgsize=imgpath.files[0].size;
				if(imgsize>500000)	
				{		
					alert(imgpath.files[0].name+" - Size is larger than 500KB. Please Reduce it.");	
					return false;	
				}
			}
			if(imageOrVideo==2&& !videoLink)
				{
					alert("Please enter video link.");     
				return false;  
					
				}
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Article</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Edit Article</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="action.php" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormArticle">

				

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Article Category <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="Category" id="Category" >
                            <option value="0" selected="selected" >- Select Category -</option>
                                <?php 
                                $ArticleCategoryRow=mysqli_query($con,"SELECT * FROM tbl_article_category order by CategoryId desc");
                                while($ArticleCategoryResult=mysqli_fetch_array($ArticleCategoryRow)){?>
                                    <option value="<?php echo $ArticleCategoryResult['CategoryId'];?>" ><?php echo $ArticleCategoryResult['Category']; ?></option>
                                <?php } ?>
                                
                         <script>
							var Category= document.getElementById('Category');
							Category.value=<?php echo $result['CategoryId'];?>
							</script>	
								
                        </select>
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Article Heading<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Heading" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Heading'];?>"> 
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Article Content<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea name="Content" required class="form-control col-md-7 col-xs-12" rows="5"><?php echo $result['Content'];?></textarea>
				</div></div>
				
					<script src="ckeditor/ckeditor.js" type="text/javascript"></script>
                    
            <script>
                CKEDITOR.replace( 'Content' );
            </script>
            
            
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Date<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="date" name="Date" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Date'];?>">
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Author<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Author" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Author']; ?>">
				</div></div>
				
				<div class="form-group">

            <label class="control-label col-md-3 col-sm-3 col-xs-12">Image/Video</label>
<div class="col-md-6 col-sm-6 col-xs-12">
            
            <select class="form-control col-md-7 col-xs-12" name="imageOrVideo" id="imageOrVideo" >
                <option value="1" selected="selected" >Image Only</option>
                <option value="2"  >Video</option>
            </select>
             <script type="text/javascript">
						var element = document.getElementById('imageOrVideo');
						element.value = <?php echo $result['ImageOrVideo'];?>;
					 </script>
					 
					  </div>
                      <script>
						  
					  $(document).ready(function () {
						   if($('#imageOrVideo').val()=='1')
							{
								$('#VideoField').hide();
							}
							
							else
							{
								$('#VideoField').show();
							}
							
							$('#imageOrVideo').change(function (){
							if($('#imageOrVideo').val()=='1')
							{
								$('#VideoField').hide();
							}
							else
							{
								$('#VideoField').show();
							}
							});
						});
					  </script>                  
        </div>
        
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						
							<font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>750px</b> , Height: <b>450px</b> )</font>
						<input type="file" name="mainImage" id="mainImage"  class="form-control col-md-7 col-xs-12" accept="image/*"  >
						
						  <a href="<?php echo "../Resource/Article/".$result['Image'];?>" download> <img src="<?php echo "../Resource/Article/".$result['Image'];?>" width="150" > </a>
						  
				</div></div>
              
              	<div class="form-group" id="VideoField">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Video Link (embedded) <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="videoLink" class="form-control col-md-7 col-xs-12" value="<?php echo $result['Video'];?>">
				</div></div>
              
               

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <input type="hidden" name="ArticleUpdate" value="<?php echo $result['ArticleId'];?>">
						<button type="submit"  class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>


	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
