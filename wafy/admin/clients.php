<?php include('General/Header.php'); 
if(isset($_POST['ClientSubmit']))
{
	$Client=$_POST['Client'];
	$Display=$_POST['Display'];

	if(isset($_FILES['LogoBlk']['name']) && !empty($_FILES['LogoBlk']['name']))
	{	if($_FILES['LogoBlk']['size'] > 400000)		{
			echo '<script type="text/javascript">alert("'.$_FILES['LogoBlk']['name'].' - Size is larger than 400KB. Please Reduce it.");</script>';	}
		else if($_FILES['YtLogo']['size'] > 400000)		{
			echo '<script type="text/javascript">alert("'.$_FILES['YtLogo']['name'].' - Size is larger than 400KB. Please Reduce it.");</script>';	}
		else{
			$isok=true;

				$filename=$_FILES['LogoBlk']['name'];
				$temp=$_FILES['LogoBlk']['tmp_name'];
				$ra=rand(10,10000000000);
				$rb=rand(10,10000000000);
				$r=$ra.$rb;
				$BlkLogo=$r.$filename;
				move_uploaded_file($temp,"../AdminImage/client/".$BlkLogo);

				$filename=$_FILES['YtLogo']['name'];
				$temp=$_FILES['YtLogo']['tmp_name'];
				$ra=rand(10,10000000000);
				$rb=rand(10,10000000000);
				$r=$ra.$rb;
				$YtLogo=$r.$filename;
				move_uploaded_file($temp,"../AdminImage/client/".$YtLogo);

$result=mysqli_query($con,"INSERT INTO tbl_clients (ClientName,BlkLogo,YtLogo,Display)
					 VALUES('$Client','$BlkLogo','$YtLogo','$Display')");


									if($result)	{				echo '<script>alert(" Successfully Inserted");</script>';			}
									else		{				echo '<script>alert("Data Not Inserted");</script>';			}
									echo "<script>window.location='clients.php'</script>";
}	}	  }
?>

	<script>
        function validateForm() {
			var imgpath=document.getElementById('LogoBlk');
//			var imgpath=document.getElementById('LogoYt');
			var Images = document.getElementById('LogoYt');  

            if (!imgpath.value==""){
				var imgsize=imgpath.files[0].size;
				if(imgsize>400000)
				{				alert(imgpath.files[0].name+" - Size is larger than 400KB. Please Reduce it.");				return false;				}
			  }

            if (!Images.value==""){
				var imgsize=Images.files[0].size;
				if(imgsize>400000)
				{				alert(Images.files[0].name+" - Size is larger than 400KB. Please Reduce it.");				return false;				}
			  }
		}
     </script>		

		<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Clients</h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>New Client</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormBlog">

                      <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Client Name<span class="required">*</span>	</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                        <input type="text" name="Client" required class="form-control col-md-7 col-xs-12">
                      </div></div>

                      <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Client's Logo<span class="required">*</span>			</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                      <font color="#FF0004"><b>Black Color</b> &nbsp; (Image Dimension --- width: <b>150px</b> , Height: <b>75px</b> )</font>
                          <input type="file" id="LogoBlk" name="LogoBlk"  class="form-control col-md-7 col-xs-12" accept="image/*" required >
	                  </div></div>

                      <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Approve <span class="required">*</span></label>
                      <div class="col-md-10 col-sm-6 col-xs-12">
                          <input type="checkbox" name="Display" class="form-control col-md-7 col-xs-12" value="1" checked="checked" >
                      </div></div>

                      <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Client's Logo<span class="required">*</span>			</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                      <font color="#FF0004"><b>White Color</b> &nbsp; (Image Dimension --- width: <b>150px</b> , Height: <b>75px</b> )</font>
                          <input type="file" id="YtLogo" name="YtLogo"  class="form-control col-md-7 col-xs-12" accept="image/*" required >
	                  </div></div>

                      <div class="form-group">
                      <div class="col-md-10  col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" name="ClientSubmit" class="btn btn-success">Submit</button>
                      </div></div>

                  </form>
		</div></div></div></div>

 		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Clients</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />

                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>                            <th>Client Name</th>                              <th>Black Logo</th>
                              <th>Front Page</th>                            <th>White Logo</th>                               <th>Action</th> 
                            </tr>
                          </thead>
                          <tbody>
<?php
	$i=0;
	 $BlogRow=mysqli_query($con,"select * from tbl_clients order by ClientId desc");
	while($BlogResult=mysqli_fetch_array($BlogRow))
	{	$i=$i+1;
	 ?>
                            <tr>
                              <td><?php echo $i; ?></td>

                              <td><?php echo $BlogResult['ClientName']; ?></td>
                              <td><img src="../AdminImage/client/<?php echo $BlogResult['BlkLogo']; ?>" style="height:80px;width:80px;"> </td>
                              <td><?php
										if($BlogResult['Display']=='1')
											echo "True";
										else
											echo "False"; ?></td>
                              <td><img src="../AdminImage/client/<?php echo $BlogResult['YtLogo']; ?>" style="height:80px;width:80px;"> </td>

                              <td> <a class="btn btn-info" href="Edit_Clients.php?ClientId=<?php echo $BlogResult['ClientId']; ?>">
                <i class="glyphicon glyphicon-edit icon-white"></i>
                Edit
            </a>
            <a class="btn btn-danger" href="action.php?ClientId=<?php echo $BlogResult['ClientId']; ?>" onclick="return window.confirm('Do you want to delete this record?')">
                <i class="glyphicon glyphicon-trash icon-white"></i>
                Delete
            </a></td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>
		</div></div></div></div>
</div></div>
          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
