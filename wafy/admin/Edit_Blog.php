<?php include('General/Header.php'); 

if(isset($_REQUEST['BlogId']))
{
   $id=$_REQUEST['BlogId'];
   //echo $id;exit;
   $result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_blog WHERE BlogId='$id'"));
}
else{	echo "<script>window.location='Blog.php'</script>";	}
?>
	<script>
        function validateForm() {
            var BlogCategory = document.forms["FormBlog"]["BlogCategory"].value;
			var imgpath=document.getElementById('MainImage');

            if (BlogCategory==0) {                alert("Please Select Category.");                return false;            }
			else if (!imgpath.value==""){
				var imgsize=imgpath.files[0].size;
				if(imgsize>400000)
				{
				alert(imgpath.files[0].name+" - Size is larger than 400KB. Please Reduce it.");
				return false;
				}
			  }

				var Images = document.getElementById('SubImages');  
				for (var i = 0; i < Images.files.length; i++)
				{
					 var imageSize = Images.files[i].size;
					 if (imageSize > 400000) {
						alert(Images.files[i].name+" - Size is larger than 400KB. Please Reduce it.");
						return false;
						break;
		}		}	}
     </script>

	<div class="right_col" role="main">
	<div class="">
	<div class="page-title">
		<div class="title_left">		<h3>Blog</h3>		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Edit Content</h2>
			<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			</ul>
			<div class="clearfix"></div>
		</div>
	<div class="x_content">
	<br />
		<form data-parsley-validate class="form-horizontal form-label-left" action="action.php" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormBlog">

			<div class="form-group">
				<label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Category <span class="required">*</span>	</label>
				<div class="col-md-10 col-sm-6 col-xs-12">
					<select class="form-control" name="BlogCategory" id="BlogCategory" >
					<option value="0"  >- Select Category -</option>
				<?php
					$MainCategoryRow=mysqli_query($con,"SELECT * FROM tbl_blogcategory order by BlogCategoryId desc");
					while($MainCategoryResult=mysqli_fetch_array($MainCategoryRow)){?>
					<option value="<?php echo $MainCategoryResult['BlogCategoryId'];?>" ><?php echo $MainCategoryResult['BlogCategoryName']; ?></option>
				<?php } ?>
				<script>
					var MainCategory=document.getElementById('MainCategory');
					MainCategory.value=<?php echo $result['BlogCategoryId'];?>;
				</script>
					</select>
			</div></div>

			<div class="form-group">
				<label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Blog Title<span class="required">*</span>	</label>
				<div class="col-md-10 col-sm-6 col-xs-12">
				<input type="text" name="Title" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['BlogTitle'];?>">
			</div></div>

			<div class="form-group">
				<label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Author <span class="required">*</span>	</label>
				<div class="col-md-10 col-sm-6 col-xs-12">
				<input type="text" name="Author" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Author'];?>">
			</div></div>

			<div class="form-group">
				<label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Post Date <span class="required">*</span>	</label>
				<div class="col-md-10 col-sm-6 col-xs-12">
				<input type="date" required value="<?php echo $result['PostDate'];?>" name="BlogDate" class="form-control col-md-7 col-xs-12">
			</div></div>

			<div class="form-group">
				<label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Tags 			</label>
				<div class="col-md-10 col-sm-6 col-xs-12">
<?php /*?><?php
	$TagsRow=mysqli_query($con,"SELECT * FROM tbl_tags order by TagId desc");
	while($TagsResult=mysqli_fetch_array($TagsRow)){?>
			<input type="checkbox" name="tag" value="<?php echo $TagsResult['TagId'];?>" ><?php echo $TagsResult['TagName'];?>
<?php } ?><?php */?>

<?php
	$TagsRow=mysqli_query($con,"SELECT * FROM tbl_tags order by TagId desc");
	while($TagsResult=mysqli_fetch_array($TagsRow)){?>
			<input type="checkbox" name="Tags[]" value="<?php echo $TagsResult['TagId'];?>" ><?php echo $TagsResult['TagName'];?>
<?php } ?>

            </div></div>

			<div class="form-group">
				<label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Summary <span class="required">*</span>	</label>
				<div class="col-md-10 col-sm-6 col-xs-12">
				<textarea name="Summary" required class="form-control col-md-7 col-xs-12" rows="3"><?php echo $result['Summary'];?></textarea>
			</div></div>

			<div class="form-group">
                <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Detailed Content <span class="required">*</span>	</label>
                <div class="col-md-10 col-sm-6 col-xs-12">
                <textarea name="DetailedContent" required class="form-control col-md-7 col-xs-12" rows="6"><?php echo $result['Description'];?></textarea>
            </div></div> 

			<div class="form-group">
				<label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Main Image <span class="required">*</span>	</label>
				<div class="col-md-10 col-sm-6 col-xs-12">
				<font color="#FF0004">Max-Image Size: <b>400KB</b> &nbsp;  (Image Dimension --- width: <b>424px</b> , Height: <b>318px</b> )</font>
				<input type="file" name="BlogImage" id="BlogImage" class="form-control col-md-7 col-xs-12" accept="image/*"  >
			</div></div>

        <!--<div class="form-group">
						<label class="control-label col-md-2 col-sm-3 col-xs-12">Sub</label>
						<div class="col-md-10  col-sm-6 col-xs-12">
            <select class="form-control" name="Sub" id="Sub" >
                <option value="1"  >Images</option>
                <option value="2"  >Video</option>
            </select>
             <script type="text/javascript">
						var element = document.getElementById('Sub');
						element.value = <?php //echo $result['imageorVideoId'];?>;
					 </script>
        </div>        </div>-->

        <div class="form-group">
            <label id="fileLabel" class="control-label col-md-2 col-sm-3 col-xs-12">Image</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
            <input type="file"  class="form-control col-md-7 col-xs-12" name="SubImages[]" id="SubImages" accept="image/*" multiple >
        </div></div>

  <!--<div class="form-group">
          <label id="videoLinkLabel" class="control-label col-md-3 col-sm-3 col-xs-12">Video Link</label>
                      <div class="col-md-6  col-sm-6 col-xs-12">
         <input type="text" class="form-control col-md-7 col-xs-12" name="videoLink" value="<?php //echo $result['videoLink']; ?>" id="videoLink" >
        </div></div>

       <script>
		$(document).ready(function () {
			if($('#Sub').val()=='1')
			{		$('#imageLabel').show();		$('#SubImages').show();			$('#videoLinkLabel').hide();		$('#videoLink').hide();			}
			else
			{		$('#videoLink').show();		$('#SubImages').hide();			$('#videoLinkLabel').show();		$('#imageLabel').hide();			}

			$('#Sub').change(function (){
				if($('#Sub').val()=='1')
				{	$('#imageLabel').show();		$('#SubImages').show();			$('#videoLinkLabel').hide();		$('#videoLink').hide();		}

				else
				{	$('#videoLink').show();		$('#SubImages').hide();			$('#videoLinkLabel').show();		$('#imageLabel').hide();			}
		});	});
					  </script>-->

<div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">SEO Title <span class="required">*</span>	</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
				<input type="text" name="SEOTitle" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['SEOTitle'];?>">
                      </div>
                    </div>
<div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">SEO Keyword <span class="required">*</span>	</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
				<input type="text" name="Keyword" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['SEOKeyword'];?>">
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">SEO Description <span class="required">*</span>	</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
				<textarea name="SEO" required class="form-control col-md-7 col-xs-12" rows="3"><?php echo $result['SEODesc'];?></textarea>
                      </div>
                    </div>

                     <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Approve <span class="required">*</span></label>
                      <div class="col-md-10 col-sm-6 col-xs-12">
                          <input type="checkbox" name="Display" checked class="form-control  col-md-7 col-xs-12" >
                     </div></div>

			<div class="form-group">
				<div class="col-md-10 col-sm-6 col-xs-12 col-md-offset-3">
				<input type="hidden" name="hidden" value="<?php echo $result['BlogId'];?>">
				<button type="submit" name="BlogUpdate" class="btn btn-success">Submit</button>
			</div></div>
                    <script src="ckeditor/ckeditor.js" type="text/javascript"></script>
                      <script>
                CKEDITOR.replace( 'Summary' );
                CKEDITOR.replace( 'DetailedContent' );
            </script>
		</form>

	</div></div></div></div>
	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>


<?php include('General/Footer.php'); ?>