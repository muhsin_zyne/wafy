<?php include('General/Header.php'); 


if(isset($_REQUEST['ClientId']))
{
   $id=$_REQUEST['ClientId'];
   //echo $id;exit;
   $result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_clients WHERE ClientId='$id'"));
}
else{	echo "<script>window.location='clients.php'</script>";	}

?>

	<script>
        function validateForm() {
			var imgpath=document.getElementById('LogoBlk');
//			var imgpath=document.getElementById('LogoYt');
			var Images = document.getElementById('LogoYt');  

            if (!imgpath.value==""){
				var imgsize=imgpath.files[0].size;
				if(imgsize>400000)
				{				alert(imgpath.files[0].name+" - Size is larger than 400KB. Please Reduce it.");				return false;				}
			  }

            if (!Images.value==""){
				var imgsize=Images.files[0].size;
				if(imgsize>400000)
				{				alert(Images.files[0].name+" - Size is larger than 400KB. Please Reduce it.");				return false;				}
			  }
		}
     </script>		

		<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Clients</h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edit Client</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form data-parsley-validate class="form-horizontal form-label-left" action="action.php" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormBlog">

                      <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Client Name<span class="required">*</span>	</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                        <input type="text" name="Client" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['ClientName'];?>">
                      </div></div>

                      <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Client's Logo<span class="required">*</span>			</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                      <font color="#FF0004"><b>Black Color</b> &nbsp; (Image Dimension --- width: <b>150px</b> , Height: <b>75px</b> )</font>
                          <input type="file" id="LogoBlk" name="LogoBlk"  class="form-control col-md-7 col-xs-12" accept="image/*" >
	                  </div></div>

                      <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Approve <span class="required">*</span></label>
                      <div class="col-md-10 col-sm-6 col-xs-12">
                          <input type="checkbox" name="Display" class="form-control col-md-7 col-xs-12" value="1" checked="checked" >
                      </div></div>

                      <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Client's Logo<span class="required">*</span>			</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                      <font color="#FF0004"><b>White Color</b> &nbsp; (Image Dimension --- width: <b>150px</b> , Height: <b>75px</b> )</font>
                          <input type="file" id="YtLogo" name="YtLogo"  class="form-control col-md-7 col-xs-12" accept="image/*" >
	                  </div></div>

                      <div class="form-group">
                      <div class="col-md-10  col-sm-6 col-xs-12 col-md-offset-3">
				<input type="hidden" name="hidden" value="<?php echo $result['ClientId'];?>">
				<button type="submit" name="ClientUpdate" class="btn btn-success">Submit</button>
                      </div></div>

                  </form>
		</div></div></div></div>

</div></div>
          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
