<?php 
include('General/DBConnection.php'); 
@session_start();



define ("MAX_SIZE","1000");

function ImageUpload($image)
{
	$errors=0;
	$image_name="";
	$image1 =$_FILES[$image]["name"];
	$uploadedfile = $_FILES[$image]['tmp_name'];
	if ($image1) 
	{	
		$filename = stripslashes($_FILES[$image]['name']);
		$extension = pathinfo($filename,PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
		{			
			echo '<script>alert("Unknown Image extension");</script>';
			$errors=1;
		}
		else
		{
			$size=filesize($_FILES[$image]['tmp_name']);
			if ($size > MAX_SIZE*1024)
			{				
				echo '<script>alert("You have exceeded the size limit");</script>';
				$errors=1;
			}
			else
			{

			if($extension=="jpg" || $extension=="jpeg" )
			{		
				$uploadedfile = $_FILES[$image]['tmp_name'];
				$src = imagecreatefromjpeg($uploadedfile);	
			}
			else if($extension=="png")
			{			
				$uploadedfile = $_FILES[$image]['tmp_name'];	
				$src = imagecreatefrompng($uploadedfile);	
			}
			else
			{	
				$src = imagecreatefromgif($uploadedfile);	
			}

			list($width,$height)=getimagesize($uploadedfile);

			$mainwidth=750;	
				$mainheight=500;			/*	$newheight=($height/$width)*$newwidth;		*/		$tmp=imagecreatetruecolor($mainwidth,$mainheight);
			$displaywidth=390;	
				$displayheight=263;		/*	$newheight1=($height/$width)*$newwidth1;	*/		$tmp1=imagecreatetruecolor($displaywidth,$displayheight);


			imagecopyresampled($tmp,$src,0,0,0,0,$mainwidth,$mainheight,$width,$height);
			imagecopyresampled($tmp1,$src,0,0,0,0,$displaywidth,$displayheight,$width,$height);
				
			$ra=rand(10,10000000000);
			$rb=rand(10,10000000000);
			$r="Gallay-".$ra.$rb."_";
			$image_name=$r.$_FILES[$image]['name'];
			
			$main = "../Resource/Gallery/".$image_name;
			$display = "../Resource/Gallery/"."thump_".$image_name;
			imagejpeg($tmp,$main,100);	
				imagejpeg($tmp1,$display,100);				

			imagedestroy($src);	
				imagedestroy($tmp);	
				imagedestroy($tmp1);		
			}
	}	
	}
	return $image_name;
}


function ImageUploadArticle($image)
{
	$errors=0;
	$image_name="";
	$image1 =$_FILES[$image]["name"];
	$uploadedfile = $_FILES[$image]['tmp_name'];
	if ($image1) 
	{	
		$filename = stripslashes($_FILES[$image]['name']);
		$extension = pathinfo($filename,PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
		{			
			echo '<script>alert("Unknown Image extension");</script>';
			$errors=1;
		}
		else
		{
			$size=filesize($_FILES[$image]['tmp_name']);
			if ($size > MAX_SIZE*1024)
			{				
				echo '<script>alert("You have exceeded the size limit");</script>';
				$errors=1;
			}
			else
			{

			if($extension=="jpg" || $extension=="jpeg" )
			{		
				$uploadedfile = $_FILES[$image]['tmp_name'];
				$src = imagecreatefromjpeg($uploadedfile);	
			}
			else if($extension=="png")
			{			
				$uploadedfile = $_FILES[$image]['tmp_name'];	
				$src = imagecreatefrompng($uploadedfile);	
			}
			else
			{	
				$src = imagecreatefromgif($uploadedfile);	
			}

			list($width,$height)=getimagesize($uploadedfile);

			$mainwidth=748;	
				$mainheight=335;			/*	$newheight=($height/$width)*$newwidth;		*/		$tmp=imagecreatetruecolor($mainwidth,$mainheight);
			$displaywidth=360;	
				$displayheight=263;		/*	$newheight1=($height/$width)*$newwidth1;	*/		$tmp1=imagecreatetruecolor($displaywidth,$displayheight);
	

			imagecopyresampled($tmp,$src,0,0,0,0,$mainwidth,$mainheight,$width,$height);
			imagecopyresampled($tmp1,$src,0,0,0,0,$displaywidth,$displayheight,$width,$height);
				
			
				
			$ra=rand(10,10000000000);
			$rb=rand(10,10000000000);
			$r="Article-".$ra.$rb."_";
			$image_name=$r.$_FILES[$image]['name'];
			
			$main = "../Resource/Article/".$image_name;
			$display = "../Resource/Article/"."thump_".$image_name;
				
				
			imagejpeg($tmp,$main,100);	
				imagejpeg($tmp1,$display,100);	
				
				

			imagedestroy($src);	
				imagedestroy($tmp);	
				imagedestroy($tmp1);	
				
			}
	}	
	}
	return $image_name;
}




//////////////////////////////////////// User Delete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['UserIdDelete']))
{
    $UserId=$_REQUEST['UserIdDelete'];
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_login WHERE UserId='".$UserId."'"));
	$image=$ImageQuery[0];
	if(!empty($image))
	{
		unlink("../Resource/User/".$image);
	}
	 $result1 =mysqli_query($con,"delete from  tbl_login WHERE UserId='$UserId'");
    if($result1)
    {
        echo "<script>window.location='User.php'</script>";
    }
}

//////////////////////////////////////// User Edit //////////////////////////////////////////////////////////////////////////

else if(isset($_POST['UserUpdate']))
{
	
    $UserId=$_POST['UserUpdate'];
	$Name=trim(mysqli_real_escape_string($con,$_POST['Name']));
	$Password=trim(mysqli_real_escape_string($con,$_POST['Password']));
	
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_login WHERE UserId='".$UserId."'"));
	$image=$ImageQuery[0];
	
	if(isset($_FILES['MainImage']['name']) && !empty($_FILES['MainImage']['name']))
	{
		if(!empty($image))
			unlink("../Resource/User/".$image);
		$filename=$_FILES['MainImage']['name'];
		$temp=$_FILES['MainImage']['tmp_name'];
		$ra=rand(10,10000000000);
		$rb=rand(10,10000000000);
		$r="User_".$ra.$rb."_";
		$image_name=$r.$filename;
		move_uploaded_file($temp,"../Resource/User/".$image_name);
		$image=$image_name;
	}
	$result=mysqli_query($con,"update tbl_login set Name='$Name',Password='$Password',image='$image' WHERE UserId='$UserId'");
	if($result)
	{
		if($_SESSION['DUIUserId']==$UserId)
		{
			$_SESSION['DUIName']=$Name;
			$_SESSION['DUIPassword']=$Password;
			$_SESSION['DUIImage']=$image;
		}
		echo '<script>alert(" Successfully Updated");</script>';	
	}
	else
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='User.php'</script>";
}










//////////////////////////////////////// NewsCategoryUpdate //////////////////////////////////////////////////////////////////////////
if(isset($_POST['NewsCategoryUpdate']))
{
    $NewsCategoryId=$_POST['NewsCategoryUpdate'];
	$Category=trim(mysqli_real_escape_string($con,$_POST['NewsCategory']));
	$result=mysqli_query($con,"update tbl_news_category set Category='$Category' where CategoryId='$NewsCategoryId' ");
	if($result)	
	{	
		echo '<script>alert(" Successfully Updated");</script>';
	}
	else	
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='NewsCategory.php'</script>";
}




//////////////////////////////////////// NewsCategoryDelete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['NewsCategoryDelete']))
{
    $NewsCategoryId=$_REQUEST['NewsCategoryDelete'];
	$result =mysqli_query($con,"DELETE FROM tbl_news_category WHERE CategoryId='$NewsCategoryId'");
   
    echo "<script>window.location='NewsCategory.php'</script>";
}











//////////////////////////////////////// News Delete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['NewsIdDelete']))
{
    $NewsIdDelete=$_REQUEST['NewsIdDelete'];
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_news WHERE NewsId='".$NewsIdDelete."'"));
	$image=$ImageQuery[0];
	if(!empty($image))
	{
		unlink("../Resource/News/".$image);
	}
	 $result1 =mysqli_query($con,"delete from  tbl_news WHERE NewsId='$NewsIdDelete'");
    if($result1)
    {
        echo "<script>window.location='News.php'</script>";
    }
}



//////////////////////////////////////// News Edit //////////////////////////////////////////////////////////////////////////

else if(isset($_POST['NewsUpdate']))
{
	$NewsUpdate=$_POST['NewsUpdate'];
   	$Heading=trim(mysqli_real_escape_string($con,$_POST['Heading']));
	$Content=trim(mysqli_real_escape_string($con,$_POST['Content']));
	$Category=$_POST['Category'];
	$Date=$_POST['Date'];
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_news WHERE NewsId='".$NewsUpdate."'"));
	$image=$ImageQuery[0];
	
	if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
	{
		if(!empty($image))
			unlink("../Resource/News/".$image);
		
		$filename=$_FILES['mainImage']['name'];
		$temp=$_FILES['mainImage']['tmp_name'];
		$ra=rand(10,10000000000);
		$rb=rand(10,10000000000);
		$r="News_".$ra.$rb."_";
		$image_name=$r.$filename;
		move_uploaded_file($temp,"../Resource/News/".$image_name);
		$image=$image_name;
	}
	$result=mysqli_query($con,"update tbl_news set Heading='$Heading',Content='$Content',CategoryId='$Category',Date='$Date',Image='$image' WHERE NewsId='$NewsUpdate'");
	if($result)
	{
		echo '<script>alert(" Successfully Updated");</script>';	
	}
	else
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='News.php'</script>";
}

	
//////////////////////////////////////// NewsIdApprove //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['NewsIdApprove']))
{
    $NewsIdApprove=$_REQUEST['NewsIdApprove'];
	$Appr=mysqli_fetch_array(mysqli_query($con,"select Approve from tbl_news WHERE NewsId='".$NewsIdApprove."'"));
	if($Appr[0]==0)
		$Approve=1;
	else
		$Approve=0;
	
	$result=mysqli_query($con,"update tbl_news set Approve='$Approve' WHERE NewsId='$NewsIdApprove'");

    if($result)
    {
        echo "<script>window.location='News.php'</script>";
    }
}











//////////////////////////////////////// GalleryCategoryUpdate //////////////////////////////////////////////////////////////////////////
if(isset($_POST['GalleryCategoryUpdate']))
{
    $GalleryCategoryId=$_POST['GalleryCategoryUpdate'];
	$Category=trim(mysqli_real_escape_string($con,$_POST['GalleryCategory']));
	$result=mysqli_query($con,"update tbl_gallery_category set Category='$Category' where CategoryId='$GalleryCategoryId' ");
	if($result)	
	{	
		echo '<script>alert(" Successfully Updated");</script>';
	}
	else	
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='GalleryCategory.php'</script>";
}




//////////////////////////////////////// GalleryCategoryDelete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['GalleryCategoryDelete']))
{
    $GalleryCategoryId=$_REQUEST['GalleryCategoryDelete'];
	$result =mysqli_query($con,"DELETE FROM tbl_gallery_category WHERE CategoryId='$GalleryCategoryId'");
   
    echo "<script>window.location='GalleryCategory.php'</script>";
}







//////////////////////////////////////// Gallery Delete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['GalleryIdDelete']))
{
    $GalleryIdDelete=$_REQUEST['GalleryIdDelete'];
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_gallery WHERE GalleryId='".$GalleryIdDelete."'"));
	$image=$ImageQuery[0];
	if(!empty($image))
	{
		unlink("../Resource/Gallery/".$image);
		unlink("../Resource/Gallery/"."thump_".$image);
	}
	
	$ProductSubImagesArray=mysqli_query($con,"SELECT * FROM tbl_gallary_images where GalleryId='$GalleryIdDelete' ");

						   while($ProductSubImages = mysqli_fetch_array($ProductSubImagesArray))
						   {
							   unlink('../Resource/Gallery/'.$ProductSubImages['Image']);
						   }
						
						   $ProductSub=mysqli_query($con,"delete FROM tbl_gallary_images where GalleryId='$GalleryIdDelete' ");
	
	 $result1 =mysqli_query($con,"delete from  tbl_gallery WHERE GalleryId='$GalleryIdDelete'");
    if($result1)
    {
        echo "<script>window.location='Gallery.php'</script>";
    }
}



//////////////////////////////////////// Gallery Edit //////////////////////////////////////////////////////////////////////////





else if(isset($_POST['GalleryUpdate']))
{
	$GalleryUpdate=$_POST['GalleryUpdate'];
   	$Heading=trim(mysqli_real_escape_string($con,$_POST['Heading']));
	$Content=trim(mysqli_real_escape_string($con,$_POST['Content']));
	$Category=$_POST['Category'];
	$Date=$_POST['Date'];
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_gallery WHERE GalleryId='".$GalleryUpdate."'"));
	$image=$ImageQuery[0];
	
	
	
	if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
	{
		if(!empty($image))
		{
			unlink("../Resource/Gallery/".$image);
			unlink("../Resource/Gallery/"."thump_".$image);
		}
		
		$image_name1= ImageUpload('mainImage');
		
		if(!empty($image_name1))
			$image=$image_name1;
	
	}
	
	
	if(isset($_FILES['subImages']) && !empty($_FILES['subImages']['name'][0]))
					{
						
						$ProductSubImagesArray=mysqli_query($con,"SELECT * FROM tbl_gallary_images where GalleryId='$GalleryUpdate' ");

						   while($ProductSubImages = mysqli_fetch_array($ProductSubImagesArray))
						   {
							   unlink('../Resource/Gallery/'.$ProductSubImages['Image']);
						   }
						
						   $ProductSub=mysqli_query($con,"delete FROM tbl_gallary_images where GalleryId='$GalleryUpdate' ");

							foreach($_FILES['subImages']['tmp_name'] as $key => $tmp_name )
					{
						
						$files_name = $key.$_FILES['subImages']['name'][$key];
						$files_tmp =$_FILES['subImages']['tmp_name'][$key];
						$ra=rand(10,10000000000);
						$rb=rand(10,10000000000);
						$r="Gallery_Sub_".$ra.$rb."_";
						$images_name=$r.$files_name;
						move_uploaded_file($files_tmp,"../Resource/Gallery/".$images_name);
						mysqli_query($con,"INSERT INTO tbl_gallary_images(id,GalleryId,Image)values((SELECT IFNULL((SELECT MAX(id)+1 FROM  tbl_gallary_images temp),1)),'$GalleryUpdate','$images_name')");
					}
						
					}
	$result=mysqli_query($con,"update tbl_gallery set Heading='$Heading',Content='$Content',CategoryId='$Category',Date='$Date',Image='$image' WHERE GalleryId='$GalleryUpdate'");
	if($result)
	{
		echo '<script>alert(" Successfully Updated");</script>';	
	}
	else
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='Gallery.php'</script>";
}

	
//////////////////////////////////////// GalleryIdApprove //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['GalleryIdApprove']))
{
    $GalleryIdApprove=$_REQUEST['GalleryIdApprove'];
	$Appr=mysqli_fetch_array(mysqli_query($con,"select Approve from tbl_gallery WHERE GalleryId='".$GalleryIdApprove."'"));
	if($Appr[0]==0)
		$Approve=1;
	else
		$Approve=0;
	
	$result=mysqli_query($con,"update tbl_gallery set Approve='$Approve' WHERE GalleryId='$GalleryIdApprove'");

    if($result)
    {
        echo "<script>window.location='Gallery.php'</script>";
    }
}

























//////////////////////////////////////// ArticleCategoryUpdate //////////////////////////////////////////////////////////////////////////
if(isset($_POST['ArticleCategoryUpdate']))
{
    $ArticleCategoryId=$_POST['ArticleCategoryUpdate'];
	$Category=trim(mysqli_real_escape_string($con,$_POST['ArticleCategory']));
	$result=mysqli_query($con,"update tbl_article_category set Category='$Category' where CategoryId='$ArticleCategoryId' ");
	if($result)	
	{	
		echo '<script>alert(" Successfully Updated");</script>';
	}
	else	
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='ArticleCategory.php'</script>";
}




//////////////////////////////////////// ArticleCategoryDelete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['ArticleCategoryDelete']))
{
    $ArticleCategoryId=$_REQUEST['ArticleCategoryDelete'];
	$result =mysqli_query($con,"DELETE FROM tbl_article_category WHERE CategoryId='$ArticleCategoryId'");
   
    echo "<script>window.location='ArticleCategory.php'</script>";
}







//////////////////////////////////////// Article Delete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['ArticleIdDelete']))
{
    $ArticleIdDelete=$_REQUEST['ArticleIdDelete'];
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_article WHERE ArticleId='".$ArticleIdDelete."'"));
	$image=$ImageQuery[0];
	if(!empty($image))
	{
		unlink("../Resource/Article/".$image);
		unlink("../Resource/Article/"."thump_".$image);
	
	}
	 $result1 =mysqli_query($con,"delete from  tbl_article WHERE ArticleId='$ArticleIdDelete'");
    if($result1)
    {
        echo "<script>window.location='Article.php'</script>";
    }
}



//////////////////////////////////////// Article Edit //////////////////////////////////////////////////////////////////////////





else if(isset($_POST['ArticleUpdate']))
{
	$ArticleUpdate=$_POST['ArticleUpdate'];
   	$Heading=trim(mysqli_real_escape_string($con,$_POST['Heading']));
	$Content=trim(mysqli_real_escape_string($con,$_POST['Content']));
	$Author=trim(mysqli_real_escape_string($con,$_POST['Author']));
	$Category=$_POST['Category'];
	$Date=$_POST['Date'];
	
	$imageOrVideo=$_POST['imageOrVideo'];
	if($imageOrVideo==1)
		{
			$videoLink='';
		}
		else
		{
			$videoLink=$_POST['videoLink'];
		}
	
	
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_article WHERE ArticleId='".$ArticleUpdate."'"));
	$image=$ImageQuery[0];
	
	
	
	if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
	{
		if(!empty($image))
		{
			unlink("../Resource/Article/".$image);
			unlink("../Resource/Article/"."thump_".$image);
			
		}
		
		$image_name1= ImageUploadArticle('mainImage');
		
		if(!empty($image_name1))
			$image=$image_name1;
	
	}
	$result=mysqli_query($con,"update tbl_article set Heading='$Heading',Content='$Content',CategoryId='$Category',Date='$Date',Image='$image',Author='$Author',ImageOrVideo='$imageOrVideo',Video='$videoLink' WHERE ArticleId='$ArticleUpdate'");
	
	if($result)
	{
		echo '<script>alert(" Successfully Updated");</script>';	
	}
	else
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='Article.php'</script>";
}

	
//////////////////////////////////////// ArticleIdApprove //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['ArticleIdApprove']))
{
    $ArticleIdApprove=$_REQUEST['ArticleIdApprove'];
	$Appr=mysqli_fetch_array(mysqli_query($con,"select Approve from tbl_article WHERE ArticleId='".$ArticleIdApprove."'"));
	if($Appr[0]==0)
		$Approve=1;
	else
		$Approve=0;
	
	$result=mysqli_query($con,"update tbl_article set Approve='$Approve' WHERE ArticleId='$ArticleIdApprove'");

    if($result)
    {
        echo "<script>window.location='Article.php'</script>";
    }
}







//////////////////////////////////////// Testimonial Delete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['TestimonialIdDelete']))
{
    $UserId=$_REQUEST['TestimonialIdDelete'];
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select image from tbl_testimonial WHERE id='".$UserId."'"));
	$image=$ImageQuery[0];
	if(!empty($image))
	{
		unlink("../Resource/Testimonial/".$image);
	}
	 $result1 =mysqli_query($con,"delete from  tbl_testimonial WHERE id='$UserId'");
    if($result1)
    {
        echo "<script>window.location='Testimonial.php'</script>";
    }
}

//////////////////////////////////////// User Edit //////////////////////////////////////////////////////////////////////////

else if(isset($_POST['TestimonialUpdate']))
{
	
    $UserId=$_POST['TestimonialUpdate'];
	
	$name=trim(mysqli_real_escape_string($con,$_POST['name']));
	$designation=trim(mysqli_real_escape_string($con,$_POST['designation']));
	$testimonial=trim(mysqli_real_escape_string($con,$_POST['testimonial']));
	
	
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select image from tbl_testimonial WHERE id='".$UserId."'"));
	$image=$ImageQuery[0];
	
	if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
	{
		if(!empty($image))
			unlink("../Resource/Testimonial/".$image);
		$filename=$_FILES['mainImage']['name'];
		$temp=$_FILES['mainImage']['tmp_name'];
		$ra=rand(10,10000000000);
		$rb=rand(10,10000000000);
		$r="Testimonial_".$ra.$rb."_";
		$image_name=$r.$filename;
		move_uploaded_file($temp,"../Resource/Testimonial/".$image_name);
		$image=$image_name;
	}
	$result=mysqli_query($con,"update tbl_testimonial set name='$name',designation='$designation',image='$image',testimonial='$testimonial' WHERE id='$UserId'");
	
	if($result)
	{
		echo '<script>alert(" Successfully Updated");</script>';	
	}
	else
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='Testimonial.php'</script>";
}








//////////////////////////////////////// Staff Delete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['StaffIdDelete']))
{
    $UserId=$_REQUEST['StaffIdDelete'];
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_staff WHERE StaffId='".$UserId."'"));
	$image=$ImageQuery[0];
	if(!empty($image))
	{
		unlink("../Resource/Staff/".$image);
	}
	 $result1 =mysqli_query($con,"delete from  tbl_staff WHERE StaffId='$UserId'");
    if($result1)
    {
        echo "<script>window.location='Staff.php'</script>";
    }
}


//////////////////////////////////////// Staff Edit //////////////////////////////////////////////////////////////////////////

else if(isset($_POST['StaffUpdate']))
{
	
    $UserId=$_POST['StaffUpdate'];
	
	$name=trim(mysqli_real_escape_string($con,$_POST['name']));
	$designation=trim(mysqli_real_escape_string($con,$_POST['designation']));
	$FbLink=trim(mysqli_real_escape_string($con,$_POST['FbLink']));
	$Order=trim(mysqli_real_escape_string($con,$_POST['Order']));
	
	
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_staff WHERE StaffId='".$UserId."'"));
	$image=$ImageQuery[0];
	
	if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
	{
		if(!empty($image))
			unlink("../Resource/Staff/".$image);
		$filename=$_FILES['mainImage']['name'];
		$temp=$_FILES['mainImage']['tmp_name'];
		$ra=rand(10,10000000000);
		$rb=rand(10,10000000000);
		$r="Staff_".$ra.$rb."_";
		$image_name=$r.$filename;
		move_uploaded_file($temp,"../Resource/Staff/".$image_name);
		$image=$image_name;
	}
	$result=mysqli_query($con,"update tbl_staff set Name='$name',Designation='$designation',Image='$image',FbLink='$FbLink',DisplayOrder='$Order' WHERE StaffId='$UserId'");
	
	if($result)
	{
		echo '<script>alert(" Successfully Updated");</script>';	
	}
	else
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='Staff.php'</script>";
}




//////////////////////////////////////// committee Delete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['CommitteeIdDelete']))
{
    $UserId=$_REQUEST['CommitteeIdDelete'];
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_committee WHERE CommitteeId='".$UserId."'"));
	$image=$ImageQuery[0];
	if(!empty($image))
	{
		unlink("../Resource/committee/".$image);
	}
	 $result1 =mysqli_query($con,"delete from  tbl_committee WHERE CommitteeId='$UserId'");
    if($result1)
    {
        echo "<script>window.location='committee.php'</script>";
    }
}

//////////////////////////////////////// committee Edit //////////////////////////////////////////////////////////////////////////

else if(isset($_POST['committeeUpdate']))
{
	
    $UserId=$_POST['committeeUpdate'];
	
	$name=trim(mysqli_real_escape_string($con,$_POST['name']));
	$designation=trim(mysqli_real_escape_string($con,$_POST['designation']));
	$region=trim(mysqli_real_escape_string($con,$_POST['Region']));
	$FbLink=trim(mysqli_real_escape_string($con,$_POST['FbLink']));
	$Order=trim(mysqli_real_escape_string($con,$_POST['Order']));
	
	
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_committee WHERE CommitteeId='".$UserId."'"));
	$image=$ImageQuery[0];
	
	if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
	{
		if(!empty($image))
			unlink("../Resource/committee/".$image);
		$filename=$_FILES['mainImage']['name'];
		$temp=$_FILES['mainImage']['tmp_name'];
		$ra=rand(10,10000000000);
		$rb=rand(10,10000000000);
		$r="committee_".$ra.$rb."_";
		$image_name=$r.$filename;
		move_uploaded_file($temp,"../Resource/committee/".$image_name);
		$image=$image_name;
	}
	$result=mysqli_query($con,"update tbl_committee set Name='$name',Designation='$designation',Region='$region',Image='$image',FbLink='$FbLink',DisplayOrder='$Order' WHERE CommitteeId='$UserId'");
	
	if($result)
	{
		echo '<script>alert(" Successfully Updated");</script>';	
	}
	else
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='committee.php'</script>";
}






//////////////////////////////////////// Upload Delete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['UploadFileDelete']))
{
    $UserId=$_REQUEST['UploadFileDelete'];
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Name from tbl_upload WHERE id='".$UserId."'"));
	$image=$ImageQuery[0];
	if(!empty($image))
	{
		unlink("../Resource/General/".$image);
	}
	 $result1 =mysqli_query($con,"delete from  tbl_upload WHERE id='$UserId'");
    if($result1)
    {
        echo "<script>window.location='Upload.php'</script>";
    }
}






/////////////////////////////////////// StudentsCategoryUpdate //////////////////////////////////////////////////////////////////////////
if(isset($_POST['StudentsCategoryUpdate']))
{
    $StudentsCategoryId=$_POST['StudentsCategoryUpdate'];
	$Category=trim(mysqli_real_escape_string($con,$_POST['StudentsCategory']));
	$result=mysqli_query($con,"update tbl_students_category set Category='$Category' where CategoryId='$StudentsCategoryId' ");
	if($result)	
	{	
		echo '<script>alert(" Successfully Updated");</script>';
	}
	else	
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='StudentsCategory.php'</script>";
}




//////////////////////////////////////// StudentsCategoryDelete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['StudentsCategoryDelete']))
{
    $StudentsCategoryId=$_REQUEST['StudentsCategoryDelete'];
	$result =mysqli_query($con,"DELETE FROM tbl_students_category WHERE CategoryId='$StudentsCategoryId'");
   
    echo "<script>window.location='StudentsCategory.php'</script>";
}









//////////////////////////////////////// Students Delete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['StudentsIdDelete']))
{
    $UserId=$_REQUEST['StudentsIdDelete'];
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_students WHERE StudentsId='".$UserId."'"));
	$image=$ImageQuery[0];
	if(!empty($image))
	{
		unlink("../Resource/Students/".$image);
	}
	 $result1 =mysqli_query($con,"delete from  tbl_students WHERE StudentsId='$UserId'");
    if($result1)
    {
        echo "<script>window.location='Students.php'</script>";
    }
}

//////////////////////////////////////// User Edit //////////////////////////////////////////////////////////////////////////

else if(isset($_POST['StudentsUpdate']))
{
	
    $UserId=$_POST['StudentsUpdate'];
	
	$Name=trim(mysqli_real_escape_string($con,$_POST['Name']));
	$BatchId=trim(mysqli_real_escape_string($con,$_POST['BatchId']));
	$Occupation=trim(mysqli_real_escape_string($con,$_POST['Occupation']));
	$DisplayOrder=trim(mysqli_real_escape_string($con,$_POST['DisplayOrder']));
	$Address=trim(mysqli_real_escape_string($con,$_POST['Address']));
	$Email=trim(mysqli_real_escape_string($con,$_POST['Email']));
	$Phone=trim(mysqli_real_escape_string($con,$_POST['Phone']));
	$Mobile=trim(mysqli_real_escape_string($con,$_POST['Mobile']));
	$RegCIC=trim(mysqli_real_escape_string($con,$_POST['RegCIC']));
	$RegCLG=trim(mysqli_real_escape_string($con,$_POST['RegCLG']));
	$FbLink=trim(mysqli_real_escape_string($con,$_POST['FbLink']));
	$TwitterLink=trim(mysqli_real_escape_string($con,$_POST['TwitterLink']));
	$GoogleLink=trim(mysqli_real_escape_string($con,$_POST['GoogleLink']));
	$LinkedInLink=trim(mysqli_real_escape_string($con,$_POST['LinkedInLink']));
	
	
	$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select Image from tbl_students WHERE StudentsId='".$UserId."'"));
	$image=$ImageQuery[0];
	
	if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
	{
		if(!empty($image))
			unlink("../Resource/Students/".$image);
		$filename=$_FILES['mainImage']['name'];
		$temp=$_FILES['mainImage']['tmp_name'];
		$ra=rand(10,10000000000);
		$rb=rand(10,10000000000);
		$r="Students_".$ra.$rb."_";
		$image_name=$r.$filename;
		move_uploaded_file($temp,"../Resource/Students/".$image_name);
		$image=$image_name;
	}
	$result=mysqli_query($con,"update tbl_students set Name='$Name',Image='$image',BatchId='$BatchId',Occupation='$Occupation',DisplayOrder='$DisplayOrder',Address='$Address',Email='$Email',Phone='$Phone',Mobile='$Mobile',RegCIC='$RegCIC',RegCLG='$RegCLG',FbLink='$FbLink',TwitterLink='$TwitterLink',GoogleLink='$GoogleLink',LinkedInLink='$LinkedInLink' WHERE StudentsId='$UserId'");
	
	if($result)
	{
		echo '<script>alert(" Successfully Updated");</script>';	
	}
	else
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='Students.php'</script>";
}


//////////////////////////////////////// ContactsCategoryUpdate //////////////////////////////////////////////////////////////////////////
if(isset($_POST['ContactsCategoryUpdate']))
{
    $ContactsCategoryId=$_POST['ContactsCategoryUpdate'];
	$Category=trim(mysqli_real_escape_string($con,$_POST['ContactsCategory']));
	$result=mysqli_query($con,"update tbl_contacts_category set Category='$Category' where CategoryId='$ContactsCategoryId' ");
	if($result)	
	{	
		echo '<script>alert(" Successfully Updated");</script>';
	}
	else	
	{		
		echo '<script>alert("Data Not Updated");</script>';		
	}
	echo "<script>window.location='ContactsCategory.php'</script>";
}




//////////////////////////////////////// ContactsCategoryDelete //////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['ContactsCategoryDelete']))
{
    $ContactsCategoryId=$_REQUEST['ContactsCategoryDelete'];
	$result =mysqli_query($con,"DELETE FROM tbl_contacts_category WHERE CategoryId='$ContactsCategoryId'");
   
    echo "<script>window.location='ContactsCategory.php'</script>";
}


