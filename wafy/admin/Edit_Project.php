<?php include('General/Header.php'); 

	if(isset($_REQUEST['projectId']))
	{
		$id=$_REQUEST['projectId'];
		$result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_project WHERE projectId='$id'"));
	}
	else	{	echo "<script>window.location='Project.php'</script>";	}
?>

	<script>
	function validateForm() {
		var mainimgpath=document.getElementById('mainImage');
		if (!mainimgpath.value==""){		var mainimgsize=mainimgpath.files[0].size;
			if(mainimgsize>2000000)		{		alert(mainimgpath.files[0].name+" - Size is larger than 2MB. Please Reduce it.");		return false;		}
		}

		var displayimgpath=document.getElementById('displayImage');
		if (!displayimgpath.value==""){		var displayimgsize=displayimgpath.files[0].size;
			if(displayimgsize>270000)		{		alert(displayimgpath.files[0].name+" - Size is larger than 100KB. Please Reduce it.");		return false;		}
		}
/*		var descimgpath=document.getElementById('descImage');
		if (!descimgpath.value==""){		var descimgsize=descimgpath.files[0].size;
			if(descimgsize>1000000)		{		alert(descimgpath.files[0].name+" - Size is larger than 1MB. Please Reduce it.");		return false;		}
		}*/
	}
     </script>

	<div class="right_col" role="main">
		<div class="">

			<div class="page-title">
				<div class="title_left">	<h3>Project</h3>	</div>
			</div>
			<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Edit</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />

				<form data-parsley-validate class="form-horizontal form-label-left" action="action.php" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormProject">

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Project Name<span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="project" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['projectName'];?>">
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Display Image <span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<font color="#FF0004">Max-Image Size: <b>100KB</b> &nbsp; (Image Dimension --- width: <b>640px</b> , Height: <b>480px</b> )</font>
						<input type="file" name="displayImage" id="displayImage" class="form-control col-md-7 col-xs-12" accept="image/*" >
				</div></div>

				<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Main Image <span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<font color="#FF0004">Max-Image Size: <b>2MB</b> &nbsp; (Image Dimension --- width: <b>1400px</b> , Height: <b>600px</b> )</font>
						<input type="file" name="mainImage" id="mainImage" class="form-control col-md-7 col-xs-12" accept="image/*" >
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Slogan<span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="projectSlogan" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['projectSlogan'];?>">
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Service<span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="projectService" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['projectService'];?>">
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Project Description <span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<textarea name="projectDescription" required class="form-control col-md-7 col-xs-12" rows="6"><?php echo $result['projectDesc'];?></textarea>
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">My Slogan<span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="myslogan" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['mySlogan'];?>">
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">My Desc<span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<textarea name="mydesc" required class="form-control col-md-7 col-xs-12" rows="3"><?php echo $result['myDesc'];?></textarea>
				</div></div>

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<input type="hidden" name="hidden" value="<?php echo $result['projectId'];?>">
					<button type="submit" name="PojectUpdate" class="btn btn-success">Submit</button>
				</div></div>
				</form>

		</div></div></div></div>
</div></div>
          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>


<?php include('General/Footer.php'); ?>