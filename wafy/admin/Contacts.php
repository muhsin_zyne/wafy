<?php include('General/Header.php'); 



if(isset($_POST['ContactsSubmit']))
{
	$Name=trim(mysqli_real_escape_string($con,$_POST['Name']));
	$Content=trim(mysqli_real_escape_string($con,$_POST['Content']));
	$Category=$_POST['Category'];
	$Mobile=$_POST['Mobile'];
	
			if($_SESSION['DUIRoleId']==1) 
				$Approve=1;
			else
				$Approve=0;

			$result=mysqli_query($con,"INSERT INTO tbl_Contacts (ContactsId,Name,Mobile,CategoryId)
										VALUES((SELECT IFNULL((SELECT MAX(ContactsId)+1 FROM  tbl_Contacts temp),1)),'$Name','$Mobile','$Category')");
			if($result)
			{
				echo '<script>alert(" Successfully Inserted");</script>';		
			}
			else	
			{			
				echo '<script>alert("Data Not Inserted");</script>';			
			}
			echo "<script>window.location='Contacts.php'</script>";
		}
	


?>
	<script>
        function valiMobileForm() {
            
            var Category = document.forms["FormContacts"]["Category"].value;
			var imgpath=document.getElementById('mainImage');
            if (Category==0) 
			{
				alert("Please Select Category.");     
				return false;       
			}
			
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Contacts</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Add New</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-valiMobile class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" onsubmit="return valiMobileForm()" name="FormContacts">

				

				

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contact Name<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Name" required class="form-control col-md-7 col-xs-12">
				</div></div>
				

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Mobile<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Mobile" required class="form-control col-md-7 col-xs-12">
				</div></div>
				
				
                    
           
            
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contacts Category <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="Category" id="Category" >
                            <option value="0" selected="selected" >- Select Category -</option>
                                <?php 
                                $ContactsCategoryRow=mysqli_query($con,"SELECT * FROM tbl_Contacts_category order by CategoryId desc");
                                while($ContactsCategoryResult=mysqli_fetch_array($ContactsCategoryRow)){?>
                                    <option value="<?php echo $ContactsCategoryResult['CategoryId'];?>" ><?php echo $ContactsCategoryResult['Category']; ?></option>
                                <?php } ?>
                        </select>
				</div></div>
				
               

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" name="ContactsSubmit" class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>

 		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Contacts List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                         <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>                             
                              <th>Name</th>
                              <th>Mobile</th>
                              <th>Category</th>                            
                             <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
				
  if($_SESSION['DUIRoleId']==1)
  { 
			$ContactsRow=mysqli_query($con,"select * from tbl_Contacts order by ContactsId desc");
  }
  else
  {
	  $ContactsRow=mysqli_query($con,"select * from tbl_Contacts where EUserId='".$_SESSION['DUIUserId']."' order by ContactsId desc");

  }
	while($ContactsResult=mysqli_fetch_array($ContactsRow))
	{
		$i=$i+1;
	 ?>
                            <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $ContactsResult['Name']; ?></td>
                              <td><?php echo $ContactsResult['Mobile']; ?></td>
                              
                               <td><?php  
		$Category=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_Contacts_category where CategoryId ='".$ContactsResult['CategoryId']."'"));
		echo $Category[0]; ?></td>
                              
                             
                             <td> 
                             <form method="post" enctype="multipart/form-data" action="Edit_Contacts.php"  style="float: left"> 
                             <input type="hidden" name="ContactsIdUpMobile" value="<?php echo $ContactsResult['ContactsId']; ?>">
                             	<input type="submit" class="btn btn-info" value="Edit" >
							</form>
        					
         					
          					<form method="post" enctype="multipart/form-data" action="action.php" onSubmit="return confirm('Are you sure want to delete this record ?');"  style="float: left">
                            	<input type="hidden" name="ContactsIdDelete" value="<?php echo $ContactsResult['ContactsId']; ?>"> 
                             	<input type="submit" class="btn btn-danger" value="Delete">
                            	 
							</form>
							
							
							
            </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>

          </div></div></div></div>
	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').Mobilerangepicker({
                singleMobilePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
