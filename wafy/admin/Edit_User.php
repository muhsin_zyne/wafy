<?php include('General/Header.php'); 

if(isset($_REQUEST['UserIdUpdate']))
{
   $id=$_REQUEST['UserIdUpdate'];
   //echo $id;exit;
   $result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_login WHERE UserId='$id'"));
}
else
{
	echo "<script>window.location='User.php'</script>";
}
?>
 


	<script>
        function validateForm() {
           
             
            var Password = document.forms["FormUser"]["Password"].value;
			
            var ConfirmPassword = document.forms["FormUser"]["ConfirmPassword"].value;
          
            if (Password!=ConfirmPassword) {
                alert("Password doesn't match");
                return false;
            }
			
        }
        </script>


<div class="right_col" role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>User Details</h3>
            </div>
            
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edit User</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form data-parsley-validate class="form-horizontal form-label-left" action="action.php" method="post" enctype="multipart/form-data"
                  onsubmit="return validateForm()" name="FormUser">
					<input type="hidden" name="UserId" id="UserId" value="<?php echo $result['UserId']; ?>">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Name  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="Name" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['Name']; ?>">
                      </div>
                    </div>


                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Username  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="UserName" required class="form-control col-md-7 col-xs-12" id="UserName" readonly  value="<?php echo $result['UserName']; ?>">
                         <span id="name_status" ></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" name="Password" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Password']; ?>" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Confirm Password  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" name="ConfirmPassword" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Password']; ?>" autocomplete="off">
                      </div>
                    </div>
                                       
                   <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                     
                          <input type="file" id="MainImage" name="MainImage"  class="form-control col-md-7 col-xs-12" accept="image/*"  >
                          <a href="<?php if(empty($result['Image'])) echo "../Resource/User/userimg.png"; else echo "../Resource/User/".$result['Image'];?>" download> <img src="<?php if(empty($result['Image'])) echo "../Resource/User/userimg.png"; else echo "../Resource/User/".$result['Image'];?>" width="80" > </a>
                    </div>
					 
                    </div>
                      
                    

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <input type="hidden" name="UserUpdate" value="<?php echo $result['UserId'];?>">
                      
                        <button type="submit" name="UserEdit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </form>
          </div></div></div></div>




</div></div>
          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>
 
      
<?php include('General/Footer.php'); ?>
