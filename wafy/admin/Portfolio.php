<?php include('General/Header.php'); 

define ("MAX_SIZE","2000");		$errors=0;

if(isset($_POST['PortfolioSubmit']))
{	$category=$_POST['Category'];
	$portfolio=$_POST['Portfolio'];
	$Comment=$_POST['Comment'];
	$Project=$_POST['Project'];
	$Display=$_POST['Display'];

if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))			{

	$image =$_FILES["mainImage"]["name"];
	$uploadedfile = $_FILES['mainImage']['tmp_name'];

	if ($image) 
	{	$filename = stripslashes($_FILES['mainImage']['name']);
		$extension = pathinfo($filename,PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
		{			echo ' Unknown Image extension ';			$errors=1;		}

		else
		{	$size=filesize($_FILES['mainImage']['tmp_name']);

			if ($size > MAX_SIZE*1024)
			{				echo "You have exceeded the size limit";				$errors=1;			}
			if($extension=="jpg" || $extension=="jpeg" )
			{				$uploadedfile = $_FILES['mainImage']['tmp_name'];			$src = imagecreatefromjpeg($uploadedfile);			}
			else if($extension=="png")
			{				$uploadedfile = $_FILES['mainImage']['tmp_name'];			$src = imagecreatefrompng($uploadedfile);			}
			else
			{				$src = imagecreatefromgif($uploadedfile);			}

			list($width,$height)=getimagesize($uploadedfile);

			$mainwidth=640;			$mainheight=480;			/*	$newheight=($height/$width)*$newwidth;		*/		$tmp=imagecreatetruecolor($mainwidth,$mainheight);
			$displaywidth=337;		$displayheight=253;		/*	$newheight1=($height/$width)*$newwidth1;	*/		$tmp1=imagecreatetruecolor($displaywidth,$displayheight);
			$projectwidth=555;		$projectheight=416;		/*	$newheight1=($height/$width)*$newwidth1;	*/		$tmp2=imagecreatetruecolor($projectwidth,$projectheight);

			imagecopyresampled($tmp,$src,0,0,0,0,$mainwidth,$mainheight,$width,$height);
			imagecopyresampled($tmp1,$src,0,0,0,0,$displaywidth,$displayheight,$width,$height);
			imagecopyresampled($tmp2,$src,0,0,0,0,$projectwidth,$projectheight,$width,$height);

			$main = "../AdminImage/portfolio/main/". $_FILES['mainImage']['name'];
			$display = "../AdminImage/portfolio/display/". $_FILES['mainImage']['name'];
			$project = "../AdminImage/portfolio/category/". $_FILES['mainImage']['name'];

			imagejpeg($tmp,$main,100);				imagejpeg($tmp1,$display,100);				imagejpeg($tmp2,$project,100);

			imagedestroy($src);			imagedestroy($tmp);			imagedestroy($tmp1);			imagedestroy($tmp2);
}	}	}

	$result=mysqli_query($con,"INSERT INTO tbl_portfolio (portfolioName,categoryId,projectId,comment,mainImage,displayImage,Display)
								VALUES('$portfolio','$category','$Project','$Comment','$image','$image','$Display')");

	if($result)	{				echo '<script>alert(" Successfully Inserted");</script>';			}
	else		{				echo '<script>alert("Data Not Inserted");</script>';				}
	echo "<script>window.location='Portfolio.php'</script>";
}
?>
	<script>
        function validateForm() {
            var Project = document.forms["FormPortfolio"]["Project"].value;
            var Category = document.forms["FormPortfolio"]["Category"].value;
			var imgpath=document.getElementById('mainImage');
            if (Project==0) {                alert("Please Select Project.");                return false;            }
            if (Category==0) {                alert("Please Select Category.");                return false;            }
			else if (!imgpath.value==""){
				var imgsize=imgpath.files[0].size;
				if(imgsize>2000000)	{		alert(imgpath.files[0].name+" - Size is larger than 2MB. Please Reduce it.");			return false;		}
			}
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Portfolio</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Add New</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormPortfolio">

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Project <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="Project" id="Project" >
                            <option value="0" selected="selected" >- Select Project -</option>
                                <?php 
                                $ProjectRow=mysqli_query($con,"SELECT * FROM tbl_project order by projectId desc");
                                while($ProjectResult=mysqli_fetch_array($ProjectRow)){?>
                                    <option value="<?php echo $ProjectResult['projectId'];?>" ><?php echo $ProjectResult['projectName']; ?></option>
                                <?php } ?>
                        </select>
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="Category" id="Category" >
                            <option value="0" selected="selected" >- Select Category -</option>
                                <?php 
                                $PortfolioCategoryRow=mysqli_query($con,"SELECT * FROM tbl_category order by CategoryId desc");
                                while($PortfolioCategoryResult=mysqli_fetch_array($PortfolioCategoryRow)){?>
                                    <option value="<?php echo $PortfolioCategoryResult['CategoryId'];?>" ><?php echo $PortfolioCategoryResult['CategoryName']; ?></option>
                                <?php } ?>
                        </select>
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Portfolio Name<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Portfolio" required class="form-control col-md-7 col-xs-12">
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Portfolio Comment<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Comment" required class="form-control col-md-7 col-xs-12">
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<font color="#FF0004">Max-Image Size: <b>2MB</b> &nbsp; (Image Dimension --- width: <b>1280px</b> , Height: <b>720px</b> )</font>
						<input type="file" name="mainImage" id="mainImage"  class="form-control col-md-7 col-xs-12" accept="image/*" required >
				</div></div>
                <div class="form-group">
                   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Visibility <span class="required">*</span></label>
                   <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="checkbox" name="Display"  checked class="form-control  col-md-7 col-xs-12" >
                </div></div>

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" name="PortfolioSubmit" class="btn btn-success">Submit</button>
				</div></div>
				</form>
          </div></div></div></div>

 		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Portfolio List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>			<th>Project</th>			<th>Portfolio Name</th>		<th>Category</th>
                              <th>Image</th>			<th>Display</th>			<th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
	$ProductRow=mysqli_query($con,"select * from tbl_portfolio order by portfolioId desc");
	while($ProductResult=mysqli_fetch_array($ProductRow))
	{
		$i=$i+1;
	?>
                            <tr>
                              <td><?php echo $i; ?></td>
<?php
	$PrjId=$ProductResult['projectId'];
	$PrjRow=mysqli_fetch_array(mysqli_query($con,"select * from tbl_project WHERE projectId='$PrjId'"));

	$CatId=$ProductResult['categoryId'];
	$CatRow=mysqli_fetch_array(mysqli_query($con,"select * from tbl_category WHERE CategoryId='$CatId'"));
?>
                              <td><?php echo $PrjRow['projectName']; ?></td>
                              <td><?php echo $ProductResult['portfolioName']; ?></td>
                              <td><?php echo $CatRow['CategoryName']; ?></td>
                              <td><img src="../AdminImage/portfolio/display/<?php echo $ProductResult['mainImage']; ?>" style="height:80px;width:80px;"> </td>
                              <td><?php echo $ProductResult['Display']; ?></td>
                              <td> <a class="btn btn-info" href="Edit_Portfolio.php?PortfolioId=<?php echo $ProductResult['portfolioId']; ?>">
                <i class="glyphicon glyphicon-edit icon-white"></i>                Edit            </a>
            <a class="btn btn-danger" href="action.php?PortfolioId=<?php echo $ProductResult['portfolioId']; ?>" onclick="return window.confirm('Do you want to delete this record?')">
                <i class="glyphicon glyphicon-trash icon-white"></i>               Delete          </a></td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>
          </div></div></div></div>
	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
