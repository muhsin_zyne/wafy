<?php include('General/Header.php'); 

	if(isset($_POST['TagSubmit']))
	{
		$TagName=trim($_POST['Tag']);
				$result=mysqli_query($con,"INSERT INTO tbl_tags(TagName) VALUES('$TagName')");
				if($result)
				{	echo '<script>alert(" Successfully Inserted");</script>';	}
				else
				{	echo '<script>alert("Data Not Inserted");</script>';	}
				echo "<script>window.location='Tags.php'</script>";
	}
?>

<div class="right_col" role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Tags</h3>
            </div>
            
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Add New</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tag Name <span class="required">*</span>			</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="Tag" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" name="TagSubmit" class="btn btn-success">Submit</button>
                    </div></div>

                  </form>
          </div></div></div></div>


 <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Tags List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />


                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>
                              <th>Tag</th>
                             <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
	 $PortfolioCategoryRow=mysqli_query($con,"select * from  tbl_tags order by TagId desc");
	while($PortfolioCategoryResult=mysqli_fetch_array($PortfolioCategoryRow))
	{
		$i=$i+1;
	 ?>
     
                            <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $PortfolioCategoryResult['TagName']; ?></td>
                             <td> <a class="btn btn-info" href="Edit_Tag.php?TagId=<?php echo $PortfolioCategoryResult['TagId']; ?>">
                <i class="glyphicon glyphicon-edit icon-white"></i>
                Edit
            </a>
            <a class="btn btn-danger" href="action.php?TagId=<?php echo $PortfolioCategoryResult['TagId']; ?>" onclick="return window.confirm('Do you want to delete this record?')">
                <i class="glyphicon glyphicon-trash icon-white"></i>
                Delete
            </a></td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>


          </div></div></div></div>

</div></div>
          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>

<?php include('General/Footer.php'); ?>