<?php include('General/Header.php'); 


define ("MAX_SIZE","1000");

function ImageUpload($image)
{
	$errors=0;
	$image_name="";
	$image1 =$_FILES[$image]["name"];
	$uploadedfile = $_FILES[$image]['tmp_name'];
	if ($image1) 
	{	
		$filename = stripslashes($_FILES[$image]['name']);
		$extension = pathinfo($filename,PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
		{			
			echo '<script>alert("Unknown Image extension");</script>';
			$errors=1;
		}
		else
		{
			$size=filesize($_FILES[$image]['tmp_name']);
			if ($size > MAX_SIZE*1024)
			{				
				echo '<script>alert("You have exceeded the size limit");</script>';
				$errors=1;
			}
			else
			{

			if($extension=="jpg" || $extension=="jpeg" )
			{		
				$uploadedfile = $_FILES[$image]['tmp_name'];
				$src = imagecreatefromjpeg($uploadedfile);	
			}
			else if($extension=="png")
			{			
				$uploadedfile = $_FILES[$image]['tmp_name'];	
				$src = imagecreatefrompng($uploadedfile);	
			}
			else
			{	
				$src = imagecreatefromgif($uploadedfile);	
			}

			list($width,$height)=getimagesize($uploadedfile);

			$mainwidth=750;	
				$mainheight=500;			/*	$newheight=($height/$width)*$newwidth;		*/		$tmp=imagecreatetruecolor($mainwidth,$mainheight);
			$displaywidth=390;	
				$displayheight=263;		/*	$newheight1=($height/$width)*$newwidth1;	*/		$tmp1=imagecreatetruecolor($displaywidth,$displayheight);


			imagecopyresampled($tmp,$src,0,0,0,0,$mainwidth,$mainheight,$width,$height);
			imagecopyresampled($tmp1,$src,0,0,0,0,$displaywidth,$displayheight,$width,$height);
				
			$ra=rand(10,10000000000);
			$rb=rand(10,10000000000);
			$r="facilities-".$ra.$rb."_";
			$image_name=$r.$_FILES[$image]['name'];
			
			$main = "../Resource/facilities/".$image_name;
			$display = "../Resource/facilities/"."thump_".$image_name;
			imagejpeg($tmp,$main,100);	
				imagejpeg($tmp1,$display,100);				

			imagedestroy($src);	
				imagedestroy($tmp);	
				imagedestroy($tmp1);		
			}
	}	
	}
	return $image_name;
}







if(isset($_POST['facilitiesSubmit']))
{
	$Heading=trim(mysqli_real_escape_string($con,$_POST['Heading']));
	$Content=trim(mysqli_real_escape_string($con,$_POST['Content']));
	$image="";
	
	
	
		if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
		{
			
		$image_name1= ImageUpload('mainImage');
		if(!empty($image_name1))
			$image=$image_name1;
			$result=mysqli_query($con,"INSERT INTO tbl_facilities (facilitiesId,Heading,Content,Image)
										VALUES((SELECT IFNULL((SELECT MAX(facilitiesId)+1 FROM  tbl_facilities temp),1)),'$Heading','$Content','$image')");
			if($result)
			{
				echo '<script>alert(" Successfully Inserted");</script>';		
			}
			else	
			{			
				echo '<script>alert("Data Not Inserted");</script>';			
			}
			echo "<script>window.location='facilities.php'</script>";
		}
	    else
		{
			echo '<script>alert(" Please Select Image");</script>';	
		}
	

	
}
?>
	<script>
        function validateForm() {
            
            var Category = document.forms["Formfacilities"]["Category"].value;
			var imgpath=document.getElementById('mainImage');
			
			
			
			
            if (Category==0) 
			{
				alert("Please Select Category.");     
				return false;       
			}
			else if (imgpath.value=="")
			{
				alert("Please Select Image.");     
				return false;    
			}
			else if (!imgpath.value=="")
			{
				var imgsize=imgpath.files[0].size;
				if(imgsize>1000000)	
				{		
					alert(imgpath.files[0].name+" - Size is larger than 1MB. Please Reduce it.");	
					return false;	
				}
			}
			else
			{
				var isok=true;
				for (var i = 0; i < subimages.files.length; i++)
				{
				 var imageSize = subimages.files[i].size;
				 if (imageSize > 1000000) 
				 {
						alert(subimages.files[i].name+" - Size is larger than 1MB. Please Reduce it.");	
						isok= false;
						 break;
				 	}

			 	}
				if(!isok)
					return flase;
				
			}
			
			
   
    
			
			
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Facilities</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Add New</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="Formfacilities">

				

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">facilities Category <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="Heading" id="Heading" >
                            <option value="LIBRARY" selected="selected" >LIBRARY</option>
                            <option value="COMPUTER LAB" >COMPUTER LAB</option>
                            <option value="CAMPUS MASJID" >CAMPUS MASJID</option>
                            <option value="CANTEEN" >CANTEEN</option>
                                
                        </select>
				</div></div>
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Description Content<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea name="Content" required class="form-control col-md-7 col-xs-12" rows="5"></textarea>
				</div></div>			
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>750px</b> , Height: <b>500px</b> )</font>
						<input type="file" name="mainImage" id="mainImage" class="form-control col-md-7 col-xs-12" accept="image/*" required >
				</div></div>
                           
            
				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" name="facilitiesSubmit" class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>

 		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>facilities List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                         <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>                           
                              <th>Facility</th>                             
                              <th>Image</th>                            
                             <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
				
  if($_SESSION['DUIRoleId']==1)
  { 
			$facilitiesRow=mysqli_query($con,"select * from tbl_facilities order by facilitiesId desc");
  }
  else
  {
	  $facilitiesRow=mysqli_query($con,"select * from tbl_facilities where EUserId='".$_SESSION['DUIUserId']."' order by facilitiesId desc");

  }
	while($facilitiesResult=mysqli_fetch_array($facilitiesRow))
	{
		$i=$i+1;
	 ?>
                            <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $facilitiesResult['Heading']; ?></td>
                              <td> <img src="<?php echo "../Resource/facilities/".$facilitiesResult['Image'];?>" width="80" height="80"></td>
                              <td> 
	                            <form method="post" enctype="multipart/form-data" action="Edit_facilities.php"  style="float: left"> 
	                                <input type="hidden" name="facilitiesIdUpdate" value="<?php echo $facilitiesResult['facilitiesId']; ?>">
	                             	<input type="submit" class="btn btn-info" value="Edit" >
								</form>
	          					<form method="post" enctype="multipart/form-data" action="action.php" onSubmit="return confirm('Are you sure want to delete this record ?');"  style="float: left">
	                            	<input type="hidden" name="facilitiesIdDelete" value="<?php echo $facilitiesResult['facilitiesId']; ?>"> 
	                             	<input type="submit" class="btn btn-danger" value="Delete">
								</form>	
	           				  </td>
                            </tr> 
     <?php } ?>                        
                          </tbody>
                        </table>

          </div></div></div></div>
	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
