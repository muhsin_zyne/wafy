<?php include('General/Header.php'); 




if(isset($_POST['ArticleSubmit']))
{
	$name=trim(mysqli_real_escape_string($con,$_POST['name']));
	$designation=trim(mysqli_real_escape_string($con,$_POST['designation']));
	$testimonial=trim(mysqli_real_escape_string($con,$_POST['testimonial']));
	
	if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
	{
			$filename=$_FILES['mainImage']['name'];
			$temp=$_FILES['mainImage']['tmp_name'];
			$ra=rand(10,10000000000);
			$rb=rand(10,10000000000);
			$r="News_".$ra.$rb."_";
			$image_name1=$r.$filename;
			move_uploaded_file($temp,"../Resource/Testimonial/".$image_name1);
		

			$result=mysqli_query($con,"INSERT INTO tbl_testimonial (id,name,designation,testimonial,image)
										VALUES((SELECT IFNULL((SELECT MAX(id)+1 FROM  tbl_testimonial temp),1)),'$name','$designation','$testimonial','$image_name1')");
			if($result)
			{
				echo '<script>alert(" Successfully Inserted");</script>';		
			}
			else	
			{			
				echo '<script>alert("Data Not Inserted");</script>';			
			}
			echo "<script>window.location='Testimonial.php'</script>";
		}
	else
	{
		echo '<script>alert(" Please Select Image");</script>';	
	}
}
?>
	<script>
        function validateForm() {
            var name = document.forms["FormArticle"]["name"].value;
			var designation = document.forms["FormArticle"]["designation"].value;
			var testimonial = document.forms["FormArticle"]["testimonial"].value;
			var imgpath=document.getElementById('mainImage');
            if (!name||!designation||!testimonial) 
			{
				alert("Please Fill All Fields.");     
				return false;       
			}
			else if (imgpath.value=="")
			{
				alert("Please Select Image.");     
				return false;    
			}
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Testimonial</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Add New</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormArticle">

				

			

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="name" required class="form-control col-md-7 col-xs-12">
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Designation<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="designation" required class="form-control col-md-7 col-xs-12">
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Testimonial<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea name="testimonial" required class="form-control col-md-7 col-xs-12" rows="8"></textarea>
				</div></div>
				
            
				
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<font color="#FF0004"> Image Dimension --- width: <b>70px</b> , Height: <b>70px</b> </font>
						<input type="file" name="mainImage" id="mainImage"  class="form-control col-md-7 col-xs-12" accept="image/*" required >
				</div></div>
               

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" name="ArticleSubmit" class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>

 		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Article List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                         <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>
                             <th>Name</th>
                              <th>Designation</th>
                              <th>Testimonial</th>
                             
                              <th>Image</th>
                             
                             <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
				

			$ArticleRow=mysqli_query($con,"select * from tbl_testimonial order by id desc");

  
	while($ArticleResult=mysqli_fetch_array($ArticleRow))
	{
		$i=$i+1;
	 ?>
                            <tr>
                              <td><?php echo $i; ?></td>
                              
                              
                              <td><?php echo $ArticleResult['name']; ?></td>
								<td><?php echo $ArticleResult['designation']; ?></td>
                             <td><?php echo $ArticleResult['testimonial']; ?></td>
                             
                             
                                <td> <img src="<?php echo "../Resource/Testimonial/".$ArticleResult['image'];?>" width="80" height="80"></td>
                           	
                             
                             <td> 
                             <form method="post" enctype="multipart/form-data" action="Edit_Testimonial.php"  style="float: left"> 
                             <input type="hidden" name="TestimonialIdUpdate" value="<?php echo $ArticleResult['id']; ?>">
                             	<input type="submit" class="btn btn-info" value="Edit" >
							</form>
        					
         					
          					<form method="post" enctype="multipart/form-data" action="action.php" onSubmit="return confirm('Are you sure want to delete this record ?');"  style="float: left">
                            	<input type="hidden" name="TestimonialIdDelete" value="<?php echo $ArticleResult['id']; ?>"> 
                             	<input type="submit" class="btn btn-danger" value="Delete">
                            	 
							</form>
							
							
							
            </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>

          </div></div></div></div>
	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
