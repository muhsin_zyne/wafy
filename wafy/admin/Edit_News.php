<?php include('General/Header.php'); 

if(isset($_REQUEST['NewsIdUpdate']))
{
   $id=$_REQUEST['NewsIdUpdate'];
   //echo $id;exit;
   $result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_news WHERE NewsId='$id'"));
}
else
{
	echo "<script>window.location='News.php'</script>";
}

?>
	<script>
        function validateForm() {
            
            var Category = document.forms["FormNews"]["Category"].value;
			var imgpath=document.getElementById('mainImage');
            if (Category==0) 
			{
				alert("Please Select Category.");     
				return false;       
			}
			else if (!imgpath.value=="")
			{
				var imgsize=imgpath.files[0].size;
				if(imgsize>500000)	
				{		
					alert(imgpath.files[0].name+" - Size is larger than 500KB. Please Reduce it.");	
					return false;	
				}
			}
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>News</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Edit News</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="action.php" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormNews">

				

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">News Category <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="Category" id="Category" >
                            <option value="0" selected="selected" >- Select Category -</option>
                                <?php 
                                $NewsCategoryRow=mysqli_query($con,"SELECT * FROM tbl_news_category order by CategoryId desc");
                                while($NewsCategoryResult=mysqli_fetch_array($NewsCategoryRow)){?>
                                    <option value="<?php echo $NewsCategoryResult['CategoryId'];?>" ><?php echo $NewsCategoryResult['Category']; ?></option>
                                <?php } ?>
                                
                         <script>
							var Category= document.getElementById('Category');
							Category.value=<?php echo $result['CategoryId'];?>
							</script>	
								
                        </select>
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">News Heading<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Heading" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Heading'];?>"> 
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">News Content<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea name="Content" required class="form-control col-md-7 col-xs-12" rows="5"><?php echo $result['Content'];?></textarea>
				</div></div>
				<script src="ckeditor/ckeditor.js" type="text/javascript"></script>
                    
            <script>
                CKEDITOR.replace( 'Content' );
            </script>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Date<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="date" name="Date" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Date'];?>">
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<font color="#FF0004">Max-Image Size: <b>500KB</b> &nbsp; (Image Dimension --- width: <b>370px</b> , Height: <b>270px</b> )</font>
						<input type="file" name="mainImage" id="mainImage"  class="form-control col-md-7 col-xs-12" accept="image/*"  >
						
						  <a href="<?php echo "../Resource/News/".$result['Image'];?>" download> <img src="<?php echo "../Resource/News/".$result['Image'];?>" width="120" > </a>
						  
				</div></div>
               

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <input type="hidden" name="NewsUpdate" value="<?php echo $result['NewsId'];?>">
						<button type="submit"  class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>


	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
