<?php
include('admin/General/DBConnection.php');
$SliderResult=mysqli_fetch_array(mysqli_query($con,"select * from tbl_slider where id=1"));
$AbotUsEng=mysqli_fetch_array(mysqli_query($con,"select * from tbl_about where id=1"));
$AbotUsArb=mysqli_fetch_array(mysqli_query($con,"select * from tbl_about where id=2"));
$AbotUsMal=mysqli_fetch_array(mysqli_query($con,"select * from tbl_about where id=3"));

$HomeGeneral=mysqli_fetch_array(mysqli_query($con,"select * from tbl_home_general where id=1"));


?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!--IE Compatibility Meta-->
<meta name="author" content="DUIAC" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="KMECC MARAYAMANGALAM ">
<link href="favicon.png" rel="icon">


<!-- Fonts
    ============================================= -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i%7CPoppins:300,400,500,600,700' rel='stylesheet' type='text/css'>


<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">

<!-- RS5.0 Main Stylesheet -->
<link rel="stylesheet" type="text/css" href="assets/revolution/css/settings.css">
<link rel="stylesheet" type="text/css" href="assets/revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="assets/revolution/css/navigation.css"> 


<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->

<!-- Document Title
    ============================================= -->
<title>KMECC MARAYAMANGALAM  | Home </title>

</head>
<body>

<div class="preloader">
	<div class="spinner">
	  <div class="bounce1"></div>
	  <div class="bounce2"></div>
	  <div class="bounce3"></div>
	</div>
</div>

<!-- Document Wrapper
	============================================= -->

<?php include('header.php'); ?>

<!-- Hero Section
====================================== -->
<section id="hero" class="hero">
	<!-- START REVOLUTION SLIDER 5.0 -->
	<div class="rev_slider_wrapper">
		<div id="slider1" class="rev_slider"  data-version="5.0">
			<ul>
				<!-- slide 1 -->
				<li data-transition="zoomout" 
					data-slotamount="default" 
					data-easein="Power4.easeInOut" 
					data-easeout="Power4.easeInOut" 
					data-masterspeed="2000" 
					style="background-color: rgba(34, 34, 34, 0.3);"
					data-thumb="<?php echo "Resource/Slider/thump_".$SliderResult['s1image'];?>">
					<!-- MAIN IMAGE -->
					<img src="<?php echo "Resource/Slider/".$SliderResult['s1image'];?>" alt="Slide Background Image"  width="1920" height="1280">
					<!-- LAYER NR. 1 -->
					<div class="tp-caption ShadowCss1" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="-130" 
						data-whitespace="nowrap"
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
						data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
						data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
						data-start="500" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						data-fontsize="['16','16','15','15']"
						data-lineheight="['45','45','25','25']"
						data-fontweight="['700','500','600','300']"
						data-color="#fff" style="font-family: Ubuntu; text-transform:uppercase">
						<?php echo $SliderResult['s1content1'];?>
					</div>
					
					<!-- LAYER NR. 2 -->
					<div class="tp-caption ShadowCss2" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="-80" 
						data-whitespace="nowrap"
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
						data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
						data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
						data-start="750" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						data-fontsize="['61','17','15','15']"
						data-lineheight="['40','45','25','25']"
						data-fontweight="['700','500','600','300']"
						>
						<h1 style="color:#fff; font-size:61px;font-family: Lora; text-transform:uppercase"><?php echo $SliderResult['s1content2'];?></h1>
					</div>
					
					<!-- LAYER NR. 3 -->
					<div class="tp-caption ShadowCss3" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="10" 
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
						data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
						data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
						data-start="1000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						data-fontsize="['15','15','15','15']"
						data-lineheight="['24','24,'25','25']"
						data-fontweight="['700','500','500','500']"
						data-color="#fff" style="font-family:Open Sans; text-align:center; text-transform:uppercase"
						>
						<?php echo $SliderResult['s1content3'];?>
					</div>
					
					<!-- LAYER NR. 4 -->
					<div class="tp-caption" 
						id="slide-163-layer-6" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="90" 
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;"
						data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power3.easeOut;"
						data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);"
						data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
						data-transform_out="y:[175%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-mask_out="x:inherit;y:inherit;" 
						data-start="1250" 
						data-splitin="none" 
						data-splitout="none" 
						data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-164","delay":""}]'
						data-basealign="slide" 
						data-responsive_offset="on" 
						data-responsive="off">
						<a class="btn btn-secondary btn-white" href="about.php">Read More <i class="fa fa-angle-double-right"></i></a>
					</div>
				</li>
				
				<!-- slide 2 -->
				<li data-transition="scaledownfromright" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="<?php echo "Resource/Slider/thump_".$SliderResult['s2image'];?>">
					<!-- MAIN IMAGE -->
					<img src="<?php echo "Resource/Slider/".$SliderResult['s2image'];?>" alt="Slide Background Image"  width="1920" height="1280">
					<!-- LAYER NR. 1 -->
					<div class="tp-caption ShadowCss1" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="-130" 
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
						data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power3.easeInOut;" 
						data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
						data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
						data-start="1000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						data-fontsize="['16','17','15','15']"
						data-lineheight="['45','45','25','25']"
						data-fontweight="['700','500','600','300']"
						data-color="#fff" style="font-family: Ubuntu; text-transform:uppercase">
						<?php echo $SliderResult['s2content1'];?>
					</div>
					
					<!-- LAYER NR. 2 -->
					<div class="tp-caption ShadowCss2" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="-80" 
						data-whitespace="nowrap"
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
						data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
						data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
						data-start="2000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						data-fontsize="['61','17','15','15']"
						data-lineheight="['45','45','25','25']"
						data-fontweight="['700','500','600','300']"
						data-color="#fff" style="font-family: Lora; text-transform:uppercase">
						<?php echo $SliderResult['s2content2'];?>
					</div>
					
					<!-- LAYER NR. 3 -->
					<div class="tp-caption ShadowCss3" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="10" 
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
						data-transform_in="x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
						data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
						data-start="3000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						data-fontsize="['15','15','15','15']"
						data-lineheight="['24','24,'25','25']"
						data-fontweight="['700','500','500','500']"
						data-color="#fff" style="font-family: Open Sans; text-align:center; text-transform:uppercase">
						<?php echo $SliderResult['s2content3'];?>
					</div>
					
					<!-- LAYER NR. 4 -->
					<div class="tp-caption" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="90"  
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
						data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;" 
						data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
						data-start="5000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on">
						
						<a class="btn btn-secondary btn-white" href="about.php">Read More <i class="fa fa-angle-double-right"></i></a>
					</div>
				</li>
				
				<!-- slide 3 -->
				<li data-index='rs-367' data-transition='fadetotopfadefrombottom' data-slotamount='default' data-easein='default' data-easeout='default' data-masterspeed='default' data-thumb="<?php echo "Resource/Slider/thump_".$SliderResult['s3image'];?>">
					<!-- MAIN IMAGE -->
					<img src="<?php echo "Resource/Slider/".$SliderResult['s3image'];?>" alt="Slide Background Image"  width="1920" height="1280">
					<!-- LAYER NR. 1 -->
					<div class="tp-caption ShadowCss1" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="-130" 
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
						data-transform_in="y:-30px;rX:70deg;opacity:0;s:2000;e:Power4.easeInOut;" 
						data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-start="1000" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on" 
						data-fontsize="['16','16','15','15']"
						data-lineheight="['45','45','25','25']"
						data-fontweight="['700','500','600','300']"
						data-color="#fff" style="font-family: Ubuntu; text-transform:uppercase">
						<?php echo $SliderResult['s3content1'];?>
					</div>
					
					<!-- LAYER NR. 2 -->
					<div class="tp-caption ShadowCss2" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="-80" 
						data-whitespace="nowrap"
						data-width="['auto','auto','auto','auto']"
						data-height="['auto','auto','auto','auto']"
						data-transform_idle="o:1;"
						data-transform_in="x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
						data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
						data-start="2000" 
						data-splitin="chars" 
						data-splitout="none" 
						data-responsive_offset="on" 
						data-elementdelay="0.05"
						data-fontsize="['61','17','15','15']"
						data-lineheight="['45','45','25','25']"
						data-fontweight="['700','500','600','300']"
						data-color="#fff" style="font-family: Lora; text-transform:uppercase">
						<?php echo $SliderResult['s3content2'];?>
					</div>
					
					<!-- LAYER NR. 3 -->
					<div class="tp-caption ShadowCss3" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="10" 
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
						data-transform_in="x:-50px;skX:100px;opacity:0;s:2000;e:Power4.easeInOut;" 
						data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-start="4810" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						data-fontsize="['15','15','15','15']"
						data-lineheight="['24','24,'25','25']"
						data-fontweight="['700','500','500','500']"
						data-color="#fff" style="font-family: Open Sans ;text-align:center; text-transform:uppercase">
						<?php echo $SliderResult['s3content3'];?>
					</div>
					
					<!-- LAYER NR. 4 -->
					<div class="tp-caption" 
						data-x="center" data-hoffset="0" 
						data-y="center" data-voffset="90"  
						data-width="none"
						data-height="none"
						data-transform_idle="o:1;"
						data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
						data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(41, 46, 49, 0);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
						data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
						data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-start="5670" 
						data-splitin="none" 
						data-splitout="none" 
						data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
						data-responsive_offset="on" 
						data-responsive="off">
						
						<a class="btn btn-secondary btn-white" href="about.php">Read More <i class="fa fa-angle-double-right"></i></a>
					</div>
				</li>
			</ul>
		</div>
		<!-- END REVOLUTION SLIDER -->
	</div>
	<!-- END OF SLIDER WRAPPER -->
</section>
<!-- #hero end -->

<!-- Service Block #1
============================================= -->

<section id="service1" class="service service-7">
	<div class="container">
		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="heading mb-0">
					
					<h2>About Us</h2>
				</div><!-- .heading end -->
			</div><!-- .col-md-4 end -->
			
		</div><!-- .row end -->
        <style>
body {font-family: "Lato", sans-serif;}

ul.tab {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
  /*  border: 1px solid #ccc;
    background-color: #f1f1f1;*/
}

/* Float the list items side by side */
ul.tab li 
{
	float: left;
margin-left: 5px;
}

/* Style the links inside the list items */
ul.tab li a {
    display: inline-block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    transition: 0.3s;
    font-size: 14px;
	    border-radius: 10%;
}

/* Change background color of links on hover */
ul.tab li a:hover {
    background-color: #f7f7f7;
}

/* Create an active/current tablink class */
ul.tab li a:focus, .tabactive {
    background-color: #efeded;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
 /*   border: 1px solid #ccc;*/
    border-top: none;
}

.topright {
 float: right;
 cursor: pointer;
 font-size: 20px;
}

.topright:hover {color: red;}
</style>

<ul class="tab">
  <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">English</a></li>
  <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Paris')">العربية</a></li>
  <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Tokyo')">മലയാളം</a></li>
</ul>

<div id="London" class="tabcontent shortcode-1">
  <div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row">
					
					<div class="col-xs-12 col-sm-12 col-md-12">
						<img src="<?php echo "Resource/AboutUs/".$AbotUsEng['image'];?>" alt="About Us">
						<p class="mb-0"><?php echo $AbotUsEng['summery'];?></p><br />
					</div><!-- .col-md-10 end -->
				</div><!-- .row end -->
			</div><!-- .col-md-6 end -->
            </div>
            </div>
          	<div class="row">
			<div class="col-xs-12  col-sm-12  col-md-2  portfolio-more " style="margin-bottom:20px;">
				<a href="about.php">Read More <i class="fa fa-angle-double-right"></i></a>
			</div><!-- .col-md-2 end -->
		</div><!-- .row end -->
        
               		<div class="row">
			<!-- Service #1 -->
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsEng['icon1'];?>">
						<h3><?php echo $AbotUsEng['head1'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsEng['content1'],0,80)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			
						<!-- Service #1 -->
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsEng['icon2'];?>">
						<h3><?php echo $AbotUsEng['head2'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsEng['content2'],0,80)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			
				

			
						<!-- Service #1 -->
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsEng['icon3'];?>">
						<h3><?php echo $AbotUsEng['head3'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsEng['content3'],0,80)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
           	
            	<!-- Service #3 -->
						<!-- Service #1 -->
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsEng['icon4'];?>">
						<h3><?php echo $AbotUsEng['head4'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsEng['content4'],0,80)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
            
		</div><!-- .row end -->
        
            
            
            
        
                    
</div>

<div id="Paris" class="tabcontent shortcodeArabic">
  
  <div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row">
					
					<div class="col-xs-12 col-sm-12 col-md-12">
						<img src="<?php echo "Resource/AboutUs/".$AbotUsArb['image'];?>" alt="feature">
                        	
						<p class="mb-0"><?php echo $AbotUsArb['summery'];?>

                        </p> <br />


					</div><!-- .col-md-10 end -->
				</div><!-- .row end -->
			</div><!-- .col-md-6 end -->
            </div>
            </div>
            <div class="row">
			<div class="col-xs-12  col-sm-12  col-md-2  portfolio-more " style="margin-bottom:20px;">
				<a href="about.php">Read More <i class="fa fa-angle-double-right"></i></a>
			</div><!-- .col-md-2 end -->
		</div><!-- .row end -->
               		<div class="row">
			<!-- Service #1 -->
		<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsArb['icon1'];?>">
						<h3><?php echo $AbotUsArb['head1'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsArb['content1'],0,200)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			
			<!-- Service #2 -->
				<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsArb['icon2'];?>">
						<h3><?php echo $AbotUsArb['head2'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsArb['content2'],0,200)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			
			<!-- Service #3 -->
				<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsArb['icon3'];?>">
						<h3><?php echo $AbotUsArb['head3'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsArb['content3'],0,200)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			
			<!-- Service #4 -->
	<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsArb['icon4'];?>">
						<h3><?php echo $AbotUsArb['head4'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsArb['content4'],0,200)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
        
            
            
            
</div>

<div id="Tokyo" class="tabcontent shortcode-1">

 <div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row">
					
					<div class="col-xs-12 col-sm-12 col-md-12">
						<img src="<?php echo "Resource/AboutUs/".$AbotUsMal['image'];?>" alt="feature">
                        
<p class="mb-0">

<?php echo $AbotUsMal['summery'];?>
</p><br />
					</div><!-- .col-md-10 end -->
				</div><!-- .row end -->
			</div><!-- .col-md-6 end -->
            </div>
            </div>


<div class="row">
			<div class="col-xs-12  col-sm-12  col-md-2  portfolio-more " style="margin-bottom:20px;">
				<a href="about.php">Read More <i class="fa fa-angle-double-right"></i></a>
			</div><!-- .col-md-2 end -->
		</div><!-- .row end -->

     		<div class="row">
			<!-- Service #1 -->
					
						<!-- Service #4 -->
	<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsMal['icon1'];?>">
						<h3><?php echo $AbotUsMal['head1'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsMal['content1'],0,160)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
		
			
									<!-- Service #4 -->
	<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsMal['icon2'];?>">
						<h3><?php echo $AbotUsMal['head2'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsMal['content2'],0,162)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			
									<!-- Service #4 -->
	<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsMal['icon3'];?>">
						<h3><?php echo $AbotUsMal['head3'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsMal['content3'],0,160)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			
						<!-- Service #4 -->
	<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsMal['icon4'];?>">
						<h3><?php echo $AbotUsMal['head4'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo substr($AbotUsMal['content4'],0,178)."..";?></p>
                        <a class="btn-more ServiceReadMore" href="about.php">read more</a>
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			
			
		</div><!-- .row end -->
 
</div>

<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" tabactive", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " tabactive";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


         
        
        

	</div><!-- .container end -->
</section><!-- #service1 end -->



<section id="blog-carousel-hero" class="blog-carousel-hero pt-0 pb-0">
	<div class="container-fluid">
		<div class="row">
					<div class="col-xs-12  col-sm-12  col-md-12 pr-0 pl-0">
						<div id="blog-carousel4" class="blog-carousel blog blog-grid">
							<!-- Blog Panel #1 -->
							
							<?php $newsQuery=mysqli_query($con,"select * from tbl_news where Approve=1 order by NewsId desc limit 10");
							while($newsResult=mysqli_fetch_array($newsQuery))
							{
							?>
							
							<div class="blog-panel">
								<!-- Entry #1 -->
								<div class="entry">
									<div class="entry-featured">
										<img src="<?php echo "Resource/News/".$newsResult['Image'];?>" alt="Wafy News">
										<div class="entry-overlay">
											<div class="entry-overlay-bio">
											<a href="news.php#News_<?php echo $newsResult['NewsId'];?>">
												<div class="entry-cat">
													<i class="fa fa-image"></i>
												</div>
												</a>
												<div class="entry-title">
													<h3>
														<a href="news.php#News_<?php echo $newsResult['NewsId'];?>"><?php echo $newsResult['Heading'];?></a>
													</h3>
												</div>
												<!-- .entry-title end -->
											</div>
											<div class="entry-overlay-meta">
													<div class="entry-meta">
														
														<span>In : </span>
														<span>
														<a ><?php $cat=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_news_category where CategoryId='".$newsResult['CategoryId']."'"));
															echo  $cat[0];?></a>
														</span>
														/
														<span>On : </span>
														<span>
														<a ><?php echo date('M d Y',strtotime($newsResult['Date']));?></a>
														</span>
													</div>
													<!-- .entry-meta end -->
											</div>
											
										</div>
									</div>
									<!-- .entry-featured end -->
								</div>
								<!-- .entry end -->
							</div>
							
							<?php } ?>
							<!-- .blog-panel end -->
							
							<!-- Blog Panel #2 -->
							

						</div>
						<!-- .blog-carousel end -->
					</div>
					<!-- .col-md-12 end -->
				</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
<!-- #shortcode16 end -->



<!-- Service Block #8
============================================= -->
<section id="service2" class="service service-2 service-4 service-8">
	<div class="container">
		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="heading mb-0">
					
					<h2>FAVOURITES</h2>
				</div><!-- .heading end -->
			</div><!-- .col-md-4 end -->
			<div class="col-xs-12  col-sm-12  col-md-7">
				<p class="mb-0"><?php echo $HomeGeneral['favourites'];?></p>
			</div><!-- .col-md-8 end -->
		</div><!-- .row end -->
		<div class="row row-bordered">
			<!-- Service #1 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel">
					<div class="service-heading">
					
					<img  src="assets/images/ICON/Committed-to-Excellence.png" >
					
						
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Committed to Excellence</h3>
						<p>The CIC accords utmost importance to maintain excellent academic quality for all its courses...</p>
						
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
			
			<!-- Service #2 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel">
					<div class="service-heading">
						
					<img  src="assets/images/ICON/Facilities.png" >
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Facilities</h3>
						<p>The CIC makes it mandatory for all affiliated colleges to set full facilities for study...</p>
					
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
			
			<!-- Service #3 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel last">
					<div class="service-heading">
						<img  src="assets/images/ICON/Discipline.png" >
					
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Discipline</h3>
						<p>students are obliged to provide importance for Islamic studies and to abide by all Islamic disciplines...

</p>
						
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
		<div class="row row-bordered">			
			<!-- Service #4 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel">
					<div class="service-heading">
						<img  src="assets/images/ICON/Continuous-Assessment.png" >
				
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Continuous Assessment</h3>
						<p>The program are designed with various continuous evaluation activities...</p>
					
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
			
			<!-- Service #5 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel">
					<div class="service-heading">
					<img  src="assets/images/ICON/Dissertation.png" >
						
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Dissertation</h3>
						<p>After PG , the students have to submit a dissertation in not less than 100 pages...</p>
						
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
			
			<!-- Service #6 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel last">
					<div class="service-heading">
						<img  src="assets/images/ICON/Work-Shops.png" >
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Work Shops</h3>
						<p>Workshops are conducted under CIC for the different sections - teachers, managements, principals, students’ union leaders...</p>
					
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
        
        		<div class="row">			
			<!-- Service #4 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel">
					<div class="service-heading">
						<img  src="assets/images/ICON/Arabic-and-Urdu-Diplomas.png" >
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Arabic and Urdu Diplomas</h3>
						<p>During the course, students can also complete a two-year functional Arabic and Urdu course...</p>
					
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
			
			<!-- Service #5 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel">
					<div class="service-heading">
						<img  src="assets/images/ICON/Co-curricular-Activities.png" >
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Co-curricular Activities</h3>
						<p>Various educational and intellectual sessions and cultural programmes are organized at regular intervals...</p>
						
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
			
			<!-- Service #6 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel last">
					<div class="service-heading">
						<img  src="assets/images/ICON/Research-Council.png" >
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Research Council</h3>
						<p>Conducts discussions and debates with senior professors from all affiliated colleges...</p>
					
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
       
	</div><!-- .container end -->
</section>


<section class="h300 bg-overlay bg-parallex">
	<div class="bg-section">
		<img src="Resource/AboutUs/download-brochure.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12  col-sm-12  col-md-8 col-md-offset-2 text-center">
				<h3 class="text-white">Braucher</h3>
				<p class="text-white"><?php echo $HomeGeneral['braucher'];?></p>
				<a class="btn btn-secondary btn-white" href="<?php echo "Resource/AboutUs/".$HomeGeneral['braucher_file'];?>" download >Download <i class="fa fa-angle-double-right"></i></a>
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section>



<!-- Portfolio Gallery 2 Column Without Space
============================================= -->
<section id="portfolio-fullwidth" class="portfolio portfolio-gallery-space portfolio-3col">
	<div class="container">
    	<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="heading mb-0">
				
					<h2>Gallery</h2>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-4 end -->
			<div class="col-xs-12  col-sm-12  col-md-7">
				<p class="mb-0"><?php echo $HomeGeneral['gallery'];?></p>
			</div>
			<!-- .col-md-8 end -->
		</div>
        
		<div class="row">
			<!-- Portfolio Filter
			============================================= -->
			<div class="col-xs-12  col-sm-12  col-md-12 portfolio-filter">
				<ul class="list-inline">
					<li><a class="active-filter" href="#" data-filter="*">All</a></li>
					<?php $galleryCatquery=mysqli_query($con,"select * from tbl_gallery_category");
					while($galleryCatResult=mysqli_fetch_array($galleryCatquery))
					{
					?>
					<li><a href="#" data-filter=".<?php echo $galleryCatResult['CategoryId'];?>"><?php echo $galleryCatResult['Category'];?></a></li>
					
					<?php } ?>
				</ul>
			</div>
			<!-- .Portfolio-filter end --> 
		</div><div id="portfolio-all" class="row">
			<!-- Portfolio #1 -->
			
		<?php $galleryquery=mysqli_query($con,"select * from tbl_gallery where Approve=1 order by GalleryId desc limit 9");
					while($galleryResult=mysqli_fetch_array($galleryquery))
					{
					?>
			
			<div class="col-xs-12 col-sm-6 col-md-4  portfolio-item  <?php echo $galleryResult['CategoryId'];?>">
				<div class="portfolio-img">
					<img src="<?php echo "Resource/Gallery/thump_".$galleryResult['Image'];?>" alt="Wafy Gallery Image">
					<div class="portfolio-hover">
						<div class="portfolio-meta">
							<div class="portfolio-cat">
								<span>In </span><span><?php  
		$Category=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_gallery_category where CategoryId ='".$galleryResult['CategoryId']."'"));
		echo $Category[0]; ?>
						</span>
							</div>
							<div class="portfolio-title">
								<h4><?php echo $galleryResult['Heading'];?></h4>
							</div>
						</div><!-- .Portfolio-meta end -->
						<div class="portfolio-action">
						<div class="portfolio-zoom-like">
							<a class="img-popup zoom" href="<?php echo "Resource/Gallery/".$galleryResult['Image'];?>" title="<?php echo $galleryResult['Content'];?>" style="float: left">
								<i class="fa fa-search"></i>
							</a>
							 <div class="portfolio-link" style="float: left">
								<a href="gallery_single.php?GalleryId=<?php echo  $galleryResult['GalleryId'];?>&<?php echo strtolower(str_replace(' ', '_',$galleryResult['Heading'])); ?>"><i class="fa fa-link"></i></a>
							</div>
							
						</div>
						 
					</div><!-- .Portfolio-action end -->
					</div><!-- .Portfolio-hover end --> 
				</div><!-- .Portfolio-img end -->
			</div><!-- . portfolio-item  end -->
			
			
			<?php } ?>
			
	

		</div><!-- .row end -->
		<!-- .row end -->
		<div class="row">
			<div class="col-xs-12  col-sm-12  col-md-2 col-md-offset-1 portfolio-more mt-50">
				<a href="gallery.php">Load More <i class="fa fa-angle-double-right"></i></a>
			</div><!-- .col-md-2 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #portfolio-fullwidth end -->





<!-- Testimonial #4
============================================= -->
<section id="testimonial4" class="testimonial testimonial-4 bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-xs-12  col-sm-12  col-md-12">
				<div id="testimonial-oc4" class="testimonial-oc style-2">
					<!-- Testimonial #1 -->
					
					<?php $TestQuery=mysqli_query($con,"select * from tbl_testimonial");
					while($TestArray= mysqli_fetch_array($TestQuery))
					{
					?>
					<div class="testimonial-panel">
						<div class="testimonial-icon"></div><!-- .testimonial-icon end -->
						<div class="testimonial-body">
							<p><?php echo $TestArray['testimonial']; ?></p>
							<div class="testimonial-meta">
								<div class="testimonial-img">
									<img src="<?php echo "Resource/Testimonial/".$TestArray['image']; ?>" alt="wafy Testimonial">
								</div>
								<h4><?php echo $TestArray['name']; ?></h4>
								<p><?php echo $TestArray['designation']; ?></p>
							</div><!-- .testimonial-meta end -->
						</div><!-- .testimonial-body end -->
					</div><!-- .testimonial-panel end -->
					<?php } ?>
					<!-- Testimonial #2 -->
					
				</div><!-- #testimonial-oc end -->
			</div><!-- .col-md-6 end -->
			
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #testimonial4 end -->


<!-- Blog Grid 3 Column
============================================= -->
<section id="blog-grid-3-col" class="blog blog-grid blog-grid-extend blog-3col">
	<div class="container">
    
    	<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="heading mb-0">
				
					<h2>Articles</h2>
				</div>
				<!-- .heading end -->
			</div>
			<!-- .col-md-4 end -->
			<div class="col-xs-12  col-sm-12  col-md-7">
				<p class="mb-0"><?php echo $HomeGeneral['articles'];?></p>
			</div>
			<!-- .col-md-8 end -->
		</div>
        
		<div class="row">
			<!-- Entry #1 -->
			
			<?php $ArtQuery=mysqli_query($con,"select * from tbl_article where Approve = 1 order by ArticleId desc limit 3");
				while($ArtResult=mysqli_fetch_array($ArtQuery))
				{
			?>
			
			<div class="col-xs-12 col-sm-6 col-md-4 entry clearfix">
				<div class="entry-featured">
					<?php if($ArtResult['ImageOrVideo']==2)
			{	?>
				<div class="videoWrapper">
				<iframe width="100%" height="263" src="<?php echo $ArtResult['Video'];?>" frameborder="0" ></iframe>
					</div>
			<?php } else {	?>
					<img src="<?php echo "Resource/Article/thump_".$ArtResult['Image'];?>" alt="Wafy Article">
					<div class="entry-overlay">

						<div class="entry-author">
							<span>By : </span>
							<a ><?php echo $ArtResult['Author'];?></a>
						</div>
					</div>
					<?php  } ?>
					
				</div>
				<!-- .entry-featured end -->
				<div class="entry-meta">
					<span>In : </span>
					<span>
					<a ><?php $cat=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_article_category where CategoryId='".$ArtResult['CategoryId']."'")); echo $cat[0];?></a>
					</span>
					/
					<span>On : </span>
					<span>
					<a><?php echo date('M d Y',strtotime($ArtResult['Date']));?></a>
					</span>
					
				</div>
				<!-- .entry-meta end -->
				<div class="entry-title">
					<h3><a href="article_single.php?ArticleId=<?php echo $ArtResult['ArticleId'];?>&<?php echo strtolower(str_replace(' ', '_',$ArtResult['Heading']));;?>"><?php echo $ArtResult['Heading'];?></a></h3>
				</div>
				<!-- .entry-title end -->
				<div class="entry-content">					<p ><?php echo substr($ArtResult['Content'],0,200);?></p>
				<br />
					<a class="pull-right" href="article_single.php?ArticleId=<?php echo $ArtResult['ArticleId'];?>&<?php echo strtolower(str_replace(' ', '_',$ArtResult['Heading'])); ?>" style="padding: 7px;color: #474747;background-color:#E9E9E9; "> Read More</a>
</div>
				<!-- .entry-content end -->
				
			</div>
			
			<?php } ?>
			

			<!-- .col-md-4 end -->
              <div class="col-xs-12 col-sm-12 col-md-12 ">
			
				<a class="btn btn-primary mr-30" href="article.php">Load More</a>
				
			</div><!-- .col-md-12 end -->
            </div>
            </div>
            </section>
            
            
<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="assets/js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="assets/js/plugins.js"></script>
<script type="text/javascript" src="assets/js/functions.js"></script>
<script type="text/javascript" src="assets/revolution/js/jquery.themepunch.tools.min838f.js?rev=5.0"></script>
<script type="text/javascript" src="assets/revolution/js/jquery.themepunch.revolution.min838f.js?rev=5.0"></script>

<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="assets/js/rsconfig.js"></script>
</body>
</html>