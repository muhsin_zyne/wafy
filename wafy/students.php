
<?php
include('admin/General/DBConnection.php');
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="DUIAC" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="KMECC MARAYAMANGALAM ">
<link href="favicon.png" rel="icon">


<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">


<!-- Document Title
    ============================================= -->
<title>KMECC MARAYAMANGALAM  | Students</title>
</head>
<body>

<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">

<?php include('header.php'); ?>

		

<!-- Page Title #1
============================================= -->
<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="Resource/students.jpg" alt="Contact Wafy"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="title-1 text-center">
					
					<div class="title-heading">
						<h2>Students</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->

<section id="shortcode9" class="shortcode-9">
	<div class="container">
		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1 col-sm-12 col-md-3 col-md-offset-1">
				<div class="heading mb-0">
					
					<h2>Al Miras Students Association</h2>
				</div>
				<!--.headingend -->
			</div>
			<!--.col-md-4end -->
			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0" style="color: black">A students' organization namely <b>Al Miras</b> works under the strict supervision of teachers in the institution, to facilitate literary, arts and sports programs in the campus. It is also aimed at fostering the organizing skills of the students.  </p>
				
				<p class="mb-0" style="color: black">Programmes by <b>Al Miras</b> include ensuring the availability of News Papers in different languages and Periodicals, and running a full-fledged library and computer lab. This union also organizes programs like street speeches, debates, seminars, expert guest lectures, and other competitions among students.  </p>
				
			</div>
			<!--.col-md-8end -->
		</div>
		<!-- .row end -->
		
		
		
		
				<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1 col-sm-12 col-md-3 col-md-offset-1">
				<div class="heading mb-0">
					
					<h2>Darul Uloom`s Wafy  Alumni Association</h2>
				</div>
				<!--.headingend -->
			</div>
			<!--.col-md-4end -->
			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0" style="color: black"><b>Duiac</b> graduates are known as wafies who have perused their  studies in this institution. They are spread in different fields of Islamic propagation. For the smooth operation of this great mission it has been formed an association named <b> DAWA </b>( Darul Uloom`s Wafy  Alumni Association).</p>		
			</div>
			<!--.col-md-8end -->
		</div>
		<!-- .row end -->
		
			
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
<!-- #shortcode9end -->


<section id="team" class="team team-1 pb-lg">
	<div class="container">
	
	<?php $studentbatchquery=mysqli_query($con,"select * from tbl_students_category order by CategoryId asc"); 
		while( $studentbatch=mysqli_fetch_array($studentbatchquery))
		{
		?>
	<h3 > Batch : <?php echo $studentbatch['Category'];?> </h3>
		<div class="row">
			
			<?php $StudentListQuery=mysqli_query($con,"select * from tbl_students where BatchId='".$studentbatch['CategoryId']."' order by DisplayOrder asc"); 
		while( $StudentList=mysqli_fetch_array($StudentListQuery))
		{
		?>
			<!-- Member #1 -->
			<div class="col-xs-12 col-sm-6 col-md-3 member">
				<div class="member-img">
					<img src="<?php echo "Resource/Students/".$StudentList['Image'];?>" alt="Wafy Students Image"/>
					<div class="member-bg"></div>
					<div class="member-overlay">
						<h6><?php echo $StudentList['Occupation'];?></h6>
						<h5><?php echo $StudentList['Name'];?></h5>
					</div><!-- .memebr-ovelay end -->
					<div class="member-hover">
						<h6>Reg# CIC: <b><?php echo $StudentList['RegCIC'];?></b>, CLG: <b><?php echo $StudentList['RegCLG'];?></b> </h6>
						<h5><?php echo $StudentList['Name'];?></h5>
						<p>
						
						<i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?php echo $StudentList['Address'];?><br/>
							<i class="fa fa-envelope"></i> &nbsp;&nbsp; <a href="mailto:<?php echo $StudentList['Email'];?>"><?php echo $StudentList['Email'];?></a> <br/>
							<i class="fa fa-phone"></i>&nbsp;&nbsp;<a href="tel:<?php echo $StudentList['Phone'];?>"> <?php echo $StudentList['Phone'];?></a> <br/>
						<i class="fa fa-mobile"></i>&nbsp;&nbsp; <a href="tel:<?php echo $StudentList['Mobile'];?>"> <?php echo $StudentList['Mobile'];?></a> <br/>
						</p>
					
						<div class="member-social">
						<?php if(!empty($StudentList['FbLink'])) { ?>
							<a href="<?php echo $StudentList['FbLink'];?>" target="_blank"><i class="fa fa-facebook"></i></a>
							<?php } ?>
							<?php if(!empty($StudentList['TwitterLink'])) { ?>
							<a href="<?php echo $StudentList['TwitterLink'];?>" target="_blank"><i class="fa fa-twitter"></i></a>
							<?php } ?>
							<?php if(!empty($StudentList['GoogleLink'])) { ?>
							<a href="<?php echo $StudentList['GoogleLink'];?>" target="_blank"><i class="fa fa-google-plus"></i></a>
							<?php } ?>
							<?php if(!empty($StudentList['LinkedInLink'])) { ?>
							<a href="<?php echo $StudentList['LinkedInLink'];?>" target="_blank"><i class="fa fa-linkedin"></i></a>
							<?php } ?>
						</div>
					</div><!-- .member-hover end -->
				</div><!-- .member-img end -->
			</div><!-- .member end -->
			<?php } ?>
			<!-- Member #2 -->
		
			
		</div><!-- .row end -->
		
		<?php } ?>
		
	</div><!-- .container end -->
</section><!-- #team end -->





<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/functions.js"></script>
</body>

<!-- Mirrored from 7oroof.com/tfdemos/mount/contact-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Nov 2016 05:26:56 GMT -->
</html>