-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2018 at 03:37 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `college_website_a`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_about`
--

CREATE TABLE `tbl_about` (
  `id` int(11) NOT NULL,
  `summery` varchar(5000) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `video` varchar(300) NOT NULL,
  `head1` varchar(50) NOT NULL,
  `head2` varchar(50) NOT NULL,
  `head3` varchar(50) NOT NULL,
  `head4` varchar(50) NOT NULL,
  `head5` varchar(50) NOT NULL,
  `head6` varchar(50) NOT NULL,
  `content1` varchar(1000) NOT NULL,
  `content2` varchar(1000) NOT NULL,
  `content3` varchar(1000) NOT NULL,
  `content4` varchar(1000) NOT NULL,
  `content5` varchar(1000) NOT NULL,
  `content6` varchar(1000) NOT NULL,
  `icon1` varchar(300) NOT NULL,
  `icon2` varchar(300) NOT NULL,
  `icon3` varchar(300) NOT NULL,
  `icon4` varchar(300) NOT NULL,
  `icon5` varchar(300) NOT NULL,
  `icon6` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_about`
--

INSERT INTO `tbl_about` (`id`, `summery`, `description`, `image`, `video`, `head1`, `head2`, `head3`, `head4`, `head5`, `head6`, `content1`, `content2`, `content3`, `content4`, `content5`, `content6`, `icon1`, `icon2`, `icon3`, `icon4`, `icon5`, `icon6`) VALUES
(1, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2>Why do we use it?</h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '<p><strong>Darul Uloom Islamic and arts college</strong> is one of the prospective Islamic institutions providing religious knowledge along with material education in accordance with surging need of the modern time. This institution is located at Paral,Thootha nearby Palakkad District which is an educationally back ward district in Kerala, India. It follows WAFY curriculum tailored by CIC (Co-ordination of Islamic colleges). It started its campus at Thootha on 07/06/2004 enrolling 28 students, but later, taking the facilities into account it shifted its campus to Paral. Currently, the institution has emerged to be one of the leading institutes with about 250 students.</p>\r\n\r\n<p>The institution provides education and residential facilities free of cost. The institution follows the Wafy Syllabus, which came in to existence bridging the existing gap in the scenario of religious education. All the Wafy institutions are run under Samastha Kerala Jamyithul Ulama which is an Umbrella Organization for Mulsim scholars. The admission to this course is given for those candidates who completed the 10th standard successfully after an entrance test. At present, around 5000 students are studying in about forty affiliated colleges including Wafy (8 years course exclusively for boys) and Wafiyya (5 years course exclusively for girls).</p>\r\n\r\n<p>The course has been broken in to 2years for preparatory, four years for degree and two years for PG. The whole course goes with 16 semesters and students are provided chance to chose the specialized areas according to their need and aptitudes.</p>\r\n', 'AboutUs_Eng_634890906244549621_270x182.jpg', 'https://www.youtube.com/watch?v=UiN4YeR8PTY', 'Objective', 'Leadership', 'Vision', 'Lorem Ipson', 'Lorem Ipson', 'Lorem Ipson', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model', 'Sample Data for Leadership', 'Sample Data fro Vision', 'Lorem Ipson', 'Lorem Ipson', 'Lorem Ipsondd', 'AboutUs_Eng_5060537541051051260_Objective.png', 'AboutUs_Eng_4137075521252353931_Leadership.png', 'AboutUs_Eng_8584840401085734875_Vision.png', 'AboutUs_Eng_629124647100737409_Exam.png', 'AboutUs_Eng_1002210188271745706_Exam.png', 'AboutUs_Eng_826554458205218573_Exam.png'),
(2, '<p>ÙˆÙ„Ø§ÙŠØ© ÙƒÙŠØ±Ø§Ù„Ø§ Ø§Ù„Ù‡Ù†Ø¯ÙŠØ© Ù„Ù…Ù† Ø§Ù„Ø£Ø±Ø¶ÙŠØ§Øª Ø§Ù„ØºÙ†ÙŠØ© Ø¨Ø§Ù„Ù…Ù†Ø§Ø® Ø§Ù„Ù…Ù„Ø§Ø¦Ù… Ù„Ù„Ø¯Ø¹ÙˆØ© Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ©. ÙˆÙ„Ù„Ø¥Ø³Ù„Ø§Ù… ÙˆØ§Ù„Ù…Ø³Ù„Ù…ÙŠÙ† ÙÙŠÙ‡Ø§ Ø­Ø¶ÙˆØ± ÙƒØ«ÙŠÙ ÙÙŠ Ø§Ù„Ù…Ø§Ø¶ÙŠ ÙˆØ§Ù„Ø­Ø§Ø¶Ø± Ø­ÙŠØ« ÙŠØ´ÙƒÙ„ Ø§Ù„Ù…Ø³Ù„Ù…ÙˆÙ† Ø±Ø¨Ø¹ Ø³ÙƒØ§Ù†Ù‡Ø§ ÙˆÙŠØ¯ÙŠØ±ÙˆÙ† Ø¨Ø¬Ù‡ÙˆØ¯Ù‡Ù… Ø§Ù„Ø°Ø§ØªÙŠØ© Ù…Ø¹ØªÙ…Ø¯ÙŠÙ† Ø¹Ù„Ù‰ Ø§Ù„Ø£ÙŠØ§Ø¯ÙŠ Ø§Ù„Ø³Ø®ÙŠØ© Ø£ÙƒØ«Ø± Ù…Ù† Ø£Ù„Ù Ø¯Ø±ÙˆØ³ ØªÙ‚Ù„ÙŠØ¯ÙŠØ© ÙÙŠ Ø§Ù„Ù…Ø³Ø§Ø¬Ø¯ ÙˆØ­ÙˆØ§Ù„ÙŠ 13000 Ù…Ø¯Ø±Ø³Ø© Ø¥Ø³Ù„Ø§Ù…ÙŠØ© Ùˆ500 ÙƒÙ„ÙŠØ© Ùˆ200 Ø¯Ø§Ø± Ø£ÙŠØªØ§Ù…. ÙˆÙ„Ù‡Ù… Ø¬Ù…Ø¹ÙŠØ§Øª ÙˆÙ…Ù†Ø¸Ù…Ø§Øª ØªÙ‚ÙˆÙ… Ø¨Ù…Ø®ØªÙ„Ù Ø§Ù„Ù…Ù‡Ø§Ù… Ø§Ù„Ø¯Ø¹ÙˆÙŠØ© ÙˆØ§Ù„Ø«Ù‚Ø§ÙÙŠØ© ÙˆØ§Ù„Ø§Ø¬ØªÙ…Ø§Ø¹ÙŠØ© Ù…Ù† Ø®Ù„Ø§Ù„ Ø§Ù„ØªÙƒÙˆÙŠÙ†Ø§Øª Ø§Ù„Ø­Ø±ÙƒÙŠØ© ÙˆØ§Ù„Ù…Ø¹Ø§Ù‡Ø¯ Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ©.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>ÙˆÙ…Ù† Ù‡Ø°Ù‡ Ø§Ù„Ù…Ø¹Ø§Ù‡Ø¯ Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ© &quot;ÙƒÙ„ÙŠØ© Ø¯Ø§Ø± Ø§Ù„Ø¹Ù„ÙˆÙ… Ù„Ù„Ø¢Ø¯Ø§Ø¨ ÙˆØ§Ù„Ø¯Ø±Ø§Ø³Ø§Øª Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ©&quot; Ø§Ù„ØªÙŠ ØªØ£Ø³Ø³Øª ØªØ­Øª Ø±Ø¹Ø§ÙŠØ© Ø¬Ù…Ø¹ÙŠØ© Ø§Ù„Ø¹Ù„Ù…Ø§Ø¡ Ù„Ø¹Ù…ÙˆÙ… ÙƒÙŠØ±Ø§Ù„Ø§ (ÙƒØ¨Ø±Ù‰ Ø§Ù„Ø¬Ù…Ø¹ÙŠØ§Øª Ø§Ù„Ø¯ÙŠÙ†ÙŠØ© ÙÙŠ ÙƒÙŠØ±Ø§Ù„Ø§ Ø¹Ù„Ù‰ Ø§Ù„Ø§Ø·Ù„Ø§Ù‚) Ø³Ù†Ø© 2004Ù… ÙˆØ³Ø¬Ù„Øª Ù„Ø¯Ù‰ Ø§Ù„Ø­ÙƒÙˆÙ…Ø© Ø¨Ø±Ù‚Ù… (102/2005)ØŒ ÙˆÙŠØ¯Ø±Ø³ ÙÙŠÙ‡Ø§ Ø­Ø§Ù„ÙŠÙ‹Ø§ Ø­ÙˆØ§Ù„ÙŠ 250 Ø·Ø§Ù„Ø¨Ù‹Ø§. ÙˆÙ‡Ø°Ø§ Ø§Ù„ØµØ±Ø­ Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠ Ø§Ù„ÙˆØ§Ù‚Ø¹ Ø¹Ù„Ù‰ Ø­Ø§ÙØ© Ù†Ù‡Ø± &quot;ØªÙˆØªØ§&quot; Ø¨Ø¨Ø§Ø±Ø§Ù„/ Ù…Ù‚Ø§Ø·Ø¹Ø© Ù…Ø§Ù„Ø§Ø¨ÙˆØ±Ø§Ù… Ø§Ù„Ù…Ø¬Ø§ÙˆØ±Ø© Ù„Ù…Ù‚Ø§Ø·Ø¹Ø© Ø¨Ø§Ù„Ø§ÙƒÙ‘Ø§Ø¯ Ø§Ù„Ù…ØªØ®Ù„ÙØ© ÙÙŠ Ø§Ù„ÙˆØ¹ÙŠ Ø§Ù„Ø«Ù‚Ø§ÙÙŠ ÙˆØ§Ù„ØªØ±Ø¨ÙˆÙŠØŒ ÙŠÙ‡Ø¯Ù Ø¥Ù„Ù‰ ØªØ®Ø±ÙŠØ¬ Ø£Ø¬ÙŠØ§Ù„ Ø¬Ø¯ÙŠØ¯Ø© Ù…Ù† Ø§Ù„Ø¹Ù„Ù…Ø§Ø¡ ÙŠØ¬Ù…Ø¹ÙˆÙ† Ø¨ÙŠÙ† Ù…Ø¹Ø§Ø±Ù Ø§Ù„ÙˆØ­ÙŠ ÙˆØ§Ù„Ø¹Ù„ÙˆÙ… Ø§Ù„Ø¥Ù†Ø³Ø§Ù†ÙŠØ© ØªØ­Øª Ø³Ù‚Ù ÙˆØ§Ø­Ø¯ØŒ ÙˆÙŠÙ†ØªØ³Ø¨ Ø¥Ù„Ù‰ &quot;ØªÙ†Ø³ÙŠÙ‚ Ø§Ù„ÙƒÙ„ÙŠØ§Øª Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ©&quot; Ø§Ù„Ø°ÙŠ Ù‡Ùˆ Ù‡ÙŠØ¦Ø© Ø¹Ù„Ù…ÙŠØ© Ø´Ø¨Ù‡ Ø¬Ø§Ù…Ø¹Ø© Ø¥Ø³Ù„Ø§Ù…ÙŠØ© ÙŠØ±Ø£Ø³Ù‡Ø§ Ø§Ù„Ø³ÙŠØ¯ Ø­ÙŠØ¯Ø± Ø¹Ù„Ù‰ Ø´Ù‡Ø§Ø¨ (Ø±Ø¦ÙŠØ³ Ø±Ø§Ø¨Ø·Ø© Ø§Ù„Ù…Ø³Ù„Ù…ÙŠÙ† Ø«Ø§Ù„Ø« Ø£ÙƒØ¨Ø± Ø§Ù„Ø§Ø­Ø²Ø§Ø¨ Ø§Ù„Ø³ÙŠØ§Ø³ÙŠØ© Ø¨Ø§Ù„ÙˆÙ„Ø§ÙŠØ© ÙˆØ§Ù„Ø²Ø¹ÙŠÙ… Ø§Ù„Ø±ÙˆØ­ÙŠ Ù„Ù…Ø³Ù„Ù…ÙŠ ÙƒÙŠØ±Ø§Ù„Ø§) ÙˆÙˆÙ‚Ø¹Øª Ø§ØªÙØ§Ù‚ÙŠØ© Ø§Ù„ØªØ¹Ø§ÙˆÙ† Ø§Ù„Ø¹Ù„Ù…ÙŠ ÙˆØ§Ù„Ø«Ù‚Ø§ÙÙŠ Ù…Ø¹ Ù…Ø¤Ø³Ø³Ø§Øª ÙˆÙ‡ÙŠØ¦Ø§Øª Ø¹Ø§Ù„Ù…ÙŠØ© Ø¹Ø¯ÙŠØ¯Ø© Ø¨Ù…Ø§ ÙÙŠÙ‡Ø§ Ø¬Ø§Ù…Ø¹Ø© Ø§Ù„Ø£Ø²Ù‡Ø± Ø§Ù„Ø´Ø±ÙŠÙ Ø¨Ø§Ù„Ù‚Ø§Ù‡Ø±Ø©.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>ØªØ¹Ø¯ Ù‡Ø°Ù‡ Ø§Ù„ÙƒÙŠØ© Ø§Ù„Ø·Ù„Ø§Ø¨ Ø¨Ø¹Ø¯ Ø§Ù„Ø«Ø§Ù†ÙˆÙŠØ© Ù„Ù„Ù…Ø§Ø¬Ø³ØªÙŠØ± ÙÙŠ Ø§Ù„Ø¯Ø±Ø§Ø³Ø§Øª Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ© ÙˆØ§Ù„Ø¹Ø±Ø¨ÙŠØ© Ø­Ø³Ø¨ Ù…Ù†Ù‡Ø¬ ØªÙ†Ø³ÙŠÙ‚ Ø§Ù„ÙƒÙ„ÙŠØ§Øª Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ©<a href="http://www.wafycic.com" target="_blank">&nbsp;</a><a href="http://www.wafycic.com/">www.wafycic.com</a>. ÙˆÙ‡Ø°Ø§ Ø§Ù„Ù…Ù†Ù‡Ø¬ Ø¹Ø¨Ø§Ø±Ø© Ø¹Ù† Ø¯ÙˆØ±Ø© ØªØ±Ø¨ÙˆÙŠØ© ØªØ¯Ø±ÙŠØ¨ÙŠØ© Ø´Ø§Ù…Ù„Ø© ÙÙŠ Ù…Ø¬Ø§Ù„ Ø§Ù„Ø¯Ø±Ø§Ø³Ø§Øª Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ© Ù…Ù‚Ø±ÙˆÙ†Ø© Ø¨Ø§Ù„Ø­ØµÙˆÙ„ Ø¹Ù„Ù‰ Ø§Ù„Ø¨ÙƒØ§Ù„ÙˆØ±ÙŠÙˆØ³ ÙÙŠ Ø¥Ø­Ø¯Ù‰ Ø§Ù„Ø¹Ù„ÙˆÙ… Ø§Ù„Ø¥Ù†Ø³Ø§Ù†ÙŠØ© Ø§Ù„ØªÙŠ ØªÙ…Ù†Ø­Ù‡Ø§ Ø¬Ø§Ù…Ø¹Ø© Ù…Ø¹ØªØ±Ù Ø¨Ù‡Ø§. ÙˆØµÙ…Ù…Øª Ù‡Ø°Ù‡ Ø§Ù„Ø¯ÙˆØ±Ø© Ø§Ù„ØªÙŠ ØªÙ…ØªØ¯ Ø«Ù…Ø§Ù†ÙŠ Ø³Ù†ÙˆØ§Øª Ù„Ø¥Ø¹Ø§Ø¯Ø© ØªØ´ÙƒÙŠÙ„ Ø¬ÙŠÙ„ Ø¬Ø¯ÙŠØ¯ ÙˆØ§Ø¹Ø¯ Ù…Ù† Ø§Ù„Ø¹Ù„Ù…Ø§Ø¡ Ø§Ù„Ù…ØªØ¹Ù…Ù‚ÙŠÙ† ÙÙŠ Ø¹Ù„ÙˆÙ… Ø§Ù„Ø´Ø±ÙŠØ¹Ø© Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ© ÙˆØ§Ù„ÙÙ‡Ù… Ø§Ù„Ù…ØªØ·ÙˆØ± Ù„Ø§Ø­ØªÙŠØ§Ø¬Ø§Øª Ø§Ù„Ù…Ø¬ØªÙ…Ø¹Ø§Øª Ø§Ù„Ù…Ø¹Ø§ØµØ±Ø©. ÙˆÙŠÙ„Ù‚Ø¨ Ø§Ù„Ù…ØªØ®Ø±Ø¬ÙˆÙ† Ø¹Ù„Ù‰ Ù‡Ø°Ø§ Ø§Ù„Ù…Ù†Ù‡Ø¬ Ø¨Ù€&quot;Ø§Ù„ÙˆØ§ÙÙŠ&quot; Ø¥Ø´Ø§Ø±Ø© Ø¥Ù„Ù‰ Ø§Ù„ØªÙƒØ§Ù…Ù„ Ø§Ù„Ù…Ø¹Ø±ÙÙŠ Ø§Ù„Ù…ØªÙ…Ù†Ù‰ Ù„Ù‡Ù… ÙˆØªÙŠÙ…Ù†Ù‹Ø§ Ø¨Ù‚ÙˆÙ„ Ø§Ù„Ù„Ù‡ Ø¬Ù„ Ø´Ø£Ù†Ù‡: Ø¥Ù†ÙŽÙ‘ Ø§Ù„ÙŽÙ‘Ø°ÙÙŠÙ†ÙŽ ÙŠÙØ¨ÙŽØ§ÙŠÙØ¹ÙÙˆÙ†ÙŽÙƒÙŽ Ø¥ÙÙ†ÙŽÙ‘Ù…ÙŽØ§ ÙŠÙØ¨ÙŽØ§ÙŠÙØ¹ÙÙˆÙ†ÙŽ Ø§Ù„Ù„ÙŽÙ‘Ù‡ÙŽ ÙŠÙŽØ¯Ù Ø§Ù„Ù„ÙŽÙ‘Ù‡Ù ÙÙŽÙˆÙ’Ù‚ÙŽ Ø£ÙŽÙŠÙ’Ø¯ÙÙŠÙ‡ÙÙ…Ù’ ÙÙŽÙ…ÙŽÙ†Ù’ Ù†ÙŽÙƒÙŽØ«ÙŽ ÙÙŽØ¥ÙÙ†ÙŽÙ‘Ù…ÙŽØ§ ÙŠÙŽÙ†Ù’ÙƒÙØ«Ù Ø¹ÙŽÙ„ÙŽÙ‰ Ù†ÙŽÙÙ’Ø³ÙÙ‡Ù ÙˆÙŽÙ…ÙŽÙ†Ù’ Ø£ÙŽÙˆÙ’ÙÙŽÙ‰ Ø¨ÙÙ…ÙŽØ§ Ø¹ÙŽØ§Ù‡ÙŽØ¯ÙŽ Ø¹ÙŽÙ„ÙŽÙŠÙ’Ù‡Ù Ø§Ù„Ù„ÙŽÙ‘Ù‡ÙŽ ÙÙŽØ³ÙŽÙŠÙØ¤Ù’ØªÙÙŠÙ‡Ù Ø£ÙŽØ¬Ù’Ø±Ù‹Ø§ Ø¹ÙŽØ¸ÙÙŠÙ…Ù‹Ø§.</p>\r\n', '<p>ÙˆÙ„Ø§ÙŠØ© ÙƒÙŠØ±Ø§Ù„Ø§ Ø§Ù„Ù‡Ù†Ø¯ÙŠØ© Ù„Ù…Ù† Ø§Ù„Ø£Ø±Ø¶ÙŠØ§Øª Ø§Ù„ØºÙ†ÙŠØ© Ø¨Ø§Ù„Ù…Ù†Ø§Ø® Ø§Ù„Ù…Ù„Ø§Ø¦Ù… Ù„Ù„Ø¯Ø¹ÙˆØ© Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ©. ÙˆÙ„Ù„Ø¥Ø³Ù„Ø§Ù… ÙˆØ§Ù„Ù…Ø³Ù„Ù…ÙŠÙ† ÙÙŠÙ‡Ø§ Ø­Ø¶ÙˆØ± ÙƒØ«ÙŠÙ ÙÙŠ Ø§Ù„Ù…Ø§Ø¶ÙŠ ÙˆØ§Ù„Ø­Ø§Ø¶Ø± Ø­ÙŠØ« ÙŠØ´ÙƒÙ„ Ø§Ù„Ù…Ø³Ù„Ù…ÙˆÙ† Ø±Ø¨Ø¹ Ø³ÙƒØ§Ù†Ù‡Ø§ ÙˆÙŠØ¯ÙŠØ±ÙˆÙ† Ø¨Ø¬Ù‡ÙˆØ¯Ù‡Ù… Ø§Ù„Ø°Ø§ØªÙŠØ© Ù…Ø¹ØªÙ…Ø¯ÙŠÙ† Ø¹Ù„Ù‰ Ø§Ù„Ø£ÙŠØ§Ø¯ÙŠ Ø§Ù„Ø³Ø®ÙŠØ© Ø£ÙƒØ«Ø± Ù…Ù† Ø£Ù„Ù Ø¯Ø±ÙˆØ³ ØªÙ‚Ù„ÙŠØ¯ÙŠØ© ÙÙŠ Ø§Ù„Ù…Ø³Ø§Ø¬Ø¯ ÙˆØ­ÙˆØ§Ù„ÙŠ 13000 Ù…Ø¯Ø±Ø³Ø© Ø¥Ø³Ù„Ø§Ù…ÙŠØ© Ùˆ500 ÙƒÙ„ÙŠØ© Ùˆ200 Ø¯Ø§Ø± Ø£ÙŠØªØ§Ù…. ÙˆÙ„Ù‡Ù… Ø¬Ù…Ø¹ÙŠØ§Øª ÙˆÙ…Ù†Ø¸Ù…Ø§Øª ØªÙ‚ÙˆÙ… Ø¨Ù…Ø®ØªÙ„Ù Ø§Ù„Ù…Ù‡Ø§Ù… Ø§Ù„Ø¯Ø¹ÙˆÙŠØ© ÙˆØ§Ù„Ø«Ù‚Ø§ÙÙŠØ© ÙˆØ§Ù„Ø§Ø¬ØªÙ…Ø§Ø¹ÙŠØ© Ù…Ù† Ø®Ù„Ø§Ù„ Ø§Ù„ØªÙƒÙˆÙŠÙ†Ø§Øª Ø§Ù„Ø­Ø±ÙƒÙŠØ© ÙˆØ§Ù„Ù…Ø¹Ø§Ù‡Ø¯ Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ©.</p>\n\n<p>&nbsp;</p>\n\n<p>ÙˆÙ…Ù† Ù‡Ø°Ù‡ Ø§Ù„Ù…Ø¹Ø§Ù‡Ø¯ Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ© &quot;ÙƒÙ„ÙŠØ© Ø¯Ø§Ø± Ø§Ù„Ø¹Ù„ÙˆÙ… Ù„Ù„Ø¢Ø¯Ø§Ø¨ ÙˆØ§Ù„Ø¯Ø±Ø§Ø³Ø§Øª Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ©&quot; Ø§Ù„ØªÙŠ ØªØ£Ø³Ø³Øª ØªØ­Øª Ø±Ø¹Ø§ÙŠØ© Ø¬Ù…Ø¹ÙŠØ© Ø§Ù„Ø¹Ù„Ù…Ø§Ø¡ Ù„Ø¹Ù…ÙˆÙ… ÙƒÙŠØ±Ø§Ù„Ø§ (ÙƒØ¨Ø±Ù‰ Ø§Ù„Ø¬Ù…Ø¹ÙŠØ§Øª Ø§Ù„Ø¯ÙŠÙ†ÙŠØ© ÙÙŠ ÙƒÙŠØ±Ø§Ù„Ø§ Ø¹Ù„Ù‰ Ø§Ù„Ø§Ø·Ù„Ø§Ù‚) Ø³Ù†Ø© 2004Ù… ÙˆØ³Ø¬Ù„Øª Ù„Ø¯Ù‰ Ø§Ù„Ø­ÙƒÙˆÙ…Ø© Ø¨Ø±Ù‚Ù… (102/2005)ØŒ ÙˆÙŠØ¯Ø±Ø³ ÙÙŠÙ‡Ø§ Ø­Ø§Ù„ÙŠÙ‹Ø§ Ø­ÙˆØ§Ù„ÙŠ 250 Ø·Ø§Ù„Ø¨Ù‹Ø§. ÙˆÙ‡Ø°Ø§ Ø§Ù„ØµØ±Ø­ Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠ Ø§Ù„ÙˆØ§Ù‚Ø¹ Ø¹Ù„Ù‰ Ø­Ø§ÙØ© Ù†Ù‡Ø± &quot;ØªÙˆØªØ§&quot; Ø¨Ø¨Ø§Ø±Ø§Ù„/ Ù…Ù‚Ø§Ø·Ø¹Ø© Ù…Ø§Ù„Ø§Ø¨ÙˆØ±Ø§Ù… Ø§Ù„Ù…Ø¬Ø§ÙˆØ±Ø© Ù„Ù…Ù‚Ø§Ø·Ø¹Ø© Ø¨Ø§Ù„Ø§ÙƒÙ‘Ø§Ø¯ Ø§Ù„Ù…ØªØ®Ù„ÙØ© ÙÙŠ Ø§Ù„ÙˆØ¹ÙŠ Ø§Ù„Ø«Ù‚Ø§ÙÙŠ ÙˆØ§Ù„ØªØ±Ø¨ÙˆÙŠØŒ ÙŠÙ‡Ø¯Ù Ø¥Ù„Ù‰ ØªØ®Ø±ÙŠØ¬ Ø£Ø¬ÙŠØ§Ù„ Ø¬Ø¯ÙŠØ¯Ø© Ù…Ù† Ø§Ù„Ø¹Ù„Ù…Ø§Ø¡ ÙŠØ¬Ù…Ø¹ÙˆÙ† Ø¨ÙŠÙ† Ù…Ø¹Ø§Ø±Ù Ø§Ù„ÙˆØ­ÙŠ ÙˆØ§Ù„Ø¹Ù„ÙˆÙ… Ø§Ù„Ø¥Ù†Ø³Ø§Ù†ÙŠØ© ØªØ­Øª Ø³Ù‚Ù ÙˆØ§Ø­Ø¯ØŒ ÙˆÙŠÙ†ØªØ³Ø¨ Ø¥Ù„Ù‰ &quot;ØªÙ†Ø³ÙŠÙ‚ Ø§Ù„ÙƒÙ„ÙŠØ§Øª Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ©&quot; Ø§Ù„Ø°ÙŠ Ù‡Ùˆ Ù‡ÙŠØ¦Ø© Ø¹Ù„Ù…ÙŠØ© Ø´Ø¨Ù‡ Ø¬Ø§Ù…Ø¹Ø© Ø¥Ø³Ù„Ø§Ù…ÙŠØ© ÙŠØ±Ø£Ø³Ù‡Ø§ Ø§Ù„Ø³ÙŠØ¯ Ø­ÙŠØ¯Ø± Ø¹Ù„Ù‰ Ø´Ù‡Ø§Ø¨ (Ø±Ø¦ÙŠØ³ Ø±Ø§Ø¨Ø·Ø© Ø§Ù„Ù…Ø³Ù„Ù…ÙŠÙ† Ø«Ø§Ù„Ø« Ø£ÙƒØ¨Ø± Ø§Ù„Ø§Ø­Ø²Ø§Ø¨ Ø§Ù„Ø³ÙŠØ§Ø³ÙŠØ© Ø¨Ø§Ù„ÙˆÙ„Ø§ÙŠØ© ÙˆØ§Ù„Ø²Ø¹ÙŠÙ… Ø§Ù„Ø±ÙˆØ­ÙŠ Ù„Ù…Ø³Ù„Ù…ÙŠ ÙƒÙŠØ±Ø§Ù„Ø§) ÙˆÙˆÙ‚Ø¹Øª Ø§ØªÙØ§Ù‚ÙŠØ© Ø§Ù„ØªØ¹Ø§ÙˆÙ† Ø§Ù„Ø¹Ù„Ù…ÙŠ ÙˆØ§Ù„Ø«Ù‚Ø§ÙÙŠ Ù…Ø¹ Ù…Ø¤Ø³Ø³Ø§Øª ÙˆÙ‡ÙŠØ¦Ø§Øª Ø¹Ø§Ù„Ù…ÙŠØ© Ø¹Ø¯ÙŠØ¯Ø© Ø¨Ù…Ø§ ÙÙŠÙ‡Ø§ Ø¬Ø§Ù…Ø¹Ø© Ø§Ù„Ø£Ø²Ù‡Ø± Ø§Ù„Ø´Ø±ÙŠÙ Ø¨Ø§Ù„Ù‚Ø§Ù‡Ø±Ø©.</p>\n\n<p>&nbsp;</p>\n\n<p>ØªØ¹Ø¯ Ù‡Ø°Ù‡ Ø§Ù„ÙƒÙŠØ© Ø§Ù„Ø·Ù„Ø§Ø¨ Ø¨Ø¹Ø¯ Ø§Ù„Ø«Ø§Ù†ÙˆÙŠØ© Ù„Ù„Ù…Ø§Ø¬Ø³ØªÙŠØ± ÙÙŠ Ø§Ù„Ø¯Ø±Ø§Ø³Ø§Øª Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ© ÙˆØ§Ù„Ø¹Ø±Ø¨ÙŠØ© Ø­Ø³Ø¨ Ù…Ù†Ù‡Ø¬ ØªÙ†Ø³ÙŠÙ‚ Ø§Ù„ÙƒÙ„ÙŠØ§Øª Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ©&nbsp;<a href="http://localhost/wafy/www.wafycic.com" target="_blank">www.wafycic.com</a>. ÙˆÙ‡Ø°Ø§ Ø§Ù„Ù…Ù†Ù‡Ø¬ Ø¹Ø¨Ø§Ø±Ø© Ø¹Ù† Ø¯ÙˆØ±Ø© ØªØ±Ø¨ÙˆÙŠØ© ØªØ¯Ø±ÙŠØ¨ÙŠØ© Ø´Ø§Ù…Ù„Ø© ÙÙŠ Ù…Ø¬Ø§Ù„ Ø§Ù„Ø¯Ø±Ø§Ø³Ø§Øª Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ© Ù…Ù‚Ø±ÙˆÙ†Ø© Ø¨Ø§Ù„Ø­ØµÙˆÙ„ Ø¹Ù„Ù‰ Ø§Ù„Ø¨ÙƒØ§Ù„ÙˆØ±ÙŠÙˆØ³ ÙÙŠ Ø¥Ø­Ø¯Ù‰ Ø§Ù„Ø¹Ù„ÙˆÙ… Ø§Ù„Ø¥Ù†Ø³Ø§Ù†ÙŠØ© Ø§Ù„ØªÙŠ ØªÙ…Ù†Ø­Ù‡Ø§ Ø¬Ø§Ù…Ø¹Ø© Ù…Ø¹ØªØ±Ù Ø¨Ù‡Ø§. ÙˆØµÙ…Ù…Øª Ù‡Ø°Ù‡ Ø§Ù„Ø¯ÙˆØ±Ø© Ø§Ù„ØªÙŠ ØªÙ…ØªØ¯ Ø«Ù…Ø§Ù†ÙŠ Ø³Ù†ÙˆØ§Øª Ù„Ø¥Ø¹Ø§Ø¯Ø© ØªØ´ÙƒÙŠÙ„ Ø¬ÙŠÙ„ Ø¬Ø¯ÙŠØ¯ ÙˆØ§Ø¹Ø¯ Ù…Ù† Ø§Ù„Ø¹Ù„Ù…Ø§Ø¡ Ø§Ù„Ù…ØªØ¹Ù…Ù‚ÙŠÙ† ÙÙŠ Ø¹Ù„ÙˆÙ… Ø§Ù„Ø´Ø±ÙŠØ¹Ø© Ø§Ù„Ø¥Ø³Ù„Ø§Ù…ÙŠØ© ÙˆØ§Ù„ÙÙ‡Ù… Ø§Ù„Ù…ØªØ·ÙˆØ± Ù„Ø§Ø­ØªÙŠØ§Ø¬Ø§Øª Ø§Ù„Ù…Ø¬ØªÙ…Ø¹Ø§Øª Ø§Ù„Ù…Ø¹Ø§ØµØ±Ø©. ÙˆÙŠÙ„Ù‚Ø¨ Ø§Ù„Ù…ØªØ®Ø±Ø¬ÙˆÙ† Ø¹Ù„Ù‰ Ù‡Ø°Ø§ Ø§Ù„Ù…Ù†Ù‡Ø¬ Ø¨Ù€&quot;Ø§Ù„ÙˆØ§ÙÙŠ&quot; Ø¥Ø´Ø§Ø±Ø© Ø¥Ù„Ù‰ Ø§Ù„ØªÙƒØ§Ù…Ù„ Ø§Ù„Ù…Ø¹Ø±ÙÙŠ Ø§Ù„Ù…ØªÙ…Ù†Ù‰ Ù„Ù‡Ù… ÙˆØªÙŠÙ…Ù†Ù‹Ø§ Ø¨Ù‚ÙˆÙ„ Ø§Ù„Ù„Ù‡ Ø¬Ù„ Ø´Ø£Ù†Ù‡: Ø¥Ù†ÙŽÙ‘ Ø§Ù„ÙŽÙ‘Ø°ÙÙŠÙ†ÙŽ ÙŠÙØ¨ÙŽØ§ÙŠÙØ¹ÙÙˆÙ†ÙŽÙƒÙŽ Ø¥ÙÙ†ÙŽÙ‘Ù…ÙŽØ§ ÙŠÙØ¨ÙŽØ§ÙŠÙØ¹ÙÙˆÙ†ÙŽ Ø§Ù„Ù„ÙŽÙ‘Ù‡ÙŽ ÙŠÙŽØ¯Ù Ø§Ù„Ù„ÙŽÙ‘Ù‡Ù ÙÙŽÙˆÙ’Ù‚ÙŽ Ø£ÙŽÙŠÙ’Ø¯ÙÙŠÙ‡ÙÙ…Ù’ ÙÙŽÙ…ÙŽÙ†Ù’ Ù†ÙŽÙƒÙŽØ«ÙŽ ÙÙŽØ¥ÙÙ†ÙŽÙ‘Ù…ÙŽØ§ ÙŠÙŽÙ†Ù’ÙƒÙØ«Ù Ø¹ÙŽÙ„ÙŽÙ‰ Ù†ÙŽÙÙ’Ø³ÙÙ‡Ù ÙˆÙŽÙ…ÙŽÙ†Ù’ Ø£ÙŽÙˆÙ’ÙÙŽÙ‰ Ø¨ÙÙ…ÙŽØ§ Ø¹ÙŽØ§Ù‡ÙŽØ¯ÙŽ Ø¹ÙŽÙ„ÙŽÙŠÙ’Ù‡Ù Ø§Ù„Ù„ÙŽÙ‘Ù‡ÙŽ ÙÙŽØ³ÙŽÙŠÙØ¤Ù’ØªÙÙŠÙ‡Ù Ø£ÙŽØ¬Ù’Ø±Ù‹Ø§ Ø¹ÙŽØ¸ÙÙŠÙ…Ù‹Ø§.</p>\n', 'AboutUs_Arb_731109968245066002_270x182.png', 'https://www.youtube.com/embed/UiN4YeR8PTY', 'Ø§Ù„Ù†Ø¸Ø§Ù… Ø§Ù„Ø¯Ø±Ø§Ø³ÙŠ', 'Ø§Ù„Ù…ÙŠØ²Ø§Øª', 'Ø§Ù„Ø¨Ù†ÙŠØ© Ø§Ù„ØªØ­ØªÙŠØ©', 'Ø§ØªØ­Ø§Ø¯ Ø§Ù„Ø·Ù„Ø§Ø¨', 'Ø§ØªØ­Ø§Ø¯ Ø§Ù„Ø·Ù„Ø§Ø¨', 'Ø§ØªØ­Ø§Ø¯ Ø§Ù„Ø·Ù„Ø§Ø¨', 'ØªÙ… ØªÙ‚Ø³ÙŠÙ… Ø§Ù„Ù…Ø±Ø§Ø­Ù„ Ø§Ù„Ø¯Ø±Ø§Ø³ÙŠØ© Ø¨Ù…Ù†Ù‡Ø¬ "Ø§Ù„ÙˆØ§ÙÙŠ" Ø¥Ù„Ù‰ Ø«Ù„Ø§Ø«Ø© Ù…Ø±Ø§Ø­Ù„ Ø¯Ø±Ø§Ø³ÙŠØ©: Ù…Ø±Ø­Ù„Ø© Ø§Ù„ØªÙ…Ù‡ÙŠØ¯ØŒ Ù…Ø±Ø­Ù„Ø© Ø§Ù„Ø¥Ø¬Ø§Ø²Ø© Ø§Ù„Ø¹Ø§Ù„ÙŠØ©, Ù…Ø±Ø­Ù„Ø© Ø§Ù„Ø¯Ø±Ø§Ø³Ø§Øª Ø§Ù„Ø¹Ù„ÙŠØ§', 'ØªØ­Ø±Øµ Ù‡Ø°Ù‡ Ø§Ù„ÙƒÙ„ÙŠØ© Ø¹Ù„Ù‰ Ø£Ù† ØªÙƒÙˆÙ† Ù‡ÙŠØ¦Ø© Ø§Ù„ØªØ¯Ø±ÙŠØ³ Ø¨Ù‡Ø§ Ø¹Ø§Ù„ÙŠØ© Ø§Ù„ÙƒÙØ§Ø¡Ø© Ù…Ù† Ø£ÙˆÙ„Ø¦Ùƒ Ø§Ù„Ù…Ø´Ù‡ÙˆØ¯ Ù„Ù‡Ù… Ø¨Ø§Ù„Ø¹Ù„Ù… ÙˆØ§Ù„Ø¹Ù…Ù„ â€“ Ø§Ù„Ù…ØªØ®Ø±Ø¬ÙŠÙ† Ø¹Ù„Ù‰ Ø£Ø¹Ø±Ù‚', 'ØªÙˆÙØ± Ù‡Ø°Ù‡ Ø§Ù„Ù…Ø¤Ø³Ø³Ø© Ø§Ù„Ø®ÙŠØ±ÙŠØ© Ù„Ù„Ø·Ù„Ø§Ø¨ Ù‚Ø§Ø¹Ø© Ø§Ù„Ù…Ø­Ø§Ø¶Ø±Ø§Øª Ø§Ù„ÙˆØ§Ø³Ø¹Ø©, ÙˆØ§Ù„Ù…ÙƒØªØ¨Ø© Ø§Ù„ÙˆØ§Ø³Ø¹Ø© Ø§Ù„Ù…Ø²ÙˆØ¯Ø© Ø¨Ø§Ù„ÙƒØªØ¨ Ø§Ù„Ø¹Ø±Ø¨ÙŠØ© ÙˆØ§Ù„Ø¥Ù†Ø¬Ù„ÙŠØ²ÙŠØ© ÙˆØ§Ù„Ù„ØºØ© Ø§Ù„Ø£Ù…', 'ÙŠÙ‚ÙˆÙ… Ø§ØªØ­Ø§Ø¯ Ø§Ù„Ø·Ù„Ø§Ø¨ (Ù…Ù†Ø¸Ù…Ø© Ø§Ù„Ù…Ø±Ø§Ø³ Ø§Ù„Ø·Ù„Ø§Ø¨ÙŠØ©) Ø¨ØªØ¯Ø±ÙŠØ¨ Ø§Ù„Ø·Ù„Ø§Ø¨ ÙÙŠ Ù…Ù‡Ø§Ø±Ø§Øª Ø§Ù„Ø¹Ø±Ø¶ Ù…Ø«Ù„ Ø§Ù„Ø®Ø·Ø§Ø¨Ø© ÙˆØ§Ù„Ù…Ù†Ø§Ø¸Ø±Ø§Øª', 'ÙŠÙ‚ÙˆÙ… Ø§ØªØ­Ø§Ø¯ Ø§Ù„Ø·Ù„Ø§Ø¨ (Ù…Ù†Ø¸Ù…Ø© Ø§Ù„Ù…Ø±Ø§Ø³ Ø§Ù„Ø·Ù„Ø§Ø¨ÙŠØ©) Ø¨ØªØ¯Ø±ÙŠØ¨ Ø§Ù„Ø·Ù„Ø§Ø¨ ÙÙŠ Ù…Ù‡Ø§Ø±Ø§Øª Ø§Ù„Ø¹Ø±Ø¶ Ù…Ø«Ù„ Ø§Ù„Ø®Ø·Ø§Ø¨Ø© ÙˆØ§Ù„Ù…Ù†Ø§Ø¸Ø±Ø§Øª ÙˆØ¥Ø¹Ø¯Ø§Ø¯ Ø§Ù„Ù…Ù‚Ø§Ù„Ø§Øª', 'ÙŠÙ‚ÙˆÙ… Ø§ØªØ­Ø§Ø¯ Ø§Ù„Ø·Ù„Ø§Ø¨ (Ù…Ù†Ø¸Ù…Ø© Ø§Ù„Ù…Ø±Ø§Ø³ Ø§Ù„Ø·Ù„Ø§Ø¨ÙŠØ©) Ø¨ØªØ¯Ø±ÙŠØ¨ Ø§Ù„Ø·Ù„Ø§Ø¨ ÙÙŠ Ù…Ù‡Ø§Ø±Ø§Øª Ø§Ù„Ø¹Ø±Ø¶ Ù…Ø«Ù„ Ø§Ù„Ø®Ø·Ø§Ø¨Ø© ÙˆØ§Ù„Ù…Ù†Ø§Ø¸Ø±Ø§Øª ÙˆØ¥Ø¹Ø¯Ø§Ø¯ Ø§Ù„Ù…Ù‚Ø§Ù„Ø§Øª', 'AboutUs_Arb_1048168130786577934_Objective.png', 'AboutUs_Arb_1333813093944504570_Leadership.png', 'AboutUs_Arb_124361857118208312_Discipline.png', 'AboutUs_Arb_6592468941357093286_Exam.png', 'AboutUs_Arb_199237155259868935_Exam.png', 'AboutUs_Arb_677664496280093872_Exam.png'),
(3, '<p>à´®à´²à´ªàµà´ªàµà´±à´‚ à´œà´¿à´²àµà´²à´¯à´¿à´²àµ† à´ªàµ†à´°à´¿à´¨àµà´¤à´²àµ&zwj;à´®à´£àµà´£ à´¨à´¿à´¨àµà´¨àµ à´Žà´Ÿàµà´Ÿàµ à´•à´¿. à´®à´¿. à´…à´•à´²àµ† à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸ à´ªà´°à´®à´¾à´¯à´¿ à´ªà´¿à´¨àµà´¨à´¾à´•àµà´•à´‚ à´¨à´¿à´²àµ&zwj;à´•àµà´•àµà´¨àµà´¨ <strong>à´ªà´¾à´²à´•àµà´•à´¾à´Ÿàµ à´œà´¿à´²àµà´²à´¯àµ‹à´Ÿàµ à´šàµ‡à´°àµ&zwj;à´¨àµà´¨àµàµ à´ªà´¾à´±à´²àµ&zwj; à´®à´¹à´²àµà´²à´¿à´²àµ&zwj; à´¤àµ‚à´¤à´ªàµà´ªàµà´´à´•àµà´•àµ à´¸à´®àµ€à´ªà´‚ 10/4/2006 à´®àµà´¤à´²àµ&zwj; à´ªàµà´°à´µà´°àµ&zwj;à´¤àµà´¤à´¿à´šàµà´šàµ à´µà´°àµà´¨àµà´¨ à´¦àµ€à´¨àµ€ à´•à´²à´¾à´²à´¯à´®à´¾à´£àµ à´¦à´¾à´±àµà´²àµ&zwj; à´‰à´²àµ‚à´‚ à´‡à´¸àµà´²à´¾à´®à´¿à´•àµ à´†à´¨àµà´±àµ à´†à´°àµ&zwj;à´Ÿàµ&zwnj;à´¸àµ à´•àµ‹à´³àµ‡à´œàµ. 7/6/2004 à´¨àµ 28 à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj;</strong>à´•àµà´•àµ à´ªàµà´°à´µàµ‡à´¶à´¨à´‚ à´¨à´²àµ&zwj;à´•à´¿ à´¤àµ‚à´¤ à´“à´°àµ&zwj;à´«à´¨àµ‡à´œàµ à´•à´¾à´®àµà´ªà´¸à´¿à´²àµ&zwj; à´†à´°à´‚à´­à´¿à´šàµà´š à´®à´¹à´¤àµ à´¸àµà´¥à´¾à´ªà´¨à´‚ à´ªàµà´°à´µà´°àµ&zwj;à´¤àµà´¤à´¨ à´¸àµ—à´•à´°àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´®à´¾à´£àµ à´ªà´¾à´±à´²à´¿à´²àµ&zwj; à´¸àµà´µà´¨àµà´¤à´‚ à´•à´¾à´®àµà´ªà´¸à´¿à´²àµ‡à´•àµà´•àµ à´®à´¾à´±à´¿à´¯à´¤àµ. à´‡à´ªàµà´ªàµ‹à´³àµ&zwj; à´ˆ à´¸àµà´¥à´¾à´ªà´¨à´‚ 250 à´²à´§à´¿à´•à´‚ à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj;à´•àµà´•àµ à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸à´‚, à´­à´•àµà´·à´£à´‚, à´¤à´¾à´®à´¸à´‚ à´Žà´¨àµà´¨à´¿à´µ à´¸àµ—à´œà´¨àµà´¯à´®à´¾à´¯à´¿ à´¨à´²àµ&zwj;à´•àµà´¨àµà´¨ à´•àµ‡à´°à´³à´¤àµà´¤à´¿à´²àµ† à´®à´¿à´•à´šàµà´š à´µà´¾à´«à´¿ à´¸àµà´¥à´¾à´ªà´¨à´®à´¾à´¯à´¿ à´®à´¾à´±à´¿à´¯à´¿à´°à´¿à´•àµà´•àµà´¨àµà´¨àµ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>à´¸à´®àµ€à´ª à´•à´¾à´²à´‚ à´µà´°àµ† à´®à´¤ à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸ à´°à´‚à´—à´¤àµà´¤àµ à´‰à´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨ à´µà´¿à´Ÿà´µàµ à´¨à´¿à´•à´¤àµà´¤à´¿à´•àµà´•àµŠà´£àµà´Ÿàµ à´•à´Ÿà´¨àµà´¨àµ à´µà´¨àµà´¨ à´¸à´®à´¨àµà´µà´¯ à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸ à´•àµ‹à´´àµ&zwnj;à´¸àµ à´µà´¾à´«à´¿ à´•à´°à´¿à´•àµà´•àµà´²à´®à´¾à´£àµ à´¸àµà´¥à´¾à´ªà´¨à´‚ à´ªà´¿à´¨àµà´¤àµà´Ÿà´°àµà´¨àµà´¨à´¤àµ. à´Žà´¸àµ. à´Žà´¸àµ. à´Žà´²àµ&zwj;. à´¸à´¿. à´ªà´°àµ€à´•àµà´·à´¯à´¿à´²àµ&zwj; à´‰à´¨àµà´¨à´¤ à´ªà´ à´¨à´¤àµà´¤à´¿à´¨àµ à´…à´°àµ&zwj;à´¹à´¤ à´¨àµ‡à´Ÿà´¿à´¯ à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj;à´•àµà´•àµ à´¸à´¿. à´. à´¸à´¿ à´¨à´Ÿà´¤àµà´¤à´¾à´±àµà´³àµà´³ à´ªàµà´°à´µàµ‡à´¶à´¨ à´ªà´°àµ€à´•àµà´·à´¯à´¿à´²àµ&zwj; à´¯àµ‹à´—àµà´¯à´¤ à´¨àµ‡à´Ÿàµà´¨àµà´¨ à´ªàµà´°à´—à´¤àµà´­à´°à´¾à´¯ à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj;à´•àµà´•à´¾à´£àµ à´ªàµà´°à´µàµ‡à´¶à´¨à´‚ à´¨à´²àµ&zwj;à´•àµà´¨àµà´¨à´¤àµ. à´•àµ‡à´°à´³à´¤àµà´¤à´¿à´²àµ&zwj; à´‡à´¨àµà´¨àµ à´¸à´®à´¸àµà´¤à´•àµà´•àµ à´•àµ€à´´à´¿à´²àµ&zwj; à´¨à´¾à´²àµ&zwj;à´ªà´¤à´¿à´²àµ&zwj; à´ªà´°à´‚ à´µà´¾à´«à´¿ à´•àµ‹à´³àµ‡à´œàµà´•à´³à´¿à´²àµà´‚ à´µà´¨à´¿à´¤à´•à´³àµ&zwj;à´•àµà´•àµ à´®à´¾à´¤àµà´°à´®àµà´³àµà´³ à´…à´žàµà´šàµàµ à´µà´«à´¿à´¯àµà´¯ à´•àµ‹à´³àµ‡à´œàµà´•à´³à´¿à´²àµà´®à´¾à´¯à´¿ à´…à´¯àµà´¯à´¾à´¯à´¿à´°à´¤àµà´¤àµ‹à´³à´‚ à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj; à´ªà´ à´¿à´¤à´¾à´•àµà´•à´³à´¾à´¯àµà´£àµà´Ÿàµ. à´µà´¾à´«à´¿à´•àµà´•àµ à´‡à´¨àµà´±à´°àµ&zwj;à´¨à´¾à´·à´£à´²àµ&zwj; à´¯àµ‚à´£à´¿à´µàµ‡à´´àµ&zwnj;à´¸à´¿à´±àµà´±àµ€à´¸àµ à´²àµ€à´—à´¿à´²àµ&zwj; à´…à´‚à´—à´¤àµà´µà´µàµà´‚ à´µà´¿à´µà´¿à´§ à´µà´¿à´¦àµ‡à´¶ à´¸àµà´µà´¦àµ‡à´¶ à´¯àµ‚à´£à´¿à´µàµ‡à´´àµ&zwnj;à´¸à´¿à´±àµà´±à´¿à´•à´³àµà´Ÿàµ† à´…à´‚à´—àµ€à´•à´¾à´°à´µàµà´‚ (MOU) à´‰à´£àµà´Ÿàµ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>à´ªàµà´°à´¿à´ªà´±àµ‡à´±àµà´±à´±à´¿ à´°à´£àµà´Ÿàµ à´µà´°àµ&zwj;à´·à´‚ à´¡à´¿à´—àµà´°à´¿ à´¨à´¾à´²àµ à´µà´°àµ&zwj;à´·à´‚ à´ªà´¿. à´œà´¿. à´°à´£àµà´Ÿàµ à´µà´°àµ&zwj;à´·à´‚ à´Žà´¨àµà´¨à´¿à´™àµà´™à´¨àµ† 16 à´¸àµ†à´®à´¸àµà´±àµà´±à´±àµà´•à´³à´¾à´¯à´¾à´£àµ à´•àµ‡à´´àµ&zwnj;à´¸àµ à´•àµà´°à´®àµ€à´•à´°à´¿à´šàµà´šà´¿à´Ÿàµà´Ÿàµà´³àµà´³à´¤àµ. à´ªà´¿. à´œà´¿. à´˜à´Ÿàµà´Ÿà´¤àµà´¤à´¿à´²àµ&zwj; à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj;à´•àµà´•àµ à´¯à´¥àµ‡à´·àµà´Ÿà´‚ à´…à´µà´°àµ&zwj; à´‰à´¦àµà´§àµ‡à´¶à´¿à´•àµà´•àµà´¨àµà´¨ à´µà´¿à´·à´¯à´™àµà´™à´³àµ&zwj; à´¤àµ†à´°àµ†à´žàµà´žàµ†à´Ÿàµà´¤àµà´¤àµ à´ªà´ à´¿à´•àµà´•à´¾à´¨àµà´³àµà´³ à´…à´µà´¸à´°à´®àµà´£àµà´Ÿàµ. à´…à´¥à´µà´¾ à´†à´§àµà´¨à´¿à´• à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸ à´¸à´®àµà´ªàµà´°à´¦à´¾à´¯à´‚ à´ªàµ‹à´²àµ† (CBCUSS) à´¦àµ€à´¨àµ€ à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸à´‚ à´ªàµà´¨à´°à´¾à´µà´¿à´·àµ&zwnj;à´•à´°à´¿à´šàµà´šà´¿à´°à´¿à´•àµà´•àµà´•à´¯à´¾à´£à´¿à´µà´¿à´Ÿàµ†.</p>\r\n', '<p>à´®à´²à´ªàµà´ªàµà´±à´‚ à´œà´¿à´²àµà´²à´¯à´¿à´²àµ† à´ªàµ†à´°à´¿à´¨àµà´¤à´²àµ&zwj;à´®à´£àµà´£ à´¨à´¿à´¨àµà´¨àµ à´Žà´Ÿàµà´Ÿàµ à´•à´¿. à´®à´¿. à´…à´•à´²àµ† à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸ à´ªà´°à´®à´¾à´¯à´¿ à´ªà´¿à´¨àµà´¨à´¾à´•àµà´•à´‚ à´¨à´¿à´²àµ&zwj;à´•àµà´•àµà´¨àµà´¨ à´ªà´¾à´²à´•àµà´•à´¾à´Ÿàµ à´œà´¿à´²àµà´²à´¯àµ‹à´Ÿàµ à´šàµ‡à´°àµ&zwj;à´¨àµà´¨àµàµ à´ªà´¾à´±à´²àµ&zwj; à´®à´¹à´²àµà´²à´¿à´²àµ&zwj; à´¤àµ‚à´¤à´ªàµà´ªàµà´´à´•àµà´•àµ à´¸à´®àµ€à´ªà´‚ 10/4/2006 à´®àµà´¤à´²àµ&zwj; à´ªàµà´°à´µà´°àµ&zwj;à´¤àµà´¤à´¿à´šàµà´šàµ à´µà´°àµà´¨àµà´¨ à´¦àµ€à´¨àµ€ à´•à´²à´¾à´²à´¯à´®à´¾à´£àµ à´¦à´¾à´±àµà´²àµ&zwj; à´‰à´²àµ‚à´‚ à´‡à´¸àµà´²à´¾à´®à´¿à´•àµ à´†à´¨àµà´±àµ à´†à´°àµ&zwj;à´Ÿàµ&zwnj;à´¸àµ à´•àµ‹à´³àµ‡à´œàµ. 7/6/2004 à´¨àµ 28 à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj;à´•àµà´•àµ à´ªàµà´°à´µàµ‡à´¶à´¨à´‚ à´¨à´²àµ&zwj;à´•à´¿ à´¤àµ‚à´¤ à´“à´°àµ&zwj;à´«à´¨àµ‡à´œàµ à´•à´¾à´®àµà´ªà´¸à´¿à´²àµ&zwj; à´†à´°à´‚à´­à´¿à´šàµà´š à´®à´¹à´¤àµ à´¸àµà´¥à´¾à´ªà´¨à´‚ à´ªàµà´°à´µà´°àµ&zwj;à´¤àµà´¤à´¨ à´¸àµ—à´•à´°àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´®à´¾à´£àµ à´ªà´¾à´±à´²à´¿à´²àµ&zwj; à´¸àµà´µà´¨àµà´¤à´‚ à´•à´¾à´®àµà´ªà´¸à´¿à´²àµ‡à´•àµà´•àµ à´®à´¾à´±à´¿à´¯à´¤àµ. à´‡à´ªàµà´ªàµ‹à´³àµ&zwj; à´ˆ à´¸àµà´¥à´¾à´ªà´¨à´‚ 250 à´²à´§à´¿à´•à´‚ à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj;à´•àµà´•àµ à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸à´‚, à´­à´•àµà´·à´£à´‚, à´¤à´¾à´®à´¸à´‚ à´Žà´¨àµà´¨à´¿à´µ à´¸àµ—à´œà´¨àµà´¯à´®à´¾à´¯à´¿ à´¨à´²àµ&zwj;à´•àµà´¨àµà´¨ à´•àµ‡à´°à´³à´¤àµà´¤à´¿à´²àµ† à´®à´¿à´•à´šàµà´š à´µà´¾à´«à´¿ à´¸àµà´¥à´¾à´ªà´¨à´®à´¾à´¯à´¿ à´®à´¾à´±à´¿à´¯à´¿à´°à´¿à´•àµà´•àµà´¨àµà´¨àµ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>à´¸à´®àµ€à´ª à´•à´¾à´²à´‚ à´µà´°àµ† à´®à´¤ à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸ à´°à´‚à´—à´¤àµà´¤àµ à´‰à´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨ à´µà´¿à´Ÿà´µàµ à´¨à´¿à´•à´¤àµà´¤à´¿à´•àµà´•àµŠà´£àµà´Ÿàµ à´•à´Ÿà´¨àµà´¨àµ à´µà´¨àµà´¨ à´¸à´®à´¨àµà´µà´¯ à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸ à´•àµ‹à´´àµ&zwnj;à´¸àµ à´µà´¾à´«à´¿ à´•à´°à´¿à´•àµà´•àµà´²à´®à´¾à´£àµ à´¸àµà´¥à´¾à´ªà´¨à´‚ à´ªà´¿à´¨àµà´¤àµà´Ÿà´°àµà´¨àµà´¨à´¤àµ. à´Žà´¸àµ. à´Žà´¸àµ. à´Žà´²àµ&zwj;. à´¸à´¿. à´ªà´°àµ€à´•àµà´·à´¯à´¿à´²àµ&zwj; à´‰à´¨àµà´¨à´¤ à´ªà´ à´¨à´¤àµà´¤à´¿à´¨àµ à´…à´°àµ&zwj;à´¹à´¤ à´¨àµ‡à´Ÿà´¿à´¯ à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj;à´•àµà´•àµ à´¸à´¿. à´. à´¸à´¿ à´¨à´Ÿà´¤àµà´¤à´¾à´±àµà´³àµà´³ à´ªàµà´°à´µàµ‡à´¶à´¨ à´ªà´°àµ€à´•àµà´·à´¯à´¿à´²àµ&zwj; à´¯àµ‹à´—àµà´¯à´¤ à´¨àµ‡à´Ÿàµà´¨àµà´¨ à´ªàµà´°à´—à´¤àµà´­à´°à´¾à´¯ à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj;à´•àµà´•à´¾à´£àµ à´ªàµà´°à´µàµ‡à´¶à´¨à´‚ à´¨à´²àµ&zwj;à´•àµà´¨àµà´¨à´¤àµ. à´•àµ‡à´°à´³à´¤àµà´¤à´¿à´²àµ&zwj; à´‡à´¨àµà´¨àµ à´¸à´®à´¸àµà´¤à´•àµà´•àµ à´•àµ€à´´à´¿à´²àµ&zwj; à´¨à´¾à´²àµ&zwj;à´ªà´¤à´¿à´²àµ&zwj; à´ªà´°à´‚ à´µà´¾à´«à´¿ à´•àµ‹à´³àµ‡à´œàµà´•à´³à´¿à´²àµà´‚ à´µà´¨à´¿à´¤à´•à´³àµ&zwj;à´•àµà´•àµ à´®à´¾à´¤àµà´°à´®àµà´³àµà´³ à´…à´žàµà´šàµàµ à´µà´«à´¿à´¯àµà´¯ à´•àµ‹à´³àµ‡à´œàµà´•à´³à´¿à´²àµà´®à´¾à´¯à´¿ à´…à´¯àµà´¯à´¾à´¯à´¿à´°à´¤àµà´¤àµ‹à´³à´‚ à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj; à´ªà´ à´¿à´¤à´¾à´•àµà´•à´³à´¾à´¯àµà´£àµà´Ÿàµ. à´µà´¾à´«à´¿à´•àµà´•àµ à´‡à´¨àµà´±à´°àµ&zwj;à´¨à´¾à´·à´£à´²àµ&zwj; à´¯àµ‚à´£à´¿à´µàµ‡à´´àµ&zwnj;à´¸à´¿à´±àµà´±àµ€à´¸àµ à´²àµ€à´—à´¿à´²àµ&zwj; à´…à´‚à´—à´¤àµà´µà´µàµà´‚ à´µà´¿à´µà´¿à´§ à´µà´¿à´¦àµ‡à´¶ à´¸àµà´µà´¦àµ‡à´¶ à´¯àµ‚à´£à´¿à´µàµ‡à´´àµ&zwnj;à´¸à´¿à´±àµà´±à´¿à´•à´³àµà´Ÿàµ† à´…à´‚à´—àµ€à´•à´¾à´°à´µàµà´‚ (MOU) à´‰à´£àµà´Ÿàµ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>à´ªàµà´°à´¿à´ªà´±àµ‡à´±àµà´±à´±à´¿ à´°à´£àµà´Ÿàµ à´µà´°àµ&zwj;à´·à´‚ à´¡à´¿à´—àµà´°à´¿ à´¨à´¾à´²àµ à´µà´°àµ&zwj;à´·à´‚ à´ªà´¿. à´œà´¿. à´°à´£àµà´Ÿàµ à´µà´°àµ&zwj;à´·à´‚ à´Žà´¨àµà´¨à´¿à´™àµà´™à´¨àµ† 16 à´¸àµ†à´®à´¸àµà´±àµà´±à´±àµà´•à´³à´¾à´¯à´¾à´£àµ à´•àµ‡à´´àµ&zwnj;à´¸àµ à´•àµà´°à´®àµ€à´•à´°à´¿à´šàµà´šà´¿à´Ÿàµà´Ÿàµà´³àµà´³à´¤àµ. à´ªà´¿. à´œà´¿. à´˜à´Ÿàµà´Ÿà´¤àµà´¤à´¿à´²àµ&zwj; à´µà´¿à´¦àµà´¯à´¾à´°àµ&zwj;à´¤àµà´¥à´¿à´•à´³àµ&zwj;à´•àµà´•àµ à´¯à´¥àµ‡à´·àµà´Ÿà´‚ à´…à´µà´°àµ&zwj; à´‰à´¦àµà´§àµ‡à´¶à´¿à´•àµà´•àµà´¨àµà´¨ à´µà´¿à´·à´¯à´™àµà´™à´³àµ&zwj; à´¤àµ†à´°àµ†à´žàµà´žàµ†à´Ÿàµà´¤àµà´¤àµ à´ªà´ à´¿à´•àµà´•à´¾à´¨àµà´³àµà´³ à´…à´µà´¸à´°à´®àµà´£àµà´Ÿàµ. à´…à´¥à´µà´¾ à´†à´§àµà´¨à´¿à´• à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸ à´¸à´®àµà´ªàµà´°à´¦à´¾à´¯à´‚ à´ªàµ‹à´²àµ† (CBCUSS) à´¦àµ€à´¨àµ€ à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸à´‚ à´ªàµà´¨à´°à´¾à´µà´¿à´·àµ&zwnj;à´•à´°à´¿à´šàµà´šà´¿à´°à´¿à´•àµà´•àµà´•à´¯à´¾à´£à´¿à´µà´¿à´Ÿàµ†.</p>\r\n', 'AboutUs_Mal_109244783338900740_270x182_03.jpg', 'https://www.youtube.com/embed/IU955puHeCY', 'à´²à´•àµà´·àµà´¯à´‚', 'à´¨àµ‡à´¤àµƒà´¤àµà´µà´‚', 'à´…à´šàµà´šà´Ÿà´•àµà´•à´‚', 'à´ªà´°àµ€à´•àµà´·', 'à´ªà´°àµ€à´•àµà´·', 'à´ªà´°àµ€à´•àµà´·', 'à´‡à´¸àµâ€Œà´²à´¾à´®à´¿à´• à´ªàµà´°à´¬àµ‹à´§à´¨ à´ªàµà´°à´µà´°àµâ€à´¤àµà´¤à´¨à´™àµà´™à´³àµâ€ à´•à´¾à´²àµ‹à´šà´¿à´¤à´®à´¾à´¯à´¿ à´¨à´Ÿà´¤àµà´¤à´¾à´¨àµâ€ à´ªàµà´°à´¾à´ªàµà´¤à´°à´¾à´¯ à´®à´¤à´®àµ‚à´²àµà´¯à´™àµà´™à´³àµâ€ à´‰à´³àµâ€à´•àµà´•àµŠà´³àµà´³àµà´¨àµà´¨ ...', 'à´ªà´¾à´£à´•àµà´•à´¾à´Ÿàµ à´¸à´¯àµà´¯à´¿à´¦àµ à´¹àµˆà´¦à´±à´²à´¿ à´¶à´¿à´¹à´¾à´¬àµ à´¤à´™àµà´™à´³àµà´Ÿàµ† à´¨àµ‡à´¤àµƒà´¤àµà´µà´¤àµà´¤à´¿à´²àµà´³àµà´³ à´‡à´¸àµà´²à´¾à´®à´¿à´• à´•àµ‡à´³àµ‡à´œàµà´•à´³àµà´Ÿàµ† à´•àµ‚à´Ÿàµà´Ÿà´¾à´¯à´®à´¯à´¾à´£àµ ...', '75% à´®à´¤ à´­àµ—à´¤à´¿à´• à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸à´¤àµà´¤à´¿à´¨àµà´‚ 25% à´­àµ—à´¤à´¿à´• à´µà´¿à´¦àµà´¯à´¾à´­àµà´¯à´¾à´¸à´¤àµà´¤à´¿à´¨àµà´‚ à´¸à´®à´¯à´‚ à´•àµà´°à´®à´ªàµà´ªàµ†à´Ÿàµà´¤àµà´¤à´¿à´¯à´¿à´Ÿàµà´Ÿàµà´³àµà´³ ...', 'à´¨à´¿à´°à´¨àµà´¤à´° à´®àµ‚à´²àµà´¯ à´¨à´¿à´°àµâ€à´£àµà´£à´¯à´¤àµà´¤à´¿à´¨àµà´±àµ† à´­à´¾à´—à´®à´¾à´¯à´¿ à´¨à´Ÿà´•àµà´•àµà´¨àµà´¨ à´…à´¸àµˆà´¨àµâ€à´®àµ†à´¨àµà´±àµ, à´¸àµ†à´®à´¿à´¨à´¾à´°àµâ€, à´†à´²à´¿à´¯ à´˜à´Ÿàµà´Ÿà´¤àµà´¤à´¿à´²àµ† ...', 'à´¨à´¿à´°à´¨àµà´¤à´° à´®àµ‚à´²àµà´¯ à´¨à´¿à´°àµâ€à´£àµà´£à´¯à´¤àµà´¤à´¿à´¨àµà´±àµ† à´­à´¾à´—à´®à´¾à´¯à´¿ à´¨à´Ÿà´•àµà´•àµà´¨àµà´¨ à´…à´¸àµˆà´¨àµâ€à´®àµ†à´¨àµà´±àµ, à´¸àµ†à´®à´¿à´¨à´¾à´°àµâ€, à´†à´²à´¿à´¯ à´˜à´Ÿàµà´Ÿà´¤àµà´¤à´¿à´²àµ† ...', 'à´¨à´¿à´°à´¨àµà´¤à´° à´®àµ‚à´²àµà´¯ à´¨à´¿à´°àµâ€à´£àµà´£à´¯à´¤àµà´¤à´¿à´¨àµà´±àµ† à´­à´¾à´—à´®à´¾à´¯à´¿ à´¨à´Ÿà´•àµà´•àµà´¨àµà´¨ à´…à´¸àµˆà´¨àµâ€à´®àµ†à´¨àµà´±àµ, à´¸àµ†à´®à´¿à´¨à´¾à´°àµâ€, à´†à´²à´¿à´¯ à´˜à´Ÿàµà´Ÿà´¤àµà´¤à´¿à´²àµ† ...', 'AboutUs_Mal_424336402317832744_Objective.png', 'AboutUs_Mal_1113103088175225422_Leadership.png', 'AboutUs_Mal_418656207491853266_Discipline.png', 'AboutUs_Mal_781069866733433684_Exam.png', 'AboutUs_Mal_423174544962319727_Exam.png', 'AboutUs_Mal_308107561449639089_Exam.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_article`
--

CREATE TABLE `tbl_article` (
  `ArticleId` int(11) NOT NULL,
  `Heading` varchar(300) NOT NULL,
  `Content` varchar(5000) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Approve` int(1) NOT NULL,
  `ImageOrVideo` int(1) NOT NULL,
  `Image` varchar(300) NOT NULL,
  `Video` varchar(300) NOT NULL,
  `Author` varchar(100) NOT NULL,
  `EUserId` int(11) NOT NULL,
  `EDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_article`
--

INSERT INTO `tbl_article` (`ArticleId`, `Heading`, `Content`, `CategoryId`, `Date`, `Approve`, `ImageOrVideo`, `Image`, `Video`, `Author`, `EUserId`, `EDate`) VALUES
(1, 'HOW TO STABLISH YOUR OFFICE ?', '<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. Lacus eu nulla ante lorem, viverra pretium ipsum etiam, platea nec purus malesuada id leo, aliquam metus, ac velit viverra. Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor. Magna convallis mi mollis, neque nostra mi vel volutpat lacinia, vitae blandit est, bibendum vel ut. Congue ultricies, libero velit amet magna erat. Orci in, eleifend venenatis lacus tincidunt nisl malesuada tristique, cum egestas vel ac dapibus euismod suspendisse.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. Lacus eu nulla ante lorem, viverra pretium ipsum etiam, platea nec purus malesuada id leo, aliquam metus, ac velit viverra. Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor. Magna convallis mi mollis, neque nostra mi vel volutpat lacinia, vitae blandit est, bibendum vel ut. Congue ultricies, libero velit amet magna erat. Orci in, eleifend venenatis lacus tincidunt nisl malesuada tristique, cum egestas vel ac dapibus euismod suspendisse.</p>\r\n', 2, '2016-12-18', 1, 0, 'Article-1922660071306660039_2.jpg', '', 'Begha', 1, '2016-12-18 07:12:38'),
(2, 'HOW TO STABLISH YOUR OFFICE ?', '<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. Lacus eu nulla ante lorem, viverra pretium ipsum etiam, platea nec purus malesuada id leo, aliquam metus, ac velit viverra. Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor. Magna convallis mi mollis, neque nostra mi vel volutpat lacinia, vitae blandit est, bibendum vel ut. Congue ultricies, libero velit amet magna erat. Orci in, eleifend venenatis lacus tincidunt nisl malesuada tristique, cum egestas vel ac dapibus euismod suspendisse.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. Lacus eu nulla ante lorem, viverra pretium ipsum etiam, platea nec purus malesuada id leo, aliquam metus, ac velit viverra. Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor. Magna convallis mi mollis, neque nostra mi vel volutpat lacinia, vitae blandit est, bibendum vel ut. Congue ultricies, libero velit amet magna erat. Orci in, eleifend venenatis lacus tincidunt nisl malesuada tristique, cum egestas vel ac dapibus euismod suspendisse.</p>\r\n', 1, '2016-12-18', 1, 0, 'Article-115144440558135946_5.jpg', '', 'Begha', 1, '2016-12-18 07:12:38'),
(3, 'HOW TO STABLISH YOUR OFFICE ?', '<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. Lacus eu nulla ante lorem, viverra pretium ipsum etiam, platea nec purus malesuada id leo, aliquam metus, ac velit viverra. Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor. Magna convallis mi mollis, neque nostra mi vel volutpat lacinia, vitae blandit est, bibendum vel ut. Congue ultricies, libero velit amet magna erat. Orci in, eleifend venenatis lacus tincidunt nisl malesuada tristique, cum egestas vel ac dapibus euismod suspendisse.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. Lacus eu nulla ante lorem, viverra pretium ipsum etiam, platea nec purus malesuada id leo, aliquam metus, ac velit viverra. Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor. Magna convallis mi mollis, neque nostra mi vel volutpat lacinia, vitae blandit est, bibendum vel ut. Congue ultricies, libero velit amet magna erat. Orci in, eleifend venenatis lacus tincidunt nisl malesuada tristique, cum egestas vel ac dapibus euismod suspendisse.</p>\r\n', 2, '2016-12-18', 1, 0, 'Article-8812478521253601853_1.jpg', '', 'Begha', 1, '2016-12-18 07:12:38'),
(4, 'HOW TO STABLISH YOUR OFFICE ?', '<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. Lacus eu nulla ante lorem, viverra pretium ipsum etiam, platea nec purus malesuada id leo, aliquam metus, ac velit viverra. Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor. Magna convallis mi mollis, neque nostra mi vel volutpat lacinia, vitae blandit est, bibendum vel ut. Congue ultricies, libero velit amet magna erat. Orci in, eleifend venenatis lacus tincidunt nisl malesuada tristique, cum egestas vel ac dapibus euismod suspendisse.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. Lacus eu nulla ante lorem, viverra pretium ipsum etiam, platea nec purus malesuada id leo, aliquam metus, ac velit viverra. Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor. Magna convallis mi mollis, neque nostra mi vel volutpat lacinia, vitae blandit est, bibendum vel ut. Congue ultricies, libero velit amet magna erat. Orci in, eleifend venenatis lacus tincidunt nisl malesuada tristique, cum egestas vel ac dapibus euismod suspendisse.</p>\r\n', 3, '2016-12-18', 1, 0, 'Article-931379877967569605_4.jpg', '', 'Begha', 1, '2016-12-18 07:12:38'),
(5, 'HOW TO STABLISH YOUR OFFICE ?', '<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. Lacus eu nulla ante lorem, viverra pretium ipsum etiam, platea nec purus malesuada id leo, aliquam metus, ac velit viverra. Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor. Magna convallis mi mollis, neque nostra mi vel volutpat lacinia, vitae blandit est, bibendum vel ut. Congue ultricies, libero velit amet magna erat. Orci in, eleifend venenatis lacus tincidunt nisl malesuada tristique, cum egestas vel ac dapibus euismod suspendisse.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit viverra minim sed metus egestas sapien consectetuer, ac etiam bibendum cras posuere pede placerat, velit neque felis. Turpis ut mollis, elit et vestibulum mattis integer aenean nulla, in vitae id augue vitae. Lacus eu nulla ante lorem, viverra pretium ipsum etiam, platea nec purus malesuada id leo, aliquam metus, ac velit viverra. Imperdiet mauris viverra maecenas, tortor enim aliquam at nec. Pellentesque penatibus, sed rutrum viverra quisque pede, mauris commodo sodales enim porttitor. Magna convallis mi mollis, neque nostra mi vel volutpat lacinia, vitae blandit est, bibendum vel ut. Congue ultricies, libero velit amet magna erat. Orci in, eleifend venenatis lacus tincidunt nisl malesuada tristique, cum egestas vel ac dapibus euismod suspendisse.</p>\r\n', 3, '2016-12-18', 1, 0, 'Article-918169120827501158_3.jpg', '', 'Begha', 1, '2016-12-18 07:12:38'),
(6, '12th Anniversary and 1st Convocation Ceremony', '<p>12th Anniversary and 1st Convocation Ceremony</p>\r\n', 3, '2016-12-21', 1, 2, 'Article-16714174476102844578_Gallery-23711047305479985457_IMG_9097.JPG', 'https://www.youtube.com/embed/jkQmg0_9psk', 'DUIAC', 1, '2016-12-21 11:12:55'),
(7, 'WAFY ARTS CARNIVAL 2017', '<p>WAFY ARTS CARNIVAL 2017</p>\r\n', 3, '2017-01-11', 1, 2, 'Article-53797128058062438846_Capture.jpg', 'https://www.youtube.com/embed/k-fWEk5mo_4', 'DUIAC', 1, '2017-01-11 21:01:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_article_category`
--

CREATE TABLE `tbl_article_category` (
  `CategoryId` int(11) NOT NULL,
  `Category` varchar(50) NOT NULL,
  `DisplayOrder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_article_category`
--

INSERT INTO `tbl_article_category` (`CategoryId`, `Category`, `DisplayOrder`) VALUES
(1, 'Islamic', 0),
(2, 'Science', 0),
(3, 'General', 0),
(4, 'ge', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

CREATE TABLE `tbl_blog` (
  `BlogId` int(11) NOT NULL,
  `BlogCategoryId` int(11) NOT NULL,
  `tagId` int(11) NOT NULL,
  `BlogTitle` text NOT NULL,
  `Author` text NOT NULL,
  `Summary` text NOT NULL,
  `BlogImage` text NOT NULL,
  `SubImage` text NOT NULL,
  `Description` text NOT NULL,
  `Video` text NOT NULL,
  `PostDate` date NOT NULL,
  `Date` date NOT NULL,
  `Time` time NOT NULL,
  `Display` bit(1) NOT NULL,
  `SEOTitle` text NOT NULL,
  `SEOKeyword` text NOT NULL,
  `SEODesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_blog`
--

INSERT INTO `tbl_blog` (`BlogId`, `BlogCategoryId`, `tagId`, `BlogTitle`, `Author`, `Summary`, `BlogImage`, `SubImage`, `Description`, `Video`, `PostDate`, `Date`, `Time`, `Display`, `SEOTitle`, `SEOKeyword`, `SEODesc`) VALUES
(2, 1, 0, '3 Critical Ways Your Brand Boosts Your SEO', 'Admin', '<p>In the past ten years, digital marketing has eaten into the market share of traditional marketing. Despite this radical shift, a few centuries-old marketing tactics still return the highest on investment. Branding is one of them.</p>', '33091440571389734seo.jpeg', '', '<p>In the past ten years, digital marketing has eaten into the market share of traditional marketing. Despite this radical shift, a few centuries-old marketing tactics still return the highest on investment. Branding is one of them.</p>\r\n\r\n<p>&nbsp;Even in this hyper-digital era, branding supports online marketing efforts -- as long as business owners and marketers know how to leverage it. Brand name and affinity boosts search engine optimization in three critical ways:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>It drives direct traffic.</p>\r\n	</li>\r\n	<li>\r\n	<p>Google indirectly includes it in the algorithm.</p>\r\n	</li>\r\n	<li>\r\n	<p>It draws links better than non-branded content. &nbsp;</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Read on to keep in mind how you can&nbsp;leverage your brand to get more traffic, leads and sales through keeping your branding efforts consistent and vigorous.</p>\r\n\r\n<p><strong><span style="font-size:18px">Building your brand results in the second largest traffic driver: direct traffic.</span></strong></p>\r\n\r\n<p>Some users come to your website via the queries they type in (organic), or via a link from a social media channel (social). Others come from other websites that have linked to your site (referral). Still, there are those who type in the actual url: target.com or usajobs.gov or ucla.edu. The graphic below represents the origin of traffic for an actual small business.</p>\r\n\r\n<p>Direct traffic (green) tends to get the second largest amount of traffic behind organic. When business owners and marketers infuse their social content, blog posts and directory listings with their brand identity and values, they remind online surfers of their name and eventually boost direct traffic.</p>\r\n\r\n<p>Think about it: how many times have you been scrolling through Facebook only to see a product you&rsquo;ll need someday? The name of the company may not stick with you the first time, but after five or six exposures, it has a good chance of remaining memorable. When you have a need for that product, the name pops up in your mind, and the first place you go is Google search. You type in the url or something close and Google takes you to the website directly.</p>\r\n\r\n<p>David Ogilvy describes &ldquo;brand&rdquo; as &ldquo;the intangible sum of a product&rsquo;s attributes.&rdquo; These attributes must be unique and target a defined sliver of the consumer market.&nbsp;The key to brand success is linking the company&rsquo;s values with the consumers&rsquo;. Target is affordable but trendy--even chic--clothes and furnishings. That&rsquo;s why some call itTarjay. REI provides the best products for the avid outdoorsman who values quality over price. Walmart? The least expensive consumer goods available. Consumers who shop there value price over quality.</p>\r\n\r\n<p>The bottom line is that all television, radio and social media outreaches begin to coalesce into the memorability of the brand. The companies that have branded adequately enjoy a direct hit as the consumer types in their name and not a competitor&rsquo;s.</p>\r\n\r\n<p><span style="font-size:18px"><strong>Google--and its indirect brand evaluation--is watching.</strong></span></p>\r\n\r\n<p>While consumers understand a brand through consistent messaging, Google works its algorithm to understand exactly what a brand offers and to whom.</p>\r\n\r\n<p>First, the search engine watches queries and where they go. People flooding your website for particular search terms attract the search engines&rsquo; attention. A positive correlation between certain queries and keywords prompts the search engine to designate the site with enriched &ldquo;brand authority.&rdquo;</p>\r\n\r\n<p>A robust, memorable brand attracts links from other websites as well. Links are a top factor in determining PageRank. The broadcast of your brand across the Internet eventually leads to mentions and even links on other websites. The &ldquo;follow&rdquo; links are a stronger indication of authority, but Google doesn&rsquo;t ignore the nofollow links and even unlinked &ldquo;mentions&rdquo; of the brand name. The more, diverse backlinks from reasonable-authority sites a website has, the higher its rankings.</p>\r\n\r\n<p>The number of links is not the only way Google determines a website&rsquo;s authority. Part of Google&rsquo;s Panda algorithm aims to decode brand authority by dividing the number of incoming links by the number of searches. It would then compare the resulting fraction to competitors&rsquo; numbers. Google regards many incoming links existing with few brand searches as lower brand authority. After all, the incoming links could have been created with some encouragement from the company. If not many are searching for the brand, it hasn&rsquo;t yet gotten a foothold in the market.</p>\r\n\r\n<p>Online and offline marketing can work hand together nicely to build brand and traffic. Consumers discuss offline branding efforts (commercials, tradeshows, sponsorships) online, and online mentions of a company (possibly via PPC or social media) drive people to visit its bricks and mortar site. Men&rsquo;s fashion brand Bonobos found that those who shopped at their online store after visiting their shop in a mallspent 75 percent more online&nbsp;than those who hadn&rsquo;t gone to the store. Also, before going to the store in the mall, 78 percent of respondents admitted to researching the store&rsquo;s offerings online first.</p>\r\n\r\n<p>Despite this, keep in mind that any brand distinction in the abstract is not a ranking factor. Google doesn&rsquo;t purposely rank Coca Cola slightly higher than Pepsi because its market share is bigger. Google does take into account how well a company has consistently branded itself. The website with clear, meaningful branding AND&nbsp;SEO best practices&nbsp;will outrank one with neither of those elements in place. Google gives companies that have invested in their branding more link juice than those that haven&rsquo;t.</p>\r\n\r\n<p><span style="font-size:18px"><strong>Branding builds reputation, which attracts links.</strong></span></p>\r\n\r\n<p>Google busted unnatural, inauthentic and constructed links in the Penguin update of April 2012, down-ranking those websites that were obviously paying for or constructing links. The search engine has made it clear that any links coming back to a site should be earned through quality, useful content. It wants to see proof that a&nbsp;relevant&nbsp;website genuinely wants to share the content to enrich their own readers&rsquo; experience.</p>\r\n\r\n<p>Acquiring links can be a rigorous exercise involving calling bloggers and asking them to write reviews, or sending press releases to newspapers and online publications and hoping for a link. Because of the budget, time, and creativity required to earn authentic backlinks, people &ldquo;game&rdquo; or try to manipulate the Google algorithm precisely because they do not have exceptional content other sites want to link to naturally. Therefore, these purchased links indicate poor quality content. Google is proud of its own brand and wants to provide only the best information to its users.</p>\r\n\r\n<p>In the go-go pace of the Internet, surfers want to know instantly whether they should read one piece over the other. If a company has a recognizable brand name, a website owner looking for a site to link to is more apt to link to a company with a reputation. One Moz study showed that those looking for a good link&nbsp;chose companies with recognizable brand names&nbsp;twice as often as more generic or unrecognizable brands. Reputable companies are more likely to produce well-researched, useful information to keep their own brand name polished.</p>\r\n\r\n<p>Referred</p>', '', '2016-05-05', '2016-08-18', '11:00:15', b'1', '', '', ''),
(3, 3, 0, 'Indus OS, the Indian operating system Is the Second Most Popular In India', 'Admin', '<p>Indus OS- The Indian Operating system became the second most popular Mobile OS in India, overtaking Windows and IOS according to a research study conducted by Counterpoint Research.</p>', '5358317461036076200indus-web.jpg', '', '<p>Indus OS- The Indian Operating system became the second most popular Mobile OS in India, overtaking Windows and IOS according to a research study conducted by Counterpoint Research. The Android which is the most popular Mobile OS have 83.80 %&nbsp; of market share. The Indus company grabbed a 5.6% market share In India leaving behind Xiaomi&#39;s MIUI (4.1%), Cyanogen (2.8%), iOS (2.5%) &amp; Windows (0.3%).</p>\r\n\r\n<p>It First Launched as Firstouch and It First Started as a&nbsp; mobile OS with a Gujarati Interface. Indus is the world&rsquo;s First Regional Operating System Especially made for the regional language user. Indus also has partnered with Mobile Brands Micromax and Elite.</p>\r\n\r\n<p>Indus also has its own Mobile application market place named &#39;App Bazaar&#39;. It Have carrier billing facility and it allows users to download apps in their regional languages without an email id or paying by credit cards.</p>\r\n\r\n<p>They Have over 2 Million Users and it is Now available on English and 12 Regional Languages (Malayalam, Kannada, Gujarati, Hindi, Urdu, Bengali, Marathi, Telugu, Tamil, Odia, Assamese, Punjabi).</p>\r\n\r\n<p>It was Developed In 2015 By three alumnis of IIT Bombay named Rakesh Deshmukh, Sudhir B and Akash Dongre .</p>\r\n\r\n<p>The company have patents for keyboard allows word prediction, matra prediction, auto correction and swipe-to-translate &amp; transliterate features to its users.</p>', '', '2016-06-01', '2016-08-18', '11:50:09', b'1', '', '', ''),
(4, 3, 0, 'Google might launch its own non-nexus smartphone by the end of 2016', 'Admin', '<p>Search engine giant google is finally launching its non-nexus smart phone in this year.It will bring the company into direct competition with smartphone giants like Apple and Samsung. At Present google works with smartphone companies LG and HTC to Create nexus phones</p>', '642335404240805856Nexus.jpg', '', '<p>Search engine giant google is finally launching its non-nexus smart phone in this year.It will bring the company into direct competition with smartphone giants like Apple and Samsung. At Present google works with smartphone companies LG and HTC to Create nexus phones like 5P and 6X. The new Smartphone will be released by the end of 2016 according to a senior source.</p>\r\n\r\n<p>The Google CEO Sundar Pichai Said that n last month &ldquo;Google was Investing more effort into smartphones &rdquo;and he added that the company will continue to support the nexus phones.</p>\r\n\r\n<p>Google Declined to Comment on this.</p>', '', '2016-06-28', '2016-08-18', '11:59:21', b'1', '', '', ''),
(5, 3, 0, 'Google Announced Official Android 7.0 Name â€˜Nougatâ€™', 'Admin', '<p>Google officially announced Android 7.0 name as Nougat via twitter. The company outlines the schedule on their website and indicates final release in August or September. Nougat is a family of confections made with sugar or honey, roasted , whipped egg whites,</p>', '44124789224054775nougat.jpg', '', '<p>Google officially announced Android 7.0 name as Nougat via twitter. The company outlines the schedule on their website and indicates final release in August or September. Nougat is a family of confections made with sugar or honey, roasted , whipped egg whites, and sometimes chopped candied fruit. The consistency of nougat is chewy, and it is used in a variety of candy bars and chocolates.</p>\r\n\r\n<p>Earlier, Android had shown off the various statues representing the all the named versions of Android, before they uncovering the statue for Nougat in a short video story. Android Nougat introduces a multi-window split-screen mode, so two apps can be snapped to occupy halves of the screen. The notification shade of the version was also redesigned, featuring a smaller row of icons for settings, replacing notification cards with a sheet-like design, and allowing inline replies to notifications implemented via existing APIs used with Android Wear. Multiple notifications Facility from a single app also included.</p>\r\n\r\n<p>The 1.4 billion android users around the world are eagerly waiting for the new Nougat experiences.</p>', '', '2016-07-01', '2016-08-18', '12:01:49', b'1', '', '', ''),
(6, 2, 0, 'Top 10 Social Networking Websites in The World', 'Admin', '<p>&nbsp;</p>\r\n\r\n<p>&nbsp;We can check the Top 10 leading social networking websites based on number of active user accounts as of April 2016.</p>', '399636159754088939Top10_Social.jpg', '', '<p>We can check the Top 10 leading social networking websites based on number of active user accounts as of April 2016.</p>\r\n\r\n<p>1. <strong>Facebook</strong></p>\r\n\r\n<p>Facebook is the most popular free social networking website that allows users to create new profiles, upload photos and video, send messages and keep in touch with friends, family and colleagues. Facebook is now available in 37 different languages.Facebook Have 1,590,000,000 active users</p>\r\n\r\n<p>2. <strong>Whatsapp </strong></p>\r\n\r\n<p>WhatsApp is especially popular with end users who do not have unlimited text messaging. In addition to basic messaging, WhatsApp provides group chat and location,vedio,audio and file sharing options. Whatsapp Have 1,000,000,000 active users.</p>\r\n\r\n<p>3.<strong>FB Messenger</strong></p>\r\n\r\n<p>Facebook Messenger is a messaging service and software application which provides text and voice communication. Integrated with Facebook&#39;s chat feature and it lets Facebook users chat with friends both on mobile and on the main website. Facebook Messenger have 900,000,000 active users.</p>\r\n\r\n<p>4. <strong>QQ</strong></p>\r\n\r\n<p>Tencent QQ, known as QQ, is an instant messaging service developed by a Chinese company named Tencent Holdings Limited. lt offers a variety of services including online social games, music, shopping, microblogging, movies, platform of games and group and voice chat.QQ have 853,000,000 active users.</p>\r\n\r\n<p>5. <strong>WeChat </strong></p>\r\n\r\n<p>WeChat is a mobile text messaging and voice messaging communication service developed by Tancent in China, It first released in January 2011. WeChat is one of the largest standalone messaging apps by monthly active users. WeChat have 697,000,000 active users.</p>\r\n\r\n<p>6. <strong>QZone</strong></p>\r\n\r\n<p>Qzone is a social media website, which was created by Tancent, China&nbsp; in&nbsp; 2005. It allows users to write blogs, keep diaries, send photos, listen to music, and watch videos. Users can set their Qzone background and select accessories based on their preferences so that every Qzone is customized to the individual member&#39;s taste. QZone have 640,000,000 active users.</p>\r\n\r\n<p>7. <strong>Tumblr</strong></p>\r\n\r\n<p>Tumblr is a social media website and micro blogging website founded by David Karp in 2007, and owned by the search engine company Yahoo! since 2013. The service allows users to post multimedia and other content to short-form blog.Tumblr Have 555,000,000 active users.</p>\r\n\r\n<p>8. <strong>Instagram</strong></p>\r\n\r\n<p>nstagram is an online mobile photo-sharing, video-sharing, and social media service that enables its users to take pictures and videos, and share them either publicly or privately on the app, as well as through a variety of other social networking platforms, such as Facebook and Twitter. Instagram have 400,000,000 active users.</p>\r\n\r\n<p>9. <strong>Twitter</strong></p>\r\n\r\n<p>Twitter is an online social media service that enables users to send and read short 140-character messages called &quot;tweets&quot;. Registered users can read and post tweets, but those who are unregistered can only read them. Twitter Have 320,000,000 active users.</p>\r\n\r\n<p>10. <strong>Baidu</strong></p>\r\n\r\n<p>Baidu Space the social media service of Baidu, allows registered users to create personalized homepages in a query-based searchable community. Registered users can post their Web logs, or blogs, photo album and certain personal information on their homepages and establish their own communities of friends who are also registered users. Baidu have 300,000,000 active users.</p>', '', '2016-07-02', '2016-08-18', '12:08:03', b'1', '', '', ''),
(7, 2, 0, 'When Social Media Becoming An Unavoidable Entity In Life..!', 'Admin', '<p>Social media is the collective of online communications channels that allow people, companies and other organizations to create, share, or exchange information,</p>', '603864992586178930social.jpg', '', '<p>Social media is the collective of online communications channels that allow people, companies and other organizations to create, share, or exchange information, pictures and videos in virtual communities and networks. Social Media have taken over our lives. It is difficult to imagine that 15 years ago there was no Facebook, Whatsapp or Twitter!</p>\r\n\r\n<p>Social media sites Classified into many different Types like blogs, social networks, business networks, forums ,enterprise social networks, virtual worlds, photo sharing, products/services review, social bookmarking, social gaming, video sharing, and micro blogs.</p>\r\n\r\n<p>Information technology began to grow very rapidly in the 20th Century. And it became quite common for people to be engaged socially online. Still, more and more people began to utilize chat rooms for making friends, dating and discussing topics that they wanted to talk about. But the huge boom of social media was still to come.</p>\r\n\r\n<p>Today, social media is an essential part of life for people from all around the world. It is used for interactive, educational, informational or entertaining purposes. Today we have many social networking websites available on internet. This is a list of the <a href="http://localhost/d5n/d5n-blog-details.php?BlogId=6">Top 10 leading social networks</a> based on number of active user accounts as of April 2016.</p>\r\n\r\n<ol>\r\n	<li>&nbsp;Facebook: 1,590,000,000 users.</li>\r\n	<li>&nbsp;Whatsapp: 1,000,000,000 users.</li>\r\n	<li>FB Messenger: 900,000,000 users.</li>\r\n	<li>QQ: 853,000,000 users.</li>\r\n	<li>WeChat: 697,000,000 users.</li>\r\n	<li>QZone : 640,000,000 users.</li>\r\n	<li>Tumblr: 555,000,000 users.</li>\r\n	<li>Instagram: 400,000,000 users.</li>\r\n	<li>Twitter : 320,000,000 users.</li>\r\n	<li>Baidu : 300,000,000 users.</li>\r\n</ol>\r\n\r\n<p>Click here to Read the Article <a href="d5n-blog-details.php?BlogId=6">&nbsp;</a><a href="http://localhost/d5n/d5n-blog-details.php?BlogId=6">Top 10 Social Networking Websites in The World</a></p>\r\n\r\n<p>We have already seen social media change in many ways. If present trends continue, we can expect new social media platforms making an impact. Some of the trends to watch for include:</p>\r\n\r\n<ul style="margin-left: 40px;">\r\n	<li><strong>Increasing popularity of messaging and chat Networks.</strong>&nbsp;Skype, WhatsApp and Snapchat are only a few of the leading players offering these services, which are especially popular among younger users. As they continue to grow, it is likely that such apps and services will offer more advertising opportunities.</li>\r\n	<li><strong>Video and live streaming Networks .</strong>&nbsp;YouTube, Vimeo and other video sharing sites are already huge. Facebook has recently offered its own video platform. Meanwhile, short videos can be displayed on Vine, Snapchat and Instagram and services such as Periscope and Meerkat are fast-growing live streaming services. All of these have the potential to be leveraged for marketing purposes.</li>\r\n	<li><strong>E-Commerce on social platforms.</strong>&nbsp;Another trend to watch is the ability to sell directly from social media platforms. These are now available on Pinterest and Instagram, with Facebook and other sites experimenting with this technology. Although still in the early stages, it&rsquo;s the logical next step, making it possible to sell to consumers on social media sites without the intermediary step of sending them to a website.</li>\r\n</ul>\r\n\r\n<p>So, where is Social Media going? This, We Can&rsquo;tanswer. Social media depends on its users so really, the future of social networking is Unpredictable!</p>', '', '2016-07-04', '2016-08-18', '12:34:19', b'1', '', '', ''),
(8, 1, 0, 'Mastercard Changed logo after 20 years', 'Admin', '<p>The global payement solutions company mastercard changed their logo after twenty years.the company started in 1966 as&nbsp;master charge:the interbank card and it changed to mastercard in 1979.</p>', '9684732721177134378master_card.jpg', '', '<p>The global payement solutions company mastercard changed their logo after twenty years.the company started in 1966 as master charge:the interbank card and it changed to mastercard in 1979. Mastercard&#39;s red and yellow overlapped circles logo is one of the most recognized logos in the world since 1968. The new logo also keeps the overlapping cirlces.The updated logo was designed by Luke Hayman and Michael Bierut of Pentagram, using the FF Mark type family. The company removed central strips from new logo, that were added in .1990. Michael bierut says that &quot;New logo is pure form of the brand but aims to bring into the digital age and optimise it for on-screen use.&quot;</p>', '', '2016-07-18', '2016-08-19', '11:35:26', b'1', '', '', ''),
(9, 2, 0, 'For Business, Why Digital Marketing Is Important..?', 'Admin', '<p>World is shifting from analogue to digital. People are consuming more and more digital contents daily through mobiles and computers. So it is important to change the marketing&nbsp; strategies to...</p>', '1161858096444131021digital marketing.jpg', '', '<p>World is shifting from analogue to digital. People are consuming more and more digital contents daily through mobiles and computers. So it is important to change the marketing&nbsp; strategies to digital platforms also. Digital marketing is important, not only because of its rapid growth but also because it is essentially for the future of marketing.</p>\r\n\r\n<p>In general, digital marketing refers to advertising through digital channels such as search engines, websites, social media, email, and mobile apps.</p>\r\n\r\n<p>Commonly used Digital marketing activities are&nbsp;:</p>\r\n\r\n<ul style="margin-left:40px">\r\n	<li>Search Engine Optimization&nbsp;(SEO)</li>\r\n	<li>Search Engine Marketing&nbsp;(SEM)</li>\r\n	<li>Social Media Marketing(SMM)</li>\r\n	<li>Social Media Optimization(SMO)</li>\r\n	<li>Content marketing</li>\r\n	<li>Online campaigns</li>\r\n	<li>e-Commerce</li>\r\n	<li>email marketing</li>\r\n	<li>Display Advertising etc..</li>\r\n</ul>\r\n\r\n<p>The key features of digital Marketing are:</p>\r\n\r\n<ul style="margin-left: 40px;">\r\n	<li>Digital marketing techniques are more affordable than traditional marketing. An online ad campaign can advertise the same message in less money than a television or news paper ad.</li>\r\n	<li>Social Media Audience can reach a wider audience anywhere in the world.</li>\r\n	<li>It is easier to track results in digital marketing. By using wide range of analytics and data we are able to analyze our digital marketing campaigns and find out how the campaign performed and how we can improve the campaign.</li>\r\n	<li>We will get a huge number of consumers through digital marketing&nbsp; than traditional marketing because nowadays Most people read their newspapers or watching channels on their iPad or some type of tablet.</li>\r\n	<li>Doesn&rsquo;t require field work</li>\r\n	<li>High success in less time</li>\r\n	<li>High Return on Investment(ROI).</li>\r\n</ul>\r\n\r\n<p>It is just a introduction to the world of digital marketing . We can discuss more about digital marketing through next articles. With this knowledge of digital marketing-its definition and its basic principles, you are well ready to position your organisation in the digital world!</p>', '', '2016-07-23', '2016-08-19', '11:46:01', b'1', '', '', ''),
(10, 2, 0, 'Orkut Launches new social network â€œHelloâ€', 'Admin', '<p>Do you remember orkut? The social network that we loved before facebook took over our lives. Orkut was shut down by google in 2014. Now There is a happy news from the orkut team. Orkut creator orkut B&uuml;y&uuml;kk&ouml;kten has launched a new mobile only social network &nbsp;&ldquo;hello&amp;rdqu...</p>', '1287510896568406804orkut-launches-Hello.jpg', '', '<p>Do you remember orkut? The social network that we loved before facebook took over our lives. Orkut was shut down by google in 2014. Now There is a happy news from the orkut team. Orkut creator orkut B&uuml;y&uuml;kk&ouml;kten has launched a new mobile only social network &nbsp;&ldquo;hello&rdquo;. The mobile app is available in hello.com and it is now available on US, Canada, Britain, France, Australia, Brazil, Ireland only. India,Germany and Mexico are next on the list , and the app will be availiable this month.</p>\r\n\r\n<p>The Orkut creator says that &ldquo;hello is the next generation of orkut, and it is built on love not on likes. Hello&rsquo;s heardquarters is at san Francisco and Company also have development office in mountain view.</p>\r\n\r\n<p>Hello is more visual based than text, we&rsquo;ll have to wait to know that how soon hello rolls out the rest of the world. Orkut had more than &nbsp;300 million users &nbsp;when it shuts down. Hello have to compete with the social media giants facebook and whatsapp to exist. Lets hope hello manages&nbsp; to do better. Key Features of Hello</p>\r\n\r\n<p>1. Personas</p>\r\n\r\n<p>A persona is a community focused on a particular passion. Choose 5 out of 100+ personas you most identify with.</p>\r\n\r\n<p>2. Folio</p>\r\n\r\n<p>Your folio is a personalized feed of content relevant to your chosen personas.</p>\r\n\r\n<p>3. Potentials</p>\r\n\r\n<p>Potentials are people we&#39;d like to introduce you to, based on your personas, location and personality.</p>\r\n\r\n<p>4. Connections</p>\r\n\r\n<p>Create more meaningful connections with our direct messaging feature, including photos, gifts and unique expressions.</p>\r\n\r\n<p>5. Leaderboards</p>\r\n\r\n<p>The more interactions your contributions generate, the more likely you will become a persona leader.</p>\r\n\r\n<p>6. milestones &amp; rewards</p>\r\n\r\n<p>Reach milestones, unlock rewards, and add layers of fun to your social connections as you venture through hello.</p>', '', '2016-08-12', '2016-08-19', '11:36:56', b'1', '', '', ''),
(11, 2, 0, 'Knock, Knock! Google Coming with New Video Calling app â€œDuoâ€', 'Admin', '<p>Search engine giant Google coming with a new video calling app Duo. Duo is a one-on-one interaction app. Google had announced duo-video calling app and allo -messaging app at their annual I/O conference in this year.</p>', '945107015194503660google video call.jpg', '', '<p>Search engine giant Google coming with a new video calling app Duo. Duo is a one-on-one interaction app. Google had announced duo-video calling app and allo -messaging app at their annual I/O conference in this year. Duo will work on both android and ios.&nbsp; Users need to give their mobile number to sign up to duo.</p>\r\n\r\n<p>Duo represents Google&rsquo;s response to other video calling apps like Facetime (apple), Skype (Microsoft) and&nbsp; Messenger ( Facebook ) .Duo have a difference from other video calling apps that it will give a glimpse at who is making call, Google calls this feature &ldquo;knock, knock!&rdquo;.</p>\r\n\r\n<p>The new app, announced in May, is being released Tuesday as a free service for phones running on Google&#39;s Android operating system as well as Apple&#39;s iPhones. Google has been offering video calling through its Hangout feature for several years, but the internet company is now tailoring that service for business meetings.</p>', '', '2016-08-16', '2016-08-19', '11:38:51', b'1', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blogcategory`
--

CREATE TABLE `tbl_blogcategory` (
  `BlogCategoryId` int(11) NOT NULL,
  `BlogCategoryName` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_blogcategory`
--

INSERT INTO `tbl_blogcategory` (`BlogCategoryId`, `BlogCategoryName`) VALUES
(1, 'Branding'),
(2, 'Digital Marketing'),
(3, 'Technology');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blogimage`
--

CREATE TABLE `tbl_blogimage` (
  `imageId` int(11) NOT NULL,
  `BlogId` int(11) NOT NULL,
  `BlogImages` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_blogimage`
--

INSERT INTO `tbl_blogimage` (`imageId`, `BlogId`, `BlogImages`) VALUES
(4, 14, '4935746220483128708.jpg'),
(5, 14, '1178081077427821976111.jpg'),
(6, 15, '721599945108121653803.jpg'),
(7, 15, '15865818657369971314.jpg'),
(8, 15, '12860047841351628250211.jpg'),
(9, 16, '117532704312763227204.jpg'),
(10, 16, '91532902320947871918.jpg'),
(11, 16, '10198542827673285211.jpg'),
(12, 17, '34946110317617212105.jpg'),
(13, 17, '1094685486125222483618.jpg'),
(14, 17, '1393067855507172580211.jpg'),
(15, 18, '14256430036172516005.jpg'),
(16, 18, '105027668881812022918.jpg'),
(17, 18, '790493911282088892211.jpg'),
(18, 19, '10810013796910905005.jpg'),
(19, 19, '52907575776235104118.jpg'),
(20, 19, '1403051228714069383211.jpg'),
(21, 21, '1419618559570268180indus-web.jpg'),
(22, 21, '120734268813259813081os-graph.jpg'),
(25, 3, '54813883613640644350indus-web.jpg'),
(26, 3, '5625114508596458981os-graph.jpg'),
(29, 5, '7595110311206336500androide_Versions.png'),
(30, 5, '4893143915704292981AndroidReveal.jpg'),
(31, 5, '4888840733913310262nougat.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blogtags`
--

CREATE TABLE `tbl_blogtags` (
  `BlogTagsId` int(11) NOT NULL,
  `tagId` int(11) NOT NULL,
  `tagName` text NOT NULL,
  `blogId` int(11) NOT NULL,
  `blogName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_blogtags`
--

INSERT INTO `tbl_blogtags` (`BlogTagsId`, `tagId`, `tagName`, `blogId`, `blogName`) VALUES
(1, 1, 'tag one', 9, ''),
(2, 2, '', 22, ''),
(3, 1, '', 22, ''),
(4, 3, '', 22, ''),
(5, 1, '', 20, ''),
(6, 2, '', 20, ''),
(7, 3, '', 20, ''),
(8, 2, 'tag 2', 11, ''),
(9, 1, 'tag one', 11, ''),
(24, 2, 'tag 2', 21, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `CategoryId` int(11) NOT NULL,
  `CategoryName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`CategoryId`, `CategoryName`) VALUES
(1, 'Web'),
(2, 'Branding'),
(3, 'Video'),
(4, 'Graphic');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_clients`
--

CREATE TABLE `tbl_clients` (
  `ClientId` int(11) NOT NULL,
  `ClientName` text NOT NULL,
  `BlkLogo` text NOT NULL,
  `YtLogo` text NOT NULL,
  `Display` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_clients`
--

INSERT INTO `tbl_clients` (`ClientId`, `ClientName`, `BlkLogo`, `YtLogo`, `Display`) VALUES
(1, 'britco', '', '', b'1'),
(2, 'b', '', '', b'1'),
(3, 'jt', '', '', b'1'),
(4, 'gf', '3728273601158802839tiptop.png', '1146624845590353013xpress.png', b'0'),
(5, 'fg', '10736859771254634616livein.png', '4219266221288973977tiptop.png', b'0'),
(6, 'fgh', '152074323935640023livein.png', '484537863556530033tiptop.png', b'1'),
(7, 'gf', '626284550892780369xpress.png', '1311608781255882537mahindra.png', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_committee`
--

CREATE TABLE `tbl_committee` (
  `CommitteeId` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Designation` varchar(255) NOT NULL,
  `Region` varchar(255) NOT NULL,
  `FbLink` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `DisplayOrder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_committee`
--

INSERT INTO `tbl_committee` (`CommitteeId`, `Name`, `Designation`, `Region`, `FbLink`, `Image`, `DisplayOrder`) VALUES
(1, 'ffffffffffffffffff', 'eeeeeeeeeeeee', 'INDIA', 'eeeeeeeeeeeeee', 'committee_622454721144844984_34.JPG', 2),
(2, 'KSA', 'tttttttttttttttttt', 'KSA', 'tttttttttttttttttt', 'committee_1028932924959264471_741px_Im.jpg', 2),
(3, 'UAE', 'gggggggggggggggggg', 'UAE', 'ggggggggggggggg', 'committee_657998972273682137_30.JPG', 4),
(4, 'QATAR', 'vvvvvvvvvvvvvv', 'QATAR', 'https://www.facebook.com/WafyCollegeMarayamangalam/', 'committee_222431285606834185_29634_347432918686514_932623812_n.jpg', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contacts`
--

CREATE TABLE `tbl_contacts` (
  `ContactsId` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Mobile` int(11) NOT NULL,
  `CategoryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contacts`
--

INSERT INTO `tbl_contacts` (`ContactsId`, `Name`, `Mobile`, `CategoryId`) VALUES
(1, 'test', 22222222, 4),
(2, 'Anas', 8606647, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contacts_category`
--

CREATE TABLE `tbl_contacts_category` (
  `CategoryId` int(11) NOT NULL,
  `Category` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contacts_category`
--

INSERT INTO `tbl_contacts_category` (`CategoryId`, `Category`) VALUES
(2, 'QATAR COMMITTEE'),
(3, 'UAE COMMITTEE'),
(4, 'SCHOOL COMMITTEE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_facilities`
--

CREATE TABLE `tbl_facilities` (
  `facilitiesId` int(11) NOT NULL,
  `Heading` varchar(255) NOT NULL,
  `Content` varchar(25500) NOT NULL,
  `Image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_facilities`
--

INSERT INTO `tbl_facilities` (`facilitiesId`, `Heading`, `Content`, `Image`) VALUES
(1, 'COMPUTER LAB', 'aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa', 'facilities-123931621177048315_306887-58212-12.jpg'),
(2, 'LIBRARY', 'aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaa', 'facilities-13890228681079753458_2.0 (115).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallary_images`
--

CREATE TABLE `tbl_gallary_images` (
  `id` int(11) NOT NULL,
  `GalleryId` int(11) NOT NULL,
  `Image` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_gallary_images`
--

INSERT INTO `tbl_gallary_images` (`id`, `GalleryId`, `Image`) VALUES
(1, 18, 'Gallery_Sub_18532671815895944424_0ba5fd31e-9a98-4295-b14d-8ab0fb0a6c9e.jpg'),
(2, 18, 'Gallery_Sub_89613908683861261682_1Gallay-6494786801239745619_gallery-image_6.jpg'),
(3, 18, 'Gallery_Sub_95303131883692702103_2Gallay-9547891661404083991_gallery-image_9.jpg'),
(4, 18, 'Gallery_Sub_20105928965173289092_3IMG_1035.JPG'),
(5, 18, 'Gallery_Sub_27137407546066946126_4IMG_1040.JPG'),
(6, 18, 'Gallery_Sub_57366841241189734707_5IMG_8763.JPG'),
(7, 18, 'Gallery_Sub_5583088849885797352_6IMG_8928.JPG'),
(8, 18, 'Gallery_Sub_38104456448489100856_7IMG_8965.JPG'),
(9, 18, 'Gallery_Sub_5143127782585954990_8IMG_9004.JPG'),
(10, 18, 'Gallery_Sub_2009995303378388679_9IMG_9028.JPG'),
(11, 18, 'Gallery_Sub_72754188103384237780_10IMG_9097.JPG'),
(12, 18, 'Gallery_Sub_3404111571369530185_11IMG_9104.JPG'),
(13, 18, 'Gallery_Sub_9240439812235314057_12IMG_9121.JPG'),
(14, 18, 'Gallery_Sub_65855905362976683790_13IMG_9176.JPG'),
(15, 18, 'Gallery_Sub_1554725512046875379_14IMG_9180.JPG'),
(16, 18, 'Gallery_Sub_59023745072008739722_15IMG_9198.JPG'),
(17, 18, 'Gallery_Sub_79428197934863765380_16IMG_9211.JPG'),
(18, 18, 'Gallery_Sub_58700013957473132986_17IMG_9414.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `GalleryId` int(11) NOT NULL,
  `Heading` varchar(300) NOT NULL,
  `Content` varchar(5000) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Approve` int(1) NOT NULL,
  `Image` varchar(300) NOT NULL,
  `EUserId` int(11) NOT NULL,
  `EDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`GalleryId`, `Heading`, `Content`, `CategoryId`, `Date`, `Approve`, `Image`, `EUserId`, `EDate`) VALUES
(1, 'Campus Musjid', 'Campus Musjid', 2, '2016-12-18', 1, 'Gallay-6494786801239745619_gallery-image_6.jpg', 1, '2016-12-18 03:12:53'),
(2, 'Computer Lab', 'Computer Lab', 2, '2016-12-18', 1, 'Gallay-429414152739113880_gallery-image_5.jpg', 1, '2016-12-18 03:12:54'),
(3, 'Arts Festivel', 'Arts Festivel Inaguration', 1, '2016-12-18', 1, 'Gallay-192136912124878238_gallery-image_8.jpg', 1, '2016-12-18 03:12:55'),
(4, 'Arts Festivel', 'Arts Festivel', 1, '2016-12-18', 1, 'Gallay-6151393191330370550_gallery-image_13.jpg', 1, '2016-12-18 03:12:56'),
(5, 'Arts Festivel', 'Arts Festivel', 1, '2016-12-18', 1, 'Gallay-92178379075822008_gallery-image_12.jpg', 1, '2016-12-18 03:12:56'),
(6, 'Meelad Program', 'Meelad Program', 1, '2016-12-18', 1, 'Gallay-13489172481026566176_gallery-image_11.jpg', 1, '2016-12-18 03:12:57'),
(7, 'Library', 'Library', 2, '2016-12-18', 1, 'Gallay-1119859078448950580_gallery-image_2.jpg', 1, '2016-12-18 03:12:58'),
(8, 'Republic day celebration', 'Republic day celebration', 1, '2016-12-18', 1, 'Gallay-1384289372519737860_gallery-image_1.jpg', 1, '2016-12-18 04:12:16'),
(9, 'REPUBLIC DAY CELEBRATION', 'REPUBLIC DAY CELEBRATION', 1, '2016-12-18', 1, 'Gallay-9547891661404083991_gallery-image_9.jpg', 1, '2016-12-18 04:12:16'),
(10, '12th Anniversary and 1st Convocation Ceremony', 'Milestone in campus history...', 1, '2016-12-21', 1, 'Gallery-2970281553831126184_IMG_9104.JPG', 1, '2016-12-21 08:12:10'),
(11, '12th Anniversary and 1st Convocation Ceremony', 'Graduates arrives...', 1, '2016-12-21', 1, 'Gallery-31403730748226830589_IMG_8965.JPG', 1, '2016-12-21 08:12:45'),
(12, '12th Anniversary and 1st Convocation Ceremony', 'On the stage', 1, '2016-12-21', 1, 'Gallery-37055015634555878677_IMG_9121.JPG', 1, '2016-12-21 08:12:46'),
(13, '12th Anniversary and 1st Convocation Ceremony', 'Inaguration', 1, '2016-12-21', 1, 'Gallery-23711047305479985457_IMG_9097.JPG', 1, '2016-12-21 08:12:48'),
(14, '12th Anniversary and 1st Convocation Ceremony', 'Sanad distribution...', 1, '2016-12-21', 1, 'Gallery-70402595629787325306_IMG_9211.JPG', 1, '2016-12-21 08:12:49'),
(15, '12th Anniversary and 1st Convocation Ceremony', 'Shekhuna Aali Kutty Musliyar adressing the graduates...', 1, '2016-12-21', 1, 'Gallery-71512960497455584899_IMG_9414.JPG', 1, '2016-12-21 08:12:50'),
(16, '12th Anniversary and 1st Convocation Ceremony', 'Priceless moments...', 1, '2016-12-21', 1, 'Gallery-77794156207093371203_IMG_9004.JPG', 1, '2016-12-21 08:12:52'),
(17, '12th Anniversary and 1st Convocation Ceremony', 'Honorable principal Ustad Habeebullah Faizy welcome the crowd...', 1, '2016-12-21', 1, 'Gallery-17726771718854223678_ba5fd31e-9a98-4295-b14d-8ab0fb0a6c9e.jpg', 1, '2016-12-21 08:12:57'),
(18, '12th Anniversary and 1st Convocation Ceremony', 'Milestone in college history', 1, '2016-12-21', 1, 'Gallery-54612848442925690727_IMG_9104.JPG', 1, '2016-12-21 18:12:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery_category`
--

CREATE TABLE `tbl_gallery_category` (
  `CategoryId` int(11) NOT NULL,
  `Category` varchar(50) NOT NULL,
  `DisplayOrder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_gallery_category`
--

INSERT INTO `tbl_gallery_category` (`CategoryId`, `Category`, `DisplayOrder`) VALUES
(1, 'PROGRAMS', 0),
(2, 'CAMPUS', 0),
(3, 'test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_home_general`
--

CREATE TABLE `tbl_home_general` (
  `id` int(11) NOT NULL,
  `logo` varchar(300) NOT NULL,
  `favourites` varchar(500) NOT NULL,
  `gallery` varchar(500) NOT NULL,
  `articles` varchar(500) NOT NULL,
  `braucher` varchar(500) NOT NULL,
  `fecilities` varchar(500) NOT NULL,
  `braucher_file` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_home_general`
--

INSERT INTO `tbl_home_general` (`id`, `logo`, `favourites`, `gallery`, `articles`, `braucher`, `fecilities`, `braucher_file`) VALUES
(1, 'AboutUs_Logo_625165724991151021_getImage.png', 'KMECC', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.', 'AboutUs_Brch_696340289156592660_Braucher.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_institutions`
--

CREATE TABLE `tbl_institutions` (
  `institutionsId` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `UserId` int(11) NOT NULL,
  `Name` varchar(55) NOT NULL,
  `UserName` varchar(55) NOT NULL,
  `Password` varchar(55) NOT NULL,
  `RoleId` int(11) NOT NULL,
  `Image` varchar(250) NOT NULL,
  `sts` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`UserId`, `Name`, `UserName`, `Password`, `RoleId`, `Image`, `sts`) VALUES
(1, 'admin', 'admin', 'sample', 1, 'User_82140547245921456320_duiac parel.jpg', 0),
(2, 'anas', 'anas', '1111', 2, 'User_317015140208790211_Staff_171352561216837154_Dr.swlahudheen-wafy.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE `tbl_news` (
  `NewsId` int(11) NOT NULL,
  `Heading` varchar(300) NOT NULL,
  `Content` varchar(5000) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Approve` int(1) NOT NULL,
  `Image` varchar(300) NOT NULL,
  `EUserId` int(11) NOT NULL,
  `EDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`NewsId`, `Heading`, `Content`, `CategoryId`, `Date`, `Approve`, `Image`, `EUserId`, `EDate`) VALUES
(5, '12-à´µà´¾à´°àµâ€à´·à´¿à´• à´µà´¾à´«à´¿ à´¸à´¨à´¦àµà´¦à´¾à´¨ à´¸à´®àµà´®àµ‡à´³à´¨à´‚', '<p>12-à´µà´¾à´°àµ&zwj;à´·à´¿à´• à´µà´¾à´«à´¿ à´¸à´¨à´¦àµà´¦à´¾à´¨ à´¸à´®àµà´®àµ‡à´³à´¨à´‚</p>\r\n', 3, '2016-12-18', 1, 'News_72287979294496723563_kkkkkkkkkkkkkkkkk.jpg', 1, '2016-12-18 04:12:54'),
(6, '12-à´µà´¾à´°àµâ€à´·à´¿à´• à´µà´¾à´«à´¿ à´¸à´¨à´¦àµà´¦à´¾à´¨ à´¸à´®àµà´®àµ‡à´³à´¨à´‚', '<p>12-à´µà´¾à´°àµ&zwj;à´·à´¿à´• à´µà´¾à´«à´¿ à´¸à´¨à´¦àµà´¦à´¾à´¨ à´¸à´®àµà´®àµ‡à´³à´¨à´‚</p>\r\n', 3, '2016-12-18', 1, 'News_89742115622792111670_aaaaaa.jpg', 1, '2016-12-18 04:12:54'),
(7, 'WAFY ARTS CARNIVAL'' 17', '<p>WAFY ARTS CARNIVAL&#39; 17</p>\r\n', 3, '2017-01-10', 1, 'News_5644998699657867351_HHHHHH.jpg', 1, '2016-12-18 04:12:54'),
(8, 'REPUBLIC DAY CELEBRATION', '', 3, '2017-01-26', 1, 'News_15509123677987155553_Untitled-13.jpg', 1, '2017-01-28 14:01:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_category`
--

CREATE TABLE `tbl_news_category` (
  `CategoryId` int(11) NOT NULL,
  `Category` varchar(50) NOT NULL,
  `DisplayOrder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_news_category`
--

INSERT INTO `tbl_news_category` (`CategoryId`, `Category`, `DisplayOrder`) VALUES
(1, 'General', 0),
(2, 'Accadamic', 0),
(3, 'Events', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_portfolio`
--

CREATE TABLE `tbl_portfolio` (
  `portfolioId` int(11) NOT NULL,
  `portfolioName` text NOT NULL,
  `comment` text NOT NULL,
  `projectId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `displayImage` text NOT NULL,
  `mainImage` text NOT NULL,
  `Display` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_portfolio`
--

INSERT INTO `tbl_portfolio` (`portfolioId`, `portfolioName`, `comment`, `projectId`, `categoryId`, `displayImage`, `mainImage`, `Display`) VALUES
(15, 'iat graphic name', 'iat graphic cmnt', 8, 4, 'iatmain.jpg', 'iatmain.jpg', b'1'),
(16, 'aufriss graphic name', 'aufriss graphic cmt', 5, 4, 'aufrissgraphic.jpg', 'aufrissgraphic.jpg', b'1'),
(17, 'britco graphic  hording', 'britco graphic cmnt', 19, 4, 'britcographic.jpg', 'britcographic.jpg', b'1'),
(18, 'aufriss video name', 'auf video cmnt', 5, 3, 'aufrissvideo.jpg', 'aufrissvideo.jpg', b'1'),
(19, 'alfalah logo', 'afia logo design', 4, 2, 'afiamain.jpg', 'afiamain.jpg', b'1'),
(20, 'britco', 'britco broucher', 19, 2, 'britcobrand.jpg', 'britcobrand.jpg', b'1'),
(21, 'auf brand', 'branding', 5, 2, 'aufriss.jpg', 'aufriss.jpg', b'1'),
(22, 'auf web', 'web auf', 5, 1, 'aufrissweb.jpg', 'aufrissweb.jpg', b'1'),
(23, 'le thwaib logo', 'le thwaib', 9, 2, 'lethwaibmain.jpg', 'lethwaibmain.jpg', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project`
--

CREATE TABLE `tbl_project` (
  `projectId` int(11) NOT NULL,
  `projectName` text NOT NULL,
  `displayImage` text NOT NULL,
  `mainImage` text NOT NULL,
  `projectSlogan` text NOT NULL,
  `projectService` text NOT NULL,
  `projectDesc` text NOT NULL,
  `projectDescImage` text NOT NULL,
  `mySlogan` text NOT NULL,
  `myDesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_project`
--

INSERT INTO `tbl_project` (`projectId`, `projectName`, `displayImage`, `mainImage`, `projectSlogan`, `projectService`, `projectDesc`, `projectDescImage`, `mySlogan`, `myDesc`) VALUES
(3, 'Al Iman', 'aliman.jpg', '179184345938307994alimanmain.jpg', 'q', 'w', 'e', 'aliman.jpg', 'r', 't'),
(4, 'Al Falah', 'afia.jpg', '11399118881180576921afiamain.jpg', 'Al-Falah-Islamic-Academy', 'Islamic School', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.Cum sociis natoque penatibus et magnis dis parturient montes, nascetur.', 'afia.jpg', 'what my work', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.Cum sociis natoque penatibus et magnis dis parturient montes, nascetur.'),
(5, 'Aufriss', 'aufriss.jpg', '1267544150154613199aufrissmain.jpg', 'International design School', 'Interior Design Courses', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'aufriss.jpg', 'what my work', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'),
(6, 'Al Tawakkal', 'altawakkal.jpg', '76045764350992670altawakkalmain.jpg', 'Typing Center', 'Typing Center', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'altawakkal.jpg', 'Typing Center', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'),
(7, 'Edumart', 'Edumart.jpg', '11485612761396295239Edumart.jpg', 'Edumart', 'Edumart', 'Edumart', 'Edumart.jpg', 'Edumart', 'Edumart'),
(8, 'IAT', 'iat.jpg', '459407303813687956iatmain.jpg', 'Institute Of AutoMobile Technology', 'Institute Of AutoMobile Technology', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'iat.jpg', 'Institute Of AutoMobile Technology', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'),
(9, 'Le Thwaib', 'lethwaib.jpg', '603391642953196990lethwaibmain.jpg', 'Le Thwaib', 'Le Thwaib', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'lethwaib.jpg', 'Le Thwaib', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'),
(11, 'B Trend', 'btrend.jpg', '13091128501380760766btrendmain.jpg', 'Legends From Technology', 'Mobile Repairing Tools', 'Mobile Repairing Tools', 'btrend.jpg', ' ', 'Mobile Repairing Tools'),
(12, 'xpress money', 'Express_money.jpg', '736661068299329078Express_money.jpg', 's', 'ds', 'ds', 'Express_money.jpg', 'ds', 'ds'),
(13, 'Speed Wheels', 'speed-wheels.jpg', '211759404677406306speed-wheels.jpg', 'Best Alloy Wheels', 'Alloy Wheels Supply', 'alloy wheel', 'speed-wheels.jpg', 'Best Alloy Wheels', 'alloy wheel'),
(14, 'Challenger Watches', 'challenger-watches.jpg', '419215620442667940challengermain.jpg', 'Challenger Watches', 'Challenger Watches', 'Challenger Watches', 'challenger-watches.jpg', 'Challenger Watches', 'Challenger Watches'),
(15, 'Race SPZ', 'race.jpg', '158873345663248850race.jpg', 'Race SPZ', 'Race SPZ', 'Race SPZ', 'race.jpg', 'Race SPZ', 'Race SPZ'),
(16, 'Apple Cart', 'apple-cart.jpg', '5149183011356318714apple-cart.jpg', 'Apple Cart', 'Apple Cart', 'Apple Cart', 'apple-cart.jpg', 'Apple Cart', 'Apple Cart'),
(17, 'mahindra', 'mahindra.jpg', '1056731455684463518mahindra.jpg', 'mahindra', 'mahindra', 'mahindra', 'mahindra.jpg', 'mahindra', 'mahindra'),
(18, 'tiptop', 'tiptop.jpg', '40957650197036676tiptop.jpg', 'tiptop', 'tiptop', 'tiptop', 'tiptop.jpg', 'tiptop', 'tiptop'),
(19, 'Britco & Bridco', 'Britco.jpg', '1330628741465216594britcomain.jpg', 'Indias First & Best', 'Mobile Phone Institute', 'Britco & Bridco is proud to be the first exclusive institution to pioneer in mobile phone technology and services. We anchor the widest service centre network in Asia. We welcome each and every one to this innovative world of technology and promise you a bright and prosperous future in this field of technology.', 'Britco.jpg', 'Mobile Phone Repair', 'Britco & Bridco provides hi-tech training to repair mobile phones of GSM and CDMA handsets using State of the Art technology, which is supplemented by well qualified and experienced faculty with an updated syllabus.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_projectnew`
--

CREATE TABLE `tbl_projectnew` (
  `projectId` int(11) NOT NULL,
  `projectName` text NOT NULL,
  `displayImage` text NOT NULL,
  `mainImage` text NOT NULL,
  `projectSlogan` text NOT NULL,
  `projectService` text NOT NULL,
  `projectDesc` text NOT NULL,
  `projectDescImage` text NOT NULL,
  `mySlogan` text NOT NULL,
  `myDesc` text NOT NULL,
  `sts` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_projectnew`
--

INSERT INTO `tbl_projectnew` (`projectId`, `projectName`, `displayImage`, `mainImage`, `projectSlogan`, `projectService`, `projectDesc`, `projectDescImage`, `mySlogan`, `myDesc`, `sts`) VALUES
(18, 'hgh', '', 'prd1.jpg', '', '', '', '', '', '', b'1'),
(19, 'pr1', '', 'prd3.jpg', '', '', '', '', '', '', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id` int(11) NOT NULL,
  `s1content1` varchar(100) NOT NULL,
  `s1content2` varchar(100) NOT NULL,
  `s1content3` varchar(200) NOT NULL,
  `s1image` varchar(300) NOT NULL,
  `s2content1` varchar(100) NOT NULL,
  `s2content2` varchar(100) NOT NULL,
  `s2content3` varchar(200) NOT NULL,
  `s2image` varchar(300) NOT NULL,
  `s3content1` varchar(100) NOT NULL,
  `s3content2` varchar(100) NOT NULL,
  `s3content3` varchar(200) NOT NULL,
  `s3image` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `s1content1`, `s1content2`, `s1content3`, `s1image`, `s2content1`, `s2content2`, `s2content3`, `s2image`, `s3content1`, `s3content2`, `s3content3`, `s3image`) VALUES
(1, 'KASI MUHAMMED MUSLIYAR EDUCATIONAL & CULTURAL COMPLEX', 'WAFY COLLEGE', 'THACHANGAD, MARAYAMANGALAM\r\n', 'Wafy-610190664680805816_2013-03-28 10.38.50.jpg', 'KASI MUHAMMED MUSLIYAR EDUCATIONAL & CULTURAL COMPLEX', 'WAFY COLLEGE', 'THACHANGAD, MARAYAMANGALAM\r\n', 'Wafy-79367817858738391_1345393769755.jpg', 'KASI MUHAMMED MUSLIYAR EDUCATIONAL & CULTURAL COMPLEX', 'WAFY COLLEGE', 'THACHANGAD, MARAYAMANGALAM\r\n', 'Wafy-10070297481149594038_291954_2561910687526_1509458498_n.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

CREATE TABLE `tbl_staff` (
  `StaffId` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Designation` varchar(100) NOT NULL,
  `FbLink` varchar(200) NOT NULL,
  `Image` varchar(300) NOT NULL,
  `DisplayOrder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_staff`
--

INSERT INTO `tbl_staff` (`StaffId`, `Name`, `Designation`, `FbLink`, `Image`, `DisplayOrder`) VALUES
(5, 'Dr.swlahudheen-wafy', 'Designation', 'https://www.facebook.com/ICPadinhattummuri/', 'Staff_171352561216837154_Dr.swlahudheen-wafy.jpg', 5),
(6, 'LUQUMAN-BAQAWI', 'Designation', 'https://www.facebook.com/ICPadinhattummuri/', 'Staff_10986874411225760291_LUQUMAN-BAQAWI.jpg', 6),
(7, 'd', 'dsd', 'dsd', 'Staff_305783845328547657_Unni  Passport size photo in Colour dress.jpg', 7),
(8, 'aaaa', 'aaaaa', 'aaaaa', 'Staff_124843803999016138_MuhammadWith coat D-16923.jpg', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `StudentsId` int(11) NOT NULL,
  `BatchId` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Occupation` varchar(100) NOT NULL,
  `Image` varchar(300) NOT NULL,
  `DisplayOrder` int(11) NOT NULL,
  `Address` varchar(500) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Phone` varchar(30) NOT NULL,
  `Mobile` varchar(30) NOT NULL,
  `RegCIC` int(11) NOT NULL,
  `RegCLG` int(11) NOT NULL,
  `FbLink` varchar(200) NOT NULL,
  `TwitterLink` varchar(300) NOT NULL,
  `GoogleLink` varchar(300) NOT NULL,
  `LinkedInLink` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`StudentsId`, `BatchId`, `Name`, `Occupation`, `Image`, `DisplayOrder`, `Address`, `Email`, `Phone`, `Mobile`, `RegCIC`, `RegCLG`, `FbLink`, `TwitterLink`, `GoogleLink`, `LinkedInLink`) VALUES
(5, 2, 'Mohammed KT', 'Manager', 'Students_261116856351010247_1.JPG', 1, 'Kuriyathodi House, Panagagara, Ramapuram', 'ktmmahmood@gmail.com', '8086959999', '+971553514124', 758, 12, 'https://www.facebook.com/ktmmohammed', 'https://www.facebook.com/ktmmohammed', 'https://www.facebook.com/ktmmohammed', 'https://www.facebook.com/ktmmohammed'),
(6, 1, 'ISMAIL WAFY', 'Teacher@ Vilayil wafy college', 'Students_50989555998554333352_DSC_0275.jpg', 6, 'KALAKAPPARA HOUSE RAMAPURAM PO MALAPPURAM DT 679321', 'ismailkp40@gmail.com', '9526710979', '9526710979', 977, 40, 'https://www.facebook.com/ismail kp', 'nill', 'nill', 'nill'),
(7, 1, 'SWADIQ WAFY', 'sub editor, suprabhadham daily', 'Students_63341326632537664173_DSC_0273.jpg', 7, 'KOLAKATTIL HOUSE PALLIKURUP PO MANNARKAD VIA PALAKKAD DT 678582PIN', 'sadu313@gmail.com', '9847121979', '9847121979', 970, 38, 'http://www.facebook.com/mohammed.sadu.779', 'nill', 'nill', 'nill'),
(8, 1, 'ABDUL RAHEEM WAFY', 'khatheeb@ juma masjid thiruvegappura', 'Students_4091168140700005656_DSC_0282.jpg', 8, 'KALLETHODI HOUSE MARAYAMANGALAM PO NELLAYA VIA  PALAKKAD DT 679335 PIN', 'kprt313@gmail.com', '9605443854', '9605443854', 984, 49, 'http://www.facebook.com/abdu.raheem.7330', 'nill', 'nill', 'nill'),
(9, 2, 'MUJEEB RAHMAN WAFY', 'KATHEEB @ MEA ENGINEERING COLLEGE PERINTHALMANNA', 'Students_7135511672154078770_DSC_0250.jpg', 9, 'PULIKKADAN HOUSE ALIPARMBA PO ANAMANGAD VIA MALAPPURAM DT 679357PIN', 'rahmamujeeb@gmail.com', '9656587851', '9656587851', 759, 13, 'http://www.facebook.com/mujeeb.rahman.77715869', 'nill', 'nill', 'nill'),
(10, 5, 'FARIS MUHAMMED WAFY', 'muallim @ ansarul islam madarasa mampad', 'Students_74476886346493835979_DSC_0364.JPG', 10, 'KANJIRALA HOUSE KALAMKUNNU MEPADAM PO MALAPPURAM DT', 'farismk123@gmail.com', '9961703532', '9961703532', 1542, 106, 'http://www.facebook.com/fari.mampad', 'nill', 'nill', 'nill'),
(11, 4, 'ABUL FASAL WAFY', 'teacher @ shamsul huda islamic academy', 'Students_40136089896909900371_DSC_0307.JPG', 11, 'ASHARITHODI HOUSE PANAGANGARA RAMAPURM PO MALAPPURAM DT', 'atfasal313@gmail.com', '9947505984', '9947505984', 1254, 61, 'http://www.facebook.com/abu.fasal.54', 'nill', 'nill', 'nill'),
(12, 2, 'FASIL WAFY', 'Sadarmuallim @ munavirul islamedaikkal', 'Students_98916970571685978736_DSC_0262.JPG', 12, 'CHERAKKATTIL HOUSE VAZHENKADA PO ANAMANGAD VIA MALAPPURAM DT 679357PIN', 'cfasilc@gmail.com', '9605259539', '9605259539', 769, 27, 'http://www.facebook.com/fasil.cherakkattil', 'nill', 'nill', 'nill'),
(13, 6, 'SHARAFUDHEEN WAFY', 'studying', 'Students_53780419331907724816_DSC_0404.JPG', 13, 'KANDENKAI HOUSE ALIPARAMBA PO MALAPPURAM DT 679357PIN', 'sharafu657@gmail.com', '9961895096', '9961895096', 1838, 137, 'http://www.facebook.com/sharafuzz.pkd', 'http://twitter.com/pkdsharafudheen?s=08', 'https://plus.google.com/1038224667732388745227', 'nill'),
(14, 6, 'MUHAMMED ANAS WAFY', 'TEACHER @MIC THRISSUR , B.ED STUDENT', 'Students_71978707275707172993_DSC_0392.JPG', 14, 'PUNNAKKULAYAN HOUSE MANNARKAD COLLEGE PO KUMARAMPUTHOOR CHUNGAM 678583PIN', 'anaspwafy@gmail.com', '9747018704', '9747018704', 1833, 132, 'http://www.facebook.com/anas.anu.33483', 'http://twitter.com/Anastells?s=08', 'nill', 'nill'),
(15, 2, 'MUHAMMED MUSTHFA WAFY', 'PRINCIPAL SHAMSULHUDA ISLAMIC ACADEMY VADANAPPALLY', 'Students_20219855974094139017_DSC_0254.JPG', 1, 'MANDUMPARAMBATH HOUSE VADAKKEKARA PULAMANTHOL PO MALAPPURAM 679323PIN', 'musthafawafy@gmail.com', '9946732568', '9946732568', 760, 15, 'http://www.facebook.com/mpmufa', 'nill', 'nill', 'nill'),
(16, 5, 'MUHAMMED MUZAMMIL WAFY', 'TEACHER @ LIWAUL HUDA WAFY COLLEGE', 'Students_54159851653678704679_DSC_0369.JPG', 16, 'MAYILAYIL HOUSE VENGOOR PO PATTIKKAD VIA MALAPPURAM DT 679325', 'muzzammilwafy@gmail.com', '9656990502', '9072011323', 1544, 108, 'http://www.facebook.com/muhammed.muzammil.5', 'nill', 'nill', 'nill'),
(17, 5, 'FIROS AK WAFY', 'KERALA POLICE DEPARTMENT', 'Students_21474175658525425179_DSC_0354.JPG', 17, 'AMBALAKKUNNAN HOUSE RAMAPURAM PO MALAPPURAM', 'firosrpm786@gmail.com', '9744784449', '9744784449', 1535, 99, 'http://www.facebook.com/firos.ramapuram', 'nill', 'nill', 'nill'),
(18, 5, 'MUHAMMED RIYAS T WAFY', 'Principal @ vilayil wafy college', 'Students_5156597565819310476_DSC_0345.JPG', 18, 'THITTUMMAL HOUSE KUNDURKKUNNU PO MANNARKAD VIA', 'mriyast91@gmail.com', '9539468870', '9539468870', 1531, 95, 'http://www.facebook.com/riyas.muhammed.79656', 'nill', 'nill', 'nill'),
(19, 6, 'SHAREEF T WAFY', 'Lecture @ NISA women''s college Perinthalmanna', 'Students_51974670088985435303_DSC_0411.JPG', 19, 'THOTTIYIL HOUSE ARAKKUPARAMBA PO PERINTHALMANNA VIA 679322', 'shereefputhoor@gmail.com', '9605676084', '9605676084', 1842, 141, 'http://www.facebook.com/shareef.salu.7', 'nill', 'nill', 'nill'),
(20, 6, 'SHAMSUDEEN WAFY', 'studying', 'Students_65872763327486958879_DSC_0447.JPG', 20, 'ELARATHINGAL HOUSE CHERUVAYUR MUNDAKKAL PO MALAPPURAM DT 673645', 'shamsuhafiz@gmail.com', '9846979693', '9846979693', 1957, 294, 'http://www.facebook.com/shamsu.mundakkal.1', 'nill', 'nill', 'nill'),
(21, 5, 'MUHAMMED RAFEEQ.M', 'Teacher @ Al Anvar high school Kuniyil, Areekod', 'Students_26491847109972784212_DSC_0443.JPG', 21, 'MURINGAKODAN HOUSE KUNDOORKUNNU PO MANNARKAD VIAPALAKKAD DT 678583', 'mrafeekm123@gmail.com', '9605906409', '9605906409', 1518, 82, 'http://www.facebook.com/mrafeekm', 'nill', 'nill', 'nill'),
(22, 5, 'MUHAMMED FAIZAL.C WAFY', 'Teacher & Librarian @ Darul uloom islamic & arts college Paral, Thootha', 'Students_13255594527748736565_DSC_0348.JPG', 22, 'KALAMTHODIYIL HOUSE MANGALAM PO OTTAPPALAM VIA PALAKKAD DT 679301', 'faisalckerala@gmail.com', '8086776721', '8086776721', 1532, 96, 'http://www.facebook.com/faisalckerala', 'nill', 'nill', 'nill'),
(23, 1, 'SIRAJUDDEEN P WAFY', 'Teacher@ AL rashid wafy college Kodikuthiparambu', 'Students_87455148962145591381_DSC_0265.JPG', 23, 'PALARINGAL HOUSE MELANGADI PO KONDOTTY MALAPPURAM 673638', '786msiru@gmail.com', '9633647715', '9633647715', 892, 182, 'nill', 'nill', 'nill', 'nill'),
(24, 4, 'HUSAIN TK WAFY', 'Teacher @ English medium', 'Students_92672293135295542735_DSC_0319.JPG', 24, 'THONIKKADAVIL HOUSE ALIPARAMBA PO ANAMANGAD VIA MALAPPURAM DT 679357', 'yourhusaintk@gmail.com', '9544131381', '9544131381', 1265, 72, 'http://www.facebook.com/husain,tk1', 'nill', 'nill', 'nill'),
(25, 4, 'ABDURAHMAN WAFY', 'Process specialist Infosys Banglore Electronic city', 'Students_85961383747614823461_DSC_0315.JPG', 25, 'AMANATH HOUSE PATTIKKAD PO MALAPPURAM DT 679325', 'abduirahmanamanath@gmail.com', '9947057280', '9947057280', 1264, 71, 'http://www.facebook.com/abdulrahman.amanath', 'nill', 'nill', 'nill'),
(26, 6, 'JABIR KP WAFY', 'Teacher@ KMIC Memorial islamic and arts college7034', 'Students_11636741876085906075_DSC_0389.JPG', 26, 'KANDAPPADI HOUSE VAZHENKADA PO ANAMANGAD VIA MALAPPURAM DT 679357 PIN', 'jabirkp269@gmail.com', '7034212075', '7034212075', 128, 1829, 'http://www.facebook.com/profile.php?id=100008245868030', 'nill', 'nill', 'nill'),
(27, 5, 'MOHAMMED JASIR WAFY', 'Teacher @ Kondotty kasiyarakam madarasa & Unity college kondotty', 'Students_34673684299758631825_DSC_0327.JPG', 27, 'CHEOUMBAKKUZHI HOUSE MUNDAKKULAM MUTHUVALLUR PO KONDOTTY VIA MALAPPURAM DT 673638', 'akmjasirwafy@gmail.com', '9961204690', '9961204690', 1374, 264, 'http://www.facebook.com/profile.php?id=100009581795913', 'nill', 'nill', 'nill'),
(28, 6, 'SHIHABUDHEEN PK WAFY', 'Qatheeb @ vannappuram town juma masjid Thodupuzha', 'Students_12883199707536263850_DSC_0425.JPG', 28, 'PLASSERIKUDIYL HOUSE KOVALLOOR PO ERANAKULAM DT 686671', 'shihabp595@gmail.com', '9895381219', '9895381219', 2842, 288, 'http://www.facebook.com/shihab.pk.180', '', 'nill', 'nill');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students_category`
--

CREATE TABLE `tbl_students_category` (
  `CategoryId` int(11) NOT NULL,
  `Category` varchar(50) NOT NULL,
  `DisplayOrder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_students_category`
--

INSERT INTO `tbl_students_category` (`CategoryId`, `Category`, `DisplayOrder`) VALUES
(1, 'l['';hk;l''[', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tags`
--

CREATE TABLE `tbl_tags` (
  `TagId` int(11) NOT NULL,
  `TagName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_tags`
--

INSERT INTO `tbl_tags` (`TagId`, `TagName`) VALUES
(1, 'tag one'),
(2, 'tag 2'),
(3, '3rd tag');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonial`
--

CREATE TABLE `tbl_testimonial` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `designation` varchar(300) NOT NULL,
  `testimonial` varchar(2000) NOT NULL,
  `image` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_testimonial`
--

INSERT INTO `tbl_testimonial` (`id`, `name`, `designation`, `testimonial`, `image`) VALUES
(4, 'Name...', 'Designation', 'Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla mus, dignissim neque, nulla neque. Ultrices proin mi urna nibh ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur.', 'Testimonial_94626896997132438_2.png'),
(5, 'dsd', 'dds', 'dsds', 'News_1114996486117218581_Unni  Passport size photo in Colour dress.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_upload`
--

CREATE TABLE `tbl_upload` (
  `id` int(11) NOT NULL,
  `Name` varchar(400) NOT NULL,
  `Date` datetime NOT NULL,
  `UserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_upload`
--

INSERT INTO `tbl_upload` (`id`, `Name`, `Date`, `UserId`) VALUES
(1, '7861476161154714820_Capture.png', '2017-11-11 11:48:31', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_about`
--
ALTER TABLE `tbl_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_article`
--
ALTER TABLE `tbl_article`
  ADD PRIMARY KEY (`ArticleId`);

--
-- Indexes for table `tbl_article_category`
--
ALTER TABLE `tbl_article_category`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  ADD PRIMARY KEY (`BlogId`);

--
-- Indexes for table `tbl_blogcategory`
--
ALTER TABLE `tbl_blogcategory`
  ADD PRIMARY KEY (`BlogCategoryId`);

--
-- Indexes for table `tbl_blogimage`
--
ALTER TABLE `tbl_blogimage`
  ADD PRIMARY KEY (`imageId`);

--
-- Indexes for table `tbl_blogtags`
--
ALTER TABLE `tbl_blogtags`
  ADD PRIMARY KEY (`BlogTagsId`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `tbl_clients`
--
ALTER TABLE `tbl_clients`
  ADD PRIMARY KEY (`ClientId`);

--
-- Indexes for table `tbl_committee`
--
ALTER TABLE `tbl_committee`
  ADD PRIMARY KEY (`CommitteeId`);

--
-- Indexes for table `tbl_contacts_category`
--
ALTER TABLE `tbl_contacts_category`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `tbl_facilities`
--
ALTER TABLE `tbl_facilities`
  ADD PRIMARY KEY (`facilitiesId`);

--
-- Indexes for table `tbl_gallary_images`
--
ALTER TABLE `tbl_gallary_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`GalleryId`);

--
-- Indexes for table `tbl_gallery_category`
--
ALTER TABLE `tbl_gallery_category`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `tbl_home_general`
--
ALTER TABLE `tbl_home_general`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_institutions`
--
ALTER TABLE `tbl_institutions`
  ADD PRIMARY KEY (`institutionsId`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`UserId`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`NewsId`);

--
-- Indexes for table `tbl_news_category`
--
ALTER TABLE `tbl_news_category`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `tbl_portfolio`
--
ALTER TABLE `tbl_portfolio`
  ADD PRIMARY KEY (`portfolioId`);

--
-- Indexes for table `tbl_project`
--
ALTER TABLE `tbl_project`
  ADD PRIMARY KEY (`projectId`);

--
-- Indexes for table `tbl_projectnew`
--
ALTER TABLE `tbl_projectnew`
  ADD PRIMARY KEY (`projectId`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD PRIMARY KEY (`StaffId`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`StudentsId`);

--
-- Indexes for table `tbl_students_category`
--
ALTER TABLE `tbl_students_category`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `tbl_tags`
--
ALTER TABLE `tbl_tags`
  ADD PRIMARY KEY (`TagId`);

--
-- Indexes for table `tbl_testimonial`
--
ALTER TABLE `tbl_testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_upload`
--
ALTER TABLE `tbl_upload`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  MODIFY `BlogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_blogcategory`
--
ALTER TABLE `tbl_blogcategory`
  MODIFY `BlogCategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_blogimage`
--
ALTER TABLE `tbl_blogimage`
  MODIFY `imageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `tbl_blogtags`
--
ALTER TABLE `tbl_blogtags`
  MODIFY `BlogTagsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `CategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_clients`
--
ALTER TABLE `tbl_clients`
  MODIFY `ClientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_facilities`
--
ALTER TABLE `tbl_facilities`
  MODIFY `facilitiesId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_institutions`
--
ALTER TABLE `tbl_institutions`
  MODIFY `institutionsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_portfolio`
--
ALTER TABLE `tbl_portfolio`
  MODIFY `portfolioId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tbl_project`
--
ALTER TABLE `tbl_project`
  MODIFY `projectId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_projectnew`
--
ALTER TABLE `tbl_projectnew`
  MODIFY `projectId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_tags`
--
ALTER TABLE `tbl_tags`
  MODIFY `TagId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
