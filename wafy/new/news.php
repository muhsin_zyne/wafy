
<?php
include('DUIAC_Admin/General/DBConnection.php');
?>


<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="DUIAC" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="KMECC MARAYAMANGALAM ">
<link href="favicon.png" rel="icon">

<!-- Fonts
    ============================================= -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100italic,100,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700italic,700,800,800italic,900,900italic%7COpen+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic%7CUbuntu:400,300,300italic,400italic,500,700,500italic,700italic%7CRoboto+Slab:400,100,300,700%7CLora:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">


<!-- Document Title
    ============================================= -->
<title>KMECC MARAYAMANGALAM  | News </title>
</head>
<body>

<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">

<?php include('header.php'); ?>

		

<!-- Page Title #1
============================================= -->
<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="Resource/news.jpg" alt="Wafy News"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="title-1 text-center">
					
					<div class="title-heading">
						<h2>News</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->

<section id="blog-grid-1-col" class="blog blog-grid blog-1col">
	<div class="container">
	
		<?php $newsQuery=mysqli_query($con,"select * from tbl_news where Approve=1 order by NewsId desc ");
							while($newsResult=mysqli_fetch_array($newsQuery))
							{
							?>
							
	
		<div class="row" id="<?php echo "News_".$newsResult['NewsId'];?>">
			<!-- Entry #1 -->
			<div class="col-xs-12 col-sm-12 col-md-11 entry clearfix">
				<div class="col-md-4">
				<div class="entry-featured">
					<img src="<?php echo "Resource/News/".$newsResult['Image'];?>" alt="News Image">
			
				</div><!-- .entry-featured end -->
				</div>
				<div class="col-md-8">
				
				<div class="entry-meta">
					<span>In : </span><span><a ><?php $cat=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_news_category where CategoryId='".$newsResult['CategoryId']."'"));
															echo  $cat[0];?></a></span> /
					<span>On : </span><span> <a><?php echo date('M d Y',strtotime($newsResult['Date']));?></a></span> /
				
				</div><!-- .entry-meta end -->
				<div class="entry-title">
					<h3><a><?php echo $newsResult['Heading'];?></a></h3>
				</div><!-- .entry-title end -->
				<div class="entry-content">
					<p><?php echo $newsResult['Content'];?></p>
				</div><!-- .entry-content end -->
				</div>

			</div><!-- .col-md-10 end -->
			

			
		</div><!-- .row end -->
		
		<?php } ?>
		
		

	</div><!-- .container end -->
</section><!-- #page-title end -->





			


<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/functions.js"></script>
</body>

<!-- Mirrored from 7oroof.com/tfdemos/mount/contact-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Nov 2016 05:26:56 GMT -->
</html>