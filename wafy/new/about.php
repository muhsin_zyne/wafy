<?php
include('DUIAC_Admin/General/DBConnection.php');

$AbotUsEng=mysqli_fetch_array(mysqli_query($con,"select * from tbl_about where id=1"));
$AbotUsArb=mysqli_fetch_array(mysqli_query($con,"select * from tbl_about where id=2"));
$AbotUsMal=mysqli_fetch_array(mysqli_query($con,"select * from tbl_about where id=3"));
$HomeGeneral=mysqli_fetch_array(mysqli_query($con,"select * from tbl_home_general where id=1"));
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="DUIAC" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="KMECC MARAYAMANGALAM ">
<link href="favicon.png" rel="icon">

<!-- Fonts
    ============================================= -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100italic,100,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700italic,700,800,800italic,900,900italic%7COpen+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic%7CUbuntu:400,300,300italic,400italic,500,700,500italic,700italic%7CRoboto+Slab:400,100,300,700%7CLora:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">


<!-- Document Title
    ============================================= -->
<title>KMECC MARAYAMANGALAM  | About Us</title>
</head>
<body>

<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">

<?php include('header.php'); ?>

		

<!-- Page Title #1
============================================= -->
<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="Resource/AboutUs/about-us.jpg" alt="About Us"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="title-1 text-center">
					
					<div class="title-heading">
						<h2>About Us</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->


<!-- Service Block #1
============================================= -->
<section id="service1" class="service service-7">
	<div class="container">
		
        <style>
body {font-family: "Lato", sans-serif;}

ul.tab {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
  /*  border: 1px solid #ccc;
    background-color: #f1f1f1;*/
}

/* Float the list items side by side */
ul.tab li 
{
	float: left;
margin-left: 5px;
}

/* Style the links inside the list items */
ul.tab li a {
    display: inline-block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    transition: 0.3s;
    font-size: 14px;
	    border-radius: 10%;
}

/* Change background color of links on hover */
ul.tab li a:hover {
    background-color: #f7f7f7;
}

/* Create an active/current tablink class */
ul.tab li a:focus, .tabactive {
    background-color: #efeded;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
 /*   border: 1px solid #ccc;*/
    border-top: none;
}

.topright {
 float: right;
 cursor: pointer;
 font-size: 20px;
}

.topright:hover {color: red;}
</style>

<ul class="tab">
  <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">English</a></li>
  <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Paris')">العربية</a></li>
  <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Tokyo')">മലയാളം</a></li>
</ul>

<div id="London" class="tabcontent shortcode-1">
  <div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row">
					
					<div class="col-xs-12 col-sm-12 col-md-12">
						
						
						<iframe width="55%"  height="400" src="<?php echo $AbotUsEng['video'];?>" frameborder="0" allowfullscreen style="float: left;
    margin-right: 30px;
    margin-bottom: 30px;
    margin-top: 7px;"></iframe>
						<p class="mb-0"><?php echo $AbotUsEng['description'];?> </p><br />
					</div><!-- .col-md-10 end -->
				</div><!-- .row end -->
			</div><!-- .col-md-6 end -->
            </div>
            </div>
          	
        
                  		<div class="row">
			<!-- Service #1 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsEng['icon1'];?>">
						<h3><?php echo $AbotUsEng['head1'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo $AbotUsEng['content1'];?></p>
                      
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			
			<!-- Service #1 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsEng['icon2'];?>">
						<h3><?php echo $AbotUsEng['head2'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo $AbotUsEng['content2'];?></p>
                      
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->

          <!-- Service #1 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsEng['icon5'];?>">
						<h3><?php echo $AbotUsEng['head5'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo $AbotUsEng['content5'];?></p>
					</div>
				</div>
			</div>


	</div>
            
            
            
        
                    
</div>

<div id="Paris" class="tabcontent shortcodeArabic">
  
  <div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row">
					
					<div class="col-xs-12 col-sm-12 col-md-12">
						<iframe width="55%"  height="400" src="<?php echo $AbotUsArb['video'];?>" frameborder="0" allowfullscreen style="float: right;
    margin-left: 30px;
    margin-bottom: 30px;
    margin-top: 7px;"></iframe>
                        	
						<p class="mb-0">
                       <?php echo $AbotUsArb['description'];?>
                        </p> <br />


					</div><!-- .col-md-10 end -->
				</div><!-- .row end -->
			</div><!-- .col-md-6 end -->
            </div>
            </div>
            <div class="row">
			<div class="col-xs-12  col-sm-12  col-md-2  portfolio-more " style="margin-bottom:20px;">
				
			</div><!-- .col-md-2 end -->
		</div><!-- .row end -->
               		<div class="row">
			<!-- Service #1 -->
		<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsArb['icon1'];?>">
						<h3><?php echo $AbotUsArb['head1'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo $AbotUsArb['content1'];?></p>
                      
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			
			
			<!-- Service #2 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsArb['icon2'];?>">
						<h3><?php echo $AbotUsArb['head2'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo $AbotUsArb['content2'];?></p>
                      
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->


			<!-- Service #4 -->
		<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsArb['icon5'];?>">
						<h3><?php echo $AbotUsArb['head5'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo $AbotUsArb['content5'];?></p>
					</div>
				</div>
			</div>

			
			

		</div><!-- .row end -->
</div>

<div id="Tokyo" class="tabcontent shortcode-1">

 <div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="row">
					
					<div class="col-xs-12 col-sm-12 col-md-12">
							<iframe width="55%"  height="400" src="<?php echo $AbotUsMal['video'];?>" frameborder="0" allowfullscreen style="float: left;
    margin-right: 30px;
    margin-bottom: 30px;
    margin-top: 7px;"></iframe>
                        
<p class="mb-0">
<?php echo $AbotUsMal['description'];?>

</p><br />
					</div><!-- .col-md-10 end -->
				</div><!-- .row end -->
			</div><!-- .col-md-6 end -->
            </div>
            </div>


<div class="row">
<!--			<div class="col-xs-12  col-sm-12  col-md-2  portfolio-more " style="margin-bottom:20px;">
				<a href="about.php">Read More <i class="fa fa-angle-double-right"></i></a>
			</div>
-->		</div><!-- .row end -->

     		<div class="row">
			<!-- Service #1 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsMal['icon1'];?>">
						<h3><?php echo $AbotUsMal['head1'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo $AbotUsMal['content1'];?></p>
                      
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsMal['icon2'];?>">
						<h3><?php echo $AbotUsMal['head2'];?></h3>
					</div><!-- .service-heading end -->
					<div class="service-body">
						<p><?php echo $AbotUsMal['content2'];?></p>
                      
					</div><!-- .service-body end -->
				</div>
			</div><!-- .col-md-12 end -->
			<!-- Service #3 -->

<!--			</div>
           			<div class="row" style="margin-top: 15px">
-->			<!-- Service #4 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel active">
					<div class="service-heading">
					
						<img class="AboutImgIcon" src="<?php echo "Resource/AboutUs/".$AbotUsMal['icon5'];?>">
						<h3><?php echo $AbotUsMal['head5'];?></h3>
					</div>
					<div class="service-body">
						<p><?php echo $AbotUsMal['content5'];?></p>
					</div>
				</div>
			</div>

		</div><!-- .row end -->
 
</div>

<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" tabactive", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " tabactive";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>

	</div><!-- .container end -->
</section><!-- #service1 end -->






<section id="visionandmission" class="service service-2 service-4 service-8" style="background-color: #F3F3F3">
	<div class="container">
		<div class="row">

            <div class="col-md-6">
				<div class="row heading-1 mb-30 clearfix">
					<div class="col-xs-12  col-sm-12  col-md-3">
						<div class="heading mb-0">					<h2><?php echo $AbotUsEng['head3'];?></h2>					</div>
				</div></div>
					<div class="service-body">
						<p><?php echo $AbotUsEng['content3'];?></p>
					</div>
			</div>

            <div class="col-md-6">
				<div class="row heading-1 mb-30 clearfix">
					<div class="col-xs-12  col-sm-12  col-md-3">
						<div class="heading mb-0">					<h2><?php echo $AbotUsEng['head4'];?></h2>					</div>
				</div></div>
					<div class="service-body">
						<p><?php echo $AbotUsEng['content4'];?></p>
					</div>
			</div>

        </div>
	</div>
</section>





<section class="h300 bg-overlay bg-parallex">
	<div class="bg-section">
		<img src="Resource/AboutUs/download-brochure.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12  col-sm-12  col-md-8 col-md-offset-2 text-center">
				<h3 class="text-white">Braucher</h3>
				<p class="text-white"><?php echo $HomeGeneral['braucher'];?></p>
				<a class="btn btn-secondary btn-white" href="<?php echo "Resource/AboutUs/".$HomeGeneral['braucher_file'];?>" download>Download <i class="fa fa-angle-double-right"></i></a>
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section>


<section id="service2" class="service service-2 service-4 service-8">
	<div class="container">
		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="heading mb-0">
					
					<h2>Fecilities</h2>
				</div><!-- .heading end -->
			</div><!-- .col-md-4 end -->
			<div class="col-xs-12  col-sm-12  col-md-7">
				<p class="mb-0"><?php echo $HomeGeneral['fecilities'];?></p>
			</div><!-- .col-md-8 end -->
		</div><!-- .row end -->
		<div class="row row-bordered">
			<!-- Service #1 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel">
					<div class="service-heading">
						<img src="assets/images/ICON/Library.png">
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Library</h3>
						<p>The library is blessed with more than 5000 books in different sections Islamic subjects like thafseer, hadees, fiqh, aqeeda, literature etc...  </p>
						
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
			
			<!-- Service #2 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel">
					<div class="service-heading">
						<img src="assets/images/ICON/Edu--theater.png">
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Edu- theater</h3>
						<p>DUIAC offers a well-equipped edu theatre, with the facilities of lcd projector, sound system and television.  </p>
					
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
			
			<!-- Service #3 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel last">
					<div class="service-heading">
						<img src="assets/images/ICON/Computer-Lab.png">
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Computer Lab</h3>
						<p>sAiming at the overall development of the students there is a full-fledged computer lab with internet connection and wifi.  

</p>
						
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
		<div class="row ">			
			<!-- Service #4 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel">
					<div class="service-heading">
						<img src="assets/images/ICON/Hostel.png">
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Hostel</h3>
						<p>The college provides hostel facilities free of cost to the students. </p>
					
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
			
			<!-- Service #5 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel">
					<div class="service-heading">
						<img src="assets/images/ICON/Smart-class-room.png">
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Smart class room</h3>
						<p>Classes are privileged with modern teaching aids for making the teaching more effective./p>
						
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
			
			<!-- Service #6 -->
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="service-panel last">
					<div class="service-heading">
						<img src="assets/images/ICON/Work-Shops.png">
					</div><!-- .service-heading end -->
					<div class="service-body">
						<h3>Work Shops</h3>
						<p>Workshops are conducted under CIC for the different sections - teachers, managements, principals, students’ union leaders...</p>
					
					</div><!-- .service-body end -->
				</div><!-- .service-panel end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
        

        
        
	</div><!-- .container end -->
</section>




<section id="service2" class="service service-2 service-4 service-8" style="background-color: #F3F3F3">
	<div class="container">
		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="heading mb-0">
					
					<h2>Achievements</h2>
				</div><!-- .heading end -->
			</div><!-- .col-md-4 end -->
			
		</div><!-- .row end -->
		
		<style>
			.Achievements li
			{
				.text-align: justify;
				padding: 5px;
				margin-bottom: 5px;
				font-family:Comic Sans MS,cursive;
				font-size:14px;
				color: #2A2A2A;
				list-style: circle;
				
			}
			.Achievements li strong
			{
				color: red;
				font-size:16px;
			}



		</style>


		<ul class="Achievements">
	<li ><strong>First prize</strong> in &ldquo;Sakshaym Exhibition&rdquo; held by Samastha Kerala Jamiyyathul Ulama to mark its 85<sup>th</sup> anniversary.</li>
	<li ><strong>First prize</strong> in the &ldquo;miles t o go&rdquo; quiz competition held by Islahul Uloom Arabic College, as part of its 90 <sup>th</sup> anniversary.</li>
	<li ><strong>Second prize</strong> in district &ldquo;sargalayam&rdquo; held under Skssf .</li>
	<li >Secured <strong>second prize</strong> in &ldquo;Aliparamba zone SKSSF Sargalayam&rdquo; in the year 2015.</li>
	<li >Bagged <strong>second prize</strong> in quiz competition &ldquo;Sawal Jawab&rdquo; conducted by Darshana TV.</li>
	<li ><strong>First runner up</strong> in consecutive four years 2012-2105 &nbsp;&nbsp;in State Wafy arts fest.</li>
	<li ><strong>Second runner up</strong> in Al Fakeeh all kerala fikh quiz competition held under fiqh department of Darul Huda Islamic University.</li>
</ul>




	</div>
		</section>




<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="assets/images/page-title/title-18.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12  col-sm-12  col-md-12">
				<div class="title-1 text-center">
					<div class="title-heading">
						<h2>Our Staff</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->

<!-- Team Section
============================================= -->
<section id="staff" class="pb-0 team team-2">
	<div class="container">
		<div class="row">
			
			
			<?php $staffQuery= mysqli_query($con,"select * from tbl_staff order by DisplayOrder asc");
			while($staffArray=mysqli_fetch_array($staffQuery)) 
			{
			
			?>
			
			<!-- Member #1 -->
			
			
			<div class="col-xs-12 col-sm-6 col-md-3 member">
				<div class="member-img">
					<img src="<?php echo "Resource/Staff/".$staffArray['Image']; ?>" alt="member"/>
					<?php if(!empty($staffArray['FbLink'])) {?>
					<div class="member-hover">
						<div class="member-social">
							<a href="<?php echo $staffArray['FbLink']; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
							
						</div>
					</div><!-- .member-hover end -->
					<?php } ?>
				</div><!-- .member-img end -->
				<div class="member-overlay">
					<h6><?php echo $staffArray['Designation']; ?></h6>
					<h5><?php echo $staffArray['Name']; ?></h5>
				</div><!-- .memebr-ovelay end -->
			</div><!-- .member end -->
			<?php } ?>
			<!-- Member #2 -->
		

			
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #team end -->




<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/functions.js"></script>
</body>

<!-- Mirrored from 7oroof.com/tfdemos/mount/contact-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Nov 2016 05:26:56 GMT -->
</html>