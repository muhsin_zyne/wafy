<?php include('General/Header.php'); 

if(isset($_REQUEST['GalleryIdUpdate']))
{
   $id=$_REQUEST['GalleryIdUpdate'];
   //echo $id;exit;
   $result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_gallery WHERE GalleryId='$id'"));
}
else
{
	echo "<script>window.location='Gallery.php'</script>";
}

?>
	<script>
        function validateForm() {
            
            var Category = document.forms["FormGallery"]["Category"].value;
			var imgpath=document.getElementById('mainImage');
				var subimages = document.getElementById('subImages');
            if (Category==0) 
			{
				alert("Please Select Category.");     
				return false;       
			}
			else if (!imgpath.value=="")
			{
				var imgsize=imgpath.files[0].size;
				if(imgsize>1000000)	
				{		
					alert(imgpath.files[0].name+" - Size is larger than 1MB. Please Reduce it.");	
					return false;	
				}
			}
			else
			{
				var isok=true;
				for (var i = 0; i < subimages.files.length; i++)
				{
				 var imageSize = subimages.files[i].size;
				 if (imageSize > 1000000) 
				 {
						alert(subimages.files[i].name+" - Size is larger than 1MB. Please Reduce it.");	
						isok= false;
						 break;
				 	}

			 	}
				if(!isok)
					return flase;
				
			}
			
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Gallery</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Edit Gallery</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="action.php" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormGallery">

				

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gallery Category <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="Category" id="Category" >
                            <option value="0" selected="selected" >- Select Category -</option>
                                <?php 
                                $GalleryCategoryRow=mysqli_query($con,"SELECT * FROM tbl_gallery_category order by CategoryId desc");
                                while($GalleryCategoryResult=mysqli_fetch_array($GalleryCategoryRow)){?>
                                    <option value="<?php echo $GalleryCategoryResult['CategoryId'];?>" ><?php echo $GalleryCategoryResult['Category']; ?></option>
                                <?php } ?>
                                
                         <script>
							var Category= document.getElementById('Category');
							Category.value=<?php echo $result['CategoryId'];?>
							</script>	
								
                        </select>
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gallery Heading<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Heading" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Heading'];?>"> 
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gallery Content<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea name="Content" required class="form-control col-md-7 col-xs-12" rows="5"><?php echo $result['Content'];?></textarea>
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Date<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="date" name="Date" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Date'];?>">
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>750px</b> , Height: <b>500px</b> )</font>
						<input type="file" name="mainImage" id="mainImage"  class="form-control col-md-7 col-xs-12" accept="image/*"  >
						
						  <a href="<?php echo "../Resource/Gallery/".$result['Image'];?>" download> <img src="<?php echo "../Resource/Gallery/".$result['Image'];?>" width="150" > </a>
						  
				</div></div>
              
              
                     <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Sub Images </label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>750px</b> , Height: <b>500px</b> )</font>
						<input type="file" name="subImages[]" id="subImages" multiple class="form-control col-md-7 col-xs-12" accept="image/*"  >
						<br />
						<?php $imgquery=mysqli_query($con,"select Image from tbl_gallary_images where GalleryId= '".$result['GalleryId']."'");
						 while($imgArray=mysqli_fetch_array($imgquery))
							{
							?>
							 <a href="<?php echo "../Resource/Gallery/".$imgArray['Image'];?>" download> <img src="<?php echo "../Resource/Gallery/".$imgArray['Image'];?>" width="150" > </a>
							<?php } ?>
				</div></div>
              
               

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <input type="hidden" name="GalleryUpdate" value="<?php echo $result['GalleryId'];?>">
						<button type="submit"  class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>


	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
