<?php include('General/Header.php'); 


if(isset($_REQUEST['TestimonialIdUpdate']))
{
   $id=$_REQUEST['TestimonialIdUpdate'];
   //echo $id;exit;
   $result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_testimonial WHERE id='$id'"));
}
else
{
	echo "<script>window.location='Testimonial.php'</script>";
}
	

?>
	<script>
        function validateForm() {
            var name = document.forms["FormArticle"]["name"].value;
			var designation = document.forms["FormArticle"]["designation"].value;
			var testimonial = document.forms["FormArticle"]["testimonial"].value;
			
            if (!name||!designation||!testimonial) 
			{
				alert("Please Fill All Fields.");     
				return false;       
			}
			
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Testimonial</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Update</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="action.php" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormArticle">

				

			

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="name" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['name'];?>">
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Designation<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="designation" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['designation'];?>">
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Testimonial<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea name="testimonial" required class="form-control col-md-7 col-xs-12" rows="8"> <?php echo $result['testimonial'];?></textarea>
				</div></div>
				
            
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
								<font color="#FF0004"> Image Dimension --- width: <b>70px</b> , Height: <b>70px</b> </font>
						<input type="file" name="mainImage" id="mainImage"  class="form-control col-md-7 col-xs-12" accept="image/*"  >
						<br />
						<a href="<?php echo "../Resource/Testimonial/".$result['image'];?>" download> <img src="<?php echo "../Resource/Testimonial/".$result['image'];?>" width="100" > </a>
				
				</div></div>
               <input type="hidden" name="TestimonialUpdate" value="<?php echo $result['id'];?>">

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" name="ArticleSubmit" class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>

	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
