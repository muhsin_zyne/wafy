<?php include('General/Header.php'); 
if(isset($_REQUEST['UserId']))
{
   $id=$_REQUEST['UserId'];
   $result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_login WHERE UserId='$id'"));
}
else
{	echo "<script>window.location='Home.php'</script>";}

if(isset($_POST['UserSubmit']))
{
		$Name=trim($_POST['Name']);
		$Password=trim($_POST['Password']);

		$result=mysqli_query($con,"update tbl_login set Name ='$Name',Password='$Password' where  UserId='$id' ");
		if($result)
		{	echo '<script>alert(" Successfully Updated");</script>';	}
		else
		{	echo '<script>alert("Current Password Not Math");</script>';	}
			echo "<script>window.location='Home.php'</script>";
}
?>
<script>
        function validateForm() {

            var Password = document.forms["FormUser"]["Password"].value;
            var ConfirmPassword = document.forms["FormUser"]["ConfirmPassword"].value;
			 var CurrentPassword = document.forms["FormUser"]["CurrentPassword"].value;
			 var SessionPassword = document.forms["FormUser"]["SessionPassword"].value;

            if (Password!=ConfirmPassword) {
                alert("Password doesn't match");                return false;
            }
			else if (CurrentPassword!=SessionPassword) {
                alert("Current Password doesn't match");        return false;
            }
        }
</script>
<div class="right_col" role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Profile</h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edit</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
<form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormUser">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="Name" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Name']; ?>">
                    </div></div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">User Name <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                       <input type="text" name="UserName" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['UserName']; ?>" readonly>
                    </div></div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Current Password  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" name="CurrentPassword" required class="form-control col-md-7 col-xs-12" >
                    </div></div>
                   <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">New Password  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" name="Password" required class="form-control col-md-7 col-xs-12" >
                   </div></div>
                   <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Confirm Password  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" name="ConfirmPassword" required class="form-control col-md-7 col-xs-12" >
                   </div></div>

                  <input type="hidden" name="SessionPassword" value="<?php echo $_SESSION['DUIPassword']; ?>" />

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" name="UserSubmit" class="btn btn-success">Submit</button>
                    </div></div>

                  </form>
          </div></div></div></div>
	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>
<?php include('General/Footer.php'); ?>