<?php include('General/Header.php'); 
if(isset($_POST['BlogSubmit']))
{
	$CategoryId=$_POST['BlogCategory'];
	$Title=trim($_POST['Title']);
	$Author=trim($_POST['Author']);
	$BlogDate=trim($_POST['BlogDate']);
	$Summary=trim($_POST['Summary']);
	$DetailedContent=trim($_POST['DetailedContent']);
	$SEOTitle=trim($_POST['SEOTitle']);
	$SEOKeyword=trim($_POST['Keyword']);
	$SEODesc=trim($_POST['SEO']);
					
	$Display=$_POST['Display'];

	$currentdate = date("Y:m:d H:i:s");
	$CategoryIdArray = mysqli_fetch_array(mysqli_query($con,"SELECT BlogCategoryName FROM tbl_blogcategory WHERE BlogCategoryId='$CategoryId'"));
	$Category=$CategoryIdArray['BlogCategoryName'];

	if(isset($_FILES['MainImage']['name']) && !empty($_FILES['MainImage']['name']))
	{	if($_FILES['MainImage']['size'] > 400000)		{ //10 MB (size is also in bytes)
			echo '<script type="text/javascript">alert("'.$_FILES['MainImage']['name'].' - Size is larger than 400KB. Please Reduce it.");</script>';	}
		else{
			$isok=true;
			if(isset($_FILES['SubImages']) && !empty($_FILES['SubImages']['name'][0]))
			{	foreach($_FILES['SubImages']['tmp_name'] as $key => $tmp_name )
				{	$fileSize = $_FILES['SubImages']['size'][$key];
					if ($fileSize > 400000)		{	  $isok=false;												  break;		}
			}	}
			if(!$isok)
		{ echo '<script type="text/javascript">alert("'.$key.$_FILES['SubImages']['name'][$key].' - Size is larger than 400KB. Please Reduce it.");</script>';}
			else
			{	$filename=$_FILES['MainImage']['name'];
				$temp=$_FILES['MainImage']['tmp_name'];
				$ra=rand(10,10000000000);
				$rb=rand(10,10000000000);
				$r=$ra.$rb;
				$image_name=$r.$filename;
				move_uploaded_file($temp,"../AdminImage/Blog/".$image_name);

$result=mysqli_query($con,"INSERT INTO  tbl_blog (BlogCategoryId,BlogTitle,Author,PostDate,Summary,Description,BlogImage,Display,Date,Time)
					 VALUES('$CategoryId','$Title','$Author','$BlogDate','$Summary','$DetailedContent','$image_name','$Display','$currentdate',NOW())");

										$product=mysqli_fetch_array(mysqli_query($con,"select max(BlogId) as BlogId from tbl_blog"));
										$BlogId=$product['BlogId'];

										if(isset($_FILES['SubImages']) && !empty($_FILES['SubImages']['name'][0]))
										{	foreach($_FILES['SubImages']['tmp_name'] as $key => $tmp_name )
											{	$files_name = $key.$_FILES['SubImages']['name'][$key];
												$files_tmp =$_FILES['SubImages']['tmp_name'][$key];
												$ra=rand(10,10000000000);
												$rb=rand(10,10000000000);
												$r=$ra.$rb;
												$images_name=$r.$files_name;
												move_uploaded_file($files_tmp,"../AdminImage/Blog/".$images_name);
												mysqli_query($con,"INSERT INTO tbl_blogimage(BlogId,BlogImages)values('$BlogId','$images_name')");
										}	}

$blog=mysqli_fetch_array(mysqli_query($con,"select max(BlogId) as BlogId from tbl_blog"));
$blogId=$blog['BlogId'];

	if(!empty($_POST["Tags"]))
	{	foreach($_POST["Tags"] as $tag)
		{	$in_ch=mysqli_query($con,"insert into tbl_blogtags (tagId,blogId) values ('$tag','$blogId')");	}
	}

									if($result)	{				echo '<script>alert(" Successfully Inserted");</script>';			}
									else		{				echo '<script>alert("Data Not Inserted");</script>';			}
									echo "<script>window.location='Blog.php'</script>";
}	}	  }		}
?>

	<script>
        function validateForm() {
            var MainCategory = document.forms["FormBlog"]["BlogCategory"].value;
			var imgpath=document.getElementById('MainImage');

            if (MainCategory==0) {
                alert("Please Select Category.");                return false;
            }
			else if (!imgpath.value==""){
				var imgsize=imgpath.files[0].size;
				if(imgsize>2000000)
				{				alert(imgpath.files[0].name+" - Size is larger than 400KB. Please Reduce it.");				return false;				}
			  }
				var Images = document.getElementById('SubImages');  
				for (var i = 0; i < Images.files.length; i++)
				{
					 var imageSize = Images.files[i].size;
					 if (imageSize > 400000) {	alert(Images.files[i].name+" - Size is larger than 400KB. Please Reduce it.");	return false;	break;	}
		}		 }
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">              <h3>Blog</h3>            </div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">                  <h2>New Blog</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>                    </li>
					</ul>
					<div class="clearfix"></div>
                </div>
                <div class="x_content">
					<br />
                  <form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormBlog">

                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Category <span class="required">*</span>	</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                        <select class="form-control" name="BlogCategory" id="BlogCategory" >
                            <option value="0" selected="selected" >- Select Category -</option>
                                <?php 
                                $MainCategoryRow=mysqli_query($con,"SELECT * FROM tbl_blogcategory order by BlogCategoryId desc");
                                while($MainCategoryResult=mysqli_fetch_array($MainCategoryRow)){?>
                                    <option value="<?php echo $MainCategoryResult['BlogCategoryId'];?>" ><?php echo $MainCategoryResult['BlogCategoryName']; ?></option>
                                <?php } ?>
                        </select>
                    </div></div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Blog Title<span class="required">*</span>	</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                        <input type="text" name="Title" required class="form-control col-md-7 col-xs-12">
                    </div></div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Author <span class="required">*</span>	</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" name="Author" required class="form-control col-md-7 col-xs-12">
                      </div>

					  <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Post Date <span class="required">*</span>	</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="date" value="<?php echo date('Y-m-d');?>" name="BlogDate" class="form-control col-md-7 col-xs-12">
                    </div></div>

                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Tags 			</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
<?php
	$TagsRow=mysqli_query($con,"SELECT * FROM tbl_tags order by TagId desc");
	while($TagsResult=mysqli_fetch_array($TagsRow)){?>
			<input type="checkbox" name="Tags[]" value="<?php echo $TagsResult['TagId'];?>" ><?php echo $TagsResult['TagName'];?>
<?php } ?>
                    </div></div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Summary <span class="required">*</span>			</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                        <textarea name="Summary" id="Summary" required class="form-control col-md-7 col-xs-12" rows="5"></textarea>
	                </div></div>

					<div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Detailed Content <span class="required">*</span>			</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                         <textarea name="DetailedContent" id="DetailedContent" required class="form-control col-md-7 col-xs-12" rows="6"></textarea>
                    </div></div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Main Image <span class="required">*</span>			</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                      <font color="#FF0004">Max-Image Size: <b>2MB</b> &nbsp; (Image Dimension --- width: <b>424px</b> , Height: <b>318px</b> )</font>
                          <input type="file" id="MainImage" name="MainImage"  class="form-control col-md-7 col-xs-12" accept="image/*" required >
                    </div></div>

<!--					<div class="form-group">
						<label id="" class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Sub</label>
						<div class="col-md-10  col-sm-6 col-xs-12">
							<select class="form-control" name="Sub" id="Sub" >
								<option value="1" selected="selected" >Images</option>
								<option value="2"  >Video</option>
							</select>
	<script>
		$(document).ready(function () {
			$('#videoLinkLabel').hide();			$('#videoLink').hide();
			$('#Sub').change(function (){
				if($('#Sub').val()=='1')
				{	$('#imageLabel').show();			$('#SubImages').show();			$('#videoLinkLabel').hide();			$('#videoLink').hide();		}
				else
				{	$('#videoLink').show();			$('#SubImages').hide();			$('#videoLinkLabel').show();			$('#imageLabel').hide();		}
		});	});
	</script>
					</div></div>

          <div class="form-group">
                      <label id="videoLinkLabel" class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Video Link</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
         <input type="text" class="form-control col-md-7 col-xs-12" name="videoLink" id="videoLink">
        </div></div>-->
        <div class="form-group">
            <label id="imageLabel" class="control-label col-md-2 col-sm-3 col-xs-12">Images</label>
                      <div class="col-md-10  col-sm-6 col-xs-12 multiplefile">
            <input type="file"  class="form-control col-md-7 col-xs-12" name="SubImages[]" id="SubImages" accept="image/*" multiple >
        </div></div>

<!--<div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name"> Date <span class="required">*</span>	</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="date" value="<?php //echo date('Y-m-d');?>" name="Date" class="form-control col-md-7 col-xs-12">
                      </div>
                      <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name"> Time <span class="required">*</span>	</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="time" value="<?php // echo time(); ?>" name="Time" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>-->
		<div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">SEO Title <span class="required">*</span>	</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                        <input type="text" name="SEOTitle" required class="form-control col-md-7 col-xs-12">
		</div></div>
		<div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">SEO Keyword <span class="required">*</span>	</label>
                      <div class="col-md-10  col-sm-6 col-xs-12">
                        <input type="text" name="Keyword" required class="form-control col-md-7 col-xs-12">
		</div></div>
                     <div class="form-group">
    	                  <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">SEO Description <span class="required">*</span>	</label>
	                      <div class="col-md-10  col-sm-6 col-xs-12">
         	                <textarea name="SEO" id="SEO" required class="form-control col-md-7 col-xs-12" rows="6"></textarea>
                     </div></div>
                     <div class="form-group">
	                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Approve <span class="required">*</span></label>
                      <div class="col-md-10 col-sm-6 col-xs-12">
                          <input type="checkbox" name="Display" class="form-control col-md-7 col-xs-12" value="1" >
                     </div></div>

                    <div class="form-group">
                      <div class="col-md-10  col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" name="BlogSubmit" class="btn btn-success">Submit</button>
                    </div></div>
                    <script src="ckeditor/ckeditor.js" type="text/javascript"></script>
                      <script>
                CKEDITOR.replace( 'Summary' );
                CKEDITOR.replace( 'DetailedContent' );
            </script>
                  </form>
		</div></div></div></div>

 		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">                  <h2>Blog</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />

                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>                              <th>Category</th>                              <th>Title</th>
                              <th>Author</th>                              <th>Summary</th>                              <th>Main Image</th>
                              <th>Description</th>                              <th>Approve</th>                              <th>Post Date</th>
                              <th>SEO Title</th>                              <th>SEO Keyword</th>                               <th>SEO Description</th>
<!--                              <th>Tags</th> -->                             <th>Date</th>                               <th>Time</th>                              <th>Action</th> 
                            </tr>
                          </thead>
                          <tbody>
<?php
	$i=0;
	 $BlogRow=mysqli_query($con,"select * from tbl_blog order by BlogId desc");
	while($BlogResult=mysqli_fetch_array($BlogRow))
	{	$i=$i+1;
	 ?>
                            <tr>
                              <td><?php echo $i; ?></td>
<?php
	$CatId=$BlogResult['BlogCategoryId'];
	$CatRow=mysqli_fetch_array(mysqli_query($con,"select * from tbl_blogcategory WHERE BlogCategoryId='$CatId'"));
?>
                              <td><?php echo $CatRow['BlogCategoryName']; ?></td>
                              <td><?php echo $BlogResult['BlogTitle']; ?></td>
                              <td><?php echo $BlogResult['Author']; ?></td>
                              <td><?php echo $BlogResult['Summary']; ?></td>
                              <td><img src="../AdminImage/Blog/<?php echo $BlogResult['BlogImage']; ?>" style="height:80px;width:80px;"> </td>
<td><?php echo $BlogResult['Description']; ?></td>
                              <td><?php
										if($BlogResult['Display']=='1')
											echo "True";
										else
											echo "False"; ?></td>
                              <td><?php echo $BlogResult['PostDate']; ?></td>
                              <td><?php echo $BlogResult['SEOTitle']; ?></td>
                              <td><?php echo $BlogResult['SEOKeyword']; ?></td>
                              <td><?php echo $BlogResult['SEODesc']; ?></td>
<?php /*?><?php
	$tagId=$BlogResult['BlogId'];
	$tagRow=mysqli_fetch_array(mysqli_query($con,"select tagName from tbl_blogtags WHERE blogId='$tagId'"));
?>
                             <td><?php while($tagRow) { echo $tagRow['tagName'].',	'; } ?></td><?php */?>
<td><?php echo $BlogResult['Date']; ?></td>
<td><?php echo $BlogResult['Time']; ?></td>
                              <td> <a class="btn btn-info" href="Edit_Blog.php?BlogId=<?php echo $BlogResult['BlogId']; ?>">
                <i class="glyphicon glyphicon-edit icon-white"></i>
                Edit
            </a>
            <a class="btn btn-danger" href="action.php?BlogId=<?php echo $BlogResult['BlogId']; ?>" onclick="return window.confirm('Do you want to delete this record?')">
                <i class="glyphicon glyphicon-trash icon-white"></i>
                Delete
            </a></td>
                            </tr>
<?php } ?>
                          </tbody>
                        </table>
		</div></div></div></div>
</div></div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#birthday').daterangepicker({
			singleDatePicker: true,
			calender_style: "picker_4"
		}, function(start, end, label) {
			console.log(start.toISOString(), end.toISOString(), label);
		});
	});
</script>     
<?php include('General/Footer.php'); ?>
