<?php include('General/Header.php');

if(isset($_REQUEST['PortfolioId']))
{
   $id=$_REQUEST['PortfolioId'];
   $result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_portfolio WHERE portfolioId='$id'"));
}
else{	echo "<script>window.location='Portfolio.php'</script>";	}
?>
	<script>
        function validateForm() {
            var Project = document.forms["FormPortfolio"]["Project"].value;
            var Category = document.forms["FormPortfolio"]["Category"].value;
			var imgpath=document.getElementById('mainImage');
            if (Project==0) {                alert("Please Select Project.");                return false;            }
            if (Category==0) {                alert("Please Select Category.");                return false;            }
			else if (!imgpath.value==""){
				var imgsize=imgpath.files[0].size;
				if(imgsize>2000000)	{		alert(imgpath.files[0].name+" - Size is larger than 2MB. Please Reduce it.");			return false;		}
			}
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">		<h3>Portfolio</h3>		</div>
			</div>
			<div class="clearfix"></div>

			<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Edit</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />

			<form data-parsley-validate class="form-horizontal form-label-left" action="action.php" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormPortfolio">

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Project <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="Project" id="Project" >
                            <option value="0" selected="selected" >- Select Project -</option>
<?php                           $ProjectRow=mysqli_query($con,"SELECT * FROM tbl_project order by projectId desc");
                                while($ProjectResult=mysqli_fetch_array($ProjectRow)){?>
                                    <option value="<?php echo $ProjectResult['projectId'];?>" ><?php echo $ProjectResult['projectName']; ?></option>
                                <?php } ?>
                        </select>
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="Category" id="Category" >
							<option value="0"  >- Select Category -</option>
		<?php				$MainCategoryRow=mysqli_query($con,"SELECT * FROM tbl_category order by CategoryId desc");
							while($MainCategoryResult=mysqli_fetch_array($MainCategoryRow)){?>
								<option value="<?php echo $MainCategoryResult['CategoryId'];?>" ><?php echo $MainCategoryResult['CategoryName']; ?></option>
							<?php } ?>
						</select>
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Portfolio Name<span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Portfolio" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['portfolioName'];?>">
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Portfolio Comment<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Comment" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['comment'];?>">
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Main Image </label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<font color="#FF0004">Max-Image Size: <b>2MB</b> &nbsp; (Image Dimension --- width: <b>1170px</b> , Height: <b>440px</b> )</font>
						<input type="file" name="mainImage" id="mainImage"  class="form-control col-md-7 col-xs-12" accept="image/*"  >
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Visibility <span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="checkbox" name="Display"  <?php if($result['Display'] ==1) { echo "checked"; }  ?>  class="form-control  col-md-7 col-xs-12" >
				</div></div>
				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<input type="hidden" name="hidden" value="<?php echo $result['portfolioId'];?>">
						<button type="submit" name="PortfolioUpdates" class="btn btn-success">Update</button>
				</div></div>
			</form>

          </div></div></div></div>
	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>

<?php include('General/Footer.php'); ?>