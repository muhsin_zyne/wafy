<?php include('General/Header.php'); 


define ("MAX_SIZE","1000");

function ImageUpload($image)
{
	$errors=0;
	$image_name="";
	$image1 =$_FILES[$image]["name"];
	$uploadedfile = $_FILES[$image]['tmp_name'];
	if ($image1) 
	{	
		$filename = stripslashes($_FILES[$image]['name']);
		$extension = pathinfo($filename,PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
		{			
			echo '<script>alert("Unknown Image extension");</script>';
			$errors=1;
		}
		else
		{
			$size=filesize($_FILES[$image]['tmp_name']);
			if ($size > MAX_SIZE*1024)
			{				
				echo '<script>alert("You have exceeded the size limit");</script>';
				$errors=1;
			}
			else
			{

			if($extension=="jpg" || $extension=="jpeg" )
			{		
				$uploadedfile = $_FILES[$image]['tmp_name'];
				$src = imagecreatefromjpeg($uploadedfile);	
			}
			else if($extension=="png")
			{			
				$uploadedfile = $_FILES[$image]['tmp_name'];	
				$src = imagecreatefrompng($uploadedfile);	
			}
			else
			{	
				$src = imagecreatefromgif($uploadedfile);	
			}

			list($width,$height)=getimagesize($uploadedfile);

			$mainwidth=750;	
				$mainheight=500;			/*	$newheight=($height/$width)*$newwidth;		*/		$tmp=imagecreatetruecolor($mainwidth,$mainheight);
			$displaywidth=390;	
				$displayheight=263;		/*	$newheight1=($height/$width)*$newwidth1;	*/		$tmp1=imagecreatetruecolor($displaywidth,$displayheight);


			imagecopyresampled($tmp,$src,0,0,0,0,$mainwidth,$mainheight,$width,$height);
			imagecopyresampled($tmp1,$src,0,0,0,0,$displaywidth,$displayheight,$width,$height);
				
			$ra=rand(10,10000000000);
			$rb=rand(10,10000000000);
			$r="Gallery-".$ra.$rb."_";
			$image_name=$r.$_FILES[$image]['name'];
			
			$main = "../Resource/Gallery/".$image_name;
			$display = "../Resource/Gallery/"."thump_".$image_name;
			imagejpeg($tmp,$main,100);	
				imagejpeg($tmp1,$display,100);				

			imagedestroy($src);	
				imagedestroy($tmp);	
				imagedestroy($tmp1);		
			}
	}	
	}
	return $image_name;
}







if(isset($_POST['GallerySubmit']))
{
	$Heading=trim(mysqli_real_escape_string($con,$_POST['Heading']));
	$Content=trim(mysqli_real_escape_string($con,$_POST['Content']));
	$Category=$_POST['Category'];
	$Date=$_POST['Date'];
	$image="";
	
	
	
	if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
	{
		
		$image_name1= ImageUpload('mainImage');
		if(!empty($image_name1))
			$image=$image_name1;

			if($_SESSION['DUIRoleId']==1) 
				$Approve=1;
			else
				$Approve=0;

			$result=mysqli_query($con,"INSERT INTO tbl_gallery (GalleryId,Heading,Content,CategoryId,Date,Approve,Image,EUserId,EDate)
										VALUES((SELECT IFNULL((SELECT MAX(GalleryId)+1 FROM  tbl_gallery temp),1)),'$Heading','$Content','$Category','$Date','$Approve','$image','".$_SESSION['DUIUserId']."','".date('Y-m-d H:m:i')."')");
		
				$Gallery=mysqli_fetch_array(mysqli_query($con,"select max(GalleryId) as GalleryId from tbl_gallery"));
				$GalleryId=$Gallery['GalleryId'];
		
			
				foreach($_FILES['subImages']['tmp_name'] as $key => $tmp_name )
					{
						
						$files_name = $key.$_FILES['subImages']['name'][$key];
						$files_tmp =$_FILES['subImages']['tmp_name'][$key];
						$ra=rand(10,10000000000);
						$rb=rand(10,10000000000);
						$r="Gallery_Sub_".$ra.$rb."_";
						$images_name=$r.$files_name;
						move_uploaded_file($files_tmp,"../Resource/Gallery/".$images_name);
						mysqli_query($con,"INSERT INTO tbl_gallary_images(id,GalleryId,Image)values((SELECT IFNULL((SELECT MAX(id)+1 FROM  tbl_gallary_images temp),1)),'$GalleryId','$images_name')");
					}

			if($result)
			{
				echo '<script>alert(" Successfully Inserted");</script>';		
			}
			else	
			{			
				echo '<script>alert("Data Not Inserted");</script>';			
			}
			echo "<script>window.location='Gallery.php'</script>";
		}
	else
	{
		echo '<script>alert(" Please Select Image");</script>';	
	}
	

/*if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))	
{
define ("MAX_SIZE","2000");		$errors=0;
	$image =$_FILES["mainImage"]["name"];
	$uploadedfile = $_FILES['mainImage']['tmp_name'];

	if ($image) 
	{	$filename = stripslashes($_FILES['mainImage']['name']);
		$extension = pathinfo($filename,PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
		{	
			echo ' Unknown Image extension ';
			$errors=1;	
		}

		else
		{	$size=filesize($_FILES['mainImage']['tmp_name']);

			if ($size > MAX_SIZE*1024)
			{	
				echo "You have exceeded the size limit";	
				$errors=1;	
			}
			if($extension=="jpg" || $extension=="jpeg" )
			{	
				$uploadedfile = $_FILES['mainImage']['tmp_name'];		
				$src = imagecreatefromjpeg($uploadedfile);	
			}
			else if($extension=="png")
			{	
				$uploadedfile = $_FILES['mainImage']['tmp_name'];		
				$src = imagecreatefrompng($uploadedfile);	
			}
			else
			{			
				$src = imagecreatefromgif($uploadedfile);	
			}

			list($width,$height)=getimagesize($uploadedfile);
			$mainwidth=640;			$mainheight=480;		
		 $tmp=imagecreatetruecolor($mainwidth,$mainheight);
			$displaywidth=337;		$displayheight=253;		
		 $tmp1=imagecreatetruecolor($displaywidth,$displayheight);
			$projectwidth=555;		$projectheight=416;	
		 $tmp2=imagecreatetruecolor($projectwidth,$projectheight);
			imagecopyresampled($tmp,$src,0,0,0,0,$mainwidth,$mainheight,$width,$height);
			imagecopyresampled($tmp1,$src,0,0,0,0,$displaywidth,$displayheight,$width,$height);
			imagecopyresampled($tmp2,$src,0,0,0,0,$projectwidth,$projectheight,$width,$height);
			$main = "../AdminImage/Gallery/main/". $_FILES['mainImage']['name'];
			$display = "../AdminImage/Gallery/display/". $_FILES['mainImage']['name'];
			$project = "../AdminImage/Gallery/category/". $_FILES['mainImage']['name'];
			imagejpeg($tmp,$main,100);	
		 	imagejpeg($tmp1,$display,100);	
		 	imagejpeg($tmp2,$project,100);
			imagedestroy($src);		
		 imagedestroy($tmp);	
		 imagedestroy($tmp1);	
		 imagedestroy($tmp2);
		}
	}
}*/
	
}
?>
	<script>
        function validateForm() {
            
            var Category = document.forms["FormGallery"]["Category"].value;
			var imgpath=document.getElementById('mainImage');
			
				var subimages = document.getElementById('subImages');
			
			
			
            if (Category==0) 
			{
				alert("Please Select Category.");     
				return false;       
			}
			else if (imgpath.value=="")
			{
				alert("Please Select Image.");     
				return false;    
			}
			else if (!imgpath.value=="")
			{
				var imgsize=imgpath.files[0].size;
				if(imgsize>1000000)	
				{		
					alert(imgpath.files[0].name+" - Size is larger than 1MB. Please Reduce it.");	
					return false;	
				}
			}
			else
			{
				var isok=true;
				for (var i = 0; i < subimages.files.length; i++)
				{
				 var imageSize = subimages.files[i].size;
				 if (imageSize > 1000000) 
				 {
						alert(subimages.files[i].name+" - Size is larger than 1MB. Please Reduce it.");	
						isok= false;
						 break;
				 	}

			 	}
				if(!isok)
					return flase;
				
			}
			
			
   
    
			
			
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Gallery</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Add New</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormGallery">

				

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gallery Category <span class="required">*</span>	</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="Category" id="Category" >
                            <option value="0" selected="selected" >- Select Category -</option>
                                <?php 
                                $GalleryCategoryRow=mysqli_query($con,"SELECT * FROM tbl_gallery_category order by CategoryId desc");
                                while($GalleryCategoryResult=mysqli_fetch_array($GalleryCategoryRow)){?>
                                    <option value="<?php echo $GalleryCategoryResult['CategoryId'];?>" ><?php echo $GalleryCategoryResult['Category']; ?></option>
                                <?php } ?>
                        </select>
				</div></div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gallery Heading<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="Heading" required class="form-control col-md-7 col-xs-12">
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gallery Content<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea name="Content" required class="form-control col-md-7 col-xs-12" rows="5"></textarea>
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Date<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="date" name="Date" required class="form-control col-md-7 col-xs-12" value="<?php echo date('Y-m-d'); ?>">
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>750px</b> , Height: <b>500px</b> )</font>
						<input type="file" name="mainImage" id="mainImage" class="form-control col-md-7 col-xs-12" accept="image/*" required >
				</div></div>
              
              <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Sub Images </label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>750px</b> , Height: <b>500px</b> )</font>
						<input type="file" name="subImages[]" id="subImages" multiple class="form-control col-md-7 col-xs-12" accept="image/*"  >
				</div></div>
              
            
				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" name="GallerySubmit" class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>

 		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Gallery List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                         <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>
                             <th>Status</th>
                              <th>Heading</th>
                              <th>Date</th>
                              <th>Category</th>
                              <th>Image</th>
                              <th>Content</th>
                             <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
				
  if($_SESSION['DUIRoleId']==1)
  { 
			$GalleryRow=mysqli_query($con,"select * from tbl_gallery order by GalleryId desc");
  }
  else
  {
	  $GalleryRow=mysqli_query($con,"select * from tbl_gallery where EUserId='".$_SESSION['DUIUserId']."' order by GalleryId desc");

  }
	while($GalleryResult=mysqli_fetch_array($GalleryRow))
	{
		$i=$i+1;
	 ?>
                            <tr>
                              <td><?php echo $i; ?></td>
                               <td><?php if($GalleryResult['Approve']==0) echo "Not Approved"; else echo "Approved"; ?></td>
                              <td><?php echo $GalleryResult['Heading']; ?></td>
                              <td><?php echo date('d M Y',strtotime($GalleryResult['Date'])); ?></td>
                              
                               <td><?php  
		$Category=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_gallery_category where CategoryId ='".$GalleryResult['CategoryId']."'"));
		echo $Category[0]; ?></td>
                              
                                <td> <img src="<?php echo "../Resource/Gallery/".$GalleryResult['Image'];?>" width="80" height="80"></td>
                             <td><?php echo substr($GalleryResult['Content'],0,200); ?></td>
                             
                             <td> 
                             <form method="post" enctype="multipart/form-data" action="Edit_Gallery.php"  style="float: left"> 
                             <input type="hidden" name="GalleryIdUpdate" value="<?php echo $GalleryResult['GalleryId']; ?>">
                             	<input type="submit" class="btn btn-info" value="Edit" >
							</form>
        					
         					
          					<form method="post" enctype="multipart/form-data" action="action.php" onSubmit="return confirm('Are you sure want to delete this record ?');"  style="float: left">
                            	<input type="hidden" name="GalleryIdDelete" value="<?php echo $GalleryResult['GalleryId']; ?>"> 
                             	<input type="submit" class="btn btn-danger" value="Delete">
                            	 
							</form>
							
							
							<?php 
		
		if($_SESSION['DUIRoleId']==1)
  { 
					if($GalleryResult['Approve']==0)
						$Approve="Approve";
	  				else
							$Approve="Un Approve";
								 ?>
						<form method="post" enctype="multipart/form-data" action="action.php" onSubmit="return confirm('Are you sure want to perform this action ?');"  style="float: left">
                            	<input type="hidden" name="GalleryIdApprove" value="<?php echo $GalleryResult['GalleryId']; ?>"> 
                             	<input type="submit" class="btn btn-warning" value="<?php echo $Approve;?>">
							</form>
							<?php } ?>
            </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>

          </div></div></div></div>
	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
