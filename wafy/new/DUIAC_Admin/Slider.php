<?php include('General/Header.php'); 
if($_SESSION['DUIRoleId']!=1) { 
	echo "<script>window.location='Home.php'</script>";
}

$result=mysqli_fetch_array(mysqli_query($con,"select * from tbl_slider where id=1"));

function ImageUpload($image)
{
	$errors=0;
	$image_name="";
	$image1 =$_FILES[$image]["name"];
	$uploadedfile = $_FILES[$image]['tmp_name'];
	if ($image1) 
	{	
		$filename = stripslashes($_FILES[$image]['name']);
		$extension = pathinfo($filename,PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
		{			
			echo '<script>alert("Unknown Image extension");</script>';
			$errors=1;
		}
		else
		{
			$size=filesize($_FILES[$image]['tmp_name']);
			if ($size > MAX_SIZE*1024)
			{				
				echo '<script>alert("You have exceeded the size limit");</script>';
				$errors=1;
			}
			else
			{

			if($extension=="jpg" || $extension=="jpeg" )
			{		
				$uploadedfile = $_FILES[$image]['tmp_name'];
				$src = imagecreatefromjpeg($uploadedfile);	
			}
			else if($extension=="png")
			{			
				$uploadedfile = $_FILES[$image]['tmp_name'];	
				$src = imagecreatefrompng($uploadedfile);	
			}
			else
			{	
				$src = imagecreatefromgif($uploadedfile);	
			}

			list($width,$height)=getimagesize($uploadedfile);

			$mainwidth=1920;	
				$mainheight=1280;			/*	$newheight=($height/$width)*$newwidth;		*/		$tmp=imagecreatetruecolor($mainwidth,$mainheight);
			$displaywidth=90;	
				$displayheight=50;		/*	$newheight1=($height/$width)*$newwidth1;	*/		$tmp1=imagecreatetruecolor($displaywidth,$displayheight);


			imagecopyresampled($tmp,$src,0,0,0,0,$mainwidth,$mainheight,$width,$height);
			imagecopyresampled($tmp1,$src,0,0,0,0,$displaywidth,$displayheight,$width,$height);
				
			$ra=rand(10,10000000000);
			$rb=rand(10,10000000000);
			$r="Wafy-".$ra.$rb."_";
			$image_name=$r.$_FILES[$image]['name'];
			
			$main = "../Resource/Slider/".$image_name;
			$display = "../Resource/Slider/"."thump_".$image_name;
			imagejpeg($tmp,$main,100);	
				imagejpeg($tmp1,$display,100);				

			imagedestroy($src);	
				imagedestroy($tmp);	
				imagedestroy($tmp1);		
			}
	}	
	}
	return $image_name;
}

if(isset($_POST['UserSubmit']))
{
	$S1Content1=trim(mysqli_real_escape_string($con,$_POST['S1Content1']));
	$S1Content2=trim(mysqli_real_escape_string($con,$_POST['S1Content2']));
	$S1Content3=trim(mysqli_real_escape_string($con,$_POST['S1Content3']));
	$S2Content1=trim(mysqli_real_escape_string($con,$_POST['S2Content1']));
	$S2Content2=trim(mysqli_real_escape_string($con,$_POST['S2Content2']));
	$S2Content3=trim(mysqli_real_escape_string($con,$_POST['S2Content3']));
	$S3Content1=trim(mysqli_real_escape_string($con,$_POST['S3Content1']));
	$S3Content2=trim(mysqli_real_escape_string($con,$_POST['S3Content2']));
	$S3Content3=trim(mysqli_real_escape_string($con,$_POST['S3Content3']));

	
		$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select s1image,s2image,s3image from tbl_slider WHERE id=1"));
	
	$S1image=$ImageQuery['s1image'];
	$S2image=$ImageQuery['s2image'];
	$S3image=$ImageQuery['s3image'];
	
	define ("MAX_SIZE","1000");
	
	if(isset($_FILES['S1image']['name']) && !empty($_FILES['S1image']['name']))
	{
		if(!empty($S1image))
		{
			unlink("../Resource/Slider/".$S1image);
			unlink("../Resource/Slider/"."thump_".$S1image);
		}
		
		$image_name1= ImageUpload('S1image');
		
		if(!empty($image_name1))
			$S1image=$image_name1;
	}
	if(isset($_FILES['S2image']['name']) && !empty($_FILES['S2image']['name']))
	{
		if(!empty($S2image))
		{
			unlink("../Resource/Slider/".$S2image);
			unlink("../Resource/Slider/"."thump_".$S2image);
		}
		$image_name2= ImageUpload('S2image');
		if(!empty($image_name2))
			$S2image=$image_name2;
	}

	if(isset($_FILES['S3image']['name']) && !empty($_FILES['S3image']['name']))
	{
		if(!empty($S3image))
		{
			unlink("../Resource/Slider/".$S3image);
			unlink("../Resource/Slider/"."thump_".$S3image);
		}
		$image_name3= ImageUpload('S3image');
		if(!empty($image_name3))
			$S3image=$image_name3;
		
	}

			$result=mysqli_query($con,"update tbl_slider set s1content1='$S1Content1',s1content2='$S1Content2',s1content3='$S1Content3',s1image='$S1image',s2content1='$S2Content1',s2content2='$S2Content2',s2content3='$S2Content3',s2image='$S2image',s3content1='$S3Content1',s3content2='$S3Content2',s3content3='$S3Content3',s3image='$S3image' where id=1");
		
		if($result)
		{		
			echo '<script>alert(" Successfully Updated");</script>';
		}
		else
		{		
			echo '<script>alert("Data Not Updated");</script>';		
		}
		echo "<script>window.location='Slider.php'</script>";
	
}
?>
 
<script type="text/javascript">
    function checkname()
    {
	   var name=document.getElementById( "UserName" ).value.trim();
	   if(name)
	   {
	        $.ajax({
			   type: 'post',
			   url: 'CheckExistance.php',
			   data: {
			   user_name:name
			   },
			   success: function (response) {
			   $( '#name_status' ).html(response);
  		          if(response=="OK")	
                  {
                     return true;	
                  }
                  else
                  {
                     return false;	
                  }
                }
		      });
	   }
	   else
	   {
		   $( '#name_status' ).html('');
	   }
	}
</script>
<script>
	

	
/*	var MobilePhoto=document.getElementById('MobilePhoto');
	var flag = true;
	var Photo;
	var ImageName;
	for (var i=0; i<MobilePhoto.files.length; i++) 
	{
		Photo=MobilePhoto.files[i].name;
		var blnValidImage = false;
		var ImagevalidFileExtensions = [".jpg", ".jpeg", ".gif", ".png"];  
		for (var j = 0; j < ImagevalidFileExtensions.length; j++)
		{
			var sCurExtension = ImagevalidFileExtensions[j];
			if (Photo.substr(Photo.length - sCurExtension.length, sCurExtension.length).toLowerCase()== sCurExtension.toLowerCase()) 
			{
				blnValidImage = true;
				break;
			}
		}
		if (!blnValidImage) 
		{
			flag=false;
			ImageName=Photo;
			break;
		}
	} 
	if (!flag) 
	{
		swal("", ImageName+" file extension not valid.");
		return '0';
	}
	else 
	{
		return '1';
	}
	
	*/
	function validateForm() 
	{
		var S1Content1 = document.forms["FormUser"]["S1Content1"].value;
		var S1Content2 = document.forms["FormUser"]["S1Content2"].value;
		var S1Content3 = document.forms["FormUser"]["S1Content3"].value;
		var S2Content1 = document.forms["FormUser"]["S2Content1"].value;
		var S2Content2 = document.forms["FormUser"]["S2Content2"].value;
		var S2Content3 = document.forms["FormUser"]["S2Content3"].value;
		var S3Content1 = document.forms["FormUser"]["S3Content1"].value;
		var S3Content2 = document.forms["FormUser"]["S3Content2"].value;
		var S3Content3 = document.forms["FormUser"]["S3Content3"].value;
		var S1image=document.getElementById('S1image');
		var S2image=document.getElementById('S2image');
		var S3image=document.getElementById('S3image');
		
		if (!S1Content1 || !S1Content2 ||!S1Content3 ||!S2Content1 ||!S2Content2 ||!S2Content3 ||!S3Content1 ||!S3Content2 ||!S3Content3 ) {
			alert("Plese Fill All Fields");
			return false;
		}
		else if (!S1image.value=="")
		{
			var imgsize=S1image.files[0].size;
			if(imgsize>1000000)	
			{		
				alert(S1image.files[0].name+" - Size is larger than 1MB. Please Reduce it.");	
				return false;	
			}
		}
		else if (!S2image.value=="")
		{
			var imgsize=S2image.files[0].size;
			if(imgsize>1000000)	
			{		
				alert(S2image.files[0].name+" - Size is larger than 1MB. Please Reduce it.");	
				return false;	
			}
		}
		else if (!S3image.value=="")
		{
			var imgsize=S3image.files[0].size;
			if(imgsize>1000000)	
			{		
				alert(S3image.files[0].name+" - Size is larger than 1MB. Please Reduce it.");	
				return false;	
			}
		}
		
	}
</script>
       
<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Home Page - Slider </h3>
            </div>
          </div>
          <div class="clearfix"></div>
          
           
           
           <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edit </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                 
                  <br />
                  <form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data"
                   onsubmit="return validateForm()" name="FormUser">
                   
                   <h2>Slider 1 </h2>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 1  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="S1Content1" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['s1content1']; ?>">
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 2  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="S1Content2" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['s1content2']; ?>">
                      </div>
                    </div>

                   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 3 <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea name="S1Content3" required class="form-control col-md-7 col-xs-12"><?php echo $result['s1content3']; ?> </textarea>
                        
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span>			</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                     <font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>1920px</b> , Height: <b>1280px</b> )</font>
                          <input type="file" id="S1image" name="S1image"  class="form-control col-md-7 col-xs-12" accept="image/*">
                          <br />
                          <a href="<?php echo "../Resource/Slider/".$result['s1image'];?>" download>  <img src="<?php echo "../Resource/Slider/".$result['s1image'];?>" width="200">
						  </a>
                    </div>
                    </div>
                    
                    
                    
                    
                    
                    
                    
                                       <h2>Slider 2 </h2>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 1  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="S2Content1" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['s2content1']; ?>">
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 2  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="S2Content2" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['s2content2']; ?>">
                      </div>
                    </div>

                   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 3 <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea name="S2Content3" required class="form-control col-md-7 col-xs-12"><?php echo $result['s2content3']; ?> </textarea>
                        
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span>			</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                      <font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>1920px</b> , Height: <b>1280px</b> )</font>
                          <input type="file" id="S2image" name="S2image"  class="form-control col-md-7 col-xs-12" accept="image/*">
                          <br />
                          <a href="<?php echo "../Resource/Slider/".$result['s2image'];?>" download>  <img src="<?php echo "../Resource/Slider/".$result['s2image'];?>" width="200">
						  </a>
                    </div>
                    </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                                       <h2>Slider 3 </h2>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 1  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="S3Content1" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['s3content1']; ?>">
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 2  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="S3Content2" required class="form-control col-md-7 col-xs-12" autocomplete="off" value="<?php echo $result['s3content2']; ?>">
                      </div>
                    </div>

                   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 3 <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea name="S3Content3" required class="form-control col-md-7 col-xs-12"><?php echo $result['s3content3']; ?> </textarea>
                        
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span>			</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                      <font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>1920px</b> , Height: <b>1280px</b> )</font>
                          <input type="file" id="S3image" name="S3image"  class="form-control col-md-7 col-xs-12" accept="image/*">
                          <br />
                          <a href="<?php echo "../Resource/Slider/".$result['s3image'];?>" download>  <img src="<?php echo "../Resource/Slider/".$result['s3image'];?>" width="200">
						  </a>
                    </div>
                    </div>
                    
                    
                    
                    
                    
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                     
                        <button type="submit" name="UserSubmit" class="btn btn-success">Update</button>
                    </div></div>
                  </form>
          </div></div></div></div>
          
          
          
          
 


</div>
</div>
          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>

<?php include('General/Footer.php'); ?>












<?php /*






<?php include('General/Header.php'); 
if($_SESSION['DUIRoleId']!=1) { 
	echo "<script>window.location='Home.php'</script>";
}

$result=mysqli_fetch_array(mysqli_query($con,"select * from tbl_slider where id=1"));

function ImageUpload($image)
{
	$errors=0;
	$image_name="";
	$image1 =$_FILES[$image]["name"];
	$uploadedfile = $_FILES[$image]['tmp_name'];
	if ($image1) 
	{	
		$filename = stripslashes($_FILES[$image]['name']);
		$extension = pathinfo($filename,PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
		{			
			echo '<script>alert("Unknown Image extension");</script>';
			$errors=1;
		}
		else
		{
			$size=filesize($_FILES[$image]['tmp_name']);
			if ($size > MAX_SIZE*1024)
			{				
				echo '<script>alert("You have exceeded the size limit");</script>';
				$errors=1;
			}
			else
			{

			if($extension=="jpg" || $extension=="jpeg" )
			{		
				$uploadedfile = $_FILES[$image]['tmp_name'];
				$src = imagecreatefromjpeg($uploadedfile);	
			}
			else if($extension=="png")
			{			
				$uploadedfile = $_FILES[$image]['tmp_name'];	
				$src = imagecreatefrompng($uploadedfile);	
			}
			else
			{	
				$src = imagecreatefromgif($uploadedfile);	
			}

			list($width,$height)=getimagesize($uploadedfile);

			$mainwidth=1920;	
				$mainheight=1280;			/*	$newheight=($height/$width)*$newwidth;			$tmp=imagecreatetruecolor($mainwidth,$mainheight);
			$displaywidth=90;	
				$displayheight=50;		/*	$newheight1=($height/$width)*$newwidth1;		$tmp1=imagecreatetruecolor($displaywidth,$displayheight);


			imagecopyresampled($tmp,$src,0,0,0,0,$mainwidth,$mainheight,$width,$height);
			imagecopyresampled($tmp1,$src,0,0,0,0,$displaywidth,$displayheight,$width,$height);
				
			$ra=rand(10,10000000000);
			$rb=rand(10,10000000000);
			$r="Wafy-".$ra.$rb."_";
			$image_name=$r.$_FILES[$image]['name'];
			
			$main = "../Resource/Slider/".$image_name;
			$display = "../Resource/Slider/"."thump_".$image_name;
			imagejpeg($tmp,$main,100);	
				imagejpeg($tmp1,$display,100);				

			imagedestroy($src);	
				imagedestroy($tmp);	
				imagedestroy($tmp1);		
			}
	}	
	}
	return $image_name;
}

if(isset($_POST['UserSubmit']))
{
	$S1Content1=trim(mysqli_real_escape_string($con,$_POST['S1Content1']));
	$S1Content2=trim(mysqli_real_escape_string($con,$_POST['S1Content2']));
	$S1Content3=trim(mysqli_real_escape_string($con,$_POST['S1Content3']));
	$S2Content1=trim(mysqli_real_escape_string($con,$_POST['S2Content1']));
	$S2Content2=trim(mysqli_real_escape_string($con,$_POST['S2Content2']));
	$S2Content3=trim(mysqli_real_escape_string($con,$_POST['S2Content3']));
	$S3Content1=trim(mysqli_real_escape_string($con,$_POST['S3Content1']));
	$S3Content2=trim(mysqli_real_escape_string($con,$_POST['S3Content2']));
	$S3Content3=trim(mysqli_real_escape_string($con,$_POST['S3Content3']));

	
		$ImageQuery=mysqli_fetch_array(mysqli_query($con,"select s1image,s2image,s3image from tbl_slider WHERE id=1"));
	
	$S1image=$ImageQuery['s1image'];
	$S2image=$ImageQuery['s2image'];
	$S3image=$ImageQuery['s3image'];
	
	define ("MAX_SIZE","1000");
	
	if(isset($_FILES['S1image']['name']) && !empty($_FILES['S1image']['name']))
	{
		if(!empty($S1image))
		{
			unlink("../Resource/Slider/".$S1image);
			unlink("../Resource/Slider/"."thump_".$S1image);
		}
		
		$image_name1= ImageUpload('S1image');
		
		if(!empty($image_name1))
			$S1image=$image_name1;
	}
	if(isset($_FILES['S2image']['name']) && !empty($_FILES['S2image']['name']))
	{
		if(!empty($S2image))
		{
			unlink("../Resource/Slider/".$S2image);
			unlink("../Resource/Slider/"."thump_".$S2image);
		}
		$image_name2= ImageUpload('S2image');
		if(!empty($image_name2))
			$S2image=$image_name2;
	}

	if(isset($_FILES['S3image']['name']) && !empty($_FILES['S3image']['name']))
	{
		if(!empty($S3image))
		{
			unlink("../Resource/Slider/".$S3image);
			unlink("../Resource/Slider/"."thump_".$S3image);
		}
		$image_name3= ImageUpload('S3image');
		if(!empty($image_name3))
			$S3image=$image_name3;
		
	}

			$result=mysqli_query($con,"update tbl_slider set s1content1='$S1Content1',s1content2='$S1Content2',s1content3='$S1Content3',s1image='$S1image',s2content1='$S2Content1',s2content2='$S2Content2',s2content3='$S2Content3',s2image='$S2image',s3content1='$S3Content1',s3content2='$S3Content2',s3content3='$S3Content3',s3image='$S3image' where id=1");
		
		if($result)
		{		
			echo '<script>alert(" Successfully Updated");</script>';
		}
		else
		{		
			echo '<script>alert("Data Not Updated");</script>';		
		}
		echo "<script>window.location='Slider.php'</script>";
	
}
?>
 
<script type="text/javascript">
    function checkname()
    {
	   var name=document.getElementById( "UserName" ).value.trim();
	   if(name)
	   {
	        $.ajax({
			   type: 'post',
			   url: 'CheckExistance.php',
			   data: {
			   user_name:name
			   },
			   success: function (response) {
			   $( '#name_status' ).html(response);
  		          if(response=="OK")	
                  {
                     return true;	
                  }
                  else
                  {
                     return false;	
                  }
                }
		      });
	   }
	   else
	   {
		   $( '#name_status' ).html('');
	   }
	}
</script>
<script>
	

	
/*	var MobilePhoto=document.getElementById('MobilePhoto');
	var flag = true;
	var Photo;
	var ImageName;
	for (var i=0; i<MobilePhoto.files.length; i++) 
	{
		Photo=MobilePhoto.files[i].name;
		var blnValidImage = false;
		var ImagevalidFileExtensions = [".jpg", ".jpeg", ".gif", ".png"];  
		for (var j = 0; j < ImagevalidFileExtensions.length; j++)
		{
			var sCurExtension = ImagevalidFileExtensions[j];
			if (Photo.substr(Photo.length - sCurExtension.length, sCurExtension.length).toLowerCase()== sCurExtension.toLowerCase()) 
			{
				blnValidImage = true;
				break;
			}
		}
		if (!blnValidImage) 
		{
			flag=false;
			ImageName=Photo;
			break;
		}
	} 
	if (!flag) 
	{
		swal("", ImageName+" file extension not valid.");
		return '0';
	}
	else 
	{
		return '1';
	}
	
	
	function validateForm() 
	{
		var S1Content1 = document.forms["FormUser"]["S1Content1"].value;
		var S1Content2 = document.forms["FormUser"]["S1Content2"].value;
		var S1Content3 = document.forms["FormUser"]["S1Content3"].value;
		var S2Content1 = document.forms["FormUser"]["S2Content1"].value;
		var S2Content2 = document.forms["FormUser"]["S2Content2"].value;
		var S2Content3 = document.forms["FormUser"]["S2Content3"].value;
		var S3Content1 = document.forms["FormUser"]["S3Content1"].value;
		var S3Content2 = document.forms["FormUser"]["S3Content2"].value;
		var S3Content3 = document.forms["FormUser"]["S3Content3"].value;
		var S1image=document.getElementById('S1image');
		var S2image=document.getElementById('S2image');
		var S3image=document.getElementById('S3image');
		
		if (!S1Content1 || !S1Content2 ||!S1Content3 ||!S2Content1 ||!S2Content2 ||!S2Content3 ||!S3Content1 ||!S3Content2 ||!S3Content3 ) {
			alert("Plese Fill All Fields");
			return false;
		}
		else if (!S1image.value=="")
		{
			var imgsize=S1image.files[0].size;
			if(imgsize>1000000)	
			{		
				alert(S1image.files[0].name+" - Size is larger than 1MB. Please Reduce it.");	
				return false;	
			}
		}
		else if (!S2image.value=="")
		{
			var imgsize=S2image.files[0].size;
			if(imgsize>1000000)	
			{		
				alert(S2image.files[0].name+" - Size is larger than 1MB. Please Reduce it.");	
				return false;	
			}
		}
		else if (!S3image.value=="")
		{
			var imgsize=S3image.files[0].size;
			if(imgsize>1000000)	
			{		
				alert(S3image.files[0].name+" - Size is larger than 1MB. Please Reduce it.");	
				return false;	
			}
		}
		
	}
</script>
       
<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Home Page - Slider </h3>
            </div>
          </div>
          <div class="clearfix"></div>
          
           
           
           <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edit </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                 
                  <br />
                  <form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data"
                   onsubmit="return validateForm()" name="FormUser">
                   
                   <h2>Slider 1 </h2>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 1  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea name="S1Content1" required class="form-control col-md-7 col-xs-12" autocomplete="off" ><?php echo $result['s1content1']; ?> </textarea>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 2  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea  name="S1Content2" required class="form-control col-md-7 col-xs-12" autocomplete="off" ><?php echo $result['s1content2']; ?></textarea>
                      </div>
                    </div>

                   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 3 <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea name="S1Content3" required class="form-control col-md-7 col-xs-12"><?php echo $result['s1content3']; ?> </textarea>
                        
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span>			</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                     <font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>1920px</b> , Height: <b>1280px</b> )</font>
                          <input type="file" id="S1image" name="S1image"  class="form-control col-md-7 col-xs-12" accept="image/*">
                          <br />
                          <a href="<?php echo "../Resource/Slider/".$result['s1image'];?>" download>  <img src="<?php echo "../Resource/Slider/".$result['s1image'];?>" width="200">
						  </a>
                    </div>
                    </div>
                    
                    
                    
                    
                    
                    
                    
                                       <h2>Slider 2 </h2>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 1  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea name="S2Content1" required class="form-control col-md-7 col-xs-12" autocomplete="off" ><?php echo $result['s2content1']; ?></textarea>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 2  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea name="S2Content2" required class="form-control col-md-7 col-xs-12" autocomplete="off" ><?php echo $result['s2content2']; ?></textarea>
                      </div>
                    </div>

                   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 3 <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea name="S2Content3" required class="form-control col-md-7 col-xs-12"><?php echo $result['s2content3']; ?> </textarea>
                        
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span>			</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                      <font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>1920px</b> , Height: <b>1280px</b> )</font>
                          <input type="file" id="S2image" name="S2image"  class="form-control col-md-7 col-xs-12" accept="image/*">
                          <br />
                          <a href="<?php echo "../Resource/Slider/".$result['s2image'];?>" download>  <img src="<?php echo "../Resource/Slider/".$result['s2image'];?>" width="200">
						  </a>
                    </div>
                    </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                                       <h2>Slider 3 </h2>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 1  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea name="S3Content1" required class="form-control col-md-7 col-xs-12" autocomplete="off" ><?php echo $result['s3content1']; ?></textarea>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 2  <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea name="S3Content2" required class="form-control col-md-7 col-xs-12" autocomplete="off" ><?php echo $result['s3content2']; ?></textarea>
                      </div>
                    </div>

                   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content 3 <span class="required">*</span></label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
						  <textarea name="S3Content3" required class="form-control col-md-7 col-xs-12"><?php echo $result['s3content3']; ?> </textarea>
                        
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span>			</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                      <font color="#FF0004">Max-Image Size: <b>1MB</b> &nbsp; (Image Dimension --- width: <b>1920px</b> , Height: <b>1280px</b> )</font>
                          <input type="file" id="S3image" name="S3image"  class="form-control col-md-7 col-xs-12" accept="image/*">
                          <br />
                          <a href="<?php echo "../Resource/Slider/".$result['s3image'];?>" download>  <img src="<?php echo "../Resource/Slider/".$result['s3image'];?>" width="200">
						  </a>
                    </div>
                    </div>
                    
                    
                    
                    
                    
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                     
                        <button type="submit" name="UserSubmit" class="btn btn-success">Update</button>
                    </div></div>
                  </form>
          </div></div></div></div>
          
          
          
          
 


</div>
</div>
         <script src="ckeditor/ckeditor.js" type="text/javascript"></script> 
            <script>
                CKEDITOR.replace( 'S1Content1',  {
		
			height: 50
		});
				           CKEDITOR.replace( 'S1Content2',  {
		
			height: 50
		});
				
				           CKEDITOR.replace( 'S1Content3',  {
		
			height: 80
		});
				
				
				 CKEDITOR.replace( 'S2Content1',  {
		
			height: 50
		});
				           CKEDITOR.replace( 'S2Content2',  {
		
			height: 50
		});
				
				           CKEDITOR.replace( 'S2Content3',  {
		
			height: 80
		});
				
				
				 CKEDITOR.replace( 'S3Content1',  {
		
			height: 50
		});
				           CKEDITOR.replace( 'S3Content2',  {
		
			height: 50
		});
				
				           CKEDITOR.replace( 'S3Content3',  {
		
			height: 80
		});
            </script>
            
            
          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>

<?php include('General/Footer.php'); ?>


*/ ?>

