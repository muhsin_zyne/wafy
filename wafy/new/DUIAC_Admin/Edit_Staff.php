<?php include('General/Header.php'); 


if(isset($_REQUEST['StaffIdUpdate']))
{
   $id=$_REQUEST['StaffIdUpdate'];
   //echo $id;exit;
   $result =  mysqli_fetch_array(mysqli_query($con,"SELECT * FROM tbl_staff WHERE StaffId='$id'"));
}
else
{
	echo "<script>window.location='Staff.php'</script>";
}
	

?>
	<script>
        function validateForm() {
            var name = document.forms["FormStaff"]["name"].value;
			var designation = document.forms["FormStaff"]["designation"].value;
			
			
            if (!name||!designation) 
			{
				alert("Please Fill All Fields.");     
				return false;       
			}
		
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Staff</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Update</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="action.php" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormStaff">

				

			

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="name" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Name'];?>">
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Designation<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="designation" required class="form-control col-md-7 col-xs-12" value="<?php echo $result['Designation'];?>">
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Facebook Link<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" name="FbLink" class="form-control col-md-7 col-xs-12" rows="8" value="<?php echo $result['FbLink'];?>">
				</div></div>
				
            
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Display Order<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number"  name="Order" required class="form-control col-md-7 col-xs-12" min="1" value="<?php echo $result['DisplayOrder'];?>"  >
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							
						<input type="file" name="mainImage" id="mainImage"  class="form-control col-md-7 col-xs-12" accept="image/*"  >
						<br />
						<a href="<?php echo "../Resource/Staff/".$result['Image'];?>" download> <img src="<?php echo "../Resource/Staff/".$result['Image'];?>" width="100" > </a>
				
				</div></div>
               <input type="hidden" name="StaffUpdate" value="<?php echo $result['StaffId'];?>">

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" name="StaffSubmit" class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>

	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
