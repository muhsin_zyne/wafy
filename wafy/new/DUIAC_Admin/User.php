<?php include('General/Header.php'); 
if($_SESSION['DUIRoleId']!=1) { 
	echo "<script>window.location='Home.php'</script>";
}
	
if(isset($_POST['UserSubmit']))
{
	$Name=trim(mysqli_real_escape_string($con,$_POST['Name']));
	$UserName=trim(mysqli_real_escape_string($con,$_POST['UserName']));
	$Password=trim(mysqli_real_escape_string($con,$_POST['Password']));
	$image="";
	$UserCount=mysqli_fetch_array(mysqli_query($con,"select count(UserId) from  tbl_login where UserName='".$UserName."' and sts=0"));
	if($UserCount[0]>0)
	{
		echo '<script>alert("Username already exists.");</script>';
	}
	else
	{
		if(isset($_FILES['MainImage']['name']) && !empty($_FILES['MainImage']['name']))
		{
			$filename=$_FILES['MainImage']['name'];
			$temp=$_FILES['MainImage']['tmp_name'];
			$ra=rand(10,10000000000);
			$rb=rand(10,10000000000);
			$r="User_".$ra.$rb."_";
			$image_name=$r.$filename;
			move_uploaded_file($temp,"../Resource/User/".$image_name);
			$image=$image_name;
		}
		$result=mysqli_query($con,"INSERT INTO  tbl_login(UserId,Name,UserName,Password,RoleId,Image)VALUES((SELECT IFNULL((SELECT MAX(UserId)+1 FROM  tbl_login temp),1)),'$Name','$UserName','$Password',2,'$image')");
		if($result)
		{		
			echo '<script>alert(" Successfully Saved");</script>';
		}
		else
		{		
			echo '<script>alert("Data Not Saved");</script>';		
		}
		/*echo "<script>window.location='User.php'</script>";*/
	}
}
?>
 
<script type="text/javascript">
    function checkname()
    {
	   var name=document.getElementById( "UserName" ).value.trim();
	   if(name)
	   {
	        $.ajax({
			   type: 'post',
			   url: 'CheckExistance.php',
			   data: {
			   user_name:name
			   },
			   success: function (response) {
			   $( '#name_status' ).html(response);
  		          if(response=="OK")	
                  {
                     return true;	
                  }
                  else
                  {
                     return false;	
                  }
                }
		      });
	   }
	   else
	   {
		   $( '#name_status' ).html('');
	   }
	}
</script>
<script>
	

	
/*	var MobilePhoto=document.getElementById('MobilePhoto');
	var flag = true;
	var Photo;
	var ImageName;
	for (var i=0; i<MobilePhoto.files.length; i++) 
	{
		Photo=MobilePhoto.files[i].name;
		var blnValidImage = false;
		var ImagevalidFileExtensions = [".jpg", ".jpeg", ".gif", ".png"];  
		for (var j = 0; j < ImagevalidFileExtensions.length; j++)
		{
			var sCurExtension = ImagevalidFileExtensions[j];
			if (Photo.substr(Photo.length - sCurExtension.length, sCurExtension.length).toLowerCase()== sCurExtension.toLowerCase()) 
			{
				blnValidImage = true;
				break;
			}
		}
		if (!blnValidImage) 
		{
			flag=false;
			ImageName=Photo;
			break;
		}
	} 
	if (!flag) 
	{
		swal("", ImageName+" file extension not valid.");
		return '0';
	}
	else 
	{
		return '1';
	}
	
	*/
}

	
	
	
	function validateForm() {
		var Password = document.forms["FormUser"]["Password"].value;
		var ConfirmPassword = document.forms["FormUser"]["ConfirmPassword"].value;
		var UserExist = document.forms["FormUser"]["UserExist"].value;
		if (Password!=ConfirmPassword) {
			alert("Password doesn't match");
			return false;
		}
		else if (UserExist==1) {
			alert("Please enter a valid Username.");
			return false;
		}
		
	}
</script>
       
<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>User Register</h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Add New</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data"
                   onsubmit="return validateForm()" name="FormUser">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="Name" required class="form-control col-md-7 col-xs-12" autocomplete="off">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Username  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="UserName" required class="form-control col-md-7 col-xs-12" id="UserName" onkeyup="checkname();" autocomplete="off" onfocusout="checkname();">
                         <span id="name_status" ><input type="hidden" name="NameExist" value="0"></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" name="Password" required class="form-control col-md-7 col-xs-12" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Confirm Password  <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" name="ConfirmPassword" required class="form-control col-md-7 col-xs-12" autocomplete="off">
                    </div></div>
                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span>			</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                     
                          <input type="file" id="MainImage" name="MainImage"  class="form-control col-md-7 col-xs-12" accept="image/*" required>
                    </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                     
                        <button type="submit" name="UserSubmit" class="btn btn-success">Submit</button>
                    </div></div>
                  </form>
          </div></div></div></div>
          
 <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>User List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                 
                 
                  
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>
                              <th>Name</th>
                              <th>Username</th>
                              <th>Image</th>
                             <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
	 $UserRow=mysqli_query($con,"select * from tbl_login where sts=0 order by UserId desc");
	while($UserResult=mysqli_fetch_array($UserRow))
	{
		$i=$i+1;
	 ?>
     
                            <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $UserResult['Name']; ?></td>
                              <td><?php echo $UserResult['UserName']; ?></td>
                              
                                <td> <img src="<?php if(empty($UserResult['Image'])) echo "../Resource/User/userimg.png"; else echo "../Resource/User/".$UserResult['Image'];?>" width="80" height="80"></td>
                            
                             <td> 
                             <form method="post" enctype="multipart/form-data" action="Edit_User.php"  style="float: left"> 
                             <input type="hidden" name="UserIdUpdate" value="<?php echo $UserResult['UserId']; ?>">
                             	<input type="submit" class="btn btn-info" value="Edit" >
							</form>
        					
         					<?php if($UserResult['UserId']!=1 && $_SESSION['DUIUserId']!=$UserResult['UserId']) { ?>
          					<form method="post" enctype="multipart/form-data" action="action.php" onSubmit="return confirm('Are you sure want to delete this record ?');"  style="float: left">
                            	<input type="hidden" name="UserIdDelete" value="<?php echo $UserResult['UserId']; ?>"> 
                             	<input type="submit" class="btn btn-danger" value="Delete">
                            	 
							</form>
							<?php } ?>


            </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>

          </div></div></div></div>


</div>
</div>
          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>

<?php include('General/Footer.php'); ?>
