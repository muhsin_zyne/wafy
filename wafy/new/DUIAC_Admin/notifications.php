<?php include('General/Header.php'); 

if(isset($_POST['NotificationSubmit']))
{
	$Notification=trim(mysqli_real_escape_string($con,$_POST['Notification']));

			if($_SESSION['DUIRoleId']==1) 
				$Approve=1;
			else
				$Approve=0;

			$result=mysqli_query($con,"INSERT INTO tbl_notifications (NotificationId,Notification,Approve,EUserId)
										VALUES((SELECT IFNULL((SELECT MAX(NotificationId)+1 FROM  tbl_notifications temp),1)),'$Notification','$Approve','".$_SESSION['DUIUserId']."')");
			if($result)
			{
				echo '<script>alert(" Successfully Inserted");</script>';		
			}
			else	
			{			
				echo '<script>alert("Data Not Inserted");</script>';			
			}
			echo "<script>window.location='notifications.php'</script>";

}
?>
	<script>
        function validateForm() {
            
            var Category = document.forms["FormNotification"]["Category"].value;
			var imgpath=document.getElementById('mainImage');
            if (Category==0) 
			{
				alert("Please Select Category.");     
				return false;       
			}
			else if (imgpath.value=="")
			{
				alert("Please Select Image.");     
				return false;    
			}
			else if (!imgpath.value=="")
			{
				var imgsize=imgpath.files[0].size;
				if(imgsize>500000)	
				{		
					alert(imgpath.files[0].name+" - Size is larger than 500KB. Please Reduce it.");	
					return false;	
				}
			}
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Notifications</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Add New</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormNotification">



				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Notification<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" name="Notification" required class="form-control col-md-7 col-xs-12" >
				</div></div>



				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" name="NotificationSubmit" class="btn btn-success">Submit</button>
				</div>
				</div>

				</form>
          </div></div></div></div>

 		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Notification List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                         <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                             <th>Sl. No.</th>
                             <th>Status</th>
                             <th>Notifications</th>
                             <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
				
  if($_SESSION['DUIRoleId']==1)
  { 
			$NotificationRow=mysqli_query($con,"select * from tbl_notifications order by NotificationId desc");
  }
  else
  {
	  $NotificationRow=mysqli_query($con,"select * from tbl_notifications where Approve==1 order by NotificationId desc");

  }
	while($NotificationResult=mysqli_fetch_array($NotificationRow))
	{
		$i=$i+1;
	 ?>
                            <tr>
                              <td><?php echo $i; ?></td>
                               <td><?php if($NotificationResult['Approve']==0) echo "Not Approved"; else echo "Approved"; ?></td>

                             <td><?php echo substr($NotificationResult['Notification'],0,200); ?></td>

                             <td> 
                             <form method="post" enctype="multipart/form-data" action="Edit_notifications.php"  style="float: left"> 
                             <input type="hidden" name="NotificationIdUpdate" value="<?php echo $NotificationResult['NotificationId']; ?>">
                             	<input type="submit" class="btn btn-info" value="Edit" >
							</form>
        					
         					
          					<form method="post" enctype="multipart/form-data" action="action.php" onSubmit="return confirm('Are you sure want to delete this record ?');"  style="float: left">
                            	<input type="hidden" name="NotificationIdDelete" value="<?php echo $NotificationResult['NotificationId']; ?>"> 
                             	<input type="submit" class="btn btn-danger" value="Delete">
                            	 
							</form>
							
							
							<?php 
		
		if($_SESSION['DUIRoleId']==1)
  { 
					if($NotificationResult['Approve']==0)
						$Approve="Approve";
	  				else
							$Approve="Un Approve";
								 ?>
						<form method="post" enctype="multipart/form-data" action="action.php" onSubmit="return confirm('Are you sure want to perform this action ?');"  style="float: left">
                            	<input type="hidden" name="NotificationIdApprove" value="<?php echo $NotificationResult['NotificationId']; ?>"> 
                             	<input type="submit" class="btn btn-warning" value="<?php echo $Approve;?>">
							</form>
							<?php } ?>
            </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>

          </div></div></div></div>
	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
