<?php include('General/Header.php'); 
if($_SESSION['DUIRoleId']!=1) { 
	echo "<script>window.location='Home.php'</script>";
}

	if(isset($_POST['NewsCategorySubmit']))
	{
		$NewsCategory=trim(mysqli_real_escape_string($con,$_POST['NewsCategory']));
				$result=mysqli_query($con,"INSERT INTO tbl_news_category(CategoryId,Category) 
						VALUES((SELECT IFNULL((SELECT MAX(CategoryId)+1 FROM  tbl_news_category temp),1)),'$NewsCategory')");
				if($result)
				{
					echo '<script>alert(" Successfully Inserted");</script>';
				}
				else
				{
					echo '<script>alert("Data Not Inserted");</script>';
				}
				echo "<script>window.location='NewsCategory.php'</script>";	
	}
?>

<div class="right_col" role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>News Category</h3>
            </div>
            
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Add New</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category Name <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="NewsCategory" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                        <button type="submit" name="NewsCategorySubmit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>


 <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>News Category List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />


                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>
                              <th>Category</th>
                             <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
	 $NewsCategoryRow=mysqli_query($con,"select * from tbl_news_category order by CategoryId desc");
	while($NewsCategoryResult=mysqli_fetch_array($NewsCategoryRow))
	{
		$i=$i+1;
	 ?>
     
                            <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $NewsCategoryResult['Category']; ?></td>
                             <td> 
                             
                               <form method="post" enctype="multipart/form-data" action="Edit_NewsCategory.php"  style="float: left"> 
                             <input type="hidden" name="NewsCategoryUpdate" value="<?php echo $NewsCategoryResult['CategoryId']; ?>">
                             	<input type="submit" class="btn btn-info" value="Edit" >
							</form>
        					
         					
          					<form method="post" enctype="multipart/form-data" action="action.php" onSubmit="return confirm('Are you sure want to delete this record ?');" style="float: left">
                            	<input type="hidden" name="NewsCategoryDelete" value="<?php echo $NewsCategoryResult['CategoryId']; ?>"> 
                             	<input type="submit" class="btn btn-danger" value="Delete">
                            	 
							</form>
                             
                             
                             </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>


                </div>
              </div>
            </div>
          </div>



</div>
</div>
          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>
 
      
<?php include('General/Footer.php'); ?>