<?php include('General/Header.php'); 

	if(isset($_POST['BlogCategorySubmit']))
	{
		$BlogCategory=trim($_POST['BlogCategory']);
				$result=mysqli_query($con,"INSERT INTO tbl_blogcategory(BlogCategoryName) 
						VALUES('$BlogCategory')");
				if($result)
				{
					echo '<script>alert(" Successfully Inserted");</script>';
				}
				else
				{
					echo '<script>alert("Data Not Inserted");</script>';
				}
				echo "<script>window.location='BlogCategory.php'</script>";	
	}
?>

<div class="right_col" role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Blog Category</h3>
            </div>
            
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Add New</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category Name <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="BlogCategory" required class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                        <button type="submit" name="BlogCategorySubmit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>


 <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Category List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />


                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>
                              <th>Category</th>
                             <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
	 $PortfolioCategoryRow=mysqli_query($con,"select * from tbl_blogcategory order by BlogCategoryId desc");
	while($PortfolioCategoryResult=mysqli_fetch_array($PortfolioCategoryRow))
	{
		$i=$i+1;
	 ?>
     
                            <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $PortfolioCategoryResult['BlogCategoryName']; ?></td>
                             <td> <a class="btn btn-info" href="Edit_BlogCategory.php?BlogCategoryId=<?php echo $PortfolioCategoryResult['BlogCategoryId']; ?>">
                <i class="glyphicon glyphicon-edit icon-white"></i>
                Edit
            </a>
            <a class="btn btn-danger" href="action.php?BlogCategoryId=<?php echo $PortfolioCategoryResult['BlogCategoryId']; ?>" onclick="return window.confirm('Do you want to delete this record?')">
                <i class="glyphicon glyphicon-trash icon-white"></i>
                Delete
            </a></td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>


                </div>
              </div>
            </div>
          </div>



</div>
</div>
          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>
 
      
<?php include('General/Footer.php'); ?>