<?php include('General/Header.php'); 




if(isset($_POST['StaffSubmit']))
{
	$name=trim(mysqli_real_escape_string($con,$_POST['name']));
	$designation=trim(mysqli_real_escape_string($con,$_POST['designation']));
	$FbLink=trim(mysqli_real_escape_string($con,$_POST['FbLink']));
	$Order=trim(mysqli_real_escape_string($con,$_POST['Order']));
	
	
		
	if(isset($_FILES['mainImage']['name']) && !empty($_FILES['mainImage']['name']))
	{
			$filename=$_FILES['mainImage']['name'];
			$temp=$_FILES['mainImage']['tmp_name'];
			$ra=rand(10,10000000000);
			$rb=rand(10,10000000000);
			$r="Staff_".$ra.$rb."_";
			$image_name1=$r.$filename;
			move_uploaded_file($temp,"../Resource/Staff/".$image_name1);
		

			$result=mysqli_query($con,"INSERT INTO tbl_staff (StaffId,Name,Designation,FbLink,Image,DisplayOrder)
										VALUES((SELECT IFNULL((SELECT MAX(StaffId)+1 FROM  tbl_staff temp),1)),'$name','$designation','$FbLink','$image_name1',$Order)");
			if($result)
			{
				echo '<script>alert(" Successfully Inserted");</script>';		
			}
			else	
			{			
				echo '<script>alert("Data Not Inserted");</script>';			
			}
			echo "<script>window.location='Staff.php'</script>";
		}
	else
	{
		echo '<script>alert(" Please Select Image");</script>';	
	}
}
?>
	<script>
        function validateForm() {
            var name = document.forms["FormStaff"]["name"].value;
			var designation = document.forms["FormStaff"]["designation"].value;
		
			var imgpath=document.getElementById('mainImage');
            if (!name||!designation) 
			{
				alert("Please Fill All Fields.");     
				return false;       
			}
			else if (imgpath.value=="")
			{
				alert("Please Select Image.");     
				return false;    
			}
		}
     </script>

	<div class="right_col" role="main">
		<div class="">
		<div class="page-title">
			<div class="title_left">		<h3>Staff Details</h3>		</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_title">
				<h2>Add New</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			<br />
				<form data-parsley-validate class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" onsubmit="return validateForm()" name="FormStaff">

				

			

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="name" required class="form-control col-md-7 col-xs-12">
				</div></div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Designation<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="designation" required class="form-control col-md-7 col-xs-12">
				</div></div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Facebook Link<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text"  name="FbLink" class="form-control col-md-7 col-xs-12"  >
				</div></div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Display Order<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number"  name="Order" required class="form-control col-md-7 col-xs-12" min="1" value="<?php $or= mysqli_fetch_array(mysqli_query($con,"(SELECT IFNULL((SELECT MAX(StaffId)+1 FROM  tbl_staff temp),1))"));echo $or[0];  ?>"  >
				</div></div>
				
            
				
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<font color="#FF0004">Max-Image Size: <b>500KB</b> &nbsp; (Image Dimension --- width: <b>270px</b> , Height: <b>270px</b> )</font>
						<input type="file" name="mainImage" id="mainImage"  class="form-control col-md-7 col-xs-12" accept="image/*" required >
				</div></div>
               

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" name="StaffSubmit" class="btn btn-success">Submit</button>
				</div>
				</div>
				
				</form>
          </div></div></div></div>

 		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Staff List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                   
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                         <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Sl. No.</th>
                             <th>Name</th>
                              <th>Designation</th>
                              <th>Facebook Link</th>
                               <th>Display Order</th>
                              <th>Image</th>
                             
                             
                             <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php
	$i=0;
				

			$StaffRow=mysqli_query($con,"select * from tbl_staff order by StaffId desc");

  
	while($StaffResult=mysqli_fetch_array($StaffRow))
	{
		$i=$i+1;
	 ?>
                            <tr>
                              <td><?php echo $i; ?></td>
                              
                              
                              <td><?php echo $StaffResult['Name']; ?></td>
								<td><?php echo $StaffResult['Designation']; ?></td>
                             <td><?php echo $StaffResult['FbLink']; ?></td>
                              <td><?php echo $StaffResult['DisplayOrder']; ?></td>
                             
                             
                                <td> <img src="<?php echo "../Resource/Staff/".$StaffResult['Image'];?>" width="100" height="100"></td>
                           	
                             
                             <td> 
                             <form method="post" enctype="multipart/form-data" action="Edit_Staff.php"  style="float: left"> 
                             <input type="hidden" name="StaffIdUpdate" value="<?php echo $StaffResult['StaffId']; ?>">
                             	<input type="submit" class="btn btn-info" value="Edit" >
							</form>
        					
         					
          					<form method="post" enctype="multipart/form-data" action="action.php" onSubmit="return confirm('Are you sure want to delete this record ?');"  style="float: left">
                            	<input type="hidden" name="StaffIdDelete" value="<?php echo $StaffResult['StaffId']; ?>"> 
                             	<input type="submit" class="btn btn-danger" value="Delete">
							</form>
            </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>

          </div></div></div></div>
	</div></div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>     
<?php include('General/Footer.php'); ?>
