<?php
include('DUIAC_Admin/General/DBConnection.php');
$HomeGeneral=mysqli_fetch_array(mysqli_query($con,"select * from tbl_home_general where id=1"));
?>
    


<header id="navbar-spy" class="header header-4 transparent-header ">
	<nav id="primary-menu" class="navbar navbar-fixed-top affix-top">
    <div id="top-bar" class="top-bar">


		<div class="row">

            <div class="col-xs-3 col-sm-3 col-md-2 entry clearfix">
				<div class="entry-featured">
				<a class="logo" href="index.php"><img class="img-responsive" src="Resource/AboutUs/duia_logo_new.jpg" width="125px" alt="wafy DUIAC"></a>
            </div></div>

            <div class="col-xs-6 col-sm-6 col-md-8 entry clearfix">
				<div class="entry-featured text-center">
                    <h4 style="margin:15px 0 0">DARUL ULOOM ISLAMIC AND ARTS COLLEGE</h4>
                    <h6 style="margin:5px 0">Wafy Campus Paral, Thootha ph: <span><a href="tel:04933206266">04933 206 266</a>, <a href="tel:9446240340">9446 240 340</a></span></h6>
                    <p>Email: <span><a href="mailto:duiac.wafy@gmail.com">duiac.wafy@gmail.com</a></span>
							<a class="facebook" href="https://www.facebook.com/Darul-Uloom-Islamic-Arts-College-Wafy-Parel-Thootha-307788269421983/" >
								<i class="fa fa-facebook"></i>
							</a></p>
            </div></div>

            <div class="col-xs-3 col-sm-3 col-md-2 entry clearfix">
				<div class="entry-featured">
				<a class="logo" href="index.php">
					<img src="<?php echo "Resource/AboutUs/".$HomeGeneral['logo'];?>" alt="wafy DUIAC">
				</a>
            </div></div>

        </div>


    <!--<div class="container">
		<div class="top-bar-border">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 hidden-xs">
					<ul class="list-inline top-contact">
						<li>	<p>Phone: <span><a href="tel:04933206266">04933 206 266</a>, <a href="tel:9446240340">9446 240 340</a> </span></p>	</li>
						<li>	<p>Email: <span><a href="mailto:duiac.wafy@gmail.com">duiac.wafy@gmail.com</a></span></p>	</li>
					</ul>
				</div>
			</div>
		</div>
	</div>-->
</div>


		<div class="container-fluid" style="background-color: rgba(195, 195, 195, 0.45);">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="logo rs-logo" href="index.php">
					<img src="<?php echo "Resource/AboutUs/".$HomeGeneral['logo'];?>" alt="wafy DUIAC">
				</a>
			</div>


			<div class="collapse navbar-collapse pull-right" id="navbar-collapse-1">
				<ul class="nav navbar-nav navbar-center">
	<li class="has-dropdown">		<a href="index.php"  class=" link-hover" data-hover="Home">Home</a>	</li>
    <li class="has-dropdown">
        <!--<a href="about.php" data-toggle="dropdown" class="dropdown-toggle link-hover" data-hover="About">About</a>-->
        <a href="about.php" class="dropdown-toggle link-hover" data-hover="About">About</a>
            <ul class="dropdown-menu">
    <li >		<a href="about.php#visionandmission"  class=" link-hover" >Vision & Mission</a>		</li>
    <li >		<a href="about.php"  class=" link-hover" >Governence</a>		</li>
    <li >		<a href="about.php#staff"  class=" link-hover" >Staff</a>		</li>
    <li >		<a href="contact.php"  class=" link-hover" >Contact</a>		</li>
            </ul>		</li>
    <li class="has-dropdown">		<a href="academic.php" class="dropdown-toggle link-hover" data-hover="Academic">Academic</a>
            <ul class="dropdown-menu">
    <li >		<a href="academic.php#admission"  class=" link-hover" >Admission</a>		</li>
    <li >		<a href="academic.php#course"  class=" link-hover" >Course</a>		</li>
    <li >		<a href="academic.php#calender"  class=" link-hover" >Academic Calender</a>	</li>
            </ul>		</li>
    <li class="has-dropdown">		<a href="exam.php" class="dropdown-toggle link-hover" data-hover="Exam">Exam</a>
            <ul class="dropdown-menu">
                <li><a href="#result">Result</a></li>
                <li><a href="#application">Application</a></li>
            </ul>					</li>
	<li class="has-dropdown">		<a href="downloads.php"  class=" link-hover" data-hover="Downloads">Downloads</a>		</li>
    <li class="has-dropdown">		<a href="gallery.php"  class=" link-hover" data-hover="Gallery">Gallery</a>	</li>
	<li class="has-dropdown">		<a href="event.php"  class=" link-hover" data-hover="Event">Event</a>		</li>
	<li class="has-dropdown">		<a href="students.php" class="dropdown-toggle link-hover" data-hover="Students">Students</a>
            <ul class="dropdown-menu">
    <li >		<a href="students.php#almiras"  class=" link-hover" >Almiras (students union)</a>		</li>
    <li >		<a href="students.php#dawa"  class=" link-hover" >da'wa (alumni)</a>		</li>
    <li >		<a href="students.php#publication"  class=" link-hover" >Publications</a>		</li>
            </ul>					</li>
    <li class="has-dropdown">		<a  class="dropdown-toggle link-hover" data-hover="Publications">Publications</a>
            <ul class="dropdown-menu">
                <li><a href="article.php">Articles</a></li>
            </ul>					</li>

<!--	<li >		<a href="index.php"  class=" link-hover" data-hover="Home">Home</a>	</li>
    <li >		<a href="about.php"  class=" link-hover" data-hover="About">About</a>		</li>
    <li >		<a href="news.php"  class=" link-hover" data-hover="News">News</a>		</li>
    <li >		<a href="gallery.php"  class=" link-hover" data-hover="Gallery">Gallery</a>	</li>
    <li >		<a href="article.php"  class=" link-hover" data-hover="Articles">Articles</a>					</li>
	<li >		<a href="students.php"  class=" link-hover" data-hover="Students">Students</a>					</li>
	<li >		<a href="contact.php"  class=" link-hover" data-hover="Contact">Contact</a>		</li>-->
				</ul>

			</div>

		</div>


	</nav>


</header>

