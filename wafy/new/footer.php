
<!-- Footer #1
============================================= -->
<footer id="footer" class="footer-1">
	<!-- Social bar
	============================================= -->
	<div class="footer-social">
		<div class="container">
			<div class="row">
				<div class="col-xs-12  col-sm-12  col-md-12">
					<div class="footer-social-bg">
						<div class="footer-social-info pull-left pull-none-xs">
							<p class="mb-0">Don’t Miss To Follow Us On Our Social Networks Official Accounts.</p>
						</div>
						<div class="footer-social-icon pull-right text-right pull-none-xs">
							<a class="facebook" href="https://www.facebook.com/Darul-Uloom-Islamic-Arts-College-Wafy-Parel-Thootha-307788269421983/" >
								<i class="fa fa-facebook"></i><i class="fa fa-facebook"></i>
							</a>
							<a class="facebook" href="https://www.facebook.com/%E0%B4%86%E0%B5%BC%E0%B4%9F%E0%B5%8D%E0%B4%9F%E0%B5%8D-%E0%B4%85%E0%B4%B1%E0%B5%8D%E0%B4%B1%E0%B4%BE%E0%B4%95%E0%B5%8D%E0%B4%95%E0%B5%8D-DUIAC-230865260649001/" >
								<i class="fa fa-facebook"></i><i class="fa fa-facebook"></i>
							</a>
							<a class="facebook" href="https://www.facebook.com/groups/163137010435342/" >
								<i class="fa fa-facebook"></i><i class="fa fa-facebook"></i>
							</a>
							
							
						</div>
					</div><!-- .footer-social-bg end -->
				</div>
			</div>
		</div><!-- .container end -->
	</div><!-- .footer-social end -->
	

	
	<!-- Copyrights
	============================================= -->
	<div class="footer-copyright bg-dark3">
		<div class="container">
			<div class="row">
				<div class="col-xs-12  col-sm-12  col-md-12">
					<div class="powered text-uppercase pull-left pull-none-xs mb-15-xs">
						<p><span class="text-white">KMECC MARAYAMANGALAM .</span> Powerd By <a >UAE Da'wa Committee</a> </p>
					</div>
					<div class="copyright pull-right pull-none-xs">
						<p class="text-capitalize">
							<span class="text-uppercase">&copy; 2016. All Rights Reserved</span>
						
						</p>
					</div>
				</div>
			</div>
		</div><!-- .container end -->
	</div><!-- .footer-copyright end -->
</footer>
