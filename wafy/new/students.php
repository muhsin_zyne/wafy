
<?php
include('DUIAC_Admin/General/DBConnection.php');
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="DUIAC" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="KMECC MARAYAMANGALAM ">
<link href="favicon.png" rel="icon">


<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">


<!-- Document Title
    ============================================= -->
<title>KMECC MARAYAMANGALAM  | Students</title>
</head>
<body>

<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">

<?php include('header.php'); ?>

		

<!-- Page Title #1
============================================= -->
<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="Resource/students.jpg" alt="Contact Wafy"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="title-1 text-center">
					
					<div class="title-heading">
						<h2>Students</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->

<section id="almiras" class="shortcode-9">
	<div class="container">
		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1 col-sm-12 col-md-3 col-md-offset-1">
				<div class="heading mb-0">
					
					<h2>Al Miras Students Association</h2>
				</div>
				<!--.headingend -->
			</div>
			<!--.col-md-4end -->
			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0" style="color: black"><b>Al Miras</b> Students Association is a creative platform for the students of D.U.I.A.C the association is involved in various activities aimed at promoting the co- curricular activities of the students and engaging them in different social and cultural forums, literary activities and contests, debates and seminars etc. in and out of the campus alike.  </p>
				
				


		<style>
			.Achievements li
			{
				.text-align: justify;
				padding: 5px;
				margin-bottom: 5px;
				font-family:Comic Sans MS,cursive;
				font-size:14px;
				color: #2A2A2A;
				list-style: circle;
			}
			.Achievements li strong
			{
				color: red;
				font-size:16px;
			}
			.pt-0	{	padding-top:0 !important;	}
			.text-right	{	text-align:right;	}
			//.sub-heading h3	{	text-decoration:underline;	}
				</style>



				
			</div>
			<!--.col-md-8end -->
		</div>
		<!-- .row end -->




		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>Aims and functions</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">

		<ul class="Achievements">
	<li >To consolidate the activities of students from different classes.</li>
	<li >To active them to efficiently carry out educational and co- curricular activities.</li>
	<li >To cultivate the organizational skill among the students.</li>
	<li >To enable them to participate and contribute in Islamic propagation according to the need of the time.</li>
		</ul>
				<p class="mb-0 pt-0 text-justify" style="color: black">Programmes by <b>Al Miras</b> include ensuring the availability of News Papers in different languages and Periodicals, and running a full-fledged library and computer lab. This union also organizes programs like street speeches, debates, seminars, expert guest lectures, and other competitions among students.
					<br/><b>Students union comprises about 13 sub committees doing different roles and responsibilities.</b></p>
			</div>
		</div>






		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>SAHITHYA SAMAJAM</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 pt-0 text-justify" style="color: black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It’s indeed to develop various skill of the students, such as public elocution in English, Arabic, Urdu as well as in their mother tongue Malayalam; verbal and written, translation; poetry writing and etc.
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In order to make the students well equipped for their dreams and aims, the committee divides them into six groups and hold fine art sessions (samajam) separately on a week basis and one general session once in a month.
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apart from conducting special programs on the occasion of independence day, meelad day etc. it organizes every year an arts fest wherein the students compete in various on and off stage events.</p>
			</div>
		</div>


		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>LIBRARY</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 pt-0 text-justify" style="color: black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It helps students to fulfill their quest for knowledge by providing access to them to vast array of reading materials across the spectrum. They include daily newspapers, periodicals in different languages; poems, stories and novels and research magazines.</p>
			</div>
		</div>


		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>SOCIAL AFFAIRS BOARD</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 pt-0 text-justify" style="color: black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This board is an active wing which keeps the campus safe and clean, inculcating the values of social responsibility in students it also strives to extend a helping hand to the poor and needy students through collaborative efforts.</p>
			</div>
		</div>


		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>STORE COMMITTEE</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 pt-0 text-justify" style="color: black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The committee runs as an in- house store catering to the daily requirements of the students. It sells various stationary and other commodities to the colleges students at affordable prices.</p>
			</div>
		</div>


		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>SAVING BANK</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 pt-0 text-justify" style="color: black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Saving bank offers an interest free saving scheme for students and it creates a better awareness about the significance of financial discipline</p>
			</div>
		</div>


		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>MEDICAL BOARD</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 pt-0 text-justify" style="color: black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It ensures the health and wellbeing of the students by conducting training programs and awareness campus and it gives first aids and take them to hospitals if needed.</p>
			</div>
		</div>


		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>RESEARCH CELL</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 pt-0 text-justify" style="color: black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The cell consisting ………………….. Clubs makes use of all available reference materials to held researches in all relevant topics which will be presented to the students through power point, paper presentation and debates.</p>
			</div>
		</div>


		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>PRESS & COMMUNICATION</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 pt-0 text-justify" style="color: black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The board is an active wing of Al Miras which helps the students to publish their articles in well -known magazines and periodicals and it tries to give all the campus related events in newspapers and televisions.</p>
			</div>
		</div>


		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>MAGAZINE BOARD</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 pt-0 text-justify" style="color: black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It is obliged to publish written are printed magazines in four languages (Arabic, English, Urdu and Malayalam) twice a year and keeps its activeness publishing special magazines and supplements on special occasions.</p>
			</div>
		</div>


		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>I.R.P.C (Islamic Rituals Presentation Chair)</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 pt-0 text-justify" style="color: black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The wing as its name indicates work to keep the Islamic ritual alive at the campus by giving chances to the students to perform sermons (khuthuba) in mosque and they makes students well prepared for “imamath”</p>
			</div>
		</div>


		<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1  col-sm-12  col-md-3 col-md-offset-1">
				<div class="sub-heading mb-0 text-center">					<h3>AUDITING BOARD</h3>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 pt-0 text-justify" style="color: black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The auditing board checks upon every division of the committee by giving appropriate directions to ensure that it functions to meet its objectives.</p>
			</div>
		</div>







				<div id="dawa" class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1 col-sm-12 col-md-3 col-md-offset-1">
				<div class="heading mb-0">
					<h2>Darul Uloom Association for Wafy Alumni</h2>
				</div>
				<!--.headingend -->
			</div>
			<!--.col-md-4end -->
			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0" style="color: black">Our Campus has an old students’ federation named <b>DA'WA</b> (Darul Uloom Association for Wafy Alumni) within this we have professionally skilled and highly educated personalities excel both in the job market and higher studies institution in and outside of Kerala. This testifies to the academic quality of the courses and the acceptance of the message it conveys. Some of the who have completed the course are pursuing their higher studies and many others work in different parts of the world as lectures, educationalist, Imams, Islamic propagators, media persons, preachers and business executives etc.</p>
<!--				<p class="mb-0" style="color: black"><b>Duiac</b> graduates are known as wafies who have perused their  studies in this institution. They are spread in different fields of Islamic propagation. For the smooth operation of this great mission it has been formed an association named <b> DAWA </b>( Darul Uloom`s Wafy  Alumni Association).</p>		-->
			</div>
			<!--.col-md-8end -->
		</div>
		<!-- .row end -->
		
		
		
		
				<div class="row heading-1 mb-60 clearfix">
			<div class="col-xs-12 col-xs-offset-1 col-sm-12 col-md-3 col-md-offset-1">
				<div class="heading mb-0">					<h2>PUBLICATION</h2>				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0" style="color: black"><b>Sample Text Sample Text Sample Text</b></p>
<!--				<p class="mb-0" style="color: black"><b>Duiac</b> graduates are known as wafies who have perused their  studies in this institution. They are spread in different fields of Islamic propagation. For the smooth operation of this great mission it has been formed an association named <b> DAWA </b>( Darul Uloom`s Wafy  Alumni Association).</p>		-->
			</div>
		</div>



		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
<!-- #shortcode9end -->


<section id="team" class="team team-1 pb-lg">
	<div class="container">
	
	<?php $studentbatchquery=mysqli_query($con,"select * from tbl_students_category order by CategoryId asc"); 
		while( $studentbatch=mysqli_fetch_array($studentbatchquery))
		{
		?>
	<h3 > Batch : <?php echo $studentbatch['Category'];?> </h3>
		<div class="row">
			
			<?php $StudentListQuery=mysqli_query($con,"select * from tbl_students where BatchId='".$studentbatch['CategoryId']."' order by DisplayOrder asc"); 
		while( $StudentList=mysqli_fetch_array($StudentListQuery))
		{
		?>
			<!-- Member #1 -->
			<div class="col-xs-12 col-sm-6 col-md-3 member">
				<div class="member-img">
					<img src="<?php echo "Resource/Students/".$StudentList['Image'];?>" alt="Wafy Students Image"/>
					<div class="member-bg"></div>
					<div class="member-overlay">
						<h6><?php echo $StudentList['Occupation'];?></h6>
						<h5><?php echo $StudentList['Name'];?></h5>
					</div><!-- .memebr-ovelay end -->
					<div class="member-hover">
						<h6>Reg# CIC: <b><?php echo $StudentList['RegCIC'];?></b>, CLG: <b><?php echo $StudentList['RegCLG'];?></b> </h6>
						<h5><?php echo $StudentList['Name'];?></h5>
						<p>
						
						<i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?php echo $StudentList['Address'];?><br/>
							<i class="fa fa-envelope"></i> &nbsp;&nbsp; <a href="mailto:<?php echo $StudentList['Email'];?>"><?php echo $StudentList['Email'];?></a> <br/>
							<i class="fa fa-phone"></i>&nbsp;&nbsp;<a href="tel:<?php echo $StudentList['Phone'];?>"> <?php echo $StudentList['Phone'];?></a> <br/>
						<i class="fa fa-mobile"></i>&nbsp;&nbsp; <a href="tel:<?php echo $StudentList['Mobile'];?>"> <?php echo $StudentList['Mobile'];?></a> <br/>
						</p>
					
						<div class="member-social">
						<?php if(!empty($StudentList['FbLink'])) { ?>
							<a href="<?php echo $StudentList['FbLink'];?>" target="_blank"><i class="fa fa-facebook"></i></a>
							<?php } ?>
							<?php if(!empty($StudentList['TwitterLink'])) { ?>
							<a href="<?php echo $StudentList['TwitterLink'];?>" target="_blank"><i class="fa fa-twitter"></i></a>
							<?php } ?>
							<?php if(!empty($StudentList['GoogleLink'])) { ?>
							<a href="<?php echo $StudentList['GoogleLink'];?>" target="_blank"><i class="fa fa-google-plus"></i></a>
							<?php } ?>
							<?php if(!empty($StudentList['LinkedInLink'])) { ?>
							<a href="<?php echo $StudentList['LinkedInLink'];?>" target="_blank"><i class="fa fa-linkedin"></i></a>
							<?php } ?>
						</div>
					</div><!-- .member-hover end -->
				</div><!-- .member-img end -->
			</div><!-- .member end -->
			<?php } ?>
			<!-- Member #2 -->
		
			
		</div><!-- .row end -->
		
		<?php } ?>
		
	</div><!-- .container end -->
</section><!-- #team end -->





<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/functions.js"></script>
</body>

<!-- Mirrored from 7oroof.com/tfdemos/mount/contact-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Nov 2016 05:26:56 GMT -->
</html>