
<?php
include('DUIAC_Admin/General/DBConnection.php');
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="DUIAC" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="KMECC MARAYAMANGALAM ">
<link href="favicon.png" rel="icon">


<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">


<!-- Document Title
    ============================================= -->
<title>KMECC MARAYAMANGALAM  | EXAM | Result | Application</title>
</head>
<body>

<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">

<?php include('header.php'); ?>

		

<!-- Page Title #1
============================================= -->
<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="Resource/students.jpg" alt="Contact Wafy"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="title-1 text-center">
					
					<div class="title-heading">
						<h2>Exam</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->

<section id="shortcode9" class="shortcode-9">
	<div class="container">

        <div id="result" class="row heading-1 mb-60 clearfix">
            <div class="col-xs-12 col-xs-offset-1 col-sm-12 col-md-3 col-md-offset-1">
				<div class="heading mb-0">					<h2>Result</h2>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 text-justify" style="color: red">Sample Text content should be replaced with the original content of the result </p>
			</div>
	   </div>

        <div id="application" class="row heading-1 mb-60 clearfix">
            <div class="col-xs-12 col-xs-offset-1 col-sm-12 col-md-3 col-md-offset-1">
				<div class="heading mb-0">					<h2>Application</h2>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 text-justify" style="color: red">Sample Text content should be replaced with the original content of the application>  </p>
			</div>
	   </div>


    </div>
</section>








<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/functions.js"></script>
</body>

<!-- Mirrored from 7oroof.com/tfdemos/mount/contact-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Nov 2016 05:26:56 GMT -->
</html>