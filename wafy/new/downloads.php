<?php
include('DUIAC_Admin/General/DBConnection.php');
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="DUIAC" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="KMECC MARAYAMANGALAM ">
<link href="favicon.png" rel="icon">


<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">


<!-- Document Title
    ============================================= -->
<title>KMECC MARAYAMANGALAM  | Downloads </title>
</head>
<body>

<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">

<?php include('header.php'); ?>

		

<!-- Page Title #1
============================================= -->
<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="Resource/gallerey.jpg" alt="Gallary"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="title-1 text-center">
					
					<div class="title-heading">
						<h2>Downloads</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->



<section id="blog-grid-1-col" class="blog blog-grid blog-1col">
	<div class="container">
	
		<?php $newsQuery=mysqli_query($con,"select * from tbl_news where Approve=1 order by NewsId desc ");
							while($newsResult=mysqli_fetch_array($newsQuery))
							{
							?>
							
	
		<div class="row" id="<?php echo "News_".$newsResult['NewsId'];?>">

			<div class="col-xs-12 col-sm-12 col-md-11 entry clearfix">
				<div class="col-md-4">
				<div class="entry-featured">
					<img src="<?php echo "Resource/News/".$newsResult['Image'];?>" alt="News Image">
			
				</div>
				</div>
				<div class="col-md-8">
				
				<div class="entry-meta">
					<span>In : </span><span><a ><?php $cat=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_news_category where CategoryId='".$newsResult['CategoryId']."'"));
							echo  $cat[0];?></a></span> /
					<span>On : </span><span> <a><?php echo date('M d Y',strtotime($newsResult['Date']));?></a></span> /

				</div>
				<div class="entry-title">
					<h3><a><?php echo $newsResult['Heading'];?></a></h3>
				</div>
				<div class="entry-content mb-15">
					<p><?php echo $newsResult['Content'];?></p>
				</div>
              <div class="col-xs-12 col-sm-12 col-md-12 ml--15">
				<a class="btn btn-primary" href="article.php" download >Download</a>
<!--	<a href="Downloads/CompanyProfile.pdf"  download >-->

			</div>

				</div>

			</div>



		</div>
		
		<?php } ?>
		
		

	</div>
</section>





<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/functions.js"></script>
</body>

<!-- Mirrored from 7oroof.com/tfdemos/mount/contact-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Nov 2016 05:26:56 GMT -->
</html>