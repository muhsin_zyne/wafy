<?php
include('DUIAC_Admin/General/DBConnection.php');
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="DUIAC" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="KMECC MARAYAMANGALAM ">
<link href="favicon.png" rel="icon">


<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">


<!-- Document Title
    ============================================= -->
<title>KMECC MARAYAMANGALAM  | Academic</title>
</head>
<body>

<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">

<?php include('header.php'); ?>

		

<!-- Page Title #1
============================================= -->
<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="Resource/students.jpg" alt="Contact Wafy"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="title-1 text-center">
					
					<div class="title-heading">
						<h2>Academic</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->

<section id="shortcode9" class="shortcode-9">
	<div class="container">

        <div id="admission" class="row heading-1 mb-60 clearfix">
            <div class="col-xs-12 col-xs-offset-1 col-sm-12 col-md-3 col-md-offset-1">
				<div class="heading mb-0">					<h2>Admission</h2>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 text-justify" style="color: black">The academic year begins in the month of June of every year. The CIC enrolls students on its preparatory (Tamheediyya) course through a rigorous entrance test. Those who are not above 17 years, and have passed the Secondary School (10th standard) exam and the seventh grade from any Madrassa run by All Kerala Islamic Education Board (or equivalent) are eligible to apply for the course.<br/><br/>Only those who successfully complete the Tamheediya (preparatory) course of the CIC or its equivalent will get admission to the Aliya (undergraduate) courses. The admission to the Postgraduate stage is restricted to those who successfully complete the Aliya course by the CIC or its equivalent.  </p>
			</div>
	   </div>

        <div id="course" class="row heading-1 mb-60 clearfix">
            <div class="col-xs-12 col-xs-offset-1 col-sm-12 col-md-3 col-md-offset-1">
				<div class="heading mb-0">					<h2>Course</h2>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 text-justify" style="color: black">The institution follows the Wafy Syllabus, which came in to existence bridging the existing gap in the scenario of religious education. All the Wafy institutions are run under Samastha Kerala Jamyithul Ulama which is an Umbrella Organization for Muslim scholar. The admission to this course is given for those candidates who completed the 10th standard successfully after an entrance test. At present, around 5000 students are studying in about forty affiliated colleges including Wafy (8 years course exclusively for boys) and Wafiyya (5 years course exclusively for girls).The course has been broken in to 2years for preparatory, four years for degree and two years for PG. The whole course goes with 16 semesters and students are provided chance to choose the specialized areas according to their need and aptitudes <a href="www.wafycic.com">(www.wafycic.com)</a>  </p>
			</div>
	   </div>

        <div id="calender" class="row heading-1 mb-60 clearfix">
            <div class="col-xs-12 col-xs-offset-1 col-sm-12 col-md-3 col-md-offset-1">
				<div class="heading mb-0">					<h2>Academic Calender</h2>				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<p class="mb-0 text-justify" style="color: black">  </p>
			</div>
	   </div>

    </div>
</section>








<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/functions.js"></script>
</body>

<!-- Mirrored from 7oroof.com/tfdemos/mount/contact-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Nov 2016 05:26:56 GMT -->
</html>