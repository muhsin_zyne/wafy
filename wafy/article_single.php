<?php
include('admin/General/DBConnection.php');
if(isset($_GET['ArticleId']))
{
	$ArticleId=$_GET['ArticleId'];
	$ArtResult=mysqli_fetch_array(mysqli_query($con,"select * from tbl_article where Approve=1 and ArticleId = '".$ArticleId."'"));
	if(empty($ArtResult[0]))
	{
		echo "<script>window.location='article.php'</script>";
	}
}
else
{
	
	echo "<script>window.location='article.php'</script>";
}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Document Meta
    ============================================= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--IE Compatibility Meta-->
<meta name="author" content="<?php echo $ArtResult['Author']; ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="<?php echo substr($ArtResult['Content'],0,200); ?>">
<link href="favicon.png" rel="icon">
<meta property="og:image" content="http://www.wafycampus.com/Resource/Article/<?php echo $ArtResult['Image'];?> "/>


<!-- Stylesheets
    ============================================= -->
<link href="assets/css/external.css" rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">

<!-- Fonts
    ============================================= -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100italic,100,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700italic,700,800,800italic,900,900italic%7COpen+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic%7CUbuntu:400,300,300italic,400italic,500,700,500italic,700italic%7CRoboto+Slab:400,100,300,700%7CLora:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

<!-- Document Title
    ============================================= -->
<title>KMECC MARAYAMANGALAM  | <?php echo $ArtResult['Heading']; ?></title>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" id="st_insights_js" src="http://w.sharethis.com/button/buttons.js?publisher=410547db-9da8-4fad-aa95-c3739d198b33"></script>
<script type="text/javascript">stLight.options({publisher: "410547db-9da8-4fad-aa95-c3739d198b33", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>


</head>
<body>

<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="wrapper clearfix">

<?php include('header.php'); ?>

		

<!-- Page Title #1
============================================= -->
<section id="page-title" class="page-title bg-overlay bg-overlay-dark">
	<div class="bg-section" >
		<img src="Resource/article.jpg" alt="Background"/>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="title-1 text-center">
					
					<div class="title-heading">
						<h2>Articles</h2>
					</div>
				</div><!-- .page-title end -->
			</div><!-- .col-md-12 end -->
		</div><!-- .row end -->
	</div><!-- .container end -->
</section><!-- #page-title end -->


<!-- Blog Single Standard Right Sidebar
============================================= -->
<section id="blog-single-standard" class="blog blog-single blog-single-standard">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 mb-30-xs mb-30-sm">
				<div class="entry">
					<div class="entry-featured">
					<?php if($ArtResult['ImageOrVideo']==2)
			{	?>
			<div class="videoWrapper">
				<iframe width="100%" height="400" src="<?php echo $ArtResult['Video'];?>" allowfullscreen frameborder="0" ></iframe>
						</div>
				
			<?php } else {	?>
						<img src="<?php echo "Resource/Article/".$ArtResult['Image'];?>" alt="entry title">
						<?php } ?>
					</div>
					<!-- .entry-featured end -->
					<div class="entry-post">
						<div class="entry-meta">
							<span>By : </span>
							<a ><?php echo $ArtResult['Author'];?></a>
							/
							<span>In : </span>
							<span>
							<a ><?php $cat=mysqli_fetch_array(mysqli_query($con,"select Category from tbl_article_category where CategoryId='".$ArtResult['CategoryId']."'")); echo $cat[0];?></a>
							</span>
							/
							<span>On : </span>
							<span>
							<a ><?php echo date('M d Y',strtotime($ArtResult['Date']));?></a>
							</span>
							
						</div>
						<!-- .entry-meta end -->
						<div class="entry-title">
							<h3>
								<a ><?php echo $ArtResult['Heading'];?></a>
							</h3>
						</div>
						<!-- .entry-title end -->
						<div class="entry-cat">
							<i class="fa fa-image"></i>
						</div>
						<!-- .entry-cat end -->
						<div class="entry-content">
							<p><?php echo $ArtResult['Content'];?></p>
						</div>
			
						<!-- .entry-tags end -->
						<div class="entry-share">
							<div class="share-title pull-left pull-none-xs">
								<h6>share this article: </h6>
							</div>
							<div class="share-links text-right">
								<span class='st_whatsapp_large' displayText='WhatsApp'></span>
<span class='st_facebook_large' displayText='Facebook'></span>
<span class='st_twitter_large' displayText='Tweet'></span>
<span class='st_googleplus_large' displayText='Google +'></span>
<span class='st_linkedin_large' displayText='LinkedIn'></span>
<span class='st_pinterest_large' displayText='Pinterest'></span>

<span class='st_email_large' displayText='Email'></span>
						
							</div>
						</div>
						<!-- .entry-share end -->
					</div>
					<!-- .entry-post end -->
				</div>
				<!-- .entry end -->
			


			</div>
			<!-- .col-md-8 end -->
			<div class="col-xs-12 col-sm-12 col-md-4 sidebar">
				<!-- Categories
============================================= -->
<div class="widget widget-categories">
	<div class="widget-title">
		<h5>categories</h5>
	</div>
	<div class="widget-content">
		<ul class="list-unstyled mb-0">
		<?php $catquery=mysqli_query($con,"select * from tbl_article_category"); 
			while($catRes=mysqli_fetch_array($catquery))
			{
			?>
		
			<li>
				<a href="article.php?CatId=<?php echo $catRes['CategoryId'];?>"><i class="fa fa-angle-double-right"></i><?php echo $catRes['Category']; ?></a>
			</li>
			<?php } ?>
		</ul>
	</div>
</div><!-- .widget-categories end -->

<!-- Recent Posts
============================================= -->
<div class="widget widget-recent">
	<div class="widget-title">
		<h5>recent posts</h5>
	</div>
	<div class="widget-content">
	
		<?php $ArtRecQuery=mysqli_query($con,"select * from tbl_article where Approve = 1 order by ArticleId desc limit 3");
				while($ArtRecRes=mysqli_fetch_array($ArtRecQuery))
				{
			?>
			
		<div class="entry">
			<img src="<?php echo "Resource/Article/thump_".$ArtRecRes['Image'];?>" alt="Article" width="50" height="50" />
			<div class="entry-desc">
				<div class="entry-title">
					<a href="article_single.php?ArticleId=<?php echo $ArtRecRes['ArticleId'];?>&<?php echo strtolower(str_replace(' ', '_',$ArtRecRes['Heading']));?>"><?php echo $ArtRecRes['Heading'];?></a>
				</div>	
				<div class="entry-meta">
					<span>by: <a > <?php echo $ArtRecRes['Author'];?></a></span><span class="slash"> / </span>
					<span>on: <a ><?php echo date('M d Y',strtotime($ArtRecRes['Date']));?></a></span>
				</div>
			</div>		
		</div><!-- .recent-entry end -->
		<?php } ?>
		
	</div><!-- .widget-content end -->
</div><!-- .widget-recent end -->


			</div><!-- .sidebar -->
		</div>
		<!-- .row end -->
	</div>
	<!-- .container end -->
</section>
<!-- #page-title end -->



<?php include('footer.php'); ?>

 </div><!-- #wrapper end -->
 <div id="back-to-top" class="backtop">
 	<i class="fa fa-angle-up" aria-hidden="true"></i>
 </div>

<!-- Footer Scripts
============================================= -->
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/functions.js"></script>
</body>

<!-- Mirrored from 7oroof.com/tfdemos/mount/contact-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Nov 2016 05:26:56 GMT -->
</html>